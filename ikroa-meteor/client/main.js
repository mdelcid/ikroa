if (!Meteor.isProduction) {
  Reload._onMigrate("LiveUpdate", function() {
    // Deps.autorun(function() {
    //     Autoupdate.newClientAvailable();
    //     console.log("NEW CLIENT AVAILABLE. YAY!!");
    //     LiveUpdate.refreshPage();
    // });
    return [false];
  });
  // require("/imports/compiled/devtools.preload");
  require("/imports/compiled/re_frisk.preload");
  require("/imports/compiled/shadow.cljs.devtools.client.browser");
  require("/imports/compiled/shadow.cljs.devtools.client.env");
}
// devtools.preload devtools.preload re-frisk.preload shadow.cljs.devtools.client.browser shadow.cljs.devtools.client.env vp.core
var app = require('/imports/compiled/vp.core');
app.init();

var CryptoJS = require('crypto-js');
global.CryptoJS = CryptoJS;


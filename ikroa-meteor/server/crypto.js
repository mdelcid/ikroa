var CryptoJS = require('crypto-js');

var access_key = 'D301146F3586152707B1';     // The access key received from Rapyd.
var secret_key = 'cfa6dc18129d96c404b540dc4a2d077b6c4093234e94a68f3b9b1a5c890d838e3e4501f6d5f8d2db';     // Never transmit the secret key by itself.

global.signature = function (http_method, url_path, body) {
  var salt = CryptoJS.lib.WordArray.random(10) + "";  // Randomly generated for each request.
  var timestamp = (Math.floor(new Date().getTime() / 1000) - 10).toString();
  // Current Unix time.

  // if (JSON.stringify(request.data) !== '{}' && request.data !== '') {
  //   body = JSON.stringify(JSON.parse(request.data));
  // }

  var to_sign = http_method + url_path + salt + timestamp + access_key + secret_key + body;
  console.log(to_sign);

  var signature = CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(to_sign, secret_key));

  signature = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(signature));
  // 
  return {signature, timestamp, salt};
}

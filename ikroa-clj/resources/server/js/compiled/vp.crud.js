var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./forms.re_frame.js");
require("./forms.validator.js");
require("./re_frame.core.js");
require("./reagent.core.js");
require("./vp.util.js");
require("./reagent.dom.js");
require("./vp.meteor.js");
require("./cljs.core.async.js");
require("./clojure.string.js");
require("./vp.macros.js");
require("./vp.aws.js");
require("./shadow.js.shim.module$react_phone_number_input.js");
require("./shadow.js.shim.module$jquery.js");
require("./shadow.js.shim.module$cropperjs.js");
require("./shadow.js.shim.module$react_color.js");
require("./shadow.js.shim.module$$uppy$core.js");
require("./shadow.js.shim.module$$uppy$dashboard.js");
require("./shadow.js.shim.module$$uppy$file_input.js");
require("./shadow.js.shim.module$$uppy$image_editor.js");
require("./shadow.js.shim.module$$uppy$aws_s3.js");
require("./shadow.js.shim.module$$uppy$react$lib$StatusBar.js");
require("./shadow.js.shim.module$react_datepicker.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var camel_snake_kebab=$CLJS.camel_snake_kebab || ($CLJS.camel_snake_kebab = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("vp.crud.js");

goog.provide('vp.crud');
console.log("PhoneInput",shadow.js.shim.module$react_phone_number_input.default);
vp.crud.url_pattern = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[\/?#]\S*)?$/i;
vp.crud.validations = new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"not-empty","not-empty",388922063),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"message","message",-406056002),"Can't be empty",new cljs.core.Keyword(null,"validator","validator",-1966190681),(function (v){
return (!(cljs.core.empty_QMARK_(v)));
})], null),new cljs.core.Keyword(null,"valid-number","valid-number",-677011101),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"message","message",-406056002),"Required",new cljs.core.Keyword(null,"validator","validator",-1966190681),(function (v){
if(clojure.string.blank_QMARK_(v)){
return cljs.core.not(isNaN(v));
} else {
return true;
}
})], null),new cljs.core.Keyword(null,"is-phone","is-phone",-1361958879),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"message","message",-406056002),"Invalid phone",new cljs.core.Keyword(null,"validator","validator",-1966190681),(function (v){
if((!(clojure.string.blank_QMARK_(v)))){
return shadow.js.shim.module$react_phone_number_input.isValidPhoneNumber(v);
} else {
return true;
}
})], null),new cljs.core.Keyword(null,"not-null","not-null",-1326718535),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"message","message",-406056002),"Required",new cljs.core.Keyword(null,"validator","validator",-1966190681),(function (v){
return (!((v == null)));
})], null),new cljs.core.Keyword(null,"valid-email","valid-email",1618825636),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"message","message",-406056002),"Is not a valid email",new cljs.core.Keyword(null,"validator","validator",-1966190681),(function (v){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2((-1),(function (){var or__4126__auto__ = v;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "";
}
})().indexOf("@"));
})], null)], null);
vp.crud.value_setter = (function vp$crud$value_setter(form_path,path){
return (function (v){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","set!","forms.re-frame/set!",498933512),form_path,path,v], null));
});
});
/**
 * Set the value of the key path in the data atom
 */
vp.crud.setter = (function vp$crud$setter(form_path,path){
return (function (e){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","set!","forms.re-frame/set!",498933512),form_path,path,e.target.value], null));
});
});
vp.crud.to_validator = (function vp$crud$to_validator(validations,config){
return cljs.core.reduce_kv((function (m,attr,v){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m,attr,cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (k){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(validations,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,new cljs.core.Keyword(null,"validator","validator",-1966190681)], null))], null);
}),v));
}),cljs.core.PersistentArrayMap.EMPTY,config);
});
/**
 * Renders the errors for the given key-path.
 */
vp.crud.render_errors = (function vp$crud$render_errors(form_path,path){
var errors_for_path = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","errors-for-path","forms.re-frame/errors-for-path",-1106864696),form_path,path], null));
var errors = cljs.core.deref(errors_for_path);
if(cljs.core.truth_(errors)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-danger.errors-wrap","div.text-danger.errors-wrap",572489663),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul.list-unstyled","ul.list-unstyled",1077310460),cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (error){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),error], null),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(vp.crud.validations,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [error,new cljs.core.Keyword(null,"message","message",-406056002)], null))], null);
}),cljs.core.take.cljs$core$IFn$_invoke$arity$2((1),new cljs.core.Keyword(null,"failed","failed",-1397425762).cljs$core$IFn$_invoke$arity$1(errors)))], null)], null);
} else {
return null;
}
});
vp.crud.image_editor = (function vp$crud$image_editor(img_element){
return reagent.core.create_class.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"component-did-mount","component-did-mount",-1126910518),(function (cmp){
var dn = reagent.dom.dom_node(cmp);
var _ = console.log(dn);
var cropper = (new shadow.js.shim.module$cropperjs(dn,cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"viewMode","viewMode",-86592409),(1),new cljs.core.Keyword(null,"background","background",-863952629),false,new cljs.core.Keyword(null,"autoCropArea","autoCropArea",1812657901),(1),new cljs.core.Keyword(null,"aspectRatio","aspectRatio",-218867702),(1),new cljs.core.Keyword(null,"responsive","responsive",-1606632318),true], null))));
return null;
}),new cljs.core.Keyword(null,"reagent-render","reagent-render",-985383853),(function (){
return img_element;
})], null));
});
vp.crud.openModal = (function vp$crud$openModal(){
var show_bar_QMARK_ = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"db","db",993250759),new cljs.core.Keyword("vp.crud","modal?","vp.crud/modal?",-1413404507)], null));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"button",new cljs.core.Keyword(null,"value","value",305978217),"Upload"], null)], null)], null);
});
vp.crud.file_uppy = (function vp$crud$file_uppy(p__54916){
var map__54918 = p__54916;
var map__54918__$1 = (((((!((map__54918 == null))))?(((((map__54918.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__54918.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__54918):map__54918);
var label = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__54918__$1,new cljs.core.Keyword(null,"label","label",1718410804));
var on_uppy = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__54918__$1,new cljs.core.Keyword(null,"on-uppy","on-uppy",1523974151));
var on_change = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__54918__$1,new cljs.core.Keyword(null,"on-change","on-change",-732046149));
var get_upload_promise = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__54918__$1,new cljs.core.Keyword(null,"get-upload-promise","get-upload-promise",548607442));
var uppy_opts = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__54918__$1,new cljs.core.Keyword(null,"uppy-opts","uppy-opts",1922064032));
var uppy = shadow.js.shim.module$$uppy$core(cljs.core.clj__GT_js(cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(cljs.core.merge,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"autoProceed","autoProceed",-237153537),true,new cljs.core.Keyword(null,"restrictions","restrictions",1874752994),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"maxNumberOfFiles","maxNumberOfFiles",-1469706385),(1),new cljs.core.Keyword(null,"minNumberOfFiles","minNumberOfFiles",-1457870195),(1)], null)], null),uppy_opts], 0))));
if(cljs.core.truth_(on_uppy)){
(on_uppy.cljs$core$IFn$_invoke$arity$1 ? on_uppy.cljs$core$IFn$_invoke$arity$1(uppy) : on_uppy.call(null,uppy));
} else {
}

return reagent.core.create_class.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"component-did-mount","component-did-mount",-1126910518),(function (cmp_uppy){
var dn = reagent.dom.dom_node(cmp_uppy);
return uppy.use(shadow.js.shim.module$$uppy$aws_s3,cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"getUploadParameters","getUploadParameters",-317297628),(function (file){
return vp.meteor.to_promise((function (){var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_54980){
var state_val_54981 = (state_54980[(1)]);
if((state_val_54981 === (7))){
var state_54980__$1 = state_54980;
var statearr_54985_55609 = state_54980__$1;
(statearr_54985_55609[(1)] = (9));



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54981 === (1))){
var inst_54950 = vp.aws.upload_url(file);
var state_54980__$1 = state_54980;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_54980__$1,(2),inst_54950);
} else {
if((state_val_54981 === (4))){
var inst_54952 = (state_54980[(7)]);
var state_54980__$1 = state_54980;
var statearr_54991_55639 = state_54980__$1;
(statearr_54991_55639[(2)] = inst_54952);

(statearr_54991_55639[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54981 === (6))){
var inst_54952 = (state_54980[(7)]);
var inst_54962 = console.error("err",inst_54952);
var inst_54963 = (function(){throw inst_54952})();
var state_54980__$1 = (function (){var statearr_54994 = state_54980;
(statearr_54994[(8)] = inst_54962);

return statearr_54994;
})();
var statearr_54996_55643 = state_54980__$1;
(statearr_54996_55643[(2)] = inst_54963);

(statearr_54996_55643[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54981 === (3))){
var inst_54952 = (state_54980[(7)]);
var inst_54956 = inst_54952.name;
var inst_54957 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_54956,"Error");
var state_54980__$1 = state_54980;
var statearr_55001_55651 = state_54980__$1;
(statearr_55001_55651[(2)] = inst_54957);

(statearr_55001_55651[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54981 === (2))){
var inst_54952 = (state_54980[(7)]);
var inst_54952__$1 = (state_54980[(2)]);
var state_54980__$1 = (function (){var statearr_55004 = state_54980;
(statearr_55004[(7)] = inst_54952__$1);

return statearr_55004;
})();
if(cljs.core.truth_(inst_54952__$1)){
var statearr_55007_55689 = state_54980__$1;
(statearr_55007_55689[(1)] = (3));

} else {
var statearr_55008_55691 = state_54980__$1;
(statearr_55008_55691[(1)] = (4));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54981 === (11))){
var inst_54968 = (state_54980[(2)]);
var state_54980__$1 = state_54980;
var statearr_55011_55693 = state_54980__$1;
(statearr_55011_55693[(2)] = inst_54968);

(statearr_55011_55693[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54981 === (9))){
var inst_54952 = (state_54980[(7)]);
var state_54980__$1 = state_54980;
var statearr_55014_55701 = state_54980__$1;
(statearr_55014_55701[(2)] = inst_54952);

(statearr_55014_55701[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54981 === (5))){
var inst_54960 = (state_54980[(2)]);
var state_54980__$1 = state_54980;
if(cljs.core.truth_(inst_54960)){
var statearr_55015_55715 = state_54980__$1;
(statearr_55015_55715[(1)] = (6));

} else {
var statearr_55016_55717 = state_54980__$1;
(statearr_55016_55717[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54981 === (10))){
var state_54980__$1 = state_54980;
var statearr_55018_55729 = state_54980__$1;
(statearr_55018_55729[(2)] = null);

(statearr_55018_55729[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54981 === (8))){
var inst_54970 = (state_54980[(2)]);
var inst_54971 = file.id;
var inst_54972 = [new cljs.core.Keyword(null,"assetId","assetId",-1083269875)];
var inst_54973 = new cljs.core.Keyword(null,"asset-id","asset-id",249736185).cljs$core$IFn$_invoke$arity$1(inst_54970);
var inst_54974 = cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_54973);
var inst_54975 = [inst_54974];
var inst_54976 = cljs.core.PersistentHashMap.fromArrays(inst_54972,inst_54975);
var inst_54977 = cljs.core.clj__GT_js(inst_54976);
var inst_54978 = uppy.setFileMeta(inst_54971,inst_54977);
var state_54980__$1 = (function (){var statearr_55020 = state_54980;
(statearr_55020[(9)] = inst_54978);

return statearr_55020;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_54980__$1,inst_54970);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var vp$crud$file_uppy_$_state_machine__41292__auto__ = null;
var vp$crud$file_uppy_$_state_machine__41292__auto____0 = (function (){
var statearr_55024 = [null,null,null,null,null,null,null,null,null,null];
(statearr_55024[(0)] = vp$crud$file_uppy_$_state_machine__41292__auto__);

(statearr_55024[(1)] = (1));

return statearr_55024;
});
var vp$crud$file_uppy_$_state_machine__41292__auto____1 = (function (state_54980){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_54980);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e55025){var ex__41295__auto__ = e55025;
var statearr_55027_55763 = state_54980;
(statearr_55027_55763[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_54980[(4)]))){
var statearr_55028_55769 = state_54980;
(statearr_55028_55769[(1)] = cljs.core.first((state_54980[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__55777 = state_54980;
state_54980 = G__55777;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$crud$file_uppy_$_state_machine__41292__auto__ = function(state_54980){
switch(arguments.length){
case 0:
return vp$crud$file_uppy_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$crud$file_uppy_$_state_machine__41292__auto____1.call(this,state_54980);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$crud$file_uppy_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$crud$file_uppy_$_state_machine__41292__auto____0;
vp$crud$file_uppy_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$crud$file_uppy_$_state_machine__41292__auto____1;
return vp$crud$file_uppy_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_55029 = f__41438__auto__();
(statearr_55029[(6)] = c__41437__auto__);

return statearr_55029;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
})());
})], null))).use(shadow.js.shim.module$$uppy$dashboard,cljs.core.clj__GT_js(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"locale","locale",-2115712697),new cljs.core.Keyword(null,"width","width",-384071477),new cljs.core.Keyword(null,"theme","theme",-1247880880),new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"plugins","plugins",1900073717),new cljs.core.Keyword(null,"metaFields","metaFields",-931219691),new cljs.core.Keyword(null,"pretty","pretty",-1916372486),new cljs.core.Keyword(null,"trigger","trigger",103466139),new cljs.core.Keyword(null,"showSelectedFiles","showSelectedFiles",-891967586),new cljs.core.Keyword(null,"height","height",1025178622)],[new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"strings","strings",-2055406807),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"chooseFiles","chooseFiles",-23991697),(function (){var or__4126__auto__ = label;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "Upload";
}
})()], null)], null),(950),"light","Dashboard",new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [shadow.js.shim.module$$uppy$image_editor], null),new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092),"name",new cljs.core.Keyword(null,"name","name",1843675177),"Name",new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),"File"], null),true,dn,true,(900)]))).use(shadow.js.shim.module$$uppy$image_editor,cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"id","id",-1388402092),"ImageEditor",new cljs.core.Keyword(null,"target","target",253001721),shadow.js.shim.module$$uppy$dashboard,new cljs.core.Keyword(null,"quality","quality",147850199),0.8,new cljs.core.Keyword(null,"cropperOptions","cropperOptions",1267936577),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"viewMode","viewMode",-86592409),(1),new cljs.core.Keyword(null,"background","background",-863952629),true,new cljs.core.Keyword(null,"autoCropArea","autoCropArea",1812657901),(1),new cljs.core.Keyword(null,"aspectRatio","aspectRatio",-218867702),(1),new cljs.core.Keyword(null,"responsive","responsive",-1606632318),true], null)], null)));
}),new cljs.core.Keyword(null,"component-will-unmount","component-will-unmount",-2058314698),(function (){
return uppy.close();
}),new cljs.core.Keyword(null,"reagent-render","reagent-render",-985383853),(function (){
new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632)], null);

return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.openModal], null);
})], null));
});
vp.crud.render_uppy_file = (function vp$crud$render_uppy_file(form_path,field){
var form_data_subs = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var is_valid_QMARK_ = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null));
var input_setter = vp.crud.value_setter(form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var on_change_handler = (function (v){
input_setter(v);

if(cljs.core.not(cljs.core.deref(is_valid_QMARK_))){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
} else {
return null;
}
});
var uppy_ref = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(null);
return (function (){
var form_data = cljs.core.deref(form_data_subs);
var v = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3","div.mb-3",2018571275),(cljs.core.truth_(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label","label.form-label",1421537292),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null):null),(function (){var temp__5733__auto__ = new cljs.core.Keyword(null,"render-value","render-value",1155011781).cljs$core$IFn$_invoke$arity$1(field);
if(cljs.core.truth_(temp__5733__auto__)){
var render_value = temp__5733__auto__;
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [render_value,v,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"input-setter","input-setter",-823337143),input_setter,new cljs.core.Keyword(null,"on-change-handler","on-change-handler",-1795615738),on_change_handler,new cljs.core.Keyword(null,"on-uppy","on-uppy",1523974151),(function (p1__55032_SHARP_){
return cljs.core.reset_BANG_(uppy_ref,p1__55032_SHARP_);
})], null)], null);
} else {
if(cljs.core.truth_(v)){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-circle.btn-outline-danger","button.btn.btn-circle.btn-outline-danger",-1300936193),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"button",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return input_setter(null);
})], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.fas.fa-times","i.fas.fa-times",-1487825087)], null)], null)," ",new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(v)], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.file_uppy,cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"uppy-opts","uppy-opts",1922064032),new cljs.core.Keyword(null,"uppy-opts","uppy-opts",1922064032).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.Keyword(null,"on-change","on-change",-732046149),on_change_handler,new cljs.core.Keyword(null,"label","label",1718410804),"Upload",new cljs.core.Keyword(null,"on-uppy","on-uppy",1523974151),(function (p1__55033_SHARP_){
return cljs.core.reset_BANG_(uppy_ref,p1__55033_SHARP_);
})], null)], 0))], null);
}
}
})(),(cljs.core.truth_(cljs.core.deref(uppy_ref))?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,">",">",-555517146),shadow.js.shim.module$$uppy$react$lib$StatusBar,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"uppy","uppy",875713314),cljs.core.deref(uppy_ref)], null)], null):null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
});
vp.crud.file_cmp = (function vp$crud$file_cmp(p__55037){
var map__55038 = p__55037;
var map__55038__$1 = (((((!((map__55038 == null))))?(((((map__55038.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__55038.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__55038):map__55038);
var opts = map__55038__$1;
var label = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55038__$1,new cljs.core.Keyword(null,"label","label",1718410804));
var on_uppy = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55038__$1,new cljs.core.Keyword(null,"on-uppy","on-uppy",1523974151));
var on_change = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55038__$1,new cljs.core.Keyword(null,"on-change","on-change",-732046149));
var get_upload_promise = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55038__$1,new cljs.core.Keyword(null,"get-upload-promise","get-upload-promise",548607442));
var uppy_opts = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55038__$1,new cljs.core.Keyword(null,"uppy-opts","uppy-opts",1922064032));
var uppy = shadow.js.shim.module$$uppy$core(cljs.core.clj__GT_js(cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(cljs.core.merge,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"autoProceed","autoProceed",-237153537),true,new cljs.core.Keyword(null,"restrictions","restrictions",1874752994),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"maxNumberOfFiles","maxNumberOfFiles",-1469706385),(1),new cljs.core.Keyword(null,"minNumberOfFiles","minNumberOfFiles",-1457870195),(1)], null)], null),uppy_opts], 0))));
if(cljs.core.truth_(on_uppy)){
(on_uppy.cljs$core$IFn$_invoke$arity$1 ? on_uppy.cljs$core$IFn$_invoke$arity$1(uppy) : on_uppy.call(null,uppy));
} else {
}

return reagent.core.create_class.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"component-did-mount","component-did-mount",-1126910518),(function (cmp){
var dn = reagent.dom.dom_node(cmp);
return uppy.use(shadow.js.shim.module$$uppy$aws_s3,cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"getUploadParameters","getUploadParameters",-317297628),(function (file){
return vp.meteor.to_promise((function (){var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_55098){
var state_val_55099 = (state_55098[(1)]);
if((state_val_55099 === (7))){
var state_55098__$1 = state_55098;
var statearr_55101_55826 = state_55098__$1;
(statearr_55101_55826[(1)] = (9));



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_55099 === (1))){
var inst_55060 = vp.meteor.call("UploadURL",file);
var state_55098__$1 = state_55098;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_55098__$1,(2),inst_55060);
} else {
if((state_val_55099 === (4))){
var inst_55062 = (state_55098[(7)]);
var state_55098__$1 = state_55098;
var statearr_55104_55828 = state_55098__$1;
(statearr_55104_55828[(2)] = inst_55062);

(statearr_55104_55828[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_55099 === (6))){
var inst_55062 = (state_55098[(7)]);
var inst_55078 = console.error("err",inst_55062);
var inst_55079 = (function(){throw inst_55062})();
var state_55098__$1 = (function (){var statearr_55106 = state_55098;
(statearr_55106[(8)] = inst_55078);

return statearr_55106;
})();
var statearr_55108_55830 = state_55098__$1;
(statearr_55108_55830[(2)] = inst_55079);

(statearr_55108_55830[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_55099 === (3))){
var inst_55062 = (state_55098[(7)]);
var inst_55072 = inst_55062.name;
var inst_55073 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_55072,"Error");
var state_55098__$1 = state_55098;
var statearr_55110_55833 = state_55098__$1;
(statearr_55110_55833[(2)] = inst_55073);

(statearr_55110_55833[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_55099 === (2))){
var inst_55062 = (state_55098[(7)]);
var inst_55062__$1 = (state_55098[(2)]);
var state_55098__$1 = (function (){var statearr_55112 = state_55098;
(statearr_55112[(7)] = inst_55062__$1);

return statearr_55112;
})();
if(cljs.core.truth_(inst_55062__$1)){
var statearr_55114_55835 = state_55098__$1;
(statearr_55114_55835[(1)] = (3));

} else {
var statearr_55116_55836 = state_55098__$1;
(statearr_55116_55836[(1)] = (4));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_55099 === (11))){
var inst_55086 = (state_55098[(2)]);
var state_55098__$1 = state_55098;
var statearr_55118_55839 = state_55098__$1;
(statearr_55118_55839[(2)] = inst_55086);

(statearr_55118_55839[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_55099 === (9))){
var inst_55062 = (state_55098[(7)]);
var state_55098__$1 = state_55098;
var statearr_55120_55841 = state_55098__$1;
(statearr_55120_55841[(2)] = inst_55062);

(statearr_55120_55841[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_55099 === (5))){
var inst_55076 = (state_55098[(2)]);
var state_55098__$1 = state_55098;
if(cljs.core.truth_(inst_55076)){
var statearr_55122_55843 = state_55098__$1;
(statearr_55122_55843[(1)] = (6));

} else {
var statearr_55124_55845 = state_55098__$1;
(statearr_55124_55845[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_55099 === (10))){
var state_55098__$1 = state_55098;
var statearr_55126_55847 = state_55098__$1;
(statearr_55126_55847[(2)] = null);

(statearr_55126_55847[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_55099 === (8))){
var inst_55088 = (state_55098[(2)]);
var inst_55089 = file.id;
var inst_55090 = [new cljs.core.Keyword(null,"assetId","assetId",-1083269875)];
var inst_55091 = new cljs.core.Keyword(null,"asset-id","asset-id",249736185).cljs$core$IFn$_invoke$arity$1(inst_55088);
var inst_55092 = cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_55091);
var inst_55093 = [inst_55092];
var inst_55094 = cljs.core.PersistentHashMap.fromArrays(inst_55090,inst_55093);
var inst_55095 = cljs.core.clj__GT_js(inst_55094);
var inst_55096 = uppy.setFileMeta(inst_55089,inst_55095);
var state_55098__$1 = (function (){var statearr_55128 = state_55098;
(statearr_55128[(9)] = inst_55096);

return statearr_55128;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_55098__$1,inst_55088);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var vp$crud$file_cmp_$_state_machine__41292__auto__ = null;
var vp$crud$file_cmp_$_state_machine__41292__auto____0 = (function (){
var statearr_55130 = [null,null,null,null,null,null,null,null,null,null];
(statearr_55130[(0)] = vp$crud$file_cmp_$_state_machine__41292__auto__);

(statearr_55130[(1)] = (1));

return statearr_55130;
});
var vp$crud$file_cmp_$_state_machine__41292__auto____1 = (function (state_55098){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_55098);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e55132){var ex__41295__auto__ = e55132;
var statearr_55134_55850 = state_55098;
(statearr_55134_55850[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_55098[(4)]))){
var statearr_55136_55851 = state_55098;
(statearr_55136_55851[(1)] = cljs.core.first((state_55098[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__55853 = state_55098;
state_55098 = G__55853;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$crud$file_cmp_$_state_machine__41292__auto__ = function(state_55098){
switch(arguments.length){
case 0:
return vp$crud$file_cmp_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$crud$file_cmp_$_state_machine__41292__auto____1.call(this,state_55098);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$crud$file_cmp_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$crud$file_cmp_$_state_machine__41292__auto____0;
vp$crud$file_cmp_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$crud$file_cmp_$_state_machine__41292__auto____1;
return vp$crud$file_cmp_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_55138 = f__41438__auto__();
(statearr_55138[(6)] = c__41437__auto__);

return statearr_55138;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
})());
})], null))).use(shadow.js.shim.module$$uppy$file_input,cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"target","target",253001721),dn,new cljs.core.Keyword(null,"pretty","pretty",-1916372486),true,new cljs.core.Keyword(null,"locale","locale",-2115712697),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"strings","strings",-2055406807),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"chooseFiles","chooseFiles",-23991697),(function (){var or__4126__auto__ = label;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "Upload";
}
})()], null)], null)], null))).on("upload-success",(function (file,data){
var G__55140 = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"name","name",1843675177),(file["name"]),new cljs.core.Keyword(null,"type","type",1174270348),(file["type"]),new cljs.core.Keyword(null,"asset-id","asset-id",249736185),(file["meta"]["assetId"])], null);
return (on_change.cljs$core$IFn$_invoke$arity$1 ? on_change.cljs$core$IFn$_invoke$arity$1(G__55140) : on_change.call(null,G__55140));
}));
}),new cljs.core.Keyword(null,"component-will-unmount","component-will-unmount",-2058314698),(function (){
return uppy.close();
}),new cljs.core.Keyword(null,"reagent-render","reagent-render",-985383853),(function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),cljs.core.select_keys(opts,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"class","class",-2030961996)], null))], null);
})], null));
});
vp.crud.render_field_file = (function vp$crud$render_field_file(form_path,field){
var form_data_subs = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var is_valid_QMARK_ = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null));
var input_setter = vp.crud.value_setter(form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var on_change_handler = (function (v){
input_setter(v);

if(cljs.core.not(cljs.core.deref(is_valid_QMARK_))){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
} else {
return null;
}
});
var uppy_ref = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(null);
return (function (){
var form_data = cljs.core.deref(form_data_subs);
var v = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3","div.mb-3",2018571275),(cljs.core.truth_(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label","label.form-label",1421537292),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null):null),(function (){var temp__5733__auto__ = new cljs.core.Keyword(null,"render-value","render-value",1155011781).cljs$core$IFn$_invoke$arity$1(field);
if(cljs.core.truth_(temp__5733__auto__)){
var render_value = temp__5733__auto__;
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [render_value,v,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"input-setter","input-setter",-823337143),input_setter,new cljs.core.Keyword(null,"on-change-handler","on-change-handler",-1795615738),on_change_handler,new cljs.core.Keyword(null,"on-uppy","on-uppy",1523974151),(function (p1__55142_SHARP_){
return cljs.core.reset_BANG_(uppy_ref,p1__55142_SHARP_);
})], null)], null);
} else {
if(cljs.core.truth_(v)){
if(cljs.core.truth_(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(field,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"opts","opts",155075701),new cljs.core.Keyword(null,"image?","image?",1951967096)], null)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-2.d-flex","div.mb-2.d-flex",650734638),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img.img-fluid.rounded.w-200px.mr-3","img.img-fluid.rounded.w-200px.mr-3",245922786),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"src","src",-1651076051),vp.meteor.asset_url(new cljs.core.Keyword(null,"asset-id","asset-id",249736185).cljs$core$IFn$_invoke$arity$1(v))], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.file_cmp,cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"uppy-opts","uppy-opts",1922064032),new cljs.core.Keyword(null,"uppy-opts","uppy-opts",1922064032).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.Keyword(null,"on-change","on-change",-732046149),on_change_handler,new cljs.core.Keyword(null,"label","label",1718410804),"Change",new cljs.core.Keyword(null,"on-uppy","on-uppy",1523974151),(function (p1__55143_SHARP_){
return cljs.core.reset_BANG_(uppy_ref,p1__55143_SHARP_);
})], null)], 0))], null)], null)], null);
} else {
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-circle.btn-outline-danger","button.btn.btn-circle.btn-outline-danger",-1300936193),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"button",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return input_setter(null);
})], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.fas.fa-times","i.fas.fa-times",-1487825087)], null)], null)," ",new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(v)], null);
}
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.d-flex.mb-2","div.d-flex.mb-2",-1579329286),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.gray-box.mr-3","div.gray-box.mr-3",-906175531),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"width","width",-384071477),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(field,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"opts","opts",155075701),new cljs.core.Keyword(null,"image-width","image-width",737630851)], null)),new cljs.core.Keyword(null,"height","height",1025178622),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(field,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"opts","opts",155075701),new cljs.core.Keyword(null,"image-height","image-height",-1587384498)], null))], null)], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.file_cmp,cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"uppy-opts","uppy-opts",1922064032),new cljs.core.Keyword(null,"uppy-opts","uppy-opts",1922064032).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.Keyword(null,"on-change","on-change",-732046149),on_change_handler,new cljs.core.Keyword(null,"label","label",1718410804),"Upload",new cljs.core.Keyword(null,"on-uppy","on-uppy",1523974151),(function (p1__55144_SHARP_){
return cljs.core.reset_BANG_(uppy_ref,p1__55144_SHARP_);
})], null)], 0))], null),(cljs.core.truth_(new cljs.core.Keyword(null,"help-text","help-text",1567653015).cljs$core$IFn$_invoke$arity$1(field))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-muted","div.text-muted",-1618241847),new cljs.core.Keyword(null,"help-text","help-text",1567653015).cljs$core$IFn$_invoke$arity$1(field)], null):null)], null)], null);
}
}
})(),(cljs.core.truth_(cljs.core.deref(uppy_ref))?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,">",">",-555517146),shadow.js.shim.module$$uppy$react$lib$StatusBar,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"uppy","uppy",875713314),cljs.core.deref(uppy_ref)], null)], null):null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
});
vp.crud.render_field_text = (function vp$crud$render_field_text(form_path,p__55196){
var map__55197 = p__55196;
var map__55197__$1 = (((((!((map__55197 == null))))?(((((map__55197.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__55197.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__55197):map__55197);
var field = map__55197__$1;
var input_group_append = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55197__$1,new cljs.core.Keyword(null,"input-group-append","input-group-append",304795856));
var input_group_prepend = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55197__$1,new cljs.core.Keyword(null,"input-group-prepend","input-group-prepend",-1646594811));
var form_data_subs = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var form_data = cljs.core.deref(form_data_subs);
var is_valid_QMARK_ = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var input_setter = vp.crud.setter(form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var on_change_handler = (function (e){
input_setter(e);

if(cljs.core.not(is_valid_QMARK_)){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
} else {
return null;
}
});
var errors = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","errors-for-path","forms.re-frame/errors-for-path",-1106864696),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var input_cmp = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.form-control.login-input","input.form-control.login-input",-1786934937),cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"value","value",305978217),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)),new cljs.core.Keyword(null,"type","type",1174270348),"text",new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core.not(is_valid_QMARK_))?"is-invalid":null),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.Keyword(null,"on-change","on-change",-732046149),on_change_handler,new cljs.core.Keyword(null,"on-blur","on-blur",814300747),(function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
})], null),new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field)], 0))], null);
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3","div.mb-3",2018571275),cljs.core.PersistentArrayMap.EMPTY,(cljs.core.truth_(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label","label.form-label",1421537292),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null):null),(cljs.core.truth_((function (){var or__4126__auto__ = input_group_prepend;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return input_group_append;
}
})())?new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.input-group","div.input-group",-2073660476),(cljs.core.truth_(input_group_prepend)?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.input-group-prepend","div.input-group-prepend",-1522435373),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.input-group-text","span.input-group-text",-604882300),new cljs.core.Keyword(null,"text","text",-1790561697).cljs$core$IFn$_invoke$arity$1(input_group_prepend)], null)], null):null),input_cmp,(cljs.core.truth_(input_group_append)?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.input-group-append","div.input-group-append",-1462957134),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.input-group-text","span.input-group-text",-604882300),new cljs.core.Keyword(null,"text","text",-1790561697).cljs$core$IFn$_invoke$arity$1(input_group_append)], null)], null):null)], null):input_cmp),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
vp.crud.render_field_phone = (function vp$crud$render_field_phone(form_path,p__55211){
var map__55212 = p__55211;
var map__55212__$1 = (((((!((map__55212 == null))))?(((((map__55212.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__55212.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__55212):map__55212);
var field = map__55212__$1;
var input_group_append = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55212__$1,new cljs.core.Keyword(null,"input-group-append","input-group-append",304795856));
var input_group_prepend = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55212__$1,new cljs.core.Keyword(null,"input-group-prepend","input-group-prepend",-1646594811));
var form_data_subs = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var form_data = cljs.core.deref(form_data_subs);
var is_valid_QMARK_ = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var input_setter = vp.crud.value_setter(form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var on_change_handler = (function (e){
input_setter(e);

if(cljs.core.not(is_valid_QMARK_)){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
} else {
return null;
}
});
var errors = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","errors-for-path","forms.re-frame/errors-for-path",-1106864696),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3","div.mb-3",2018571275),(cljs.core.truth_(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label","label.form-label",1421537292),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null):null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,">",">",-555517146),shadow.js.shim.module$react_phone_number_input.default,cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"value","value",305978217),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)),new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core.not(is_valid_QMARK_))?"is-invalid":null),new cljs.core.Keyword(null,"on-change","on-change",-732046149),on_change_handler,new cljs.core.Keyword(null,"on-blur","on-blur",814300747),(function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
})], null),new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field)], 0))], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
vp.crud.render_field_float = (function vp$crud$render_field_float(form_path,p__55227){
var map__55228 = p__55227;
var map__55228__$1 = (((((!((map__55228 == null))))?(((((map__55228.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__55228.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__55228):map__55228);
var field = map__55228__$1;
var input_group_append = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55228__$1,new cljs.core.Keyword(null,"input-group-append","input-group-append",304795856));
var input_group_prepend = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55228__$1,new cljs.core.Keyword(null,"input-group-prepend","input-group-prepend",-1646594811));
var form_data_subs = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var form_data = cljs.core.deref(form_data_subs);
var is_valid_QMARK_ = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var input_setter = vp.crud.value_setter(form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var on_change_handler = (function (e){
var v_55858 = e.target.value;
var parsed_number_55859 = ((clojure.string.blank_QMARK_(v_55858))?null:parseFloat(v_55858));
if(cljs.core.not(isNaN(parsed_number_55859))){
input_setter(parsed_number_55859);
} else {
}

if(cljs.core.not(is_valid_QMARK_)){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
} else {
return null;
}
});
var errors = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","errors-for-path","forms.re-frame/errors-for-path",-1106864696),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var input_cmp = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.form-control","input.form-control",-1123419636),cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(cljs.core.merge,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"default-value","default-value",232220170),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)),new cljs.core.Keyword(null,"type","type",1174270348),"number",new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core.not(is_valid_QMARK_))?"is-invalid":null),new cljs.core.Keyword(null,"on-change","on-change",-732046149),on_change_handler,new cljs.core.Keyword(null,"on-blur","on-blur",814300747),(function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
})], null),cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"step","step",1288888124),"0.01"], null),new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field)], 0))], 0))], null);
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3","div.mb-3",2018571275),(cljs.core.truth_(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label","label.form-label",1421537292),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null):null),(cljs.core.truth_((function (){var or__4126__auto__ = input_group_prepend;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return input_group_append;
}
})())?new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.input-group","div.input-group",-2073660476),(cljs.core.truth_(input_group_prepend)?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.input-group-prepend","div.input-group-prepend",-1522435373),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.input-group-text","span.input-group-text",-604882300),new cljs.core.Keyword(null,"text","text",-1790561697).cljs$core$IFn$_invoke$arity$1(input_group_prepend)], null)], null):null),input_cmp,(cljs.core.truth_(input_group_append)?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.input-group-append","div.input-group-append",-1462957134),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.input-group-text","span.input-group-text",-604882300),new cljs.core.Keyword(null,"text","text",-1790561697).cljs$core$IFn$_invoke$arity$1(input_group_append)], null)], null):null)], null):input_cmp),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
vp.crud.render_field_int = (function vp$crud$render_field_int(form_path,field){
var form_data_subs = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var form_data = cljs.core.deref(form_data_subs);
var is_valid_QMARK_ = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var input_setter = vp.crud.value_setter(form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var on_change_handler = (function (e){
var v_55864 = e.target.value;
var parsed_number_55865 = ((clojure.string.blank_QMARK_(v_55864))?null:parseInt(e.target.value));
input_setter(parsed_number_55865);

if(cljs.core.not(is_valid_QMARK_)){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
} else {
return null;
}
});
var errors = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","errors-for-path","forms.re-frame/errors-for-path",-1106864696),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3","div.mb-3",2018571275),(cljs.core.truth_(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label","label.form-label",1421537292),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null):null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.form-control","input.form-control",-1123419636),cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(cljs.core.merge,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"value","value",305978217),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)),new cljs.core.Keyword(null,"type","type",1174270348),"number",new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core.not(is_valid_QMARK_))?"is-invalid":null),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.Keyword(null,"on-change","on-change",-732046149),on_change_handler,new cljs.core.Keyword(null,"on-blur","on-blur",814300747),(function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
})], null),new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field)], 0))], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
vp.crud.render_field_checkbox = (function vp$crud$render_field_checkbox(form_path,field){
var form_data_subs = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var form_data = cljs.core.deref(form_data_subs);
var is_valid_QMARK_ = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var input_setter = vp.crud.value_setter(form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var on_change_handler = (function (e){
input_setter(e.target.checked === true);

if(cljs.core.not(is_valid_QMARK_)){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
} else {
return null;
}
});
var errors = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","errors-for-path","forms.re-frame/errors-for-path",-1106864696),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3","div.mb-3",2018571275),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.form-check","div.form-check",-44718045),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(cljs.core.truth_(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(field,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"opts","opts",155075701),new cljs.core.Keyword(null,"switch","switch",71881310)], null)))?"form-switch":null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.form-check-input","input.form-check-input",-153409294),cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"checked","checked",-50955819),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)) === true,new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core.not(is_valid_QMARK_))?"is-invalid":null),new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"on-change","on-change",-732046149),on_change_handler,new cljs.core.Keyword(null,"on-blur","on-blur",814300747),(function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
})], null),new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field)], 0))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-check-label","label.form-check-label",-432187434),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
vp.crud.render_field_radio = (function vp$crud$render_field_radio(form_path,field){
var form_data_subs = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var form_data = cljs.core.deref(form_data_subs);
var is_valid_QMARK_ = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var input_setter = vp.crud.value_setter(form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var on_change_handler = (function (v,e){
input_setter(v);

if(cljs.core.not(is_valid_QMARK_)){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
} else {
return null;
}
});
var errors = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","errors-for-path","forms.re-frame/errors-for-path",-1106864696),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3","div.mb-3",2018571275),(cljs.core.truth_(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label","label.form-label",1421537292),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null):null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex-row.d-flex","div.flex-row.d-flex",386000984),cljs.core.doall.cljs$core$IFn$_invoke$arity$1((function (){var iter__4529__auto__ = (function vp$crud$render_field_radio_$_iter__55308(s__55309){
return (new cljs.core.LazySeq(null,(function (){
var s__55309__$1 = s__55309;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55309__$1);
if(temp__5735__auto__){
var s__55309__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55309__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55309__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55311 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55310 = (0);
while(true){
if((i__55310 < size__4528__auto__)){
var option = cljs.core._nth(c__4527__auto__,i__55310);
cljs.core.chunk_append(b__55311,cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.form-check.mr-3","div.form-check.mr-3",-1207566532),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.form-check-input.cursor-pointer","input.form-check-input.cursor-pointer",-705781368),cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"checked","checked",-50955819),cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field))),new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core.not(is_valid_QMARK_))?"is-invalid":null),new cljs.core.Keyword(null,"type","type",1174270348),"radio",new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option),new cljs.core.Keyword(null,"on-change","on-change",-732046149),cljs.core.partial.cljs$core$IFn$_invoke$arity$2(on_change_handler,new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)),new cljs.core.Keyword(null,"on-blur","on-blur",814300747),((function (i__55310,option,c__4527__auto__,size__4528__auto__,b__55311,s__55309__$2,temp__5735__auto__,form_data_subs,form_data,is_valid_QMARK_,input_setter,on_change_handler,errors){
return (function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
});})(i__55310,option,c__4527__auto__,size__4528__auto__,b__55311,s__55309__$2,temp__5735__auto__,form_data_subs,form_data,is_valid_QMARK_,input_setter,on_change_handler,errors))
], null),new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field)], 0))], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label.cursor-pointer","label.form-label.cursor-pointer",-1426325450),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),cljs.core.partial.cljs$core$IFn$_invoke$arity$2(on_change_handler,new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)),new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"margin-bottom","margin-bottom",388334941),(0)], null)], null),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(option)], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)], null)));

var G__55871 = (i__55310 + (1));
i__55310 = G__55871;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55311),vp$crud$render_field_radio_$_iter__55308(cljs.core.chunk_rest(s__55309__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55311),null);
}
} else {
var option = cljs.core.first(s__55309__$2);
return cljs.core.cons(cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.form-check.mr-3","div.form-check.mr-3",-1207566532),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input.form-check-input.cursor-pointer","input.form-check-input.cursor-pointer",-705781368),cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"checked","checked",-50955819),cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field))),new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core.not(is_valid_QMARK_))?"is-invalid":null),new cljs.core.Keyword(null,"type","type",1174270348),"radio",new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option),new cljs.core.Keyword(null,"on-change","on-change",-732046149),cljs.core.partial.cljs$core$IFn$_invoke$arity$2(on_change_handler,new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)),new cljs.core.Keyword(null,"on-blur","on-blur",814300747),((function (option,s__55309__$2,temp__5735__auto__,form_data_subs,form_data,is_valid_QMARK_,input_setter,on_change_handler,errors){
return (function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
});})(option,s__55309__$2,temp__5735__auto__,form_data_subs,form_data,is_valid_QMARK_,input_setter,on_change_handler,errors))
], null),new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field)], 0))], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label.cursor-pointer","label.form-label.cursor-pointer",-1426325450),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),cljs.core.partial.cljs$core$IFn$_invoke$arity$2(on_change_handler,new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)),new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"margin-bottom","margin-bottom",388334941),(0)], null)], null),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(option)], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)], null)),vp$crud$render_field_radio_$_iter__55308(cljs.core.rest(s__55309__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(new cljs.core.Keyword(null,"options","options",99638489).cljs$core$IFn$_invoke$arity$1(field));
})())], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
vp.crud.render_field_textarea = (function vp$crud$render_field_textarea(form_path,field){
var form_data_subs = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var form_data = cljs.core.deref(form_data_subs);
var is_valid_QMARK_ = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var input_setter = vp.crud.setter(form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var on_change_handler = (function (e){
input_setter(e);

if(cljs.core.not(is_valid_QMARK_)){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
} else {
return null;
}
});
var errors = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","errors-for-path","forms.re-frame/errors-for-path",-1106864696),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3","div.mb-3",2018571275),(cljs.core.truth_(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label","label.form-label",1421537292),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null):null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"textarea.form-control","textarea.form-control",-1690362789),cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"value","value",305978217),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)),new cljs.core.Keyword(null,"type","type",1174270348),"text",new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core.not(is_valid_QMARK_))?"is-invalid":null),new cljs.core.Keyword(null,"on-change","on-change",-732046149),on_change_handler,new cljs.core.Keyword(null,"placeholder","placeholder",-104873083),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.Keyword(null,"on-blur","on-blur",814300747),(function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
})], null),new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field)], 0))], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
vp.crud.render_field_select = (function vp$crud$render_field_select(form_path,field){
return (function (){
var form_data_atom = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var form_data = cljs.core.deref(form_data_atom);
var is_valid_QMARK_ = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var select_setter = vp.crud.setter(form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var set_and_validate = (function (e){
select_setter(e);

return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
});
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3","div.mb-3",2018571275),(cljs.core.truth_(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label","label.form-label",1421537292),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null):null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select.form-select","select.form-select",1844412748),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"on-change","on-change",-732046149),set_and_validate,new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core.not(is_valid_QMARK_))?"is-invalid":null),new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__4126__auto__ = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "";
}
})()], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),""], null),(function (){var or__4126__auto__ = new cljs.core.Keyword(null,"placeholder","placeholder",-104873083).cljs$core$IFn$_invoke$arity$1(field);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "-- Select --";
}
})()], null),(function (){var iter__4529__auto__ = (function vp$crud$render_field_select_$_iter__55324(s__55325){
return (new cljs.core.LazySeq(null,(function (){
var s__55325__$1 = s__55325;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55325__$1);
if(temp__5735__auto__){
var s__55325__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55325__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55325__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55328 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55327 = (0);
while(true){
if((i__55327 < size__4528__auto__)){
var option = cljs.core._nth(c__4527__auto__,i__55327);
cljs.core.chunk_append(b__55328,cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)], null),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(option)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)], null)));

var G__55873 = (i__55327 + (1));
i__55327 = G__55873;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55328),vp$crud$render_field_select_$_iter__55324(cljs.core.chunk_rest(s__55325__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55328),null);
}
} else {
var option = cljs.core.first(s__55325__$2);
return cljs.core.cons(cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)], null),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(option)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)], null)),vp$crud$render_field_select_$_iter__55324(cljs.core.rest(s__55325__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(new cljs.core.Keyword(null,"options","options",99638489).cljs$core$IFn$_invoke$arity$1(field));
})()], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
});
vp.crud.render_field_button_toggles = (function vp$crud$render_field_button_toggles(form_path,field){
return (function (){
var form_data_atom = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var form_data = cljs.core.deref(form_data_atom);
var current_value = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var is_valid_QMARK_ = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var set_and_validate = (function (v){
re_frame.core.dispatch_sync(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","set!","forms.re-frame/set!",498933512),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field),v], null));

return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
});
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3.text-center","div.mb-3.text-center",-326981795),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core.not(is_valid_QMARK_))?"is-invalid":null)], null),(function (){var iter__4529__auto__ = (function vp$crud$render_field_button_toggles_$_iter__55345(s__55346){
return (new cljs.core.LazySeq(null,(function (){
var s__55346__$1 = s__55346;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55346__$1);
if(temp__5735__auto__){
var s__55346__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55346__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55346__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55348 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55347 = (0);
while(true){
if((i__55347 < size__4528__auto__)){
var option = cljs.core._nth(c__4527__auto__,i__55347);
cljs.core.chunk_append(b__55348,cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.btn.btn-outline-primary.mx-2","label.btn.btn-outline-primary.mx-2",-1641037952),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (i__55347,option,c__4527__auto__,size__4528__auto__,b__55348,s__55346__$2,temp__5735__auto__,form_data_atom,form_data,current_value,is_valid_QMARK_,set_and_validate){
return (function (){
return set_and_validate(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option));
});})(i__55347,option,c__4527__auto__,size__4528__auto__,b__55348,s__55346__$2,temp__5735__auto__,form_data_atom,form_data,current_value,is_valid_QMARK_,set_and_validate))
,new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"padding","padding",1660304693),"10px 35px"], null),new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option),current_value))?"active":null)], null),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(option)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)], null)));

var G__55875 = (i__55347 + (1));
i__55347 = G__55875;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55348),vp$crud$render_field_button_toggles_$_iter__55345(cljs.core.chunk_rest(s__55346__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55348),null);
}
} else {
var option = cljs.core.first(s__55346__$2);
return cljs.core.cons(cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.btn.btn-outline-primary.mx-2","label.btn.btn-outline-primary.mx-2",-1641037952),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (option,s__55346__$2,temp__5735__auto__,form_data_atom,form_data,current_value,is_valid_QMARK_,set_and_validate){
return (function (){
return set_and_validate(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option));
});})(option,s__55346__$2,temp__5735__auto__,form_data_atom,form_data,current_value,is_valid_QMARK_,set_and_validate))
,new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"padding","padding",1660304693),"10px 35px"], null),new cljs.core.Keyword(null,"class","class",-2030961996),((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option),current_value))?"active":null)], null),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(option)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(option)], null)),vp$crud$render_field_button_toggles_$_iter__55345(cljs.core.rest(s__55346__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(new cljs.core.Keyword(null,"options","options",99638489).cljs$core$IFn$_invoke$arity$1(field));
})(),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
});
vp.crud.render_field_date = (function vp$crud$render_field_date(form_path,field){
var form_data_subs = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var form_data = cljs.core.deref(form_data_subs);
var is_valid_QMARK_ = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
var on_change_handler = (function (date){
re_frame.core.dispatch_sync(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","set!","forms.re-frame/set!",498933512),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field),date], null));

if(cljs.core.not(is_valid_QMARK_)){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path,true], null));
} else {
return null;
}
});
var errors = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","errors-for-path","forms.re-frame/errors-for-path",-1106864696),form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.mb-3","div.mb-3",2018571275),cljs.core.PersistentArrayMap.EMPTY,(cljs.core.truth_(new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label.form-label","label.form-label",1421537292),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null):null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,">",">",-555517146),shadow.js.shim.module$react_datepicker.default,cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"input-opts","input-opts",1688681135).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"on-change","on-change",-732046149),on_change_handler,new cljs.core.Keyword(null,"selected","selected",574897764),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_data,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)),new cljs.core.Keyword(null,"placeholderText","placeholderText",514788846),new cljs.core.Keyword(null,"placeholder","placeholder",-104873083).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.Keyword(null,"className","className",-1983287057),[" form-control ",((cljs.core.not(is_valid_QMARK_))?"is-invalid":null)].join('')], null),((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.Keyword(null,"datetime","datetime",494675702)))?new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"showTimeSelect","showTimeSelect",1956696855),true,new cljs.core.Keyword(null,"timeIntervals","timeIntervals",1766251508),(15),new cljs.core.Keyword(null,"dateFormat","dateFormat",21680147),"MM/dd/yyyy h:mm aa"], null):null)], 0))], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_errors,form_path,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)], null);
});
vp.crud.render_row = (function vp$crud$render_row(form_path,p__55363){
var map__55364 = p__55363;
var map__55364__$1 = (((((!((map__55364 == null))))?(((((map__55364.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__55364.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__55364):map__55364);
var fields = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55364__$1,new cljs.core.Keyword(null,"fields","fields",-1932066230));
var col_class = (function (){var G__55370 = cljs.core.count(fields);
switch (G__55370) {
case (0):
return "col-sm-12";

break;
case (1):
return "col-sm-12";

break;
case (2):
return "col-sm-6";

break;
case (3):
return "col-sm-4";

break;
case (4):
return "col-sm-3";

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__55370)].join('')));

}
})();
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.row","div.row",133678515),(function (){var iter__4529__auto__ = (function vp$crud$render_row_$_iter__55372(s__55373){
return (new cljs.core.LazySeq(null,(function (){
var s__55373__$1 = s__55373;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55373__$1);
if(temp__5735__auto__){
var s__55373__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55373__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55373__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55375 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55374 = (0);
while(true){
if((i__55374 < size__4528__auto__)){
var field = cljs.core._nth(c__4527__auto__,i__55374);
cljs.core.chunk_append(b__55375,cljs.core.with_meta(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col","div.col",-1800797011),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field,form_path,field], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)));

var G__55878 = (i__55374 + (1));
i__55374 = G__55878;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55375),vp$crud$render_row_$_iter__55372(cljs.core.chunk_rest(s__55373__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55375),null);
}
} else {
var field = cljs.core.first(s__55373__$2);
return cljs.core.cons(cljs.core.with_meta(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.col","div.col",-1800797011),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field,form_path,field], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null)),vp$crud$render_row_$_iter__55372(cljs.core.rest(s__55373__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(fields);
})()], null);
});
vp.crud.render_field = (function vp$crud$render_field(form_path,field){
var G__55385 = new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(field);
var G__55385__$1 = (((G__55385 instanceof cljs.core.Keyword))?G__55385.fqn:null);
switch (G__55385__$1) {
case "date":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_date,form_path,field], null);

break;
case "datetime":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_date,form_path,field], null);

break;
case "textarea":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_textarea,form_path,field], null);

break;
case "float":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_float,form_path,field], null);

break;
case "int":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_int,form_path,field], null);

break;
case "select":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_select,form_path,field], null);

break;
case "button-toggles":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_button_toggles,form_path,field], null);

break;
case "row":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_row,form_path,field], null);

break;
case "checkbox":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_checkbox,form_path,field], null);

break;
case "radio":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_radio,form_path,field], null);

break;
case "uppy-dashboard":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_uppy_file,form_path,field], null);

break;
case "file":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_file,form_path,field], null);

break;
case "phone":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_phone,form_path,field], null);

break;
case "custom":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"render","render",-1408033454).cljs$core$IFn$_invoke$arity$1(field),form_path,field], null);

break;
default:
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field_text,form_path,field], null);

}
});
vp.crud.render_fields = (function vp$crud$render_fields(p__55392){
var map__55393 = p__55392;
var map__55393__$1 = (((((!((map__55393 == null))))?(((((map__55393.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__55393.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__55393):map__55393);
var form_path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55393__$1,new cljs.core.Keyword(null,"form-path","form-path",-1656304235));
var fields = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55393__$1,new cljs.core.Keyword(null,"fields","fields",-1932066230));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),(function (){var iter__4529__auto__ = (function vp$crud$render_fields_$_iter__55398(s__55399){
return (new cljs.core.LazySeq(null,(function (){
var s__55399__$1 = s__55399;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55399__$1);
if(temp__5735__auto__){
var s__55399__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55399__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55399__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55401 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55400 = (0);
while(true){
if((i__55400 < size__4528__auto__)){
var vec__55406 = cljs.core._nth(c__4527__auto__,i__55400);
var idx = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55406,(0),null);
var field = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55406,(1),null);
cljs.core.chunk_append(b__55401,cljs.core.with_meta(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),(cljs.core.truth_(field)?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field,form_path,field], null):null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),idx], null)));

var G__55882 = (i__55400 + (1));
i__55400 = G__55882;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55401),vp$crud$render_fields_$_iter__55398(cljs.core.chunk_rest(s__55399__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55401),null);
}
} else {
var vec__55412 = cljs.core.first(s__55399__$2);
var idx = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55412,(0),null);
var field = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55412,(1),null);
return cljs.core.cons(cljs.core.with_meta(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),(cljs.core.truth_(field)?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_field,form_path,field], null):null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),idx], null)),vp$crud$render_fields_$_iter__55398(cljs.core.rest(s__55399__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(cljs.core.vector,cljs.core.filter.cljs$core$IFn$_invoke$arity$2(cljs.core.some_QMARK_,fields)));
})()], null);
});
vp.crud.get_alignment = (function vp$crud$get_alignment(field){
var or__4126__auto__ = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(field,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"opts","opts",155075701),new cljs.core.Keyword(null,"alignment","alignment",1040093386)], null));
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"int","int",-1741416922),null,new cljs.core.Keyword(null,"float","float",-1732389368),null,new cljs.core.Keyword(null,"percentage","percentage",-1610213650),null,new cljs.core.Keyword(null,"currency","currency",-898327568),null], null), null),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(field))){
return "text-align-right";
} else {
return null;
}
}
});
vp.crud.render_table = (function vp$crud$render_table(p__55418){
var map__55419 = p__55418;
var map__55419__$1 = (((((!((map__55419 == null))))?(((((map__55419.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__55419.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__55419):map__55419);
var fields = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55419__$1,new cljs.core.Keyword(null,"fields","fields",-1932066230));
var items = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55419__$1,new cljs.core.Keyword(null,"items","items",1031954938));
var on_delete = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55419__$1,new cljs.core.Keyword(null,"on-delete","on-delete",-1882190355));
var on_edit = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55419__$1,new cljs.core.Keyword(null,"on-edit","on-edit",745088083));
var label = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55419__$1,new cljs.core.Keyword(null,"label","label",1718410804));
var actions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__55419__$1,new cljs.core.Keyword(null,"actions","actions",-812656882));
if((items == null)){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.fas.fa-spinner.fa-spin.fa-2x","i.fas.fa-spinner.fa-spin.fa-2x",-1248813142)], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.count(items),(0))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-center","p.text-center",1457951226),"There are currently no ",(function (){var or__4126__auto__ = label;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "items";
}
})()], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"table.table","table.table",-538258781),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"thead","thead",-291875296),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),(function (){var iter__4529__auto__ = (function vp$crud$render_table_$_iter__55424(s__55425){
return (new cljs.core.LazySeq(null,(function (){
var s__55425__$1 = s__55425;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55425__$1);
if(temp__5735__auto__){
var s__55425__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55425__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55425__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55427 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55426 = (0);
while(true){
if((i__55426 < size__4528__auto__)){
var field = cljs.core._nth(c__4527__auto__,i__55426);
cljs.core.chunk_append(b__55427,(function (){var alignment = vp.crud.get_alignment(field);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"scope","scope",-439358418),"col",new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [alignment], null)], null),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null);
})());

var G__55884 = (i__55426 + (1));
i__55426 = G__55884;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55427),vp$crud$render_table_$_iter__55424(cljs.core.chunk_rest(s__55425__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55427),null);
}
} else {
var field = cljs.core.first(s__55425__$2);
return cljs.core.cons((function (){var alignment = vp.crud.get_alignment(field);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"scope","scope",-439358418),"col",new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field),new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [alignment], null)], null),new cljs.core.Keyword(null,"label","label",1718410804).cljs$core$IFn$_invoke$arity$1(field)], null);
})(),vp$crud$render_table_$_iter__55424(cljs.core.rest(s__55425__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(fields);
})(),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th.table-actions","th.table-actions",1663089200),"Actions"], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tbody","tbody",-80678300),(function (){var iter__4529__auto__ = (function vp$crud$render_table_$_iter__55436(s__55437){
return (new cljs.core.LazySeq(null,(function (){
var s__55437__$1 = s__55437;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55437__$1);
if(temp__5735__auto__){
var s__55437__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55437__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55437__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55439 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55438 = (0);
while(true){
if((i__55438 < size__4528__auto__)){
var vec__55440 = cljs.core._nth(c__4527__auto__,i__55438);
var idx = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55440,(0),null);
var item = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55440,(1),null);
cljs.core.chunk_append(b__55439,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"scope","scope",-439358418),"row",new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(item)], null),(function (){var iter__4529__auto__ = ((function (i__55438,vec__55440,idx,item,c__4527__auto__,size__4528__auto__,b__55439,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions){
return (function vp$crud$render_table_$_iter__55436_$_iter__55446(s__55448){
return (new cljs.core.LazySeq(null,((function (i__55438,vec__55440,idx,item,c__4527__auto__,size__4528__auto__,b__55439,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions){
return (function (){
var s__55448__$1 = s__55448;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__55448__$1);
if(temp__5735__auto____$1){
var s__55448__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__55448__$2)){
var c__4527__auto____$1 = cljs.core.chunk_first(s__55448__$2);
var size__4528__auto____$1 = cljs.core.count(c__4527__auto____$1);
var b__55452 = cljs.core.chunk_buffer(size__4528__auto____$1);
if((function (){var i__55450 = (0);
while(true){
if((i__55450 < size__4528__auto____$1)){
var field = cljs.core._nth(c__4527__auto____$1,i__55450);
cljs.core.chunk_append(b__55452,(function (){var alignment = vp.crud.get_alignment(field);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [alignment], null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null),(function (){var v = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(item,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var G__55454 = new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(field);
var G__55454__$1 = (((G__55454 instanceof cljs.core.Keyword))?G__55454.fqn:null);
switch (G__55454__$1) {
case "date":
return vp.util.date_format.cljs$core$IFn$_invoke$arity$1(v);

break;
case "datetime":
return vp.util.datetime_format(v);

break;
case "percentage":
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(v),"%"].join('');

break;
case "currency":
return vp.util.format_currency(v);

break;
case "custom":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"render","render",-1408033454).cljs$core$IFn$_invoke$arity$1(field),item], null);

break;
case "link":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),(function (){var fexpr__55457 = new cljs.core.Keyword(null,"href","href",-793805698).cljs$core$IFn$_invoke$arity$1(field);
return (fexpr__55457.cljs$core$IFn$_invoke$arity$1 ? fexpr__55457.cljs$core$IFn$_invoke$arity$1(v) : fexpr__55457.call(null,v));
})()], null),cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)], null);

break;
default:
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(v);

}
})()], null);
})());

var G__55888 = (i__55450 + (1));
i__55450 = G__55888;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55452),vp$crud$render_table_$_iter__55436_$_iter__55446(cljs.core.chunk_rest(s__55448__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55452),null);
}
} else {
var field = cljs.core.first(s__55448__$2);
return cljs.core.cons((function (){var alignment = vp.crud.get_alignment(field);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [alignment], null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null),(function (){var v = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(item,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var G__55458 = new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(field);
var G__55458__$1 = (((G__55458 instanceof cljs.core.Keyword))?G__55458.fqn:null);
switch (G__55458__$1) {
case "date":
return vp.util.date_format.cljs$core$IFn$_invoke$arity$1(v);

break;
case "datetime":
return vp.util.datetime_format(v);

break;
case "percentage":
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(v),"%"].join('');

break;
case "currency":
return vp.util.format_currency(v);

break;
case "custom":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"render","render",-1408033454).cljs$core$IFn$_invoke$arity$1(field),item], null);

break;
case "link":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),(function (){var fexpr__55460 = new cljs.core.Keyword(null,"href","href",-793805698).cljs$core$IFn$_invoke$arity$1(field);
return (fexpr__55460.cljs$core$IFn$_invoke$arity$1 ? fexpr__55460.cljs$core$IFn$_invoke$arity$1(v) : fexpr__55460.call(null,v));
})()], null),cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)], null);

break;
default:
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(v);

}
})()], null);
})(),vp$crud$render_table_$_iter__55436_$_iter__55446(cljs.core.rest(s__55448__$2)));
}
} else {
return null;
}
break;
}
});})(i__55438,vec__55440,idx,item,c__4527__auto__,size__4528__auto__,b__55439,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions))
,null,null));
});})(i__55438,vec__55440,idx,item,c__4527__auto__,size__4528__auto__,b__55439,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions))
;
return iter__4529__auto__(fields);
})(),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.table-actions","td.table-actions",-527796693),(cljs.core.truth_(on_edit)?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-primary.btn-rounded.me-1.btn-sm","button.btn.btn-primary.btn-rounded.me-1.btn-sm",695834194),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (i__55438,vec__55440,idx,item,c__4527__auto__,size__4528__auto__,b__55439,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions){
return (function (){
return (on_edit.cljs$core$IFn$_invoke$arity$2 ? on_edit.cljs$core$IFn$_invoke$arity$2(item,idx) : on_edit.call(null,item,idx));
});})(i__55438,vec__55440,idx,item,c__4527__auto__,size__4528__auto__,b__55439,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions))
], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.fas.fa-pen","i.fas.fa-pen",1682520398)], null)], null):null),(cljs.core.truth_(on_delete)?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-danger.btn-rounded.btn-sm","button.btn.btn-danger.btn-rounded.btn-sm",1683384047),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (i__55438,vec__55440,idx,item,c__4527__auto__,size__4528__auto__,b__55439,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions){
return (function (){
return (on_delete.cljs$core$IFn$_invoke$arity$2 ? on_delete.cljs$core$IFn$_invoke$arity$2(item,idx) : on_delete.call(null,item,idx));
});})(i__55438,vec__55440,idx,item,c__4527__auto__,size__4528__auto__,b__55439,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions))
], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.fas.fa-trash","i.fas.fa-trash",25003308)], null)], null):null),(cljs.core.truth_(actions)?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [actions,item], null):null)], null)], null));

var G__55892 = (i__55438 + (1));
i__55438 = G__55892;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55439),vp$crud$render_table_$_iter__55436(cljs.core.chunk_rest(s__55437__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55439),null);
}
} else {
var vec__55465 = cljs.core.first(s__55437__$2);
var idx = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55465,(0),null);
var item = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55465,(1),null);
return cljs.core.cons(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"scope","scope",-439358418),"row",new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(item)], null),(function (){var iter__4529__auto__ = ((function (vec__55465,idx,item,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions){
return (function vp$crud$render_table_$_iter__55436_$_iter__55468(s__55469){
return (new cljs.core.LazySeq(null,(function (){
var s__55469__$1 = s__55469;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__55469__$1);
if(temp__5735__auto____$1){
var s__55469__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__55469__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55469__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55472 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55471 = (0);
while(true){
if((i__55471 < size__4528__auto__)){
var field = cljs.core._nth(c__4527__auto__,i__55471);
cljs.core.chunk_append(b__55472,(function (){var alignment = vp.crud.get_alignment(field);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [alignment], null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null),(function (){var v = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(item,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var G__55476 = new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(field);
var G__55476__$1 = (((G__55476 instanceof cljs.core.Keyword))?G__55476.fqn:null);
switch (G__55476__$1) {
case "date":
return vp.util.date_format.cljs$core$IFn$_invoke$arity$1(v);

break;
case "datetime":
return vp.util.datetime_format(v);

break;
case "percentage":
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(v),"%"].join('');

break;
case "currency":
return vp.util.format_currency(v);

break;
case "custom":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"render","render",-1408033454).cljs$core$IFn$_invoke$arity$1(field),item], null);

break;
case "link":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),(function (){var fexpr__55478 = new cljs.core.Keyword(null,"href","href",-793805698).cljs$core$IFn$_invoke$arity$1(field);
return (fexpr__55478.cljs$core$IFn$_invoke$arity$1 ? fexpr__55478.cljs$core$IFn$_invoke$arity$1(v) : fexpr__55478.call(null,v));
})()], null),cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)], null);

break;
default:
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(v);

}
})()], null);
})());

var G__55895 = (i__55471 + (1));
i__55471 = G__55895;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55472),vp$crud$render_table_$_iter__55436_$_iter__55468(cljs.core.chunk_rest(s__55469__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55472),null);
}
} else {
var field = cljs.core.first(s__55469__$2);
return cljs.core.cons((function (){var alignment = vp.crud.get_alignment(field);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [alignment], null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field)], null),(function (){var v = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(item,new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(field));
var G__55480 = new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(field);
var G__55480__$1 = (((G__55480 instanceof cljs.core.Keyword))?G__55480.fqn:null);
switch (G__55480__$1) {
case "date":
return vp.util.date_format.cljs$core$IFn$_invoke$arity$1(v);

break;
case "datetime":
return vp.util.datetime_format(v);

break;
case "percentage":
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(v),"%"].join('');

break;
case "currency":
return vp.util.format_currency(v);

break;
case "custom":
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"render","render",-1408033454).cljs$core$IFn$_invoke$arity$1(field),item], null);

break;
case "link":
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),(function (){var fexpr__55482 = new cljs.core.Keyword(null,"href","href",-793805698).cljs$core$IFn$_invoke$arity$1(field);
return (fexpr__55482.cljs$core$IFn$_invoke$arity$1 ? fexpr__55482.cljs$core$IFn$_invoke$arity$1(v) : fexpr__55482.call(null,v));
})()], null),cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)], null);

break;
default:
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(v);

}
})()], null);
})(),vp$crud$render_table_$_iter__55436_$_iter__55468(cljs.core.rest(s__55469__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});})(vec__55465,idx,item,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions))
;
return iter__4529__auto__(fields);
})(),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td.table-actions","td.table-actions",-527796693),(cljs.core.truth_(on_edit)?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-primary.btn-rounded.me-1.btn-sm","button.btn.btn-primary.btn-rounded.me-1.btn-sm",695834194),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (vec__55465,idx,item,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions){
return (function (){
return (on_edit.cljs$core$IFn$_invoke$arity$2 ? on_edit.cljs$core$IFn$_invoke$arity$2(item,idx) : on_edit.call(null,item,idx));
});})(vec__55465,idx,item,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions))
], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.fas.fa-pen","i.fas.fa-pen",1682520398)], null)], null):null),(cljs.core.truth_(on_delete)?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-danger.btn-rounded.btn-sm","button.btn.btn-danger.btn-rounded.btn-sm",1683384047),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (vec__55465,idx,item,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions){
return (function (){
return (on_delete.cljs$core$IFn$_invoke$arity$2 ? on_delete.cljs$core$IFn$_invoke$arity$2(item,idx) : on_delete.call(null,item,idx));
});})(vec__55465,idx,item,s__55437__$2,temp__5735__auto__,map__55419,map__55419__$1,fields,items,on_delete,on_edit,label,actions))
], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.fas.fa-trash","i.fas.fa-trash",25003308)], null)], null):null),(cljs.core.truth_(actions)?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [actions,item], null):null)], null)], null),vp$crud$render_table_$_iter__55436(cljs.core.rest(s__55437__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(cljs.core.vector,items));
})()], null)], null);

}
}
});
Object.defineProperty(module.exports, "render_field_select", { enumerable: false, get: function() { return vp.crud.render_field_select; } });
Object.defineProperty(module.exports, "render_field_checkbox", { enumerable: false, get: function() { return vp.crud.render_field_checkbox; } });
Object.defineProperty(module.exports, "render_uppy_file", { enumerable: false, get: function() { return vp.crud.render_uppy_file; } });
Object.defineProperty(module.exports, "render_field_float", { enumerable: false, get: function() { return vp.crud.render_field_float; } });
Object.defineProperty(module.exports, "render_field_date", { enumerable: false, get: function() { return vp.crud.render_field_date; } });
Object.defineProperty(module.exports, "file_uppy", { enumerable: false, get: function() { return vp.crud.file_uppy; } });
Object.defineProperty(module.exports, "render_table", { enumerable: false, get: function() { return vp.crud.render_table; } });
Object.defineProperty(module.exports, "get_alignment", { enumerable: false, get: function() { return vp.crud.get_alignment; } });
Object.defineProperty(module.exports, "render_row", { enumerable: false, get: function() { return vp.crud.render_row; } });
Object.defineProperty(module.exports, "openModal", { enumerable: false, get: function() { return vp.crud.openModal; } });
Object.defineProperty(module.exports, "render_field_text", { enumerable: false, get: function() { return vp.crud.render_field_text; } });
Object.defineProperty(module.exports, "render_fields", { enumerable: false, get: function() { return vp.crud.render_fields; } });
Object.defineProperty(module.exports, "image_editor", { enumerable: false, get: function() { return vp.crud.image_editor; } });
Object.defineProperty(module.exports, "render_errors", { enumerable: false, get: function() { return vp.crud.render_errors; } });
Object.defineProperty(module.exports, "url_pattern", { enumerable: false, get: function() { return vp.crud.url_pattern; } });
Object.defineProperty(module.exports, "render_field_int", { enumerable: false, get: function() { return vp.crud.render_field_int; } });
Object.defineProperty(module.exports, "render_field", { enumerable: false, get: function() { return vp.crud.render_field; } });
Object.defineProperty(module.exports, "render_field_file", { enumerable: false, get: function() { return vp.crud.render_field_file; } });
Object.defineProperty(module.exports, "render_field_button_toggles", { enumerable: false, get: function() { return vp.crud.render_field_button_toggles; } });
Object.defineProperty(module.exports, "render_field_phone", { enumerable: false, get: function() { return vp.crud.render_field_phone; } });
Object.defineProperty(module.exports, "file_cmp", { enumerable: false, get: function() { return vp.crud.file_cmp; } });
Object.defineProperty(module.exports, "setter", { enumerable: false, get: function() { return vp.crud.setter; } });
Object.defineProperty(module.exports, "to_validator", { enumerable: false, get: function() { return vp.crud.to_validator; } });
Object.defineProperty(module.exports, "value_setter", { enumerable: false, get: function() { return vp.crud.value_setter; } });
Object.defineProperty(module.exports, "validations", { enumerable: false, get: function() { return vp.crud.validations; } });
Object.defineProperty(module.exports, "render_field_textarea", { enumerable: false, get: function() { return vp.crud.render_field_textarea; } });
Object.defineProperty(module.exports, "render_field_radio", { enumerable: false, get: function() { return vp.crud.render_field_radio; } });
//# sourceMappingURL=vp.crud.js.map

var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./cljs_time.core.js");
require("./cljs_time.format.js");
require("./goog.date.utcdatetime.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("cljs_time.coerce.js");

goog.provide('cljs_time.coerce');

/**
 * @interface
 */
cljs_time.coerce.ICoerce = function(){};

var cljs_time$coerce$ICoerce$to_date_time$dyn_43248 = (function (obj){
var x__4428__auto__ = (((obj == null))?null:obj);
var m__4429__auto__ = (cljs_time.coerce.to_date_time[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(obj) : m__4429__auto__.call(null,obj));
} else {
var m__4426__auto__ = (cljs_time.coerce.to_date_time["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(obj) : m__4426__auto__.call(null,obj));
} else {
throw cljs.core.missing_protocol("ICoerce.to-date-time",obj);
}
}
});
/**
 * Convert `obj` to a goog.date.DateTime instance.
 */
cljs_time.coerce.to_date_time = (function cljs_time$coerce$to_date_time(obj){
if((((!((obj == null)))) && ((!((obj.cljs_time$coerce$ICoerce$to_date_time$arity$1 == null)))))){
return obj.cljs_time$coerce$ICoerce$to_date_time$arity$1(obj);
} else {
return cljs_time$coerce$ICoerce$to_date_time$dyn_43248(obj);
}
});

/**
 * Returns a DateTime instance in the UTC time zone corresponding to the given
 *   number of milliseconds after the Unix epoch.
 */
cljs_time.coerce.from_long = (function cljs_time$coerce$from_long(millis){
var G__43219 = millis;
if((G__43219 == null)){
return null;
} else {
return (goog.date.UtcDateTime.fromTimestamp.cljs$core$IFn$_invoke$arity$1 ? goog.date.UtcDateTime.fromTimestamp.cljs$core$IFn$_invoke$arity$1(G__43219) : goog.date.UtcDateTime.fromTimestamp.call(null,G__43219));
}
});
/**
 * Returns DateTime instance from string using formatters in cljs-time.format,
 *   returning first which parses
 */
cljs_time.coerce.from_string = (function cljs_time$coerce$from_string(s){
if(cljs.core.truth_(s)){
return cljs.core.first((function (){var iter__4529__auto__ = (function cljs_time$coerce$from_string_$_iter__43221(s__43222){
return (new cljs.core.LazySeq(null,(function (){
var s__43222__$1 = s__43222;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__43222__$1);
if(temp__5735__auto__){
var s__43222__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__43222__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__43222__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__43224 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__43223 = (0);
while(true){
if((i__43223 < size__4528__auto__)){
var f = cljs.core._nth(c__4527__auto__,i__43223);
var d = (function (){try{return cljs_time.format.parse.cljs$core$IFn$_invoke$arity$2(f,s);
}catch (e43226){if((e43226 instanceof Error)){
var _ = e43226;
return null;
} else {
throw e43226;

}
}})();
if(cljs.core.truth_(d)){
cljs.core.chunk_append(b__43224,d);

var G__43258 = (i__43223 + (1));
i__43223 = G__43258;
continue;
} else {
var G__43259 = (i__43223 + (1));
i__43223 = G__43259;
continue;
}
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__43224),cljs_time$coerce$from_string_$_iter__43221(cljs.core.chunk_rest(s__43222__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__43224),null);
}
} else {
var f = cljs.core.first(s__43222__$2);
var d = (function (){try{return cljs_time.format.parse.cljs$core$IFn$_invoke$arity$2(f,s);
}catch (e43228){if((e43228 instanceof Error)){
var _ = e43228;
return null;
} else {
throw e43228;

}
}})();
if(cljs.core.truth_(d)){
return cljs.core.cons(d,cljs_time$coerce$from_string_$_iter__43221(cljs.core.rest(s__43222__$2)));
} else {
var G__43261 = cljs.core.rest(s__43222__$2);
s__43222__$1 = G__43261;
continue;
}
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(cljs.core.vals(cljs_time.format.formatters));
})());
} else {
return null;
}
});
/**
 * Returns a DateTime instance in the UTC time zone corresponding to the given
 *   js Date object.
 */
cljs_time.coerce.from_date = (function cljs_time$coerce$from_date(date){
var G__43232 = date;
var G__43232__$1 = (((G__43232 == null))?null:G__43232.getTime());
if((G__43232__$1 == null)){
return null;
} else {
return cljs_time.coerce.from_long(G__43232__$1);
}
});
/**
 * Convert `obj` to the number of milliseconds after the Unix epoch.
 */
cljs_time.coerce.to_long = (function cljs_time$coerce$to_long(obj){
var G__43234 = obj;
var G__43234__$1 = (((G__43234 == null))?null:cljs_time.coerce.to_date_time(G__43234));
if((G__43234__$1 == null)){
return null;
} else {
return G__43234__$1.getTime();
}
});
/**
 * Convert `obj` to Unix epoch.
 */
cljs_time.coerce.to_epoch = (function cljs_time$coerce$to_epoch(obj){
var millis = cljs_time.coerce.to_long(obj);
var and__4115__auto__ = millis;
if(cljs.core.truth_(and__4115__auto__)){
return cljs.core.quot(millis,(1000));
} else {
return and__4115__auto__;
}
});
/**
 * Convert `obj` to a JavaScript Date instance.
 */
cljs_time.coerce.to_date = (function cljs_time$coerce$to_date(obj){
var G__43236 = obj;
var G__43236__$1 = (((G__43236 == null))?null:cljs_time.coerce.to_date_time(G__43236));
var G__43236__$2 = (((G__43236__$1 == null))?null:G__43236__$1.getTime());
if((G__43236__$2 == null)){
return null;
} else {
return (new Date(G__43236__$2));
}
});
/**
 * Returns a string representation of obj in UTC time-zone
 *   using "yyyy-MM-dd'T'HH:mm:ss.SSSZZ" date-time representation.
 */
cljs_time.coerce.to_string = (function cljs_time$coerce$to_string(obj){
var G__43238 = obj;
var G__43238__$1 = (((G__43238 == null))?null:cljs_time.coerce.to_date_time(G__43238));
if((G__43238__$1 == null)){
return null;
} else {
return cljs_time.format.unparse(new cljs.core.Keyword(null,"date-time","date-time",177938180).cljs$core$IFn$_invoke$arity$1(cljs_time.format.formatters),G__43238__$1);
}
});
/**
 * Convert `obj` to a goog.date.Date instance
 */
cljs_time.coerce.to_local_date = (function cljs_time$coerce$to_local_date(obj){
if(cljs.core.truth_(obj)){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(goog.date.Date,cljs.core.type(obj))){
return obj;
} else {
var temp__5733__auto__ = cljs_time.coerce.to_date_time(obj);
if(cljs.core.truth_(temp__5733__auto__)){
var dt = temp__5733__auto__;
return (new goog.date.Date(dt.getYear(),dt.getMonth(),dt.getDate()));
} else {
return null;
}
}
} else {
return null;
}
});
/**
 * Convert `obj` to a goog.date.DateTime instance
 */
cljs_time.coerce.to_local_date_time = (function cljs_time$coerce$to_local_date_time(obj){
if(cljs.core.truth_(obj)){
var temp__5733__auto__ = cljs_time.coerce.to_date_time(obj);
if(cljs.core.truth_(temp__5733__auto__)){
var dt = temp__5733__auto__;
var G__43241 = (new goog.date.DateTime(dt.getYear(),dt.getMonth(),dt.getDate()));
G__43241.setHours(dt.getHours());

G__43241.setMinutes(dt.getMinutes());

G__43241.setSeconds(dt.getSeconds());

G__43241.setMilliseconds(dt.getMilliseconds());

return G__43241;
} else {
return null;
}
} else {
return null;
}
});
goog.object.set(cljs_time.coerce.ICoerce,"null",true);

goog.object.set(cljs_time.coerce.to_date_time,"null",(function (_){
return null;
}));

(Date.prototype.cljs_time$coerce$ICoerce$ = cljs.core.PROTOCOL_SENTINEL);

(Date.prototype.cljs_time$coerce$ICoerce$to_date_time$arity$1 = (function (date){
var date__$1 = this;
return cljs_time.coerce.from_date(date__$1);
}));

(goog.date.Date.prototype.cljs_time$coerce$ICoerce$ = cljs.core.PROTOCOL_SENTINEL);

(goog.date.Date.prototype.cljs_time$coerce$ICoerce$to_date_time$arity$1 = (function (local_date){
var local_date__$1 = this;
if(cljs.core.truth_(local_date__$1)){
return cljs_time.core.to_utc_time_zone(local_date__$1);
} else {
return null;
}
}));

(goog.date.DateTime.prototype.cljs_time$coerce$ICoerce$ = cljs.core.PROTOCOL_SENTINEL);

(goog.date.DateTime.prototype.cljs_time$coerce$ICoerce$to_date_time$arity$1 = (function (local_date_time){
var local_date_time__$1 = this;
if(cljs.core.truth_(local_date_time__$1)){
return cljs_time.core.to_utc_time_zone(local_date_time__$1);
} else {
return null;
}
}));

(goog.date.UtcDateTime.prototype.cljs_time$coerce$ICoerce$ = cljs.core.PROTOCOL_SENTINEL);

(goog.date.UtcDateTime.prototype.cljs_time$coerce$ICoerce$to_date_time$arity$1 = (function (date_time){
var date_time__$1 = this;
return date_time__$1;
}));

goog.object.set(cljs_time.coerce.ICoerce,"number",true);

goog.object.set(cljs_time.coerce.to_date_time,"number",(function (long$){
return cljs_time.coerce.from_long(long$);
}));

goog.object.set(cljs_time.coerce.ICoerce,"string",true);

goog.object.set(cljs_time.coerce.to_date_time,"string",(function (string){
return cljs_time.coerce.from_string(string);
}));
Object.defineProperty(module.exports, "to_local_date", { enumerable: false, get: function() { return cljs_time.coerce.to_local_date; } });
Object.defineProperty(module.exports, "to_long", { enumerable: false, get: function() { return cljs_time.coerce.to_long; } });
Object.defineProperty(module.exports, "from_date", { enumerable: false, get: function() { return cljs_time.coerce.from_date; } });
Object.defineProperty(module.exports, "to_local_date_time", { enumerable: false, get: function() { return cljs_time.coerce.to_local_date_time; } });
Object.defineProperty(module.exports, "from_string", { enumerable: false, get: function() { return cljs_time.coerce.from_string; } });
Object.defineProperty(module.exports, "to_string", { enumerable: false, get: function() { return cljs_time.coerce.to_string; } });
Object.defineProperty(module.exports, "to_date", { enumerable: false, get: function() { return cljs_time.coerce.to_date; } });
Object.defineProperty(module.exports, "from_long", { enumerable: false, get: function() { return cljs_time.coerce.from_long; } });
Object.defineProperty(module.exports, "ICoerce", { enumerable: false, get: function() { return cljs_time.coerce.ICoerce; } });
Object.defineProperty(module.exports, "to_date_time", { enumerable: false, get: function() { return cljs_time.coerce.to_date_time; } });
Object.defineProperty(module.exports, "to_epoch", { enumerable: false, get: function() { return cljs_time.coerce.to_epoch; } });
//# sourceMappingURL=cljs_time.coerce.js.map

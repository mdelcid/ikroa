var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./cljs_time.core.js");
require("./cljs_time.format.js");
require("./cljs_time.coerce.js");
require("./clojure.string.js");
require("./shadow.js.shim.module$shortid.js");
require("./shadow.js.shim.module$numeral.js");
require("./shadow.js.shim.module$remove_accents.js");
require("./shadow.js.shim.module$moment.js");
require("./cljs.pprint.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("vp.util.js");

goog.provide('vp.util');
if((typeof vp !== 'undefined') && (typeof vp.util !== 'undefined') && (typeof vp.util.default_formatter !== 'undefined')){
} else {
vp.util.default_formatter = cljs_time.format.formatter.cljs$core$IFn$_invoke$arity$1("MMM dd, yyyy");
}
if((typeof vp !== 'undefined') && (typeof vp.util !== 'undefined') && (typeof vp.util.default_time_formatter !== 'undefined')){
} else {
vp.util.default_time_formatter = cljs_time.format.formatter.cljs$core$IFn$_invoke$arity$1("hh:mm a");
}
if((typeof vp !== 'undefined') && (typeof vp.util !== 'undefined') && (typeof vp.util.default_datetime_formatter !== 'undefined')){
} else {
vp.util.default_datetime_formatter = cljs_time.format.formatter.cljs$core$IFn$_invoke$arity$1("MMM dd, yyyy hh:mm a");
}
vp.util.cl_format = cljs.pprint.cl_format;
vp.util.get_cljs_time_date = (function vp$util$get_cljs_time_date(v){
var dateobj = ((cljs.core.int_QMARK_(v))?cljs_time.coerce.from_long(v):((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.type(v),Date))?cljs_time.coerce.from_long(v.getTime()):v
));
return cljs_time.core.to_default_time_zone(dateobj);
});
vp.util.date_format = (function vp$util$date_format(var_args){
var G__59583 = arguments.length;
switch (G__59583) {
case 1:
return vp.util.date_format.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return vp.util.date_format.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(vp.util.date_format.cljs$core$IFn$_invoke$arity$1 = (function (v){
return vp.util.date_format.cljs$core$IFn$_invoke$arity$2(v,vp.util.default_formatter);
}));

(vp.util.date_format.cljs$core$IFn$_invoke$arity$2 = (function (v,formatter){
if(cljs.core.truth_(v)){
var date = vp.util.get_cljs_time_date(v);
var formatter_aux = (((formatter instanceof cljs.core.Keyword))?(cljs_time.format.formatters.cljs$core$IFn$_invoke$arity$1 ? cljs_time.format.formatters.cljs$core$IFn$_invoke$arity$1(formatter) : cljs_time.format.formatters.call(null,formatter)):((typeof formatter === 'string')?cljs_time.format.formatter.cljs$core$IFn$_invoke$arity$1(formatter):formatter
));
return cljs_time.format.unparse(formatter_aux,date);
} else {
return null;
}
}));

(vp.util.date_format.cljs$lang$maxFixedArity = 2);

vp.util.time_format = (function vp$util$time_format(v){
if(cljs.core.truth_(v)){
var date = vp.util.get_cljs_time_date(v);
return cljs_time.format.unparse(vp.util.default_time_formatter,date);
} else {
return null;
}
});
vp.util.datetime_format = (function vp$util$datetime_format(v){
if(cljs.core.truth_(v)){
var date = vp.util.get_cljs_time_date(v);
return cljs_time.format.unparse(vp.util.default_datetime_formatter,date);
} else {
return null;
}
});
vp.util.datetime_format2 = (function vp$util$datetime_format2(v){
return cljs.core.js_invoke.cljs$core$IFn$_invoke$arity$variadic(cljs.core.js_invoke.cljs$core$IFn$_invoke$arity$variadic(shadow.js.shim.module$moment(v),"tz",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["America/Texas"], 0)),"format",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["lll"], 0));
});
vp.util.format_currency = (function vp$util$format_currency(s){
return shadow.js.shim.module$numeral(s).format("$0,0.00");
});
vp.util.format_number = (function vp$util$format_number(s){
return shadow.js.shim.module$numeral(s).format("0,0");
});
vp.util.format_decimal = (function vp$util$format_decimal(s){
return shadow.js.shim.module$numeral(s).format("0,0.00");
});
vp.util.vec_remove = (function vp$util$vec_remove(pos,coll){
return cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(cljs.core.subvec.cljs$core$IFn$_invoke$arity$3(coll,(0),pos),cljs.core.subvec.cljs$core$IFn$_invoke$arity$2(coll,(pos + (1)))));
});
vp.util.dissoc_in = (function vp$util$dissoc_in(m,path){
if((cljs.core.butlast(path) == null)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(m,cljs.core.last(path));
} else {
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(m,cljs.core.butlast(path),cljs.core.dissoc,cljs.core.last(path));
}
});
vp.util.index_by_id = (function vp$util$index_by_id(items){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__59585_SHARP_){
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(p1__59585_SHARP_)),p1__59585_SHARP_],null));
}),items));
});
vp.util.indexes_where = (function vp$util$indexes_where(pred_QMARK_,coll){
return cljs.core.keep_indexed.cljs$core$IFn$_invoke$arity$2((function (p1__59589_SHARP_,p2__59588_SHARP_){
if(cljs.core.truth_((pred_QMARK_.cljs$core$IFn$_invoke$arity$1 ? pred_QMARK_.cljs$core$IFn$_invoke$arity$1(p2__59588_SHARP_) : pred_QMARK_.call(null,p2__59588_SHARP_)))){
return p1__59589_SHARP_;
} else {
return null;
}
}),coll);
});
vp.util.update_where = (function vp$util$update_where(var_args){
var args__4742__auto__ = [];
var len__4736__auto___59604 = arguments.length;
var i__4737__auto___59605 = (0);
while(true){
if((i__4737__auto___59605 < len__4736__auto___59604)){
args__4742__auto__.push((arguments[i__4737__auto___59605]));

var G__59608 = (i__4737__auto___59605 + (1));
i__4737__auto___59605 = G__59608;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((3) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((3)),(0),null)):null);
return vp.util.update_where.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4743__auto__);
});

(vp.util.update_where.cljs$core$IFn$_invoke$arity$variadic = (function (v,pred_QMARK_,f,args){
var temp__5733__auto__ = cljs.core.first(vp.util.indexes_where(pred_QMARK_,v));
if(cljs.core.truth_(temp__5733__auto__)){
var i = temp__5733__auto__;
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(v,i,cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f,(v.cljs$core$IFn$_invoke$arity$1 ? v.cljs$core$IFn$_invoke$arity$1(i) : v.call(null,i)),args));
} else {
return v;
}
}));

(vp.util.update_where.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(vp.util.update_where.cljs$lang$applyTo = (function (seq59594){
var G__59595 = cljs.core.first(seq59594);
var seq59594__$1 = cljs.core.next(seq59594);
var G__59596 = cljs.core.first(seq59594__$1);
var seq59594__$2 = cljs.core.next(seq59594__$1);
var G__59597 = cljs.core.first(seq59594__$2);
var seq59594__$3 = cljs.core.next(seq59594__$2);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__59595,G__59596,G__59597,seq59594__$3);
}));

vp.util.gen_shortid = (function vp$util$gen_shortid(){
return shadow.js.shim.module$shortid.generate();
});
vp.util.distinct_by = (function vp$util$distinct_by(f,items){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.sorted_set_by((function (p1__59598_SHARP_,p2__59601_SHARP_){
return cljs.core.compare((f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(p1__59598_SHARP_) : f.call(null,p1__59598_SHARP_)),(f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(p2__59601_SHARP_) : f.call(null,p2__59601_SHARP_)));
})),items);
});
vp.util.prevdef = (function vp$util$prevdef(f){
return (function (ev){
ev.preventDefault();

return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(ev) : f.call(null,ev));
});
});
vp.util.md5 = (function vp$util$md5(s){
return md5(s);
});
vp.util.search_compare = (function vp$util$search_compare(s1,s2){
var and__4115__auto__ = s1;
if(cljs.core.truth_(and__4115__auto__)){
var and__4115__auto____$1 = s2;
if(cljs.core.truth_(and__4115__auto____$1)){
return clojure.string.includes_QMARK_(shadow.js.shim.module$remove_accents.remove(clojure.string.lower_case(clojure.string.trim(s1))),shadow.js.shim.module$remove_accents.remove(clojure.string.lower_case(clojure.string.trim(s2))));
} else {
return and__4115__auto____$1;
}
} else {
return and__4115__auto__;
}
});
vp.util.search_filter = (function vp$util$search_filter(items,ks,text){
return cljs.core.filter.cljs$core$IFn$_invoke$arity$2((function (item){
return cljs.core.some((function (k){
var v = (k.cljs$core$IFn$_invoke$arity$1 ? k.cljs$core$IFn$_invoke$arity$1(item) : k.call(null,item));
return vp.util.search_compare(v,text);
}),ks);
}),items);
});
vp.util.email_regex = /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
vp.util.email_QMARK_ = (function vp$util$email_QMARK_(s){
return cljs.core.boolean$(((typeof s === 'string')?cljs.core.re_matches(vp.util.email_regex,s):false));
});
vp.util.text__GT_emails = (function vp$util$text__GT_emails(text){
if(typeof text === 'string'){
return cljs.core.filterv(vp.util.email_QMARK_,cljs.core.map.cljs$core$IFn$_invoke$arity$2(clojure.string.trim,clojure.string.split.cljs$core$IFn$_invoke$arity$2(text,/[ ,\n]/)));
} else {
return null;
}
});
Object.defineProperty(module.exports, "text__GT_emails", { enumerable: false, get: function() { return vp.util.text__GT_emails; } });
Object.defineProperty(module.exports, "update_where", { enumerable: false, get: function() { return vp.util.update_where; } });
Object.defineProperty(module.exports, "dissoc_in", { enumerable: false, get: function() { return vp.util.dissoc_in; } });
Object.defineProperty(module.exports, "default_datetime_formatter", { enumerable: false, get: function() { return vp.util.default_datetime_formatter; } });
Object.defineProperty(module.exports, "email_QMARK_", { enumerable: false, get: function() { return vp.util.email_QMARK_; } });
Object.defineProperty(module.exports, "get_cljs_time_date", { enumerable: false, get: function() { return vp.util.get_cljs_time_date; } });
Object.defineProperty(module.exports, "indexes_where", { enumerable: false, get: function() { return vp.util.indexes_where; } });
Object.defineProperty(module.exports, "distinct_by", { enumerable: false, get: function() { return vp.util.distinct_by; } });
Object.defineProperty(module.exports, "format_decimal", { enumerable: false, get: function() { return vp.util.format_decimal; } });
Object.defineProperty(module.exports, "md5", { enumerable: false, get: function() { return vp.util.md5; } });
Object.defineProperty(module.exports, "search_compare", { enumerable: false, get: function() { return vp.util.search_compare; } });
Object.defineProperty(module.exports, "default_formatter", { enumerable: false, get: function() { return vp.util.default_formatter; } });
Object.defineProperty(module.exports, "format_currency", { enumerable: false, get: function() { return vp.util.format_currency; } });
Object.defineProperty(module.exports, "default_time_formatter", { enumerable: false, get: function() { return vp.util.default_time_formatter; } });
Object.defineProperty(module.exports, "datetime_format2", { enumerable: false, get: function() { return vp.util.datetime_format2; } });
Object.defineProperty(module.exports, "time_format", { enumerable: false, get: function() { return vp.util.time_format; } });
Object.defineProperty(module.exports, "cl_format", { enumerable: false, get: function() { return vp.util.cl_format; } });
Object.defineProperty(module.exports, "datetime_format", { enumerable: false, get: function() { return vp.util.datetime_format; } });
Object.defineProperty(module.exports, "gen_shortid", { enumerable: false, get: function() { return vp.util.gen_shortid; } });
Object.defineProperty(module.exports, "date_format", { enumerable: false, get: function() { return vp.util.date_format; } });
Object.defineProperty(module.exports, "email_regex", { enumerable: false, get: function() { return vp.util.email_regex; } });
Object.defineProperty(module.exports, "format_number", { enumerable: false, get: function() { return vp.util.format_number; } });
Object.defineProperty(module.exports, "index_by_id", { enumerable: false, get: function() { return vp.util.index_by_id; } });
Object.defineProperty(module.exports, "search_filter", { enumerable: false, get: function() { return vp.util.search_filter; } });
Object.defineProperty(module.exports, "vec_remove", { enumerable: false, get: function() { return vp.util.vec_remove; } });
Object.defineProperty(module.exports, "prevdef", { enumerable: false, get: function() { return vp.util.prevdef; } });
//# sourceMappingURL=vp.util.js.map

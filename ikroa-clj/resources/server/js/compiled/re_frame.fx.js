var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./re_frame.router.js");
require("./re_frame.db.js");
require("./re_frame.interceptor.js");
require("./re_frame.interop.js");
require("./re_frame.events.js");
require("./re_frame.registrar.js");
require("./re_frame.loggers.js");
require("./re_frame.trace.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("re_frame.fx.js");

goog.provide('re_frame.fx');
re_frame.fx.kind = new cljs.core.Keyword(null,"fx","fx",-1237829572);
if(cljs.core.truth_((re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1 ? re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1(re_frame.fx.kind) : re_frame.registrar.kinds.call(null,re_frame.fx.kind)))){
} else {
throw (new Error("Assert failed: (re-frame.registrar/kinds kind)"));
}
re_frame.fx.reg_fx = (function re_frame$fx$reg_fx(id,handler){
return re_frame.registrar.register_handler(re_frame.fx.kind,id,handler);
});
/**
 * An interceptor whose `:after` actions the contents of `:effects`. As a result,
 *   this interceptor is Domino 3.
 * 
 *   This interceptor is silently added (by reg-event-db etc) to the front of
 *   interceptor chains for all events.
 * 
 *   For each key in `:effects` (a map), it calls the registered `effects handler`
 *   (see `reg-fx` for registration of effect handlers).
 * 
 *   So, if `:effects` was:
 *    {:dispatch  [:hello 42]
 *     :db        {...}
 *     :undo      "set flag"}
 * 
 *   it will call the registered effect handlers for each of the map's keys:
 *   `:dispatch`, `:undo` and `:db`. When calling each handler, provides the map
 *   value for that key - so in the example above the effect handler for :dispatch
 *   will be given one arg `[:hello 42]`.
 * 
 *   You cannot rely on the ordering in which effects are executed, other than that
 *   `:db` is guaranteed to be executed first.
 */
re_frame.fx.do_fx = re_frame.interceptor.__GT_interceptor.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"do-fx","do-fx",1194163050),new cljs.core.Keyword(null,"after","after",594996914),(function re_frame$fx$do_fx_after(context){
if(re_frame.trace.is_trace_enabled_QMARK_()){
var _STAR_current_trace_STAR__orig_val__44993 = re_frame.trace._STAR_current_trace_STAR_;
var _STAR_current_trace_STAR__temp_val__44994 = re_frame.trace.start_trace(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword("event","do-fx","event/do-fx",1357330452)], null));
(re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__temp_val__44994);

try{try{var effects = new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context);
var effects_without_db = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(effects,new cljs.core.Keyword(null,"db","db",993250759));
var temp__5735__auto___45137 = new cljs.core.Keyword(null,"db","db",993250759).cljs$core$IFn$_invoke$arity$1(effects);
if(cljs.core.truth_(temp__5735__auto___45137)){
var new_db_45138 = temp__5735__auto___45137;
var fexpr__44996_45139 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,new cljs.core.Keyword(null,"db","db",993250759),false);
(fexpr__44996_45139.cljs$core$IFn$_invoke$arity$1 ? fexpr__44996_45139.cljs$core$IFn$_invoke$arity$1(new_db_45138) : fexpr__44996_45139.call(null,new_db_45138));
} else {
}

var seq__44997 = cljs.core.seq(effects_without_db);
var chunk__44998 = null;
var count__44999 = (0);
var i__45000 = (0);
while(true){
if((i__45000 < count__44999)){
var vec__45009 = chunk__44998.cljs$core$IIndexed$_nth$arity$2(null,i__45000);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45009,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45009,(1),null);
var temp__5733__auto___45140 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___45140)){
var effect_fn_45141 = temp__5733__auto___45140;
(effect_fn_45141.cljs$core$IFn$_invoke$arity$1 ? effect_fn_45141.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_45141.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__45142 = seq__44997;
var G__45143 = chunk__44998;
var G__45144 = count__44999;
var G__45145 = (i__45000 + (1));
seq__44997 = G__45142;
chunk__44998 = G__45143;
count__44999 = G__45144;
i__45000 = G__45145;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__44997);
if(temp__5735__auto__){
var seq__44997__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__44997__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__44997__$1);
var G__45146 = cljs.core.chunk_rest(seq__44997__$1);
var G__45147 = c__4556__auto__;
var G__45148 = cljs.core.count(c__4556__auto__);
var G__45149 = (0);
seq__44997 = G__45146;
chunk__44998 = G__45147;
count__44999 = G__45148;
i__45000 = G__45149;
continue;
} else {
var vec__45013 = cljs.core.first(seq__44997__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45013,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45013,(1),null);
var temp__5733__auto___45150 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___45150)){
var effect_fn_45151 = temp__5733__auto___45150;
(effect_fn_45151.cljs$core$IFn$_invoke$arity$1 ? effect_fn_45151.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_45151.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__45152 = cljs.core.next(seq__44997__$1);
var G__45153 = null;
var G__45154 = (0);
var G__45155 = (0);
seq__44997 = G__45152;
chunk__44998 = G__45153;
count__44999 = G__45154;
i__45000 = G__45155;
continue;
}
} else {
return null;
}
}
break;
}
}finally {if(re_frame.trace.is_trace_enabled_QMARK_()){
var end__40838__auto___45156 = re_frame.interop.now();
var duration__40839__auto___45157 = (end__40838__auto___45156 - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(re_frame.trace.traces,cljs.core.conj,cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__40839__auto___45157,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"end","end",-268185958),re_frame.interop.now()], 0)));

re_frame.trace.run_tracing_callbacks_BANG_(end__40838__auto___45156);
} else {
}
}}finally {(re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__orig_val__44993);
}} else {
var effects = new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context);
var effects_without_db = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(effects,new cljs.core.Keyword(null,"db","db",993250759));
var temp__5735__auto___45158 = new cljs.core.Keyword(null,"db","db",993250759).cljs$core$IFn$_invoke$arity$1(effects);
if(cljs.core.truth_(temp__5735__auto___45158)){
var new_db_45159 = temp__5735__auto___45158;
var fexpr__45017_45160 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,new cljs.core.Keyword(null,"db","db",993250759),false);
(fexpr__45017_45160.cljs$core$IFn$_invoke$arity$1 ? fexpr__45017_45160.cljs$core$IFn$_invoke$arity$1(new_db_45159) : fexpr__45017_45160.call(null,new_db_45159));
} else {
}

var seq__45019 = cljs.core.seq(effects_without_db);
var chunk__45020 = null;
var count__45021 = (0);
var i__45022 = (0);
while(true){
if((i__45022 < count__45021)){
var vec__45031 = chunk__45020.cljs$core$IIndexed$_nth$arity$2(null,i__45022);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45031,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45031,(1),null);
var temp__5733__auto___45161 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___45161)){
var effect_fn_45163 = temp__5733__auto___45161;
(effect_fn_45163.cljs$core$IFn$_invoke$arity$1 ? effect_fn_45163.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_45163.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__45165 = seq__45019;
var G__45166 = chunk__45020;
var G__45167 = count__45021;
var G__45168 = (i__45022 + (1));
seq__45019 = G__45165;
chunk__45020 = G__45166;
count__45021 = G__45167;
i__45022 = G__45168;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__45019);
if(temp__5735__auto__){
var seq__45019__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__45019__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__45019__$1);
var G__45172 = cljs.core.chunk_rest(seq__45019__$1);
var G__45173 = c__4556__auto__;
var G__45174 = cljs.core.count(c__4556__auto__);
var G__45175 = (0);
seq__45019 = G__45172;
chunk__45020 = G__45173;
count__45021 = G__45174;
i__45022 = G__45175;
continue;
} else {
var vec__45034 = cljs.core.first(seq__45019__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45034,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45034,(1),null);
var temp__5733__auto___45177 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___45177)){
var effect_fn_45178 = temp__5733__auto___45177;
(effect_fn_45178.cljs$core$IFn$_invoke$arity$1 ? effect_fn_45178.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_45178.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__45179 = cljs.core.next(seq__45019__$1);
var G__45180 = null;
var G__45181 = (0);
var G__45182 = (0);
seq__45019 = G__45179;
chunk__45020 = G__45180;
count__45021 = G__45181;
i__45022 = G__45182;
continue;
}
} else {
return null;
}
}
break;
}
}
})], 0));
re_frame.fx.dispatch_later = (function re_frame$fx$dispatch_later(p__45043){
var map__45044 = p__45043;
var map__45044__$1 = (((((!((map__45044 == null))))?(((((map__45044.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45044.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__45044):map__45044);
var effect = map__45044__$1;
var ms = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__45044__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__45044__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_(dispatch)) || ((!(typeof ms === 'number'))))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-later value:",effect], 0));
} else {
return re_frame.interop.set_timeout_BANG_((function (){
return re_frame.router.dispatch(dispatch);
}),ms);
}
});
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-later","dispatch-later",291951390),(function (value){
if(cljs.core.map_QMARK_(value)){
return re_frame.fx.dispatch_later(value);
} else {
var seq__45049 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__45050 = null;
var count__45051 = (0);
var i__45052 = (0);
while(true){
if((i__45052 < count__45051)){
var effect = chunk__45050.cljs$core$IIndexed$_nth$arity$2(null,i__45052);
re_frame.fx.dispatch_later(effect);


var G__45187 = seq__45049;
var G__45188 = chunk__45050;
var G__45189 = count__45051;
var G__45190 = (i__45052 + (1));
seq__45049 = G__45187;
chunk__45050 = G__45188;
count__45051 = G__45189;
i__45052 = G__45190;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__45049);
if(temp__5735__auto__){
var seq__45049__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__45049__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__45049__$1);
var G__45193 = cljs.core.chunk_rest(seq__45049__$1);
var G__45194 = c__4556__auto__;
var G__45195 = cljs.core.count(c__4556__auto__);
var G__45196 = (0);
seq__45049 = G__45193;
chunk__45050 = G__45194;
count__45051 = G__45195;
i__45052 = G__45196;
continue;
} else {
var effect = cljs.core.first(seq__45049__$1);
re_frame.fx.dispatch_later(effect);


var G__45200 = cljs.core.next(seq__45049__$1);
var G__45201 = null;
var G__45202 = (0);
var G__45203 = (0);
seq__45049 = G__45200;
chunk__45050 = G__45201;
count__45051 = G__45202;
i__45052 = G__45203;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"fx","fx",-1237829572),(function (seq_of_effects){
if((!(cljs.core.sequential_QMARK_(seq_of_effects)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect expects a seq, but was given ",cljs.core.type(seq_of_effects)], 0));
} else {
var seq__45067 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,seq_of_effects));
var chunk__45068 = null;
var count__45069 = (0);
var i__45070 = (0);
while(true){
if((i__45070 < count__45069)){
var vec__45088 = chunk__45068.cljs$core$IIndexed$_nth$arity$2(null,i__45070);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45088,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45088,(1),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"db","db",993250759),effect_key)){
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect should not contain a :db effect"], 0));
} else {
}

var temp__5733__auto___45205 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___45205)){
var effect_fn_45206 = temp__5733__auto___45205;
(effect_fn_45206.cljs$core$IFn$_invoke$arity$1 ? effect_fn_45206.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_45206.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: in \":fx\" effect found ",effect_key," which has no associated handler. Ignoring."], 0));
}


var G__45209 = seq__45067;
var G__45210 = chunk__45068;
var G__45211 = count__45069;
var G__45212 = (i__45070 + (1));
seq__45067 = G__45209;
chunk__45068 = G__45210;
count__45069 = G__45211;
i__45070 = G__45212;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__45067);
if(temp__5735__auto__){
var seq__45067__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__45067__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__45067__$1);
var G__45216 = cljs.core.chunk_rest(seq__45067__$1);
var G__45217 = c__4556__auto__;
var G__45218 = cljs.core.count(c__4556__auto__);
var G__45219 = (0);
seq__45067 = G__45216;
chunk__45068 = G__45217;
count__45069 = G__45218;
i__45070 = G__45219;
continue;
} else {
var vec__45092 = cljs.core.first(seq__45067__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45092,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45092,(1),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"db","db",993250759),effect_key)){
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect should not contain a :db effect"], 0));
} else {
}

var temp__5733__auto___45221 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___45221)){
var effect_fn_45222 = temp__5733__auto___45221;
(effect_fn_45222.cljs$core$IFn$_invoke$arity$1 ? effect_fn_45222.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_45222.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: in \":fx\" effect found ",effect_key," which has no associated handler. Ignoring."], 0));
}


var G__45225 = cljs.core.next(seq__45067__$1);
var G__45226 = null;
var G__45227 = (0);
var G__45228 = (0);
seq__45067 = G__45225;
chunk__45068 = G__45226;
count__45069 = G__45227;
i__45070 = G__45228;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),(function (value){
if((!(cljs.core.vector_QMARK_(value)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch value. Expected a vector, but got:",value], 0));
} else {
return re_frame.router.dispatch(value);
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),(function (value){
if((!(cljs.core.sequential_QMARK_(value)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-n value. Expected a collection, but got:",value], 0));
} else {
var seq__45098 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__45099 = null;
var count__45100 = (0);
var i__45101 = (0);
while(true){
if((i__45101 < count__45100)){
var event = chunk__45099.cljs$core$IIndexed$_nth$arity$2(null,i__45101);
re_frame.router.dispatch(event);


var G__45233 = seq__45098;
var G__45234 = chunk__45099;
var G__45235 = count__45100;
var G__45236 = (i__45101 + (1));
seq__45098 = G__45233;
chunk__45099 = G__45234;
count__45100 = G__45235;
i__45101 = G__45236;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__45098);
if(temp__5735__auto__){
var seq__45098__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__45098__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__45098__$1);
var G__45239 = cljs.core.chunk_rest(seq__45098__$1);
var G__45240 = c__4556__auto__;
var G__45241 = cljs.core.count(c__4556__auto__);
var G__45242 = (0);
seq__45098 = G__45239;
chunk__45099 = G__45240;
count__45100 = G__45241;
i__45101 = G__45242;
continue;
} else {
var event = cljs.core.first(seq__45098__$1);
re_frame.router.dispatch(event);


var G__45245 = cljs.core.next(seq__45098__$1);
var G__45246 = null;
var G__45247 = (0);
var G__45248 = (0);
seq__45098 = G__45245;
chunk__45099 = G__45246;
count__45100 = G__45247;
i__45101 = G__45248;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"deregister-event-handler","deregister-event-handler",-1096518994),(function (value){
var clear_event = cljs.core.partial.cljs$core$IFn$_invoke$arity$2(re_frame.registrar.clear_handlers,re_frame.events.kind);
if(cljs.core.sequential_QMARK_(value)){
var seq__45124 = cljs.core.seq(value);
var chunk__45125 = null;
var count__45126 = (0);
var i__45127 = (0);
while(true){
if((i__45127 < count__45126)){
var event = chunk__45125.cljs$core$IIndexed$_nth$arity$2(null,i__45127);
clear_event(event);


var G__45250 = seq__45124;
var G__45251 = chunk__45125;
var G__45252 = count__45126;
var G__45253 = (i__45127 + (1));
seq__45124 = G__45250;
chunk__45125 = G__45251;
count__45126 = G__45252;
i__45127 = G__45253;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__45124);
if(temp__5735__auto__){
var seq__45124__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__45124__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__45124__$1);
var G__45255 = cljs.core.chunk_rest(seq__45124__$1);
var G__45256 = c__4556__auto__;
var G__45257 = cljs.core.count(c__4556__auto__);
var G__45258 = (0);
seq__45124 = G__45255;
chunk__45125 = G__45256;
count__45126 = G__45257;
i__45127 = G__45258;
continue;
} else {
var event = cljs.core.first(seq__45124__$1);
clear_event(event);


var G__45261 = cljs.core.next(seq__45124__$1);
var G__45262 = null;
var G__45263 = (0);
var G__45264 = (0);
seq__45124 = G__45261;
chunk__45125 = G__45262;
count__45126 = G__45263;
i__45127 = G__45264;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return clear_event(value);
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"db","db",993250759),(function (value){
if((!((cljs.core.deref(re_frame.db.app_db) === value)))){
return cljs.core.reset_BANG_(re_frame.db.app_db,value);
} else {
return null;
}
}));
Object.defineProperty(module.exports, "kind", { enumerable: false, get: function() { return re_frame.fx.kind; } });
Object.defineProperty(module.exports, "reg_fx", { enumerable: false, get: function() { return re_frame.fx.reg_fx; } });
Object.defineProperty(module.exports, "do_fx", { enumerable: false, get: function() { return re_frame.fx.do_fx; } });
Object.defineProperty(module.exports, "dispatch_later", { enumerable: false, get: function() { return re_frame.fx.dispatch_later; } });
//# sourceMappingURL=re_frame.fx.js.map

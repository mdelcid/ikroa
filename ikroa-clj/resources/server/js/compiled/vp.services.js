var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./vp.macros.js");
require("./cljs.core.async.js");
require("./clojure.string.js");
require("./shadow.js.shim.module$meteor$meteor.js");
require("./shadow.js.shim.module$meteor$fetch.js");
require("./cljs.core.async.interop.js");
require("./vp.rapyd.js");
require("./vp.collections.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var camel_snake_kebab=$CLJS.camel_snake_kebab || ($CLJS.camel_snake_kebab = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("vp.services.js");

goog.provide('vp.services');
vp.services.ho = (function vp$services$ho(){
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_68733){
var state_val_68735 = (state_68733[(1)]);
if((state_val_68735 === (7))){
var inst_68687 = (state_68733[(7)]);
var inst_68723 = (function(){throw inst_68687})();
var state_68733__$1 = state_68733;
var statearr_68738_69102 = state_68733__$1;
(statearr_68738_69102[(2)] = inst_68723);

(statearr_68738_69102[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68735 === (1))){
var inst_68650 = shadow.js.shim.module$meteor$fetch.fetch("https://jsonplaceholder.typicode.com/todos/1");
var inst_68652 = cljs.core.async.interop.p__GT_c(inst_68650);
var state_68733__$1 = state_68733;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68733__$1,(3),inst_68652);
} else {
if((state_val_68735 === (4))){
var inst_68657 = (state_68733[(8)]);
var inst_68676 = (function(){throw inst_68657})();
var state_68733__$1 = state_68733;
var statearr_68745_69104 = state_68733__$1;
(statearr_68745_69104[(2)] = inst_68676);

(statearr_68745_69104[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68735 === (6))){
var inst_68680 = (state_68733[(2)]);
var inst_68684 = inst_68680.json();
var inst_68685 = cljs.core.async.interop.p__GT_c(inst_68684);
var state_68733__$1 = state_68733;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68733__$1,(2),inst_68685);
} else {
if((state_val_68735 === (3))){
var inst_68657 = (state_68733[(8)]);
var inst_68657__$1 = (state_68733[(2)]);
var inst_68660 = (inst_68657__$1 instanceof cljs.core.ExceptionInfo);
var inst_68665 = cljs.core.ex_data(inst_68657__$1);
var inst_68666 = new cljs.core.Keyword(null,"error","error",-978969032).cljs$core$IFn$_invoke$arity$1(inst_68665);
var inst_68668 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_68666,new cljs.core.Keyword(null,"promise-error","promise-error",-90673560));
var inst_68669 = ((inst_68660) && (inst_68668));
var state_68733__$1 = (function (){var statearr_68758 = state_68733;
(statearr_68758[(8)] = inst_68657__$1);

return statearr_68758;
})();
if(cljs.core.truth_(inst_68669)){
var statearr_68760_69107 = state_68733__$1;
(statearr_68760_69107[(1)] = (4));

} else {
var statearr_68762_69108 = state_68733__$1;
(statearr_68762_69108[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68735 === (2))){
var inst_68687 = (state_68733[(7)]);
var inst_68687__$1 = (state_68733[(2)]);
var inst_68709 = (inst_68687__$1 instanceof cljs.core.ExceptionInfo);
var inst_68714 = cljs.core.ex_data(inst_68687__$1);
var inst_68715 = new cljs.core.Keyword(null,"error","error",-978969032).cljs$core$IFn$_invoke$arity$1(inst_68714);
var inst_68716 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_68715,new cljs.core.Keyword(null,"promise-error","promise-error",-90673560));
var inst_68717 = ((inst_68709) && (inst_68716));
var state_68733__$1 = (function (){var statearr_68774 = state_68733;
(statearr_68774[(7)] = inst_68687__$1);

return statearr_68774;
})();
if(cljs.core.truth_(inst_68717)){
var statearr_68778_69111 = state_68733__$1;
(statearr_68778_69111[(1)] = (7));

} else {
var statearr_68780_69112 = state_68733__$1;
(statearr_68780_69112[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68735 === (9))){
var inst_68727 = (state_68733[(2)]);
var inst_68729 = console.log(inst_68727);
var state_68733__$1 = (function (){var statearr_68782 = state_68733;
(statearr_68782[(9)] = inst_68729);

return statearr_68782;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_68733__$1,inst_68727);
} else {
if((state_val_68735 === (5))){
var inst_68657 = (state_68733[(8)]);
var state_68733__$1 = state_68733;
var statearr_68785_69114 = state_68733__$1;
(statearr_68785_69114[(2)] = inst_68657);

(statearr_68785_69114[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68735 === (8))){
var inst_68687 = (state_68733[(7)]);
var state_68733__$1 = state_68733;
var statearr_68789_69116 = state_68733__$1;
(statearr_68789_69116[(2)] = inst_68687);

(statearr_68789_69116[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var vp$services$ho_$_state_machine__41292__auto__ = null;
var vp$services$ho_$_state_machine__41292__auto____0 = (function (){
var statearr_68798 = [null,null,null,null,null,null,null,null,null,null];
(statearr_68798[(0)] = vp$services$ho_$_state_machine__41292__auto__);

(statearr_68798[(1)] = (1));

return statearr_68798;
});
var vp$services$ho_$_state_machine__41292__auto____1 = (function (state_68733){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_68733);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e68805){var ex__41295__auto__ = e68805;
var statearr_68807_69117 = state_68733;
(statearr_68807_69117[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_68733[(4)]))){
var statearr_68808_69118 = state_68733;
(statearr_68808_69118[(1)] = cljs.core.first((state_68733[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__69121 = state_68733;
state_68733 = G__69121;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$services$ho_$_state_machine__41292__auto__ = function(state_68733){
switch(arguments.length){
case 0:
return vp$services$ho_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$services$ho_$_state_machine__41292__auto____1.call(this,state_68733);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$services$ho_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$services$ho_$_state_machine__41292__auto____0;
vp$services$ho_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$services$ho_$_state_machine__41292__auto____1;
return vp$services$ho_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_68816 = f__41438__auto__();
(statearr_68816[(6)] = c__41437__auto__);

return statearr_68816;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
});
vp.services.myfunction = (function vp$services$myfunction(){
if(cljs.core.truth_(Meteor.isServer)){
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_68855){
var state_val_68858 = (state_68855[(1)]);
if((state_val_68858 === (1))){
var inst_68828 = vp.rapyd.req(new cljs.core.Keyword(null,"get","get",1683182755),"/v1/data/countries");
var state_68855__$1 = state_68855;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68855__$1,(2),inst_68828);
} else {
if((state_val_68858 === (2))){
var inst_68831 = (state_68855[(7)]);
var inst_68831__$1 = (state_68855[(2)]);
var inst_68833 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var inst_68834 = (function (){var res = inst_68831__$1;
var chan__42098__auto__ = inst_68833;
return (function (){
return cljs.core.count(new cljs.core.Keyword(null,"plans","plans",75657163).cljs$core$IFn$_invoke$arity$1(vp.collections.collections).find(cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",276297046),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"$exists","$exists",1589502941),true], null)], null)),({"skip": (1), "limit": (2)})).fetch());
});
})();
var inst_68836 = (vp.meteor.bind_env.cljs$core$IFn$_invoke$arity$2 ? vp.meteor.bind_env.cljs$core$IFn$_invoke$arity$2(inst_68833,inst_68834) : vp.meteor.bind_env.call(null,inst_68833,inst_68834));
var state_68855__$1 = (function (){var statearr_68871 = state_68855;
(statearr_68871[(8)] = inst_68836);

(statearr_68871[(7)] = inst_68831__$1);

return statearr_68871;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68855__$1,(3),inst_68833);
} else {
if((state_val_68858 === (3))){
var inst_68831 = (state_68855[(7)]);
var inst_68840 = (state_68855[(2)]);
var inst_68844 = cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["yey"], 0));
var inst_68847 = [new cljs.core.Keyword(null,"success","success",1890645906),new cljs.core.Keyword(null,"res","res",-1395007879),new cljs.core.Keyword(null,"met","met",-378954951),new cljs.core.Keyword(null,"a","a",-2123407586)];
var inst_68849 = [true,inst_68831,inst_68840,(3)];
var inst_68851 = cljs.core.PersistentHashMap.fromArrays(inst_68847,inst_68849);
var state_68855__$1 = (function (){var statearr_68877 = state_68855;
(statearr_68877[(9)] = inst_68844);

return statearr_68877;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_68855__$1,inst_68851);
} else {
return null;
}
}
}
});
return (function() {
var vp$services$myfunction_$_state_machine__41292__auto__ = null;
var vp$services$myfunction_$_state_machine__41292__auto____0 = (function (){
var statearr_68884 = [null,null,null,null,null,null,null,null,null,null];
(statearr_68884[(0)] = vp$services$myfunction_$_state_machine__41292__auto__);

(statearr_68884[(1)] = (1));

return statearr_68884;
});
var vp$services$myfunction_$_state_machine__41292__auto____1 = (function (state_68855){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_68855);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e68886){var ex__41295__auto__ = e68886;
var statearr_68887_69171 = state_68855;
(statearr_68887_69171[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_68855[(4)]))){
var statearr_68888_69172 = state_68855;
(statearr_68888_69172[(1)] = cljs.core.first((state_68855[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__69174 = state_68855;
state_68855 = G__69174;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$services$myfunction_$_state_machine__41292__auto__ = function(state_68855){
switch(arguments.length){
case 0:
return vp$services$myfunction_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$services$myfunction_$_state_machine__41292__auto____1.call(this,state_68855);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$services$myfunction_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$services$myfunction_$_state_machine__41292__auto____0;
vp$services$myfunction_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$services$myfunction_$_state_machine__41292__auto____1;
return vp$services$myfunction_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_68890 = f__41438__auto__();
(statearr_68890[(6)] = c__41437__auto__);

return statearr_68890;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
} else {
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_68907){
var state_val_68908 = (state_68907[(1)]);
if((state_val_68908 === (1))){
var inst_68900 = cljs.core.PersistentVector.EMPTY;
var inst_68901 = vp.meteor.t_write(inst_68900);
var inst_68902 = vp.meteor.call("vp.services.myfunction",inst_68901);
var state_68907__$1 = state_68907;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68907__$1,(2),inst_68902);
} else {
if((state_val_68908 === (2))){
var inst_68904 = (state_68907[(2)]);
var inst_68905 = vp.meteor.t_read(inst_68904);
var state_68907__$1 = state_68907;
return cljs.core.async.impl.ioc_helpers.return_chan(state_68907__$1,inst_68905);
} else {
return null;
}
}
});
return (function() {
var vp$services$myfunction_$_state_machine__41292__auto__ = null;
var vp$services$myfunction_$_state_machine__41292__auto____0 = (function (){
var statearr_68910 = [null,null,null,null,null,null,null];
(statearr_68910[(0)] = vp$services$myfunction_$_state_machine__41292__auto__);

(statearr_68910[(1)] = (1));

return statearr_68910;
});
var vp$services$myfunction_$_state_machine__41292__auto____1 = (function (state_68907){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_68907);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e68913){var ex__41295__auto__ = e68913;
var statearr_68914_69183 = state_68907;
(statearr_68914_69183[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_68907[(4)]))){
var statearr_68916_69184 = state_68907;
(statearr_68916_69184[(1)] = cljs.core.first((state_68907[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__69187 = state_68907;
state_68907 = G__69187;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$services$myfunction_$_state_machine__41292__auto__ = function(state_68907){
switch(arguments.length){
case 0:
return vp$services$myfunction_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$services$myfunction_$_state_machine__41292__auto____1.call(this,state_68907);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$services$myfunction_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$services$myfunction_$_state_machine__41292__auto____0;
vp$services$myfunction_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$services$myfunction_$_state_machine__41292__auto____1;
return vp$services$myfunction_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_68918 = f__41438__auto__();
(statearr_68918[(6)] = c__41437__auto__);

return statearr_68918;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
}
});
goog.exportSymbol('vp.services.myfunction', vp.services.myfunction);

if(cljs.core.truth_(Meteor.isServer)){
Meteor["methods"](cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 1, ["vp.services.myfunction",(function (args__42089__auto__){
return vp.meteor.to_promise((function (){var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_68925){
var state_val_68927 = (state_68925[(1)]);
if((state_val_68927 === (1))){
var inst_68919 = vp.meteor.t_read(args__42089__auto__);
var inst_68920 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(vp.services.myfunction,inst_68919);
var state_68925__$1 = state_68925;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68925__$1,(2),inst_68920);
} else {
if((state_val_68927 === (2))){
var inst_68922 = (state_68925[(2)]);
var inst_68923 = vp.meteor.t_write(inst_68922);
var state_68925__$1 = state_68925;
return cljs.core.async.impl.ioc_helpers.return_chan(state_68925__$1,inst_68923);
} else {
return null;
}
}
});
return (function() {
var vp$services$state_machine__41292__auto__ = null;
var vp$services$state_machine__41292__auto____0 = (function (){
var statearr_68935 = [null,null,null,null,null,null,null];
(statearr_68935[(0)] = vp$services$state_machine__41292__auto__);

(statearr_68935[(1)] = (1));

return statearr_68935;
});
var vp$services$state_machine__41292__auto____1 = (function (state_68925){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_68925);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e68936){var ex__41295__auto__ = e68936;
var statearr_68937_69198 = state_68925;
(statearr_68937_69198[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_68925[(4)]))){
var statearr_68938_69199 = state_68925;
(statearr_68938_69199[(1)] = cljs.core.first((state_68925[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__69202 = state_68925;
state_68925 = G__69202;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$services$state_machine__41292__auto__ = function(state_68925){
switch(arguments.length){
case 0:
return vp$services$state_machine__41292__auto____0.call(this);
case 1:
return vp$services$state_machine__41292__auto____1.call(this,state_68925);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$services$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$services$state_machine__41292__auto____0;
vp$services$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$services$state_machine__41292__auto____1;
return vp$services$state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_68942 = f__41438__auto__();
(statearr_68942[(6)] = c__41437__auto__);

return statearr_68942;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
})());
})], null)));
} else {
}
vp.services.create_company_wallet = (function vp$services$create_company_wallet(){
if(cljs.core.truth_(Meteor.isServer)){
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_68958){
var state_val_68959 = (state_68958[(1)]);
if((state_val_68959 === (1))){
var inst_68945 = vp.rapyd.req(new cljs.core.Keyword(null,"get","get",1683182755),"/v1/data/countries");
var state_68958__$1 = state_68958;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68958__$1,(2),inst_68945);
} else {
if((state_val_68959 === (2))){
var inst_68947 = (state_68958[(7)]);
var inst_68947__$1 = (state_68958[(2)]);
var inst_68948 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var inst_68949 = (function (){var res = inst_68947__$1;
var chan__42098__auto__ = inst_68948;
return (function (){
return cljs.core.count(new cljs.core.Keyword(null,"plans","plans",75657163).cljs$core$IFn$_invoke$arity$1(vp.collections.collections).find(cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",276297046),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"$exists","$exists",1589502941),true], null)], null)),({"skip": (1), "limit": (2)})).fetch());
});
})();
var inst_68950 = (vp.meteor.bind_env.cljs$core$IFn$_invoke$arity$2 ? vp.meteor.bind_env.cljs$core$IFn$_invoke$arity$2(inst_68948,inst_68949) : vp.meteor.bind_env.call(null,inst_68948,inst_68949));
var state_68958__$1 = (function (){var statearr_68975 = state_68958;
(statearr_68975[(7)] = inst_68947__$1);

(statearr_68975[(8)] = inst_68950);

return statearr_68975;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68958__$1,(3),inst_68948);
} else {
if((state_val_68959 === (3))){
var inst_68947 = (state_68958[(7)]);
var inst_68952 = (state_68958[(2)]);
var inst_68953 = cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["yey"], 0));
var inst_68954 = [new cljs.core.Keyword(null,"success","success",1890645906),new cljs.core.Keyword(null,"res","res",-1395007879),new cljs.core.Keyword(null,"met","met",-378954951),new cljs.core.Keyword(null,"a","a",-2123407586)];
var inst_68955 = [true,inst_68947,inst_68952,(3)];
var inst_68956 = cljs.core.PersistentHashMap.fromArrays(inst_68954,inst_68955);
var state_68958__$1 = (function (){var statearr_68977 = state_68958;
(statearr_68977[(9)] = inst_68953);

return statearr_68977;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_68958__$1,inst_68956);
} else {
return null;
}
}
}
});
return (function() {
var vp$services$create_company_wallet_$_state_machine__41292__auto__ = null;
var vp$services$create_company_wallet_$_state_machine__41292__auto____0 = (function (){
var statearr_68979 = [null,null,null,null,null,null,null,null,null,null];
(statearr_68979[(0)] = vp$services$create_company_wallet_$_state_machine__41292__auto__);

(statearr_68979[(1)] = (1));

return statearr_68979;
});
var vp$services$create_company_wallet_$_state_machine__41292__auto____1 = (function (state_68958){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_68958);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e68981){var ex__41295__auto__ = e68981;
var statearr_68982_69215 = state_68958;
(statearr_68982_69215[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_68958[(4)]))){
var statearr_68985_69216 = state_68958;
(statearr_68985_69216[(1)] = cljs.core.first((state_68958[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__69223 = state_68958;
state_68958 = G__69223;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$services$create_company_wallet_$_state_machine__41292__auto__ = function(state_68958){
switch(arguments.length){
case 0:
return vp$services$create_company_wallet_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$services$create_company_wallet_$_state_machine__41292__auto____1.call(this,state_68958);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$services$create_company_wallet_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$services$create_company_wallet_$_state_machine__41292__auto____0;
vp$services$create_company_wallet_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$services$create_company_wallet_$_state_machine__41292__auto____1;
return vp$services$create_company_wallet_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_68987 = f__41438__auto__();
(statearr_68987[(6)] = c__41437__auto__);

return statearr_68987;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
} else {
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_69002){
var state_val_69004 = (state_69002[(1)]);
if((state_val_69004 === (1))){
var inst_68989 = cljs.core.PersistentVector.EMPTY;
var inst_68990 = vp.meteor.t_write(inst_68989);
var inst_68991 = vp.meteor.call("vp.services.create-company-wallet",inst_68990);
var state_69002__$1 = state_69002;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_69002__$1,(2),inst_68991);
} else {
if((state_val_69004 === (2))){
var inst_68993 = (state_69002[(2)]);
var inst_68994 = vp.meteor.t_read(inst_68993);
var state_69002__$1 = state_69002;
return cljs.core.async.impl.ioc_helpers.return_chan(state_69002__$1,inst_68994);
} else {
return null;
}
}
});
return (function() {
var vp$services$create_company_wallet_$_state_machine__41292__auto__ = null;
var vp$services$create_company_wallet_$_state_machine__41292__auto____0 = (function (){
var statearr_69010 = [null,null,null,null,null,null,null];
(statearr_69010[(0)] = vp$services$create_company_wallet_$_state_machine__41292__auto__);

(statearr_69010[(1)] = (1));

return statearr_69010;
});
var vp$services$create_company_wallet_$_state_machine__41292__auto____1 = (function (state_69002){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_69002);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e69012){var ex__41295__auto__ = e69012;
var statearr_69013_69268 = state_69002;
(statearr_69013_69268[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_69002[(4)]))){
var statearr_69015_69269 = state_69002;
(statearr_69015_69269[(1)] = cljs.core.first((state_69002[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__69272 = state_69002;
state_69002 = G__69272;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$services$create_company_wallet_$_state_machine__41292__auto__ = function(state_69002){
switch(arguments.length){
case 0:
return vp$services$create_company_wallet_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$services$create_company_wallet_$_state_machine__41292__auto____1.call(this,state_69002);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$services$create_company_wallet_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$services$create_company_wallet_$_state_machine__41292__auto____0;
vp$services$create_company_wallet_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$services$create_company_wallet_$_state_machine__41292__auto____1;
return vp$services$create_company_wallet_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_69016 = f__41438__auto__();
(statearr_69016[(6)] = c__41437__auto__);

return statearr_69016;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
}
});
goog.exportSymbol('vp.services.create_company_wallet', vp.services.create_company_wallet);

if(cljs.core.truth_(Meteor.isServer)){
Meteor["methods"](cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 1, ["vp.services.create-company-wallet",(function (args__42089__auto__){
return vp.meteor.to_promise((function (){var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_69031){
var state_val_69032 = (state_69031[(1)]);
if((state_val_69032 === (1))){
var inst_69025 = vp.meteor.t_read(args__42089__auto__);
var inst_69026 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(vp.services.create_company_wallet,inst_69025);
var state_69031__$1 = state_69031;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_69031__$1,(2),inst_69026);
} else {
if((state_val_69032 === (2))){
var inst_69028 = (state_69031[(2)]);
var inst_69029 = vp.meteor.t_write(inst_69028);
var state_69031__$1 = state_69031;
return cljs.core.async.impl.ioc_helpers.return_chan(state_69031__$1,inst_69029);
} else {
return null;
}
}
});
return (function() {
var vp$services$state_machine__41292__auto__ = null;
var vp$services$state_machine__41292__auto____0 = (function (){
var statearr_69036 = [null,null,null,null,null,null,null];
(statearr_69036[(0)] = vp$services$state_machine__41292__auto__);

(statearr_69036[(1)] = (1));

return statearr_69036;
});
var vp$services$state_machine__41292__auto____1 = (function (state_69031){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_69031);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e69038){var ex__41295__auto__ = e69038;
var statearr_69039_69281 = state_69031;
(statearr_69039_69281[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_69031[(4)]))){
var statearr_69041_69282 = state_69031;
(statearr_69041_69282[(1)] = cljs.core.first((state_69031[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__69285 = state_69031;
state_69031 = G__69285;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$services$state_machine__41292__auto__ = function(state_69031){
switch(arguments.length){
case 0:
return vp$services$state_machine__41292__auto____0.call(this);
case 1:
return vp$services$state_machine__41292__auto____1.call(this,state_69031);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$services$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$services$state_machine__41292__auto____0;
vp$services$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$services$state_machine__41292__auto____1;
return vp$services$state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_69042 = f__41438__auto__();
(statearr_69042[(6)] = c__41437__auto__);

return statearr_69042;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
})());
})], null)));
} else {
}
vp.services.upload_url = (function vp$services$upload_url(file){
if(cljs.core.truth_(Meteor.isServer)){
var fname_parts = clojure.string.split.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(file),/\./);
var fname = clojure.string.join.cljs$core$IFn$_invoke$arity$2("",cljs.core.butlast(fname_parts));
var ext = cljs.core.last(fname_parts);
return null;
} else {
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_69063){
var state_val_69064 = (state_69063[(1)]);
if((state_val_69064 === (1))){
var inst_69054 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_69055 = [file];
var inst_69056 = (new cljs.core.PersistentVector(null,1,(5),inst_69054,inst_69055,null));
var inst_69057 = vp.meteor.t_write(inst_69056);
var inst_69058 = vp.meteor.call("vp.services.upload-url",inst_69057);
var state_69063__$1 = state_69063;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_69063__$1,(2),inst_69058);
} else {
if((state_val_69064 === (2))){
var inst_69060 = (state_69063[(2)]);
var inst_69061 = vp.meteor.t_read(inst_69060);
var state_69063__$1 = state_69063;
return cljs.core.async.impl.ioc_helpers.return_chan(state_69063__$1,inst_69061);
} else {
return null;
}
}
});
return (function() {
var vp$services$upload_url_$_state_machine__41292__auto__ = null;
var vp$services$upload_url_$_state_machine__41292__auto____0 = (function (){
var statearr_69068 = [null,null,null,null,null,null,null];
(statearr_69068[(0)] = vp$services$upload_url_$_state_machine__41292__auto__);

(statearr_69068[(1)] = (1));

return statearr_69068;
});
var vp$services$upload_url_$_state_machine__41292__auto____1 = (function (state_69063){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_69063);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e69070){var ex__41295__auto__ = e69070;
var statearr_69071_69297 = state_69063;
(statearr_69071_69297[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_69063[(4)]))){
var statearr_69072_69298 = state_69063;
(statearr_69072_69298[(1)] = cljs.core.first((state_69063[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__69300 = state_69063;
state_69063 = G__69300;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$services$upload_url_$_state_machine__41292__auto__ = function(state_69063){
switch(arguments.length){
case 0:
return vp$services$upload_url_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$services$upload_url_$_state_machine__41292__auto____1.call(this,state_69063);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$services$upload_url_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$services$upload_url_$_state_machine__41292__auto____0;
vp$services$upload_url_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$services$upload_url_$_state_machine__41292__auto____1;
return vp$services$upload_url_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_69074 = f__41438__auto__();
(statearr_69074[(6)] = c__41437__auto__);

return statearr_69074;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
}
});
goog.exportSymbol('vp.services.upload_url', vp.services.upload_url);

if(cljs.core.truth_(Meteor.isServer)){
Meteor["methods"](cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 1, ["vp.services.upload-url",(function (args__42089__auto__){
return vp.meteor.to_promise((function (){var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_69089){
var state_val_69090 = (state_69089[(1)]);
if((state_val_69090 === (1))){
var inst_69083 = vp.meteor.t_read(args__42089__auto__);
var inst_69084 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(vp.services.upload_url,inst_69083);
var state_69089__$1 = state_69089;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_69089__$1,(2),inst_69084);
} else {
if((state_val_69090 === (2))){
var inst_69086 = (state_69089[(2)]);
var inst_69087 = vp.meteor.t_write(inst_69086);
var state_69089__$1 = state_69089;
return cljs.core.async.impl.ioc_helpers.return_chan(state_69089__$1,inst_69087);
} else {
return null;
}
}
});
return (function() {
var vp$services$state_machine__41292__auto__ = null;
var vp$services$state_machine__41292__auto____0 = (function (){
var statearr_69094 = [null,null,null,null,null,null,null];
(statearr_69094[(0)] = vp$services$state_machine__41292__auto__);

(statearr_69094[(1)] = (1));

return statearr_69094;
});
var vp$services$state_machine__41292__auto____1 = (function (state_69089){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_69089);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e69096){var ex__41295__auto__ = e69096;
var statearr_69097_69310 = state_69089;
(statearr_69097_69310[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_69089[(4)]))){
var statearr_69098_69312 = state_69089;
(statearr_69098_69312[(1)] = cljs.core.first((state_69089[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__69314 = state_69089;
state_69089 = G__69314;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$services$state_machine__41292__auto__ = function(state_69089){
switch(arguments.length){
case 0:
return vp$services$state_machine__41292__auto____0.call(this);
case 1:
return vp$services$state_machine__41292__auto____1.call(this,state_69089);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$services$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$services$state_machine__41292__auto____0;
vp$services$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$services$state_machine__41292__auto____1;
return vp$services$state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_69100 = f__41438__auto__();
(statearr_69100[(6)] = c__41437__auto__);

return statearr_69100;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
})());
})], null)));
} else {
}
Object.defineProperty(module.exports, "ho", { enumerable: false, get: function() { return vp.services.ho; } });
Object.defineProperty(module.exports, "myfunction", { enumerable: true, get: function() { return vp.services.myfunction; } });
Object.defineProperty(module.exports, "create_company_wallet", { enumerable: true, get: function() { return vp.services.create_company_wallet; } });
Object.defineProperty(module.exports, "upload_url", { enumerable: true, get: function() { return vp.services.upload_url; } });
//# sourceMappingURL=vp.services.js.map

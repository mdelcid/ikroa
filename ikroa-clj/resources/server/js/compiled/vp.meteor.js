var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./cognitect.transit.js");
require("./re_frame.core.js");
require("./cljs.core.async.js");
require("./shadow.js.shim.module$meteor$mongo.js");
require("./shadow.js.shim.module$meteor$tracker.js");
require("./shadow.js.shim.module$meteor$meteor.js");
require("./vp.collections.js");
require("./re_chain.core.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var camel_snake_kebab=$CLJS.camel_snake_kebab || ($CLJS.camel_snake_kebab = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("vp.meteor.js");

goog.provide('vp.meteor');
vp.meteor.writer = cognitect.transit.writer.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"json","json",1279968570));
vp.meteor.reader = cognitect.transit.reader.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"json","json",1279968570));
vp.meteor.t_read = (function vp$meteor$t_read(v){
return cognitect.transit.read(vp.meteor.reader,v);
});
vp.meteor.t_write = (function vp$meteor$t_write(v){
return cognitect.transit.write(vp.meteor.writer,v);
});
vp.meteor.call = (function vp$meteor$call(method_name,args){
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
shadow.js.shim.module$meteor$meteor.Meteor.call(cljs.core.name(method_name),args,(function (err,response){
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_54662){
var state_val_54663 = (state_54662[(1)]);
if((state_val_54663 === (1))){
var state_54662__$1 = state_54662;
if(cljs.core.truth_(err)){
var statearr_54665_54866 = state_54662__$1;
(statearr_54665_54866[(1)] = (2));

} else {
var statearr_54667_54867 = state_54662__$1;
(statearr_54667_54867[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54663 === (2))){
var inst_54655 = console.error(err);
var state_54662__$1 = state_54662;
var statearr_54669_54869 = state_54662__$1;
(statearr_54669_54869[(2)] = inst_54655);

(statearr_54669_54869[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54663 === (3))){
var inst_54657 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,response);
var state_54662__$1 = state_54662;
var statearr_54671_54871 = state_54662__$1;
(statearr_54671_54871[(2)] = inst_54657);

(statearr_54671_54871[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54663 === (4))){
var inst_54659 = (state_54662[(2)]);
var inst_54660 = cljs.core.async.close_BANG_(chan);
var state_54662__$1 = (function (){var statearr_54673 = state_54662;
(statearr_54673[(7)] = inst_54659);

return statearr_54673;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_54662__$1,inst_54660);
} else {
return null;
}
}
}
}
});
return (function() {
var vp$meteor$call_$_state_machine__41292__auto__ = null;
var vp$meteor$call_$_state_machine__41292__auto____0 = (function (){
var statearr_54675 = [null,null,null,null,null,null,null,null];
(statearr_54675[(0)] = vp$meteor$call_$_state_machine__41292__auto__);

(statearr_54675[(1)] = (1));

return statearr_54675;
});
var vp$meteor$call_$_state_machine__41292__auto____1 = (function (state_54662){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_54662);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e54677){var ex__41295__auto__ = e54677;
var statearr_54679_54874 = state_54662;
(statearr_54679_54874[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_54662[(4)]))){
var statearr_54681_54875 = state_54662;
(statearr_54681_54875[(1)] = cljs.core.first((state_54662[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__54877 = state_54662;
state_54662 = G__54877;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$meteor$call_$_state_machine__41292__auto__ = function(state_54662){
switch(arguments.length){
case 0:
return vp$meteor$call_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$meteor$call_$_state_machine__41292__auto____1.call(this,state_54662);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$meteor$call_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$meteor$call_$_state_machine__41292__auto____0;
vp$meteor$call_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$meteor$call_$_state_machine__41292__auto____1;
return vp$meteor$call_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_54683 = f__41438__auto__();
(statearr_54683[(6)] = c__41437__auto__);

return statearr_54683;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
}));

return chan;
});
vp.meteor.to_promise = (function vp$meteor$to_promise(c){
return (new Promise((function (res,rej){
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_54716){
var state_val_54717 = (state_54716[(1)]);
if((state_val_54717 === (1))){
var state_54716__$1 = state_54716;
var statearr_54720_54879 = state_54716__$1;
(statearr_54720_54879[(2)] = null);

(statearr_54720_54879[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54717 === (2))){
var _ = (function (){var statearr_54722 = state_54716;
(statearr_54722[(4)] = cljs.core.cons((5),(state_54716[(4)])));

return statearr_54722;
})();
var state_54716__$1 = state_54716;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_54716__$1,(6),c);
} else {
if((state_val_54717 === (3))){
var inst_54706 = (state_54716[(2)]);
var state_54716__$1 = state_54716;
return cljs.core.async.impl.ioc_helpers.return_chan(state_54716__$1,inst_54706);
} else {
if((state_val_54717 === (4))){
var inst_54684 = (state_54716[(2)]);
var inst_54686 = cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_54684);
var inst_54687 = (rej.cljs$core$IFn$_invoke$arity$1 ? rej.cljs$core$IFn$_invoke$arity$1(inst_54686) : rej.call(null,inst_54686));
var state_54716__$1 = state_54716;
var statearr_54726_54881 = state_54716__$1;
(statearr_54726_54881[(2)] = inst_54687);

(statearr_54726_54881[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54717 === (5))){
var _ = (function (){var statearr_54728 = state_54716;
(statearr_54728[(4)] = cljs.core.rest((state_54716[(4)])));

return statearr_54728;
})();
var state_54716__$1 = state_54716;
var ex54724 = (state_54716__$1[(2)]);
var statearr_54730_54884 = state_54716__$1;
(statearr_54730_54884[(5)] = ex54724);


var statearr_54732_54885 = state_54716__$1;
(statearr_54732_54885[(1)] = (4));

(statearr_54732_54885[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_54717 === (6))){
var inst_54701 = (state_54716[(2)]);
var inst_54702 = cljs.core.clj__GT_js(inst_54701);
var inst_54703 = (res.cljs$core$IFn$_invoke$arity$1 ? res.cljs$core$IFn$_invoke$arity$1(inst_54702) : res.call(null,inst_54702));
var _ = (function (){var statearr_54734 = state_54716;
(statearr_54734[(4)] = cljs.core.rest((state_54716[(4)])));

return statearr_54734;
})();
var state_54716__$1 = state_54716;
var statearr_54736_54887 = state_54716__$1;
(statearr_54736_54887[(2)] = inst_54703);

(statearr_54736_54887[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var vp$meteor$to_promise_$_state_machine__41292__auto__ = null;
var vp$meteor$to_promise_$_state_machine__41292__auto____0 = (function (){
var statearr_54738 = [null,null,null,null,null,null,null];
(statearr_54738[(0)] = vp$meteor$to_promise_$_state_machine__41292__auto__);

(statearr_54738[(1)] = (1));

return statearr_54738;
});
var vp$meteor$to_promise_$_state_machine__41292__auto____1 = (function (state_54716){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_54716);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e54740){var ex__41295__auto__ = e54740;
var statearr_54741_54890 = state_54716;
(statearr_54741_54890[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_54716[(4)]))){
var statearr_54744_54891 = state_54716;
(statearr_54744_54891[(1)] = cljs.core.first((state_54716[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__54893 = state_54716;
state_54716 = G__54893;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$meteor$to_promise_$_state_machine__41292__auto__ = function(state_54716){
switch(arguments.length){
case 0:
return vp$meteor$to_promise_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$meteor$to_promise_$_state_machine__41292__auto____1.call(this,state_54716);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$meteor$to_promise_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$meteor$to_promise_$_state_machine__41292__auto____0;
vp$meteor$to_promise_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$meteor$to_promise_$_state_machine__41292__auto____1;
return vp$meteor$to_promise_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_54746 = f__41438__auto__();
(statearr_54746[(6)] = c__41437__auto__);

return statearr_54746;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
})));
});
vp.meteor.bind_env = Meteor.bindEnvironment((function (chan,f){
var res = (f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_54751){
var state_val_54752 = (state_54751[(1)]);
if((state_val_54752 === (1))){
var inst_54748 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,res);
var inst_54749 = cljs.core.async.close_BANG_(chan);
var state_54751__$1 = (function (){var statearr_54758 = state_54751;
(statearr_54758[(7)] = inst_54748);

return statearr_54758;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_54751__$1,inst_54749);
} else {
return null;
}
});
return (function() {
var vp$meteor$state_machine__41292__auto__ = null;
var vp$meteor$state_machine__41292__auto____0 = (function (){
var statearr_54760 = [null,null,null,null,null,null,null,null];
(statearr_54760[(0)] = vp$meteor$state_machine__41292__auto__);

(statearr_54760[(1)] = (1));

return statearr_54760;
});
var vp$meteor$state_machine__41292__auto____1 = (function (state_54751){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_54751);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e54762){var ex__41295__auto__ = e54762;
var statearr_54763_54894 = state_54751;
(statearr_54763_54894[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_54751[(4)]))){
var statearr_54766_54896 = state_54751;
(statearr_54766_54896[(1)] = cljs.core.first((state_54751[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__54898 = state_54751;
state_54751 = G__54898;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$meteor$state_machine__41292__auto__ = function(state_54751){
switch(arguments.length){
case 0:
return vp$meteor$state_machine__41292__auto____0.call(this);
case 1:
return vp$meteor$state_machine__41292__auto____1.call(this,state_54751);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$meteor$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$meteor$state_machine__41292__auto____0;
vp$meteor$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$meteor$state_machine__41292__auto____1;
return vp$meteor$state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_54768 = f__41438__auto__();
(statearr_54768[(6)] = c__41437__auto__);

return statearr_54768;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
}));
vp.meteor.asset_url = (function vp$meteor$asset_url(asset_id){
return null;
});
vp.meteor.get_coll = (function vp$meteor$get_coll(coll){
if((coll instanceof cljs.core.Keyword)){
return coll.cljs$core$IFn$_invoke$arity$1(vp.collections.collections);
} else {
return coll;
}
});
vp.meteor.convert_query = (function vp$meteor$convert_query(query){
if(cljs.core.map_QMARK_(query)){
return cljs.core.clj__GT_js(query);
} else {
return ({"_id": query});
}
});
vp.meteor.handle_cb = (function vp$meteor$handle_cb(chan,err,res){
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_54777){
var state_val_54778 = (state_54777[(1)]);
if((state_val_54778 === (1))){
var inst_54770 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_54771 = [err,res];
var inst_54772 = (new cljs.core.PersistentVector(null,2,(5),inst_54770,inst_54771,null));
var inst_54773 = cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(inst_54772,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",1310784252),true], 0));
var inst_54774 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,inst_54773);
var inst_54775 = cljs.core.async.close_BANG_(chan);
var state_54777__$1 = (function (){var statearr_54788 = state_54777;
(statearr_54788[(7)] = inst_54774);

return statearr_54788;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_54777__$1,inst_54775);
} else {
return null;
}
});
return (function() {
var vp$meteor$handle_cb_$_state_machine__41292__auto__ = null;
var vp$meteor$handle_cb_$_state_machine__41292__auto____0 = (function (){
var statearr_54790 = [null,null,null,null,null,null,null,null];
(statearr_54790[(0)] = vp$meteor$handle_cb_$_state_machine__41292__auto__);

(statearr_54790[(1)] = (1));

return statearr_54790;
});
var vp$meteor$handle_cb_$_state_machine__41292__auto____1 = (function (state_54777){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_54777);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e54792){var ex__41295__auto__ = e54792;
var statearr_54793_54902 = state_54777;
(statearr_54793_54902[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_54777[(4)]))){
var statearr_54795_54903 = state_54777;
(statearr_54795_54903[(1)] = cljs.core.first((state_54777[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__54905 = state_54777;
state_54777 = G__54905;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$meteor$handle_cb_$_state_machine__41292__auto__ = function(state_54777){
switch(arguments.length){
case 0:
return vp$meteor$handle_cb_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$meteor$handle_cb_$_state_machine__41292__auto____1.call(this,state_54777);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$meteor$handle_cb_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$meteor$handle_cb_$_state_machine__41292__auto____0;
vp$meteor$handle_cb_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$meteor$handle_cb_$_state_machine__41292__auto____1;
return vp$meteor$handle_cb_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_54798 = f__41438__auto__();
(statearr_54798[(6)] = c__41437__auto__);

return statearr_54798;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
});
vp.meteor.cre = (function vp$meteor$cre(coll,doc){
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var do_f = (function (){
return vp.meteor.get_coll(coll).insert(cljs.core.clj__GT_js(doc),cljs.core.partial.cljs$core$IFn$_invoke$arity$2(vp.meteor.handle_cb,chan));
});
if(cljs.core.truth_(shadow.js.shim.module$meteor$meteor.Meteor.isServer)){
(vp.meteor.bind_env.cljs$core$IFn$_invoke$arity$2 ? vp.meteor.bind_env.cljs$core$IFn$_invoke$arity$2(chan,do_f) : vp.meteor.bind_env.call(null,chan,do_f));
} else {
do_f();
}

return chan;
});
vp.meteor.upd = (function vp$meteor$upd(var_args){
var args__4742__auto__ = [];
var len__4736__auto___54909 = arguments.length;
var i__4737__auto___54910 = (0);
while(true){
if((i__4737__auto___54910 < len__4736__auto___54909)){
args__4742__auto__.push((arguments[i__4737__auto___54910]));

var G__54911 = (i__4737__auto___54910 + (1));
i__4737__auto___54910 = G__54911;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((3) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((3)),(0),null)):null);
return vp.meteor.upd.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4743__auto__);
});

(vp.meteor.upd.cljs$core$IFn$_invoke$arity$variadic = (function (coll,query,diff,p__54809){
var map__54812 = p__54809;
var map__54812__$1 = (((((!((map__54812 == null))))?(((((map__54812.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__54812.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__54812):map__54812);
var opts = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__54812__$1,new cljs.core.Keyword(null,"opts","opts",155075701),cljs.core.PersistentArrayMap.EMPTY);
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var do_f = (function (){
vp.meteor.get_coll(coll).update(vp.meteor.convert_query(query),cljs.core.clj__GT_js(diff),cljs.core.clj__GT_js(opts),cljs.core.partial.cljs$core$IFn$_invoke$arity$2(vp.meteor.handle_cb,chan));

return true;
});
if(cljs.core.truth_(shadow.js.shim.module$meteor$meteor.Meteor.isServer)){
(vp.meteor.bind_env.cljs$core$IFn$_invoke$arity$2 ? vp.meteor.bind_env.cljs$core$IFn$_invoke$arity$2(chan,do_f) : vp.meteor.bind_env.call(null,chan,do_f));
} else {
do_f();
}

return chan;
}));

(vp.meteor.upd.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(vp.meteor.upd.cljs$lang$applyTo = (function (seq54801){
var G__54803 = cljs.core.first(seq54801);
var seq54801__$1 = cljs.core.next(seq54801);
var G__54805 = cljs.core.first(seq54801__$1);
var seq54801__$2 = cljs.core.next(seq54801__$1);
var G__54807 = cljs.core.first(seq54801__$2);
var seq54801__$3 = cljs.core.next(seq54801__$2);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__54803,G__54805,G__54807,seq54801__$3);
}));

vp.meteor.del = (function vp$meteor$del(coll,query){
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
vp.meteor.get_coll(coll).remove(cljs.core.clj__GT_js(query),cljs.core.partial.cljs$core$IFn$_invoke$arity$2(vp.meteor.handle_cb,chan));

return chan;
});
vp.meteor.upsert = (function vp$meteor$upsert(coll,doc){
var temp__5733__auto__ = new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(doc);
if(cljs.core.truth_(temp__5733__auto__)){
var id = temp__5733__auto__;
return vp.meteor.upd(coll,({"_id": id}),cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"$set","$set",-1609409324),doc], null)));
} else {
return vp.meteor.cre(coll,cljs.core.clj__GT_js(doc));
}
});
vp.meteor.lst = (function vp$meteor$lst(var_args){
var G__54817 = arguments.length;
switch (G__54817) {
case 1:
return vp.meteor.lst.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return vp.meteor.lst.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(vp.meteor.lst.cljs$core$IFn$_invoke$arity$1 = (function (coll){
return vp.meteor.lst.cljs$core$IFn$_invoke$arity$2(coll,cljs.core.PersistentArrayMap.EMPTY);
}));

(vp.meteor.lst.cljs$core$IFn$_invoke$arity$2 = (function (coll,query){
return cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(vp.meteor.get_coll(coll).find(cljs.core.clj__GT_js(query)).fetch(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",1310784252),true], 0));
}));

(vp.meteor.lst.cljs$lang$maxFixedArity = 2);

vp.meteor.cnt = (function vp$meteor$cnt(var_args){
var G__54820 = arguments.length;
switch (G__54820) {
case 1:
return vp.meteor.cnt.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return vp.meteor.cnt.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(vp.meteor.cnt.cljs$core$IFn$_invoke$arity$1 = (function (coll){
return vp.meteor.cnt.cljs$core$IFn$_invoke$arity$2(coll,cljs.core.PersistentArrayMap.EMPTY);
}));

(vp.meteor.cnt.cljs$core$IFn$_invoke$arity$2 = (function (coll,query){
return vp.meteor.get_coll(coll).find(cljs.core.clj__GT_js(query)).count();
}));

(vp.meteor.cnt.cljs$lang$maxFixedArity = 2);

vp.meteor.one = (function vp$meteor$one(coll,query){
return cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(vp.meteor.get_coll(coll).findOne(((cljs.core.map_QMARK_(query))?cljs.core.clj__GT_js(query):({"_id": query}))),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",1310784252),true], 0));
});
vp.meteor.user = (function vp$meteor$user(){
return cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(shadow.js.shim.module$meteor$meteor.Meteor.user(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",1310784252),true], 0));
});
vp.meteor.user_id = (function vp$meteor$user_id(){
return shadow.js.shim.module$meteor$meteor.Meteor.userId();
});
vp.meteor.full_name = (function vp$meteor$full_name(user_aux){
var user = ((typeof user_aux === 'string')?vp.meteor.one(new cljs.core.Keyword(null,"users","users",-713552705),user_aux):user_aux);
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(user,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"profile","profile",-545963874),new cljs.core.Keyword(null,"first-name","first-name",-1559982131)], null)))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(user,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"profile","profile",-545963874),new cljs.core.Keyword(null,"last-name","last-name",-1695738974)], null)))].join('');
});
vp.meteor.email = (function vp$meteor$email(user){
return new cljs.core.Keyword(null,"address","address",559499426).cljs$core$IFn$_invoke$arity$1(cljs.core.first(new cljs.core.Keyword(null,"emails","emails",306754554).cljs$core$IFn$_invoke$arity$1(user)));
});
vp.meteor.logout = (function vp$meteor$logout(){
return shadow.js.shim.module$meteor$meteor.Meteor.logout();
});
re_frame.core.reg_fx(new cljs.core.Keyword(null,"meteor","meteor",-1552337176),(function (p__54826){
var vec__54827 = p__54826;
var seq__54828 = cljs.core.seq(vec__54827);
var first__54829 = cljs.core.first(seq__54828);
var seq__54828__$1 = cljs.core.next(seq__54828);
var op = first__54829;
var all_args = seq__54828__$1;
var map__54831 = ((cljs.core.vector_QMARK_(cljs.core.last(all_args)))?new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"cb-key","cb-key",368881360),cljs.core.last(all_args),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.butlast(all_args)], null):new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"cb-key","cb-key",368881360),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"meteor-result","meteor-result",1864644918)], null),new cljs.core.Keyword(null,"args","args",1315556576),all_args], null));
var map__54831__$1 = (((((!((map__54831 == null))))?(((((map__54831.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__54831.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__54831):map__54831);
var cb_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__54831__$1,new cljs.core.Keyword(null,"cb-key","cb-key",368881360));
var args = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__54831__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var f = (function (){var G__54835 = op;
var G__54835__$1 = (((G__54835 instanceof cljs.core.Keyword))?G__54835.fqn:null);
switch (G__54835__$1) {
case "cre":
return vp.meteor.cre;

break;
case "upd":
return vp.meteor.upd;

break;
case "upsert":
return vp.meteor.upsert;

break;
case "del":
return vp.meteor.del;

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__54835__$1)].join('')));

}
})();
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_54850){
var state_val_54851 = (state_54850[(1)]);
if((state_val_54851 === (1))){
var inst_54844 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,args);
var state_54850__$1 = state_54850;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_54850__$1,(2),inst_54844);
} else {
if((state_val_54851 === (2))){
var inst_54846 = (state_54850[(2)]);
var inst_54847 = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cb_key,inst_54846);
var inst_54848 = re_frame.core.dispatch(inst_54847);
var state_54850__$1 = state_54850;
return cljs.core.async.impl.ioc_helpers.return_chan(state_54850__$1,inst_54848);
} else {
return null;
}
}
});
return (function() {
var vp$meteor$state_machine__41292__auto__ = null;
var vp$meteor$state_machine__41292__auto____0 = (function (){
var statearr_54853 = [null,null,null,null,null,null,null];
(statearr_54853[(0)] = vp$meteor$state_machine__41292__auto__);

(statearr_54853[(1)] = (1));

return statearr_54853;
});
var vp$meteor$state_machine__41292__auto____1 = (function (state_54850){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_54850);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e54857){var ex__41295__auto__ = e54857;
var statearr_54858_55003 = state_54850;
(statearr_54858_55003[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_54850[(4)]))){
var statearr_54859_55006 = state_54850;
(statearr_54859_55006[(1)] = cljs.core.first((state_54850[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__55009 = state_54850;
state_54850 = G__55009;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$meteor$state_machine__41292__auto__ = function(state_54850){
switch(arguments.length){
case 0:
return vp$meteor$state_machine__41292__auto____0.call(this);
case 1:
return vp$meteor$state_machine__41292__auto____1.call(this,state_54850);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$meteor$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$meteor$state_machine__41292__auto____0;
vp$meteor$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$meteor$state_machine__41292__auto____1;
return vp$meteor$state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_54861 = f__41438__auto__();
(statearr_54861[(6)] = c__41437__auto__);

return statearr_54861;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
}));
vp.meteor.default_links = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"effect-present?","effect-present?",131752804),(function (effects){
return new cljs.core.Keyword(null,"meteor","meteor",-1552337176).cljs$core$IFn$_invoke$arity$1(effects);
}),new cljs.core.Keyword(null,"get-dispatch","get-dispatch",-807865793),(function (effects){
var cb_key = cljs.core.last(new cljs.core.Keyword(null,"meteor","meteor",-1552337176).cljs$core$IFn$_invoke$arity$1(effects));
if(cljs.core.vector_QMARK_(cb_key)){
return cb_key;
} else {
return null;
}
}),new cljs.core.Keyword(null,"set-dispatch","set-dispatch",2115263401),(function (effects,dispatch){
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(effects,new cljs.core.Keyword(null,"meteor","meteor",-1552337176),(function (p1__54863_SHARP_){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(p1__54863_SHARP_,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [dispatch], null));
}));
})], null)], null);
re_chain.core.configure_BANG_(vp.meteor.default_links);
Object.defineProperty(module.exports, "user", { enumerable: false, get: function() { return vp.meteor.user; } });
Object.defineProperty(module.exports, "del", { enumerable: false, get: function() { return vp.meteor.del; } });
Object.defineProperty(module.exports, "one", { enumerable: false, get: function() { return vp.meteor.one; } });
Object.defineProperty(module.exports, "email", { enumerable: false, get: function() { return vp.meteor.email; } });
Object.defineProperty(module.exports, "to_promise", { enumerable: false, get: function() { return vp.meteor.to_promise; } });
Object.defineProperty(module.exports, "lst", { enumerable: false, get: function() { return vp.meteor.lst; } });
Object.defineProperty(module.exports, "call", { enumerable: false, get: function() { return vp.meteor.call; } });
Object.defineProperty(module.exports, "bind_env", { enumerable: false, get: function() { return vp.meteor.bind_env; } });
Object.defineProperty(module.exports, "t_read", { enumerable: false, get: function() { return vp.meteor.t_read; } });
Object.defineProperty(module.exports, "handle_cb", { enumerable: false, get: function() { return vp.meteor.handle_cb; } });
Object.defineProperty(module.exports, "logout", { enumerable: false, get: function() { return vp.meteor.logout; } });
Object.defineProperty(module.exports, "default_links", { enumerable: false, get: function() { return vp.meteor.default_links; } });
Object.defineProperty(module.exports, "user_id", { enumerable: false, get: function() { return vp.meteor.user_id; } });
Object.defineProperty(module.exports, "asset_url", { enumerable: false, get: function() { return vp.meteor.asset_url; } });
Object.defineProperty(module.exports, "cnt", { enumerable: false, get: function() { return vp.meteor.cnt; } });
Object.defineProperty(module.exports, "cre", { enumerable: false, get: function() { return vp.meteor.cre; } });
Object.defineProperty(module.exports, "get_coll", { enumerable: false, get: function() { return vp.meteor.get_coll; } });
Object.defineProperty(module.exports, "convert_query", { enumerable: false, get: function() { return vp.meteor.convert_query; } });
Object.defineProperty(module.exports, "writer", { enumerable: false, get: function() { return vp.meteor.writer; } });
Object.defineProperty(module.exports, "t_write", { enumerable: false, get: function() { return vp.meteor.t_write; } });
Object.defineProperty(module.exports, "reader", { enumerable: false, get: function() { return vp.meteor.reader; } });
Object.defineProperty(module.exports, "full_name", { enumerable: false, get: function() { return vp.meteor.full_name; } });
Object.defineProperty(module.exports, "upsert", { enumerable: false, get: function() { return vp.meteor.upsert; } });
Object.defineProperty(module.exports, "upd", { enumerable: false, get: function() { return vp.meteor.upd; } });
//# sourceMappingURL=vp.meteor.js.map

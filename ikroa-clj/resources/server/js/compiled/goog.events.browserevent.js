var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./goog.debug.debug.js");
require("./goog.events.browserfeature.js");
require("./goog.events.event.js");
require("./goog.events.eventtype.js");
require("./goog.reflect.reflect.js");
require("./goog.useragent.useragent.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("goog.events.browserevent.js");

goog.provide("goog.events.BrowserEvent");
goog.provide("goog.events.BrowserEvent.MouseButton");
goog.provide("goog.events.BrowserEvent.PointerType");
goog.require("goog.debug");
goog.require("goog.events.BrowserFeature");
goog.require("goog.events.Event");
goog.require("goog.events.EventType");
goog.require("goog.reflect");
goog.require("goog.userAgent");
goog.events.USE_LAYER_XY_AS_OFFSET_XY = goog.define("goog.events.USE_LAYER_XY_AS_OFFSET_XY", false);
goog.events.BrowserEvent = function(opt_e, opt_currentTarget) {
  goog.events.BrowserEvent.base(this, "constructor", opt_e ? opt_e.type : "");
  this.target = null;
  this.currentTarget = null;
  this.relatedTarget = null;
  this.offsetX = 0;
  this.offsetY = 0;
  this.clientX = 0;
  this.clientY = 0;
  this.screenX = 0;
  this.screenY = 0;
  this.button = 0;
  this.key = "";
  this.keyCode = 0;
  this.charCode = 0;
  this.ctrlKey = false;
  this.altKey = false;
  this.shiftKey = false;
  this.metaKey = false;
  this.state = null;
  this.platformModifierKey = false;
  this.pointerId = 0;
  this.pointerType = "";
  this.event_ = null;
  if (opt_e) {
    this.init(opt_e, opt_currentTarget);
  }
};
goog.inherits(goog.events.BrowserEvent, goog.events.Event);
goog.events.BrowserEvent.MouseButton = {LEFT:0, MIDDLE:1, RIGHT:2};
goog.events.BrowserEvent.PointerType = {MOUSE:"mouse", PEN:"pen", TOUCH:"touch"};
goog.events.BrowserEvent.IEButtonMap = goog.debug.freeze([1, 4, 2]);
goog.events.BrowserEvent.IE_BUTTON_MAP = goog.events.BrowserEvent.IEButtonMap;
goog.events.BrowserEvent.IE_POINTER_TYPE_MAP = goog.debug.freeze({2:goog.events.BrowserEvent.PointerType.TOUCH, 3:goog.events.BrowserEvent.PointerType.PEN, 4:goog.events.BrowserEvent.PointerType.MOUSE});
goog.events.BrowserEvent.prototype.init = function(e, opt_currentTarget) {
  var type = this.type = e.type;
  var relevantTouch = e.changedTouches && e.changedTouches.length ? e.changedTouches[0] : null;
  this.target = e.target || e.srcElement;
  this.currentTarget = opt_currentTarget;
  var relatedTarget = e.relatedTarget;
  if (relatedTarget) {
    if (goog.userAgent.GECKO) {
      if (!goog.reflect.canAccessProperty(relatedTarget, "nodeName")) {
        relatedTarget = null;
      }
    }
  } else {
    if (type == goog.events.EventType.MOUSEOVER) {
      relatedTarget = e.fromElement;
    } else {
      if (type == goog.events.EventType.MOUSEOUT) {
        relatedTarget = e.toElement;
      }
    }
  }
  this.relatedTarget = relatedTarget;
  if (relevantTouch) {
    this.clientX = relevantTouch.clientX !== undefined ? relevantTouch.clientX : relevantTouch.pageX;
    this.clientY = relevantTouch.clientY !== undefined ? relevantTouch.clientY : relevantTouch.pageY;
    this.screenX = relevantTouch.screenX || 0;
    this.screenY = relevantTouch.screenY || 0;
  } else {
    if (goog.events.USE_LAYER_XY_AS_OFFSET_XY) {
      this.offsetX = e.layerX !== undefined ? e.layerX : e.offsetX;
      this.offsetY = e.layerY !== undefined ? e.layerY : e.offsetY;
    } else {
      this.offsetX = goog.userAgent.WEBKIT || e.offsetX !== undefined ? e.offsetX : e.layerX;
      this.offsetY = goog.userAgent.WEBKIT || e.offsetY !== undefined ? e.offsetY : e.layerY;
    }
    this.clientX = e.clientX !== undefined ? e.clientX : e.pageX;
    this.clientY = e.clientY !== undefined ? e.clientY : e.pageY;
    this.screenX = e.screenX || 0;
    this.screenY = e.screenY || 0;
  }
  this.button = e.button;
  this.keyCode = e.keyCode || 0;
  this.key = e.key || "";
  this.charCode = e.charCode || (type == "keypress" ? e.keyCode : 0);
  this.ctrlKey = e.ctrlKey;
  this.altKey = e.altKey;
  this.shiftKey = e.shiftKey;
  this.metaKey = e.metaKey;
  this.platformModifierKey = goog.userAgent.MAC ? e.metaKey : e.ctrlKey;
  this.pointerId = e.pointerId || 0;
  this.pointerType = goog.events.BrowserEvent.getPointerType_(e);
  this.state = e.state;
  this.event_ = e;
  if (e.defaultPrevented) {
    this.preventDefault();
  }
};
goog.events.BrowserEvent.prototype.isButton = function(button) {
  if (!goog.events.BrowserFeature.HAS_W3C_BUTTON) {
    if (this.type == "click") {
      return button == goog.events.BrowserEvent.MouseButton.LEFT;
    } else {
      return !!(this.event_.button & goog.events.BrowserEvent.IE_BUTTON_MAP[button]);
    }
  } else {
    return this.event_.button == button;
  }
};
goog.events.BrowserEvent.prototype.isMouseActionButton = function() {
  return this.isButton(goog.events.BrowserEvent.MouseButton.LEFT) && !(goog.userAgent.WEBKIT && goog.userAgent.MAC && this.ctrlKey);
};
goog.events.BrowserEvent.prototype.stopPropagation = function() {
  goog.events.BrowserEvent.superClass_.stopPropagation.call(this);
  if (this.event_.stopPropagation) {
    this.event_.stopPropagation();
  } else {
    this.event_.cancelBubble = true;
  }
};
goog.events.BrowserEvent.prototype.preventDefault = function() {
  goog.events.BrowserEvent.superClass_.preventDefault.call(this);
  var be = this.event_;
  if (!be.preventDefault) {
    be.returnValue = false;
    if (goog.events.BrowserFeature.SET_KEY_CODE_TO_PREVENT_DEFAULT) {
      try {
        var VK_F1 = 112;
        var VK_F12 = 123;
        if (be.ctrlKey || be.keyCode >= VK_F1 && be.keyCode <= VK_F12) {
          be.keyCode = -1;
        }
      } catch (ex) {
      }
    }
  } else {
    be.preventDefault();
  }
};
goog.events.BrowserEvent.prototype.getBrowserEvent = function() {
  return this.event_;
};
goog.events.BrowserEvent.getPointerType_ = function(e) {
  if (typeof e.pointerType === "string") {
    return e.pointerType;
  }
  return goog.events.BrowserEvent.IE_POINTER_TYPE_MAP[e.pointerType] || "";
};

module.exports = goog.events.BrowserEvent;

//# sourceMappingURL=goog.events.browserevent.js.map

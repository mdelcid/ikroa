var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./reagent.core.js");
require("./re_frame.core.js");
require("./cljs.core.async.js");
require("./forms.re_frame.js");
require("./forms.validator.js");
require("./cljs.spec.alpha.js");
require("./vp.util.js");
require("./vp.crud.js");
require("./vp.meteor.js");
require("./vp.routes.js");
require("./vp.services.js");
require("./vp.collections.js");
require("./re_chain.core.js");
require("./shadow.js.shim.module$meteor$react_meteor_data.js");
require("./shadow.js.shim.module$reactstrap.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var camel_snake_kebab=$CLJS.camel_snake_kebab || ($CLJS.camel_snake_kebab = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("vp.pages.home_page.js");

goog.provide('vp.pages.home_page');
vp.pages.home_page.company_modal = (function vp$pages$home_page$company_modal(company){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,">",">",-555517146),shadow.js.shim.module$reactstrap.Modal,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"isOpen","isOpen",-973300387),true,new cljs.core.Keyword(null,"toggle","toggle",1291842030),(function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"close-modal","close-modal",-1882189985)], null));
})], null),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,">",">",-555517146),shadow.js.shim.module$reactstrap.ModalBody,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(company)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),new cljs.core.Keyword(null,"description","description",-1428560544).cljs$core$IFn$_invoke$arity$1(company)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),new cljs.core.Keyword(null,"website","website",649297111).cljs$core$IFn$_invoke$arity$1(company),new cljs.core.Keyword(null,"target","target",253001721),"_blank"], null),new cljs.core.Keyword(null,"website","website",649297111).cljs$core$IFn$_invoke$arity$1(company)], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,">",">",-555517146),shadow.js.shim.module$reactstrap.ModalFooter,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-primary","button.btn.btn-primary",510358192),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"button",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"close-modal","close-modal",-1882189985)], null));
})], null),"OK"], null)], null)], null);
});
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("vp.pages.home-page","show-company","vp.pages.home-page/show-company",636149313),(function (p__77350,p__77352){
var map__77355 = p__77350;
var map__77355__$1 = (((((!((map__77355 == null))))?(((((map__77355.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__77355.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__77355):map__77355);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__77355__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__77356 = p__77352;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77356,(0),null);
var company = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77356,(1),null);
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"show-modal","show-modal",-11429385),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.pages.home_page.company_modal,company], null)], null)], null);
}));
vp.pages.home_page.investment_form_path = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("vp.pages.home-page","investment-form","vp.pages.home-page/investment-form",868631783)], null);
vp.pages.home_page.investment_form = (function vp$pages$home_page$investment_form(){
var form_path = vp.pages.home_page.investment_form_path;
var form = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"db","db",993250759)], null),form_path));
var data = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),form_path], null));
var submitting_QMARK_ = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"db","db",993250759),new cljs.core.Keyword(null,"submitting?","submitting?",1281507942),form_path], null));
var on_submit = (function (event){
event.preventDefault();

re_frame.core.dispatch_sync(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),form_path], null));

if(cljs.core.truth_(cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","is-valid?","forms.re-frame/is-valid?",13167751),form_path], null))))){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("vp.pages.home-page","start-application","vp.pages.home-page/start-application",121664804),cljs.core.deref(data)], null));
} else {
return null;
}
});
return (function (){
var plan = new cljs.core.Keyword(null,"plan","plan",1118952668).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(data));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,">",">",-555517146),shadow.js.shim.module$reactstrap.Modal,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"isOpen","isOpen",-973300387),true,new cljs.core.Keyword(null,"toggle","toggle",1291842030),(function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"close-modal","close-modal",-1882189985)], null));
})], null),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"form","form",-1624062471),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-submit","on-submit",1227871159),on_submit], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,">",">",-555517146),shadow.js.shim.module$reactstrap.ModalHeader,"Investment Application Form"], null),new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,">",">",-555517146),shadow.js.shim.module$reactstrap.ModalBody,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"plan","plan",1118952668),new cljs.core.Keyword(null,"name","name",1843675177)], null))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"plan","plan",1118952668),new cljs.core.Keyword(null,"description","description",-1428560544)], null))], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"hr","hr",1377740067)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.d-flex.flex-row.justify-content-center","div.d-flex.flex-row.justify-content-center",1309283963),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.w-50","div.w-50",2092777069),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.crud.render_fields,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"form-path","form-path",-1656304235),form_path,new cljs.core.Keyword(null,"fields","fields",-1932066230),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"investment","investment",-1072757208)], null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"float","float",-1732389368),new cljs.core.Keyword(null,"input-opts","input-opts",1688681135),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"text-align","text-align",1786091845),"right"], null),new cljs.core.Keyword(null,"min","min",444991522),new cljs.core.Keyword(null,"minimum-investment","minimum-investment",-1338714453).cljs$core$IFn$_invoke$arity$1(plan)], null),new cljs.core.Keyword(null,"input-group-prepend","input-group-prepend",-1646594811),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"text","text",-1790561697),"$"], null),new cljs.core.Keyword(null,"label","label",1718410804),"Investment"], null)], null)], null)], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-5.my-2","div.px-5.my-2",-916872275),(((new cljs.core.Keyword(null,"investment","investment",-1072757208).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(data)) > (0)))?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-center","div.text-center",921869624),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.text-muted","div.text-muted",-1618241847),"With an investment of ",vp.util.format_currency(new cljs.core.Keyword(null,"investment","investment",-1072757208).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(data))),", you'd be earning:"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.my-2","div.my-2",-846842446),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"strong.fs-4","strong.fs-4",-1007466906),vp.util.format_currency(((new cljs.core.Keyword(null,"investment","investment",-1072757208).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(data)) * cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"plan","plan",1118952668),new cljs.core.Keyword(null,"interest","interest",655528052)], null))) * 0.01))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"strong","strong",269529000),"/month!"], null)], null)], null):null)], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,">",">",-555517146),shadow.js.shim.module$reactstrap.ModalFooter,(cljs.core.truth_(cljs.core.deref(submitting_QMARK_))?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-primary","button.btn.btn-primary",510358192),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),"button"], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.fas.fa-spinner.fa-spin","i.fas.fa-spinner.fa-spin",1315859993)], null)], null):new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"<>","<>",1280186386),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-link","button.btn.btn-link",-435000034),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"button",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"close-modal","close-modal",-1882189985)], null));
})], null),"Cancel"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-primary","button.btn.btn-primary",510358192),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),"submit"], null),"Start Application"], null)], null))], null)], null)], null);
});
});
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("vp.pages.home-page","show-investment","vp.pages.home-page/show-investment",2074068260),(function (p__77364,p__77365){
var map__77368 = p__77364;
var map__77368__$1 = (((((!((map__77368 == null))))?(((((map__77368.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__77368.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__77368):map__77368);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__77368__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__77369 = p__77365;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77369,(0),null);
var plan = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77369,(1),null);
if((!(cljs.core.empty_QMARK_(new cljs.core.Keyword(null,"profile","profile",-545963874).cljs$core$IFn$_invoke$arity$1(vp.meteor.user()))))){
var validator = forms.validator.validator(vp.crud.to_validator(vp.crud.validations,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"investment","investment",-1072757208),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"not-null","not-null",-1326718535),new cljs.core.Keyword(null,"valid-number","valid-number",-677011101)], null)], null)));
var form = forms.re_frame.constructor$.cljs$core$IFn$_invoke$arity$2(validator,vp.pages.home_page.investment_form_path);
form(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"plan","plan",1118952668),plan,new cljs.core.Keyword(null,"investment","investment",-1072757208),new cljs.core.Keyword(null,"minimum-investment","minimum-investment",-1338714453).cljs$core$IFn$_invoke$arity$1(plan)], null));

return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"show-modal","show-modal",-11429385),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [vp.pages.home_page.investment_form], null)], null)], null);
} else {
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"toastr","toastr",696071399),new cljs.core.Keyword(null,"error","error",-978969032),"Please complet your profile before investing"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"redirect","redirect",-1975673286),vp.routes.profile_page()], null)], null)], null);
}
}));
re_chain.core.reg_chain.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("vp.pages.home-page","start-application","vp.pages.home-page/start-application",121664804),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (p__77378,p__77379){
var map__77382 = p__77378;
var map__77382__$1 = (((((!((map__77382 == null))))?(((((map__77382.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__77382.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__77382):map__77382);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__77382__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__77383 = p__77379;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77383,(0),null);
var data = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77383,(1),null);
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"meteor","meteor",-1552337176),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"cre","cre",-711217392),new cljs.core.Keyword(null,"contracts","contracts",905357673),cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(data,new cljs.core.Keyword(null,"user-id","user-id",-206822291),vp.meteor.user_id(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"status","status",-1997798413),"pending-approval"], 0))], null)], null);
}),(function (p__77392,p__77393){
var map__77396 = p__77392;
var map__77396__$1 = (((((!((map__77396 == null))))?(((((map__77396.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__77396.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__77396):map__77396);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__77396__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__77397 = p__77393;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77397,(0),null);
var data = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77397,(1),null);
var err = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77397,(2),null);
var res = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__77397,(3),null);
if(cljs.core.truth_(err)){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"toastr","toastr",696071399),new cljs.core.Keyword(null,"error","error",-978969032),err.reason], null)], null);
} else {
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"toastr","toastr",696071399),new cljs.core.Keyword(null,"success","success",1890645906),"Contract application successfully started!"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"redirect","redirect",-1975673286),vp.routes.user_contracts_page()], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"close-modal","close-modal",-1882189985)], null)], null)], null);
}
})], 0));
vp.pages.home_page.plan_card = (function vp$pages$home_page$plan_card(plan){
var company = shadow.js.shim.module$meteor$react_meteor_data.useTracker((function (){
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(vp.meteor.one(new cljs.core.Keyword(null,"users","users",-713552705),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"_id","_id",-789960287),new cljs.core.Keyword(null,"user-id","user-id",-206822291).cljs$core$IFn$_invoke$arity$1(plan)], null)),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"profile","profile",-545963874),new cljs.core.Keyword(null,"company","company",-340475075)], null));
}));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.plan-card-container","div.plan-card-container",-1216254610),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.card.plan-card","div.card.plan-card",326114228),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.card-body","div.card-body",1538579065),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h5.card-title","h5.card-title",-2010521752),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.fas.fa-file-invoice-dollar","i.fas.fa-file-invoice-dollar",1330542509)], null)," ",new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(plan)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.card-text","div.card-text",1661912080),new cljs.core.Keyword(null,"tagline","tagline",281987682).cljs$core$IFn$_invoke$arity$1(plan)], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.bottom","div.bottom",56172931),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.card-text.text-success.mb-2","div.card-text.text-success.mb-2",1669362668),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.fs-4","span.fs-4",-1517313285),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"strong","strong",269529000),new cljs.core.Keyword(null,"interest","interest",655528052).cljs$core$IFn$_invoke$arity$1(plan),"%"], null)," Interest"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),(((((new cljs.core.Keyword(null,"minimum-investment","minimum-investment",-1338714453).cljs$core$IFn$_invoke$arity$1(plan) === (0))) || ((new cljs.core.Keyword(null,"minimum-investment","minimum-investment",-1338714453).cljs$core$IFn$_invoke$arity$1(plan) == null))))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"small.text-muted","small.text-muted",1906258664),"No minimum required"], null):new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"small.text-muted","small.text-muted",1906258664),vp.util.format_currency(new cljs.core.Keyword(null,"minimum-investment","minimum-investment",-1338714453).cljs$core$IFn$_invoke$arity$1(plan))," Minimum entry"], null))], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.btn.btn-secondary.subscribe-button.mb-2","button.btn.btn-secondary.subscribe-button.mb-2",-1741124116),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("vp.pages.home-page","show-investment","vp.pages.home-page/show-investment",2074068260),plan], null));
}),new cljs.core.Keyword(null,"type","type",1174270348),"button"], null),"Invest"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"small.text-muted","small.text-muted",1906258664),"Offered by ",new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),"#",new cljs.core.Keyword(null,"on-click","on-click",1632826543),vp.util.prevdef((function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("vp.pages.home-page","show-company","vp.pages.home-page/show-company",636149313),company], null));
}))], null),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(company)], null)], null)], null)], null)], null)], null)], null);
});
vp.pages.home_page.contracts_grid = (function vp$pages$home_page$contracts_grid(){
var plans = shadow.js.shim.module$meteor$react_meteor_data.useTracker((function (){
return vp.meteor.lst.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"plans","plans",75657163),cljs.core.PersistentArrayMap.EMPTY);
}));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.row.row-cols-md-2.row-cols-lg-4","div.row.row-cols-md-2.row-cols-lg-4",1805921889),cljs.core.doall.cljs$core$IFn$_invoke$arity$1((function (){var iter__4529__auto__ = (function vp$pages$home_page$contracts_grid_$_iter__77406(s__77407){
return (new cljs.core.LazySeq(null,(function (){
var s__77407__$1 = s__77407;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__77407__$1);
if(temp__5735__auto__){
var s__77407__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__77407__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__77407__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__77409 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__77408 = (0);
while(true){
if((i__77408 < size__4528__auto__)){
var plan = cljs.core._nth(c__4527__auto__,i__77408);
cljs.core.chunk_append(b__77409,cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"f>","f>",1484564198),vp.pages.home_page.plan_card,plan], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(plan)], null)));

var G__77416 = (i__77408 + (1));
i__77408 = G__77416;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__77409),vp$pages$home_page$contracts_grid_$_iter__77406(cljs.core.chunk_rest(s__77407__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__77409),null);
}
} else {
var plan = cljs.core.first(s__77407__$2);
return cljs.core.cons(cljs.core.with_meta(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"f>","f>",1484564198),vp.pages.home_page.plan_card,plan], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(plan)], null)),vp$pages$home_page$contracts_grid_$_iter__77406(cljs.core.rest(s__77407__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(plans);
})())], null);
});
vp.pages.home_page.home_page = (function vp$pages$home_page$home_page(){
var page_params = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"db","db",993250759),new cljs.core.Keyword(null,"page-params","page-params",1923301283)], null));
var plans = re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"db","db",993250759),new cljs.core.Keyword(null,"plans","plans",75657163)], null));
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.container.pt-4","div.container.pt-4",1823626229),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2","h2",-372662728),"Find your best deal!"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"f>","f>",1484564198),vp.pages.home_page.contracts_grid], null)], null);
});
});
Object.defineProperty(module.exports, "company_modal", { enumerable: false, get: function() { return vp.pages.home_page.company_modal; } });
Object.defineProperty(module.exports, "investment_form_path", { enumerable: false, get: function() { return vp.pages.home_page.investment_form_path; } });
Object.defineProperty(module.exports, "investment_form", { enumerable: false, get: function() { return vp.pages.home_page.investment_form; } });
Object.defineProperty(module.exports, "plan_card", { enumerable: false, get: function() { return vp.pages.home_page.plan_card; } });
Object.defineProperty(module.exports, "contracts_grid", { enumerable: false, get: function() { return vp.pages.home_page.contracts_grid; } });
Object.defineProperty(module.exports, "home_page", { enumerable: false, get: function() { return vp.pages.home_page.home_page; } });
//# sourceMappingURL=vp.pages.home_page.js.map

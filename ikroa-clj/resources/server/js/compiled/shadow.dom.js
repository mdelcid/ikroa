var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./goog.dom.dom.js");
require("./goog.dom.forms.js");
require("./goog.dom.classlist.js");
require("./goog.style.style.js");
require("./goog.style.transition.js");
require("./goog.string.string.js");
require("./clojure.string.js");
require("./cljs.core.async.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("shadow.dom.js");

goog.provide('shadow.dom');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

var shadow$dom$IElement$_to_dom$dyn_50436 = (function (this$){
var x__4428__auto__ = (((this$ == null))?null:this$);
var m__4429__auto__ = (shadow.dom._to_dom[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4429__auto__.call(null,this$));
} else {
var m__4426__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4426__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
});
shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
return shadow$dom$IElement$_to_dom$dyn_50436(this$);
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

var shadow$dom$SVGElement$_to_svg$dyn_50439 = (function (this$){
var x__4428__auto__ = (((this$ == null))?null:this$);
var m__4429__auto__ = (shadow.dom._to_svg[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4429__auto__.call(null,this$));
} else {
var m__4426__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4426__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
});
shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
return shadow$dom$SVGElement$_to_svg$dyn_50439(this$);
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__49486 = coll;
var G__49487 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__49486,G__49487) : shadow.dom.lazy_native_coll_seq.call(null,G__49486,G__49487));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__4126__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return not_found;
}
}));

(shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
}));

(shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
}));

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
}));

(shadow.dom.NativeColl.cljs$lang$type = true);

(shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl");

(shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"shadow.dom/NativeColl");
}));

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__49520 = arguments.length;
switch (G__49520) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
}));

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
}));

(shadow.dom.query_one.cljs$lang$maxFixedArity = 2);

shadow.dom.query = (function shadow$dom$query(var_args){
var G__49531 = arguments.length;
switch (G__49531) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$lang$maxFixedArity = 2);

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__49550 = arguments.length;
switch (G__49550) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
}));

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
}));

(shadow.dom.by_id.cljs$lang$maxFixedArity = 2);

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__49567 = arguments.length;
switch (G__49567) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
(e.cancelBubble = true);

(e.returnValue = false);
}

return e;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4);

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__49587 = arguments.length;
switch (G__49587) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
return goog.dom.contains(document,shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
return goog.dom.contains(shadow.dom.dom_node(parent),shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2);

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
return goog.dom.classlist.add(shadow.dom.dom_node(el),cls);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
return goog.dom.classlist.remove(shadow.dom.dom_node(el),cls);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__49609 = arguments.length;
switch (G__49609) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
return goog.dom.classlist.toggle(shadow.dom.dom_node(el),cls);
}));

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
}));

(shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3);

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__4126__auto__ = (!((typeof document !== 'undefined')));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e49626){if((e49626 instanceof Object)){
var e = e49626;
return console.log("didnt support attachEvent",el,e);
} else {
throw e49626;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__4126__auto__ = (!((typeof document !== 'undefined')));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__49640 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__49641 = null;
var count__49642 = (0);
var i__49643 = (0);
while(true){
if((i__49643 < count__49642)){
var el = chunk__49641.cljs$core$IIndexed$_nth$arity$2(null,i__49643);
var handler_50461__$1 = ((function (seq__49640,chunk__49641,count__49642,i__49643,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__49640,chunk__49641,count__49642,i__49643,el))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_50461__$1);


var G__50463 = seq__49640;
var G__50464 = chunk__49641;
var G__50465 = count__49642;
var G__50466 = (i__49643 + (1));
seq__49640 = G__50463;
chunk__49641 = G__50464;
count__49642 = G__50465;
i__49643 = G__50466;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__49640);
if(temp__5735__auto__){
var seq__49640__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__49640__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__49640__$1);
var G__50469 = cljs.core.chunk_rest(seq__49640__$1);
var G__50470 = c__4556__auto__;
var G__50471 = cljs.core.count(c__4556__auto__);
var G__50472 = (0);
seq__49640 = G__50469;
chunk__49641 = G__50470;
count__49642 = G__50471;
i__49643 = G__50472;
continue;
} else {
var el = cljs.core.first(seq__49640__$1);
var handler_50474__$1 = ((function (seq__49640,chunk__49641,count__49642,i__49643,el,seq__49640__$1,temp__5735__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__49640,chunk__49641,count__49642,i__49643,el,seq__49640__$1,temp__5735__auto__))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_50474__$1);


var G__50477 = cljs.core.next(seq__49640__$1);
var G__50478 = null;
var G__50479 = (0);
var G__50480 = (0);
seq__49640 = G__50477;
chunk__49641 = G__50478;
count__49642 = G__50479;
i__49643 = G__50480;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__49662 = arguments.length;
switch (G__49662) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
}));

(shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
return shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(ev),handler__$1);
}
}));

(shadow.dom.on.cljs$lang$maxFixedArity = 4);

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
return shadow.dom.dom_listen_remove(shadow.dom.dom_node(el),cljs.core.name(ev),handler);
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__49669 = cljs.core.seq(events);
var chunk__49670 = null;
var count__49671 = (0);
var i__49672 = (0);
while(true){
if((i__49672 < count__49671)){
var vec__49683 = chunk__49670.cljs$core$IIndexed$_nth$arity$2(null,i__49672);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49683,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49683,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__50488 = seq__49669;
var G__50489 = chunk__49670;
var G__50490 = count__49671;
var G__50491 = (i__49672 + (1));
seq__49669 = G__50488;
chunk__49670 = G__50489;
count__49671 = G__50490;
i__49672 = G__50491;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__49669);
if(temp__5735__auto__){
var seq__49669__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__49669__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__49669__$1);
var G__50493 = cljs.core.chunk_rest(seq__49669__$1);
var G__50494 = c__4556__auto__;
var G__50495 = cljs.core.count(c__4556__auto__);
var G__50496 = (0);
seq__49669 = G__50493;
chunk__49670 = G__50494;
count__49671 = G__50495;
i__49672 = G__50496;
continue;
} else {
var vec__49692 = cljs.core.first(seq__49669__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49692,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49692,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__50500 = cljs.core.next(seq__49669__$1);
var G__50501 = null;
var G__50502 = (0);
var G__50503 = (0);
seq__49669 = G__50500;
chunk__49670 = G__50501;
count__49671 = G__50502;
i__49672 = G__50503;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__49699 = cljs.core.seq(styles);
var chunk__49701 = null;
var count__49702 = (0);
var i__49703 = (0);
while(true){
if((i__49703 < count__49702)){
var vec__49719 = chunk__49701.cljs$core$IIndexed$_nth$arity$2(null,i__49703);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49719,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49719,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__50505 = seq__49699;
var G__50506 = chunk__49701;
var G__50507 = count__49702;
var G__50508 = (i__49703 + (1));
seq__49699 = G__50505;
chunk__49701 = G__50506;
count__49702 = G__50507;
i__49703 = G__50508;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__49699);
if(temp__5735__auto__){
var seq__49699__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__49699__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__49699__$1);
var G__50510 = cljs.core.chunk_rest(seq__49699__$1);
var G__50511 = c__4556__auto__;
var G__50512 = cljs.core.count(c__4556__auto__);
var G__50513 = (0);
seq__49699 = G__50510;
chunk__49701 = G__50511;
count__49702 = G__50512;
i__49703 = G__50513;
continue;
} else {
var vec__49729 = cljs.core.first(seq__49699__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49729,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49729,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__50517 = cljs.core.next(seq__49699__$1);
var G__50518 = null;
var G__50519 = (0);
var G__50520 = (0);
seq__49699 = G__50517;
chunk__49701 = G__50518;
count__49702 = G__50519;
i__49703 = G__50520;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__49738_50521 = key;
var G__49738_50522__$1 = (((G__49738_50521 instanceof cljs.core.Keyword))?G__49738_50521.fqn:null);
switch (G__49738_50522__$1) {
case "id":
(el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "class":
(el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "for":
(el.htmlFor = value);

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_50528 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__4126__auto__ = goog.string.startsWith(ks_50528,"data-");
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return goog.string.startsWith(ks_50528,"aria-");
}
})())){
el.setAttribute(ks_50528,value);
} else {
(el[ks_50528] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
return goog.dom.classlist.contains(shadow.dom.dom_node(el),cls);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__49758){
var map__49760 = p__49758;
var map__49760__$1 = (((((!((map__49760 == null))))?(((((map__49760.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__49760.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__49760):map__49760);
var props = map__49760__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__49760__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__49766 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49766,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49766,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49766,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__49769 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__49769,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__49769;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__49776 = arguments.length;
switch (G__49776) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$lang$maxFixedArity = 2);

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__49787){
var vec__49790 = p__49787;
var seq__49791 = cljs.core.seq(vec__49790);
var first__49792 = cljs.core.first(seq__49791);
var seq__49791__$1 = cljs.core.next(seq__49791);
var nn = first__49792;
var first__49792__$1 = cljs.core.first(seq__49791__$1);
var seq__49791__$2 = cljs.core.next(seq__49791__$1);
var np = first__49792__$1;
var nc = seq__49791__$2;
var node = vec__49790;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__49800 = nn;
var G__49801 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__49800,G__49801) : create_fn.call(null,G__49800,G__49801));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__49802 = nn;
var G__49803 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__49802,G__49803) : create_fn.call(null,G__49802,G__49803));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__49810 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49810,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__49810,(1),null);
var seq__49815_50559 = cljs.core.seq(node_children);
var chunk__49816_50560 = null;
var count__49817_50561 = (0);
var i__49818_50562 = (0);
while(true){
if((i__49818_50562 < count__49817_50561)){
var child_struct_50564 = chunk__49816_50560.cljs$core$IIndexed$_nth$arity$2(null,i__49818_50562);
var children_50566 = shadow.dom.dom_node(child_struct_50564);
if(cljs.core.seq_QMARK_(children_50566)){
var seq__49863_50569 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_50566));
var chunk__49865_50570 = null;
var count__49866_50571 = (0);
var i__49867_50572 = (0);
while(true){
if((i__49867_50572 < count__49866_50571)){
var child_50575 = chunk__49865_50570.cljs$core$IIndexed$_nth$arity$2(null,i__49867_50572);
if(cljs.core.truth_(child_50575)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_50575);


var G__50577 = seq__49863_50569;
var G__50578 = chunk__49865_50570;
var G__50579 = count__49866_50571;
var G__50580 = (i__49867_50572 + (1));
seq__49863_50569 = G__50577;
chunk__49865_50570 = G__50578;
count__49866_50571 = G__50579;
i__49867_50572 = G__50580;
continue;
} else {
var G__50582 = seq__49863_50569;
var G__50583 = chunk__49865_50570;
var G__50584 = count__49866_50571;
var G__50585 = (i__49867_50572 + (1));
seq__49863_50569 = G__50582;
chunk__49865_50570 = G__50583;
count__49866_50571 = G__50584;
i__49867_50572 = G__50585;
continue;
}
} else {
var temp__5735__auto___50586 = cljs.core.seq(seq__49863_50569);
if(temp__5735__auto___50586){
var seq__49863_50587__$1 = temp__5735__auto___50586;
if(cljs.core.chunked_seq_QMARK_(seq__49863_50587__$1)){
var c__4556__auto___50588 = cljs.core.chunk_first(seq__49863_50587__$1);
var G__50590 = cljs.core.chunk_rest(seq__49863_50587__$1);
var G__50591 = c__4556__auto___50588;
var G__50592 = cljs.core.count(c__4556__auto___50588);
var G__50593 = (0);
seq__49863_50569 = G__50590;
chunk__49865_50570 = G__50591;
count__49866_50571 = G__50592;
i__49867_50572 = G__50593;
continue;
} else {
var child_50595 = cljs.core.first(seq__49863_50587__$1);
if(cljs.core.truth_(child_50595)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_50595);


var G__50596 = cljs.core.next(seq__49863_50587__$1);
var G__50597 = null;
var G__50598 = (0);
var G__50599 = (0);
seq__49863_50569 = G__50596;
chunk__49865_50570 = G__50597;
count__49866_50571 = G__50598;
i__49867_50572 = G__50599;
continue;
} else {
var G__50600 = cljs.core.next(seq__49863_50587__$1);
var G__50601 = null;
var G__50602 = (0);
var G__50603 = (0);
seq__49863_50569 = G__50600;
chunk__49865_50570 = G__50601;
count__49866_50571 = G__50602;
i__49867_50572 = G__50603;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_50566);
}


var G__50604 = seq__49815_50559;
var G__50605 = chunk__49816_50560;
var G__50606 = count__49817_50561;
var G__50607 = (i__49818_50562 + (1));
seq__49815_50559 = G__50604;
chunk__49816_50560 = G__50605;
count__49817_50561 = G__50606;
i__49818_50562 = G__50607;
continue;
} else {
var temp__5735__auto___50608 = cljs.core.seq(seq__49815_50559);
if(temp__5735__auto___50608){
var seq__49815_50610__$1 = temp__5735__auto___50608;
if(cljs.core.chunked_seq_QMARK_(seq__49815_50610__$1)){
var c__4556__auto___50611 = cljs.core.chunk_first(seq__49815_50610__$1);
var G__50613 = cljs.core.chunk_rest(seq__49815_50610__$1);
var G__50614 = c__4556__auto___50611;
var G__50615 = cljs.core.count(c__4556__auto___50611);
var G__50616 = (0);
seq__49815_50559 = G__50613;
chunk__49816_50560 = G__50614;
count__49817_50561 = G__50615;
i__49818_50562 = G__50616;
continue;
} else {
var child_struct_50617 = cljs.core.first(seq__49815_50610__$1);
var children_50619 = shadow.dom.dom_node(child_struct_50617);
if(cljs.core.seq_QMARK_(children_50619)){
var seq__49890_50620 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_50619));
var chunk__49892_50621 = null;
var count__49893_50622 = (0);
var i__49894_50623 = (0);
while(true){
if((i__49894_50623 < count__49893_50622)){
var child_50624 = chunk__49892_50621.cljs$core$IIndexed$_nth$arity$2(null,i__49894_50623);
if(cljs.core.truth_(child_50624)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_50624);


var G__50625 = seq__49890_50620;
var G__50626 = chunk__49892_50621;
var G__50627 = count__49893_50622;
var G__50628 = (i__49894_50623 + (1));
seq__49890_50620 = G__50625;
chunk__49892_50621 = G__50626;
count__49893_50622 = G__50627;
i__49894_50623 = G__50628;
continue;
} else {
var G__50630 = seq__49890_50620;
var G__50631 = chunk__49892_50621;
var G__50632 = count__49893_50622;
var G__50633 = (i__49894_50623 + (1));
seq__49890_50620 = G__50630;
chunk__49892_50621 = G__50631;
count__49893_50622 = G__50632;
i__49894_50623 = G__50633;
continue;
}
} else {
var temp__5735__auto___50634__$1 = cljs.core.seq(seq__49890_50620);
if(temp__5735__auto___50634__$1){
var seq__49890_50635__$1 = temp__5735__auto___50634__$1;
if(cljs.core.chunked_seq_QMARK_(seq__49890_50635__$1)){
var c__4556__auto___50636 = cljs.core.chunk_first(seq__49890_50635__$1);
var G__50637 = cljs.core.chunk_rest(seq__49890_50635__$1);
var G__50638 = c__4556__auto___50636;
var G__50639 = cljs.core.count(c__4556__auto___50636);
var G__50640 = (0);
seq__49890_50620 = G__50637;
chunk__49892_50621 = G__50638;
count__49893_50622 = G__50639;
i__49894_50623 = G__50640;
continue;
} else {
var child_50641 = cljs.core.first(seq__49890_50635__$1);
if(cljs.core.truth_(child_50641)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_50641);


var G__50642 = cljs.core.next(seq__49890_50635__$1);
var G__50643 = null;
var G__50644 = (0);
var G__50645 = (0);
seq__49890_50620 = G__50642;
chunk__49892_50621 = G__50643;
count__49893_50622 = G__50644;
i__49894_50623 = G__50645;
continue;
} else {
var G__50649 = cljs.core.next(seq__49890_50635__$1);
var G__50650 = null;
var G__50651 = (0);
var G__50652 = (0);
seq__49890_50620 = G__50649;
chunk__49892_50621 = G__50650;
count__49893_50622 = G__50651;
i__49894_50623 = G__50652;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_50619);
}


var G__50654 = cljs.core.next(seq__49815_50610__$1);
var G__50655 = null;
var G__50656 = (0);
var G__50657 = (0);
seq__49815_50559 = G__50654;
chunk__49816_50560 = G__50655;
count__49817_50561 = G__50656;
i__49818_50562 = G__50657;
continue;
}
} else {
}
}
break;
}

return node;
});
(cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
}));

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
}));
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
(HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
(DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
return goog.dom.removeChildren(shadow.dom.dom_node(node));
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__49914 = cljs.core.seq(node);
var chunk__49915 = null;
var count__49916 = (0);
var i__49917 = (0);
while(true){
if((i__49917 < count__49916)){
var n = chunk__49915.cljs$core$IIndexed$_nth$arity$2(null,i__49917);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__50666 = seq__49914;
var G__50667 = chunk__49915;
var G__50668 = count__49916;
var G__50669 = (i__49917 + (1));
seq__49914 = G__50666;
chunk__49915 = G__50667;
count__49916 = G__50668;
i__49917 = G__50669;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__49914);
if(temp__5735__auto__){
var seq__49914__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__49914__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__49914__$1);
var G__50672 = cljs.core.chunk_rest(seq__49914__$1);
var G__50673 = c__4556__auto__;
var G__50674 = cljs.core.count(c__4556__auto__);
var G__50675 = (0);
seq__49914 = G__50672;
chunk__49915 = G__50673;
count__49916 = G__50674;
i__49917 = G__50675;
continue;
} else {
var n = cljs.core.first(seq__49914__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__50678 = cljs.core.next(seq__49914__$1);
var G__50679 = null;
var G__50680 = (0);
var G__50681 = (0);
seq__49914 = G__50678;
chunk__49915 = G__50679;
count__49916 = G__50680;
i__49917 = G__50681;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
return goog.dom.replaceNode(shadow.dom.dom_node(new$),shadow.dom.dom_node(old));
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__49936 = arguments.length;
switch (G__49936) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return (shadow.dom.dom_node(el).innerText = new_text);
}));

(shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
}));

(shadow.dom.text.cljs$lang$maxFixedArity = 2);

shadow.dom.check = (function shadow$dom$check(var_args){
var G__49949 = arguments.length;
switch (G__49949) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
}));

(shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return (shadow.dom.dom_node(el).checked = checked);
}));

(shadow.dom.check.cljs$lang$maxFixedArity = 2);

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__49962 = arguments.length;
switch (G__49962) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
}));

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__4126__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return default$;
}
}));

(shadow.dom.attr.cljs$lang$maxFixedArity = 3);

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return (shadow.dom.dom_node(node).innerHTML = text);
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__4742__auto__ = [];
var len__4736__auto___50702 = arguments.length;
var i__4737__auto___50703 = (0);
while(true){
if((i__4737__auto___50703 < len__4736__auto___50702)){
args__4742__auto__.push((arguments[i__4737__auto___50703]));

var G__50704 = (i__4737__auto___50703 + (1));
i__4737__auto___50703 = G__50704;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__49981_50709 = cljs.core.seq(nodes);
var chunk__49982_50710 = null;
var count__49983_50711 = (0);
var i__49984_50712 = (0);
while(true){
if((i__49984_50712 < count__49983_50711)){
var node_50714 = chunk__49982_50710.cljs$core$IIndexed$_nth$arity$2(null,i__49984_50712);
fragment.appendChild(shadow.dom._to_dom(node_50714));


var G__50715 = seq__49981_50709;
var G__50716 = chunk__49982_50710;
var G__50717 = count__49983_50711;
var G__50718 = (i__49984_50712 + (1));
seq__49981_50709 = G__50715;
chunk__49982_50710 = G__50716;
count__49983_50711 = G__50717;
i__49984_50712 = G__50718;
continue;
} else {
var temp__5735__auto___50719 = cljs.core.seq(seq__49981_50709);
if(temp__5735__auto___50719){
var seq__49981_50720__$1 = temp__5735__auto___50719;
if(cljs.core.chunked_seq_QMARK_(seq__49981_50720__$1)){
var c__4556__auto___50721 = cljs.core.chunk_first(seq__49981_50720__$1);
var G__50723 = cljs.core.chunk_rest(seq__49981_50720__$1);
var G__50724 = c__4556__auto___50721;
var G__50725 = cljs.core.count(c__4556__auto___50721);
var G__50726 = (0);
seq__49981_50709 = G__50723;
chunk__49982_50710 = G__50724;
count__49983_50711 = G__50725;
i__49984_50712 = G__50726;
continue;
} else {
var node_50727 = cljs.core.first(seq__49981_50720__$1);
fragment.appendChild(shadow.dom._to_dom(node_50727));


var G__50728 = cljs.core.next(seq__49981_50720__$1);
var G__50729 = null;
var G__50730 = (0);
var G__50731 = (0);
seq__49981_50709 = G__50728;
chunk__49982_50710 = G__50729;
count__49983_50711 = G__50730;
i__49984_50712 = G__50731;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
}));

(shadow.dom.fragment.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(shadow.dom.fragment.cljs$lang$applyTo = (function (seq49978){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq49978));
}));

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__49998_50733 = cljs.core.seq(scripts);
var chunk__49999_50734 = null;
var count__50000_50735 = (0);
var i__50001_50736 = (0);
while(true){
if((i__50001_50736 < count__50000_50735)){
var vec__50013_50737 = chunk__49999_50734.cljs$core$IIndexed$_nth$arity$2(null,i__50001_50736);
var script_tag_50738 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50013_50737,(0),null);
var script_body_50739 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50013_50737,(1),null);
eval(script_body_50739);


var G__50740 = seq__49998_50733;
var G__50741 = chunk__49999_50734;
var G__50742 = count__50000_50735;
var G__50743 = (i__50001_50736 + (1));
seq__49998_50733 = G__50740;
chunk__49999_50734 = G__50741;
count__50000_50735 = G__50742;
i__50001_50736 = G__50743;
continue;
} else {
var temp__5735__auto___50745 = cljs.core.seq(seq__49998_50733);
if(temp__5735__auto___50745){
var seq__49998_50747__$1 = temp__5735__auto___50745;
if(cljs.core.chunked_seq_QMARK_(seq__49998_50747__$1)){
var c__4556__auto___50748 = cljs.core.chunk_first(seq__49998_50747__$1);
var G__50749 = cljs.core.chunk_rest(seq__49998_50747__$1);
var G__50750 = c__4556__auto___50748;
var G__50751 = cljs.core.count(c__4556__auto___50748);
var G__50752 = (0);
seq__49998_50733 = G__50749;
chunk__49999_50734 = G__50750;
count__50000_50735 = G__50751;
i__50001_50736 = G__50752;
continue;
} else {
var vec__50019_50754 = cljs.core.first(seq__49998_50747__$1);
var script_tag_50755 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50019_50754,(0),null);
var script_body_50756 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50019_50754,(1),null);
eval(script_body_50756);


var G__50760 = cljs.core.next(seq__49998_50747__$1);
var G__50761 = null;
var G__50762 = (0);
var G__50763 = (0);
seq__49998_50733 = G__50760;
chunk__49999_50734 = G__50761;
count__50000_50735 = G__50762;
i__50001_50736 = G__50763;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (s__$1,p__50024){
var vec__50026 = p__50024;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50026,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50026,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
}),s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
(el.innerHTML = s);

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
return goog.dom.getAncestorByClass(shadow.dom.dom_node(el),cls);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__50039 = arguments.length;
switch (G__50039) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag));
}));

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag),cljs.core.name(cls));
}));

(shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3);

shadow.dom.get_value = (function shadow$dom$get_value(dom){
return goog.dom.forms.getValue(shadow.dom.dom_node(dom));
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
return goog.dom.forms.setValue(shadow.dom.dom_node(dom),value);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__50052 = cljs.core.seq(style_keys);
var chunk__50053 = null;
var count__50054 = (0);
var i__50055 = (0);
while(true){
if((i__50055 < count__50054)){
var it = chunk__50053.cljs$core$IIndexed$_nth$arity$2(null,i__50055);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__50775 = seq__50052;
var G__50776 = chunk__50053;
var G__50777 = count__50054;
var G__50778 = (i__50055 + (1));
seq__50052 = G__50775;
chunk__50053 = G__50776;
count__50054 = G__50777;
i__50055 = G__50778;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__50052);
if(temp__5735__auto__){
var seq__50052__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__50052__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__50052__$1);
var G__50780 = cljs.core.chunk_rest(seq__50052__$1);
var G__50781 = c__4556__auto__;
var G__50782 = cljs.core.count(c__4556__auto__);
var G__50783 = (0);
seq__50052 = G__50780;
chunk__50053 = G__50781;
count__50054 = G__50782;
i__50055 = G__50783;
continue;
} else {
var it = cljs.core.first(seq__50052__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__50784 = cljs.core.next(seq__50052__$1);
var G__50785 = null;
var G__50786 = (0);
var G__50787 = (0);
seq__50052 = G__50784;
chunk__50053 = G__50785;
count__50054 = G__50786;
i__50055 = G__50787;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4380__auto__,k__4381__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return this__4380__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4381__auto__,null);
}));

(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4382__auto__,k50063,else__4383__auto__){
var self__ = this;
var this__4382__auto____$1 = this;
var G__50067 = k50063;
var G__50067__$1 = (((G__50067 instanceof cljs.core.Keyword))?G__50067.fqn:null);
switch (G__50067__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k50063,else__4383__auto__);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4399__auto__,f__4400__auto__,init__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4402__auto__,p__50068){
var vec__50069 = p__50068;
var k__4403__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50069,(0),null);
var v__4404__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50069,(1),null);
return (f__4400__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4400__auto__.cljs$core$IFn$_invoke$arity$3(ret__4402__auto__,k__4403__auto__,v__4404__auto__) : f__4400__auto__.call(null,ret__4402__auto__,k__4403__auto__,v__4404__auto__));
}),init__4401__auto__,this__4399__auto____$1);
}));

(shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4394__auto__,writer__4395__auto__,opts__4396__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
var pr_pair__4397__auto__ = (function (keyval__4398__auto__){
return cljs.core.pr_sequential_writer(writer__4395__auto__,cljs.core.pr_writer,""," ","",opts__4396__auto__,keyval__4398__auto__);
});
return cljs.core.pr_sequential_writer(writer__4395__auto__,pr_pair__4397__auto__,"#shadow.dom.Coordinate{",", ","}",opts__4396__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__50062){
var self__ = this;
var G__50062__$1 = this;
return (new cljs.core.RecordIter((0),G__50062__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4378__auto__){
var self__ = this;
var this__4378__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4375__auto__){
var self__ = this;
var this__4375__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4384__auto__){
var self__ = this;
var this__4384__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4376__auto__){
var self__ = this;
var this__4376__auto____$1 = this;
var h__4238__auto__ = self__.__hash;
if((!((h__4238__auto__ == null)))){
return h__4238__auto__;
} else {
var h__4238__auto____$1 = (function (coll__4377__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__4377__auto__));
})(this__4376__auto____$1);
(self__.__hash = h__4238__auto____$1);

return h__4238__auto____$1;
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this50064,other50065){
var self__ = this;
var this50064__$1 = this;
return (((!((other50065 == null)))) && ((this50064__$1.constructor === other50065.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this50064__$1.x,other50065.x)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this50064__$1.y,other50065.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this50064__$1.__extmap,other50065.__extmap)));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4389__auto__,k__4390__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__4390__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4389__auto____$1),self__.__meta),k__4390__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4390__auto__)),null));
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4387__auto__,k__4388__auto__,G__50062){
var self__ = this;
var this__4387__auto____$1 = this;
var pred__50087 = cljs.core.keyword_identical_QMARK_;
var expr__50088 = k__4388__auto__;
if(cljs.core.truth_((pred__50087.cljs$core$IFn$_invoke$arity$2 ? pred__50087.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"x","x",2099068185),expr__50088) : pred__50087.call(null,new cljs.core.Keyword(null,"x","x",2099068185),expr__50088)))){
return (new shadow.dom.Coordinate(G__50062,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__50087.cljs$core$IFn$_invoke$arity$2 ? pred__50087.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"y","y",-1757859776),expr__50088) : pred__50087.call(null,new cljs.core.Keyword(null,"y","y",-1757859776),expr__50088)))){
return (new shadow.dom.Coordinate(self__.x,G__50062,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4388__auto__,G__50062),null));
}
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4392__auto__){
var self__ = this;
var this__4392__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4379__auto__,G__50062){
var self__ = this;
var this__4379__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__50062,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4385__auto__,entry__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4386__auto__)){
return this__4385__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4386__auto__,(0)),cljs.core._nth(entry__4386__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4385__auto____$1,entry__4386__auto__);
}
}));

(shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
}));

(shadow.dom.Coordinate.cljs$lang$type = true);

(shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__4423__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
}));

(shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__4423__auto__,writer__4424__auto__){
return cljs.core._write(writer__4424__auto__,"shadow.dom/Coordinate");
}));

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__50066){
var extmap__4419__auto__ = (function (){var G__50103 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__50066,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__50066)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__50103);
} else {
return G__50103;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__50066),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__50066),null,cljs.core.not_empty(extmap__4419__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = goog.style.getPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = goog.style.getClientPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = goog.style.getPageOffset(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4380__auto__,k__4381__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return this__4380__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4381__auto__,null);
}));

(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4382__auto__,k50112,else__4383__auto__){
var self__ = this;
var this__4382__auto____$1 = this;
var G__50119 = k50112;
var G__50119__$1 = (((G__50119 instanceof cljs.core.Keyword))?G__50119.fqn:null);
switch (G__50119__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k50112,else__4383__auto__);

}
}));

(shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4399__auto__,f__4400__auto__,init__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4402__auto__,p__50124){
var vec__50126 = p__50124;
var k__4403__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50126,(0),null);
var v__4404__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50126,(1),null);
return (f__4400__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4400__auto__.cljs$core$IFn$_invoke$arity$3(ret__4402__auto__,k__4403__auto__,v__4404__auto__) : f__4400__auto__.call(null,ret__4402__auto__,k__4403__auto__,v__4404__auto__));
}),init__4401__auto__,this__4399__auto____$1);
}));

(shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4394__auto__,writer__4395__auto__,opts__4396__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
var pr_pair__4397__auto__ = (function (keyval__4398__auto__){
return cljs.core.pr_sequential_writer(writer__4395__auto__,cljs.core.pr_writer,""," ","",opts__4396__auto__,keyval__4398__auto__);
});
return cljs.core.pr_sequential_writer(writer__4395__auto__,pr_pair__4397__auto__,"#shadow.dom.Size{",", ","}",opts__4396__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__50111){
var self__ = this;
var G__50111__$1 = this;
return (new cljs.core.RecordIter((0),G__50111__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4378__auto__){
var self__ = this;
var this__4378__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4375__auto__){
var self__ = this;
var this__4375__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4384__auto__){
var self__ = this;
var this__4384__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4376__auto__){
var self__ = this;
var this__4376__auto____$1 = this;
var h__4238__auto__ = self__.__hash;
if((!((h__4238__auto__ == null)))){
return h__4238__auto__;
} else {
var h__4238__auto____$1 = (function (coll__4377__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__4377__auto__));
})(this__4376__auto____$1);
(self__.__hash = h__4238__auto____$1);

return h__4238__auto____$1;
}
}));

(shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this50113,other50114){
var self__ = this;
var this50113__$1 = this;
return (((!((other50114 == null)))) && ((this50113__$1.constructor === other50114.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this50113__$1.w,other50114.w)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this50113__$1.h,other50114.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this50113__$1.__extmap,other50114.__extmap)));
}));

(shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4389__auto__,k__4390__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__4390__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4389__auto____$1),self__.__meta),k__4390__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4390__auto__)),null));
}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4387__auto__,k__4388__auto__,G__50111){
var self__ = this;
var this__4387__auto____$1 = this;
var pred__50147 = cljs.core.keyword_identical_QMARK_;
var expr__50148 = k__4388__auto__;
if(cljs.core.truth_((pred__50147.cljs$core$IFn$_invoke$arity$2 ? pred__50147.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"w","w",354169001),expr__50148) : pred__50147.call(null,new cljs.core.Keyword(null,"w","w",354169001),expr__50148)))){
return (new shadow.dom.Size(G__50111,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__50147.cljs$core$IFn$_invoke$arity$2 ? pred__50147.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"h","h",1109658740),expr__50148) : pred__50147.call(null,new cljs.core.Keyword(null,"h","h",1109658740),expr__50148)))){
return (new shadow.dom.Size(self__.w,G__50111,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4388__auto__,G__50111),null));
}
}
}));

(shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4392__auto__){
var self__ = this;
var this__4392__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4379__auto__,G__50111){
var self__ = this;
var this__4379__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__50111,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4385__auto__,entry__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4386__auto__)){
return this__4385__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4386__auto__,(0)),cljs.core._nth(entry__4386__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4385__auto____$1,entry__4386__auto__);
}
}));

(shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
}));

(shadow.dom.Size.cljs$lang$type = true);

(shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__4423__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
}));

(shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__4423__auto__,writer__4424__auto__){
return cljs.core._write(writer__4424__auto__,"shadow.dom/Size");
}));

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__50116){
var extmap__4419__auto__ = (function (){var G__50157 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__50116,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__50116)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__50157);
} else {
return G__50157;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__50116),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__50116),null,cljs.core.not_empty(extmap__4419__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj(goog.style.getSize(shadow.dom.dom_node(el)));
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return shadow.dom.get_size(el).h;
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__4610__auto__ = opts;
var l__4611__auto__ = a__4610__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__4611__auto__)){
var G__50848 = (i + (1));
var G__50849 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__50848;
ret = G__50849;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__50168){
var vec__50169 = p__50168;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50169,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50169,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__50173 = arguments.length;
switch (G__50173) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
}));

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
}));

(shadow.dom.redirect.cljs$lang$maxFixedArity = 2);

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return (document.location.href = document.location.href);
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingAfter(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingBefore(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5733__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5733__auto__)){
var child = temp__5733__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__50853 = ps;
var G__50854 = (i + (1));
el__$1 = G__50853;
i = G__50854;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
return goog.dom.getParentElement(shadow.dom.dom_node(el));
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,(function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
}),null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
return goog.dom.getNextElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
return goog.dom.getPreviousElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__50191 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50191,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50191,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50191,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__50194_50859 = cljs.core.seq(props);
var chunk__50195_50860 = null;
var count__50196_50861 = (0);
var i__50197_50862 = (0);
while(true){
if((i__50197_50862 < count__50196_50861)){
var vec__50211_50863 = chunk__50195_50860.cljs$core$IIndexed$_nth$arity$2(null,i__50197_50862);
var k_50864 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50211_50863,(0),null);
var v_50865 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50211_50863,(1),null);
el.setAttributeNS((function (){var temp__5735__auto__ = cljs.core.namespace(k_50864);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_50864),v_50865);


var G__50869 = seq__50194_50859;
var G__50870 = chunk__50195_50860;
var G__50871 = count__50196_50861;
var G__50872 = (i__50197_50862 + (1));
seq__50194_50859 = G__50869;
chunk__50195_50860 = G__50870;
count__50196_50861 = G__50871;
i__50197_50862 = G__50872;
continue;
} else {
var temp__5735__auto___50873 = cljs.core.seq(seq__50194_50859);
if(temp__5735__auto___50873){
var seq__50194_50874__$1 = temp__5735__auto___50873;
if(cljs.core.chunked_seq_QMARK_(seq__50194_50874__$1)){
var c__4556__auto___50875 = cljs.core.chunk_first(seq__50194_50874__$1);
var G__50876 = cljs.core.chunk_rest(seq__50194_50874__$1);
var G__50877 = c__4556__auto___50875;
var G__50878 = cljs.core.count(c__4556__auto___50875);
var G__50879 = (0);
seq__50194_50859 = G__50876;
chunk__50195_50860 = G__50877;
count__50196_50861 = G__50878;
i__50197_50862 = G__50879;
continue;
} else {
var vec__50222_50880 = cljs.core.first(seq__50194_50874__$1);
var k_50881 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50222_50880,(0),null);
var v_50882 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50222_50880,(1),null);
el.setAttributeNS((function (){var temp__5735__auto____$1 = cljs.core.namespace(k_50881);
if(cljs.core.truth_(temp__5735__auto____$1)){
var ns = temp__5735__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_50881),v_50882);


var G__50886 = cljs.core.next(seq__50194_50874__$1);
var G__50887 = null;
var G__50888 = (0);
var G__50889 = (0);
seq__50194_50859 = G__50886;
chunk__50195_50860 = G__50887;
count__50196_50861 = G__50888;
i__50197_50862 = G__50889;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__50233 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50233,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__50233,(1),null);
var seq__50237_50893 = cljs.core.seq(node_children);
var chunk__50239_50894 = null;
var count__50240_50895 = (0);
var i__50241_50896 = (0);
while(true){
if((i__50241_50896 < count__50240_50895)){
var child_struct_50897 = chunk__50239_50894.cljs$core$IIndexed$_nth$arity$2(null,i__50241_50896);
if((!((child_struct_50897 == null)))){
if(typeof child_struct_50897 === 'string'){
var text_50898 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_50898),child_struct_50897].join(''));
} else {
var children_50899 = shadow.dom.svg_node(child_struct_50897);
if(cljs.core.seq_QMARK_(children_50899)){
var seq__50288_50901 = cljs.core.seq(children_50899);
var chunk__50290_50902 = null;
var count__50291_50903 = (0);
var i__50292_50904 = (0);
while(true){
if((i__50292_50904 < count__50291_50903)){
var child_50905 = chunk__50290_50902.cljs$core$IIndexed$_nth$arity$2(null,i__50292_50904);
if(cljs.core.truth_(child_50905)){
node.appendChild(child_50905);


var G__50906 = seq__50288_50901;
var G__50907 = chunk__50290_50902;
var G__50908 = count__50291_50903;
var G__50909 = (i__50292_50904 + (1));
seq__50288_50901 = G__50906;
chunk__50290_50902 = G__50907;
count__50291_50903 = G__50908;
i__50292_50904 = G__50909;
continue;
} else {
var G__50910 = seq__50288_50901;
var G__50911 = chunk__50290_50902;
var G__50912 = count__50291_50903;
var G__50913 = (i__50292_50904 + (1));
seq__50288_50901 = G__50910;
chunk__50290_50902 = G__50911;
count__50291_50903 = G__50912;
i__50292_50904 = G__50913;
continue;
}
} else {
var temp__5735__auto___50919 = cljs.core.seq(seq__50288_50901);
if(temp__5735__auto___50919){
var seq__50288_50921__$1 = temp__5735__auto___50919;
if(cljs.core.chunked_seq_QMARK_(seq__50288_50921__$1)){
var c__4556__auto___50922 = cljs.core.chunk_first(seq__50288_50921__$1);
var G__50923 = cljs.core.chunk_rest(seq__50288_50921__$1);
var G__50924 = c__4556__auto___50922;
var G__50925 = cljs.core.count(c__4556__auto___50922);
var G__50926 = (0);
seq__50288_50901 = G__50923;
chunk__50290_50902 = G__50924;
count__50291_50903 = G__50925;
i__50292_50904 = G__50926;
continue;
} else {
var child_50928 = cljs.core.first(seq__50288_50921__$1);
if(cljs.core.truth_(child_50928)){
node.appendChild(child_50928);


var G__50929 = cljs.core.next(seq__50288_50921__$1);
var G__50930 = null;
var G__50931 = (0);
var G__50932 = (0);
seq__50288_50901 = G__50929;
chunk__50290_50902 = G__50930;
count__50291_50903 = G__50931;
i__50292_50904 = G__50932;
continue;
} else {
var G__50933 = cljs.core.next(seq__50288_50921__$1);
var G__50934 = null;
var G__50935 = (0);
var G__50936 = (0);
seq__50288_50901 = G__50933;
chunk__50290_50902 = G__50934;
count__50291_50903 = G__50935;
i__50292_50904 = G__50936;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_50899);
}
}


var G__50937 = seq__50237_50893;
var G__50938 = chunk__50239_50894;
var G__50939 = count__50240_50895;
var G__50940 = (i__50241_50896 + (1));
seq__50237_50893 = G__50937;
chunk__50239_50894 = G__50938;
count__50240_50895 = G__50939;
i__50241_50896 = G__50940;
continue;
} else {
var G__50941 = seq__50237_50893;
var G__50942 = chunk__50239_50894;
var G__50943 = count__50240_50895;
var G__50944 = (i__50241_50896 + (1));
seq__50237_50893 = G__50941;
chunk__50239_50894 = G__50942;
count__50240_50895 = G__50943;
i__50241_50896 = G__50944;
continue;
}
} else {
var temp__5735__auto___50945 = cljs.core.seq(seq__50237_50893);
if(temp__5735__auto___50945){
var seq__50237_50946__$1 = temp__5735__auto___50945;
if(cljs.core.chunked_seq_QMARK_(seq__50237_50946__$1)){
var c__4556__auto___50947 = cljs.core.chunk_first(seq__50237_50946__$1);
var G__50948 = cljs.core.chunk_rest(seq__50237_50946__$1);
var G__50949 = c__4556__auto___50947;
var G__50950 = cljs.core.count(c__4556__auto___50947);
var G__50951 = (0);
seq__50237_50893 = G__50948;
chunk__50239_50894 = G__50949;
count__50240_50895 = G__50950;
i__50241_50896 = G__50951;
continue;
} else {
var child_struct_50952 = cljs.core.first(seq__50237_50946__$1);
if((!((child_struct_50952 == null)))){
if(typeof child_struct_50952 === 'string'){
var text_50954 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_50954),child_struct_50952].join(''));
} else {
var children_50955 = shadow.dom.svg_node(child_struct_50952);
if(cljs.core.seq_QMARK_(children_50955)){
var seq__50308_50956 = cljs.core.seq(children_50955);
var chunk__50310_50957 = null;
var count__50311_50958 = (0);
var i__50312_50959 = (0);
while(true){
if((i__50312_50959 < count__50311_50958)){
var child_50960 = chunk__50310_50957.cljs$core$IIndexed$_nth$arity$2(null,i__50312_50959);
if(cljs.core.truth_(child_50960)){
node.appendChild(child_50960);


var G__50961 = seq__50308_50956;
var G__50962 = chunk__50310_50957;
var G__50963 = count__50311_50958;
var G__50964 = (i__50312_50959 + (1));
seq__50308_50956 = G__50961;
chunk__50310_50957 = G__50962;
count__50311_50958 = G__50963;
i__50312_50959 = G__50964;
continue;
} else {
var G__50965 = seq__50308_50956;
var G__50966 = chunk__50310_50957;
var G__50967 = count__50311_50958;
var G__50968 = (i__50312_50959 + (1));
seq__50308_50956 = G__50965;
chunk__50310_50957 = G__50966;
count__50311_50958 = G__50967;
i__50312_50959 = G__50968;
continue;
}
} else {
var temp__5735__auto___50969__$1 = cljs.core.seq(seq__50308_50956);
if(temp__5735__auto___50969__$1){
var seq__50308_50971__$1 = temp__5735__auto___50969__$1;
if(cljs.core.chunked_seq_QMARK_(seq__50308_50971__$1)){
var c__4556__auto___50972 = cljs.core.chunk_first(seq__50308_50971__$1);
var G__50973 = cljs.core.chunk_rest(seq__50308_50971__$1);
var G__50974 = c__4556__auto___50972;
var G__50975 = cljs.core.count(c__4556__auto___50972);
var G__50976 = (0);
seq__50308_50956 = G__50973;
chunk__50310_50957 = G__50974;
count__50311_50958 = G__50975;
i__50312_50959 = G__50976;
continue;
} else {
var child_50981 = cljs.core.first(seq__50308_50971__$1);
if(cljs.core.truth_(child_50981)){
node.appendChild(child_50981);


var G__50982 = cljs.core.next(seq__50308_50971__$1);
var G__50983 = null;
var G__50984 = (0);
var G__50985 = (0);
seq__50308_50956 = G__50982;
chunk__50310_50957 = G__50983;
count__50311_50958 = G__50984;
i__50312_50959 = G__50985;
continue;
} else {
var G__50986 = cljs.core.next(seq__50308_50971__$1);
var G__50987 = null;
var G__50988 = (0);
var G__50989 = (0);
seq__50308_50956 = G__50986;
chunk__50310_50957 = G__50987;
count__50311_50958 = G__50988;
i__50312_50959 = G__50989;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_50955);
}
}


var G__50994 = cljs.core.next(seq__50237_50946__$1);
var G__50995 = null;
var G__50996 = (0);
var G__50997 = (0);
seq__50237_50893 = G__50994;
chunk__50239_50894 = G__50995;
count__50240_50895 = G__50996;
i__50241_50896 = G__50997;
continue;
} else {
var G__50998 = cljs.core.next(seq__50237_50946__$1);
var G__50999 = null;
var G__51000 = (0);
var G__51001 = (0);
seq__50237_50893 = G__50998;
chunk__50239_50894 = G__50999;
count__50240_50895 = G__51000;
i__50241_50896 = G__51001;
continue;
}
}
} else {
}
}
break;
}

return node;
});
goog.object.set(shadow.dom.SVGElement,"string",true);

goog.object.set(shadow.dom._to_svg,"string",(function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
}));

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
}));

goog.object.set(shadow.dom.SVGElement,"null",true);

goog.object.set(shadow.dom._to_svg,"null",(function (_){
return null;
}));
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__4742__auto__ = [];
var len__4736__auto___51002 = arguments.length;
var i__4737__auto___51003 = (0);
while(true){
if((i__4737__auto___51003 < len__4736__auto___51002)){
args__4742__auto__.push((arguments[i__4737__auto___51003]));

var G__51004 = (i__4737__auto___51003 + (1));
i__4737__auto___51003 = G__51004;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
}));

(shadow.dom.svg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.dom.svg.cljs$lang$applyTo = (function (seq50328){
var G__50330 = cljs.core.first(seq50328);
var seq50328__$1 = cljs.core.next(seq50328);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__50330,seq50328__$1);
}));

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__50339 = arguments.length;
switch (G__50339) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});
shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(event),event_fn);

if(cljs.core.truth_((function (){var and__4115__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__4115__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__4115__auto__;
}
})())){
var c__45585__auto___51014 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_50382){
var state_val_50383 = (state_50382[(1)]);
if((state_val_50383 === (1))){
var state_50382__$1 = state_50382;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_50382__$1,(2),once_or_cleanup);
} else {
if((state_val_50383 === (2))){
var inst_50379 = (state_50382[(2)]);
var inst_50380 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_50382__$1 = (function (){var statearr_50393 = state_50382;
(statearr_50393[(7)] = inst_50379);

return statearr_50393;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_50382__$1,inst_50380);
} else {
return null;
}
}
});
return (function() {
var shadow$dom$state_machine__45510__auto__ = null;
var shadow$dom$state_machine__45510__auto____0 = (function (){
var statearr_50395 = [null,null,null,null,null,null,null,null];
(statearr_50395[(0)] = shadow$dom$state_machine__45510__auto__);

(statearr_50395[(1)] = (1));

return statearr_50395;
});
var shadow$dom$state_machine__45510__auto____1 = (function (state_50382){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_50382);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e50399){var ex__45513__auto__ = e50399;
var statearr_50400_51023 = state_50382;
(statearr_50400_51023[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_50382[(4)]))){
var statearr_50405_51025 = state_50382;
(statearr_50405_51025[(1)] = cljs.core.first((state_50382[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__51032 = state_50382;
state_50382 = G__51032;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
shadow$dom$state_machine__45510__auto__ = function(state_50382){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__45510__auto____0.call(this);
case 1:
return shadow$dom$state_machine__45510__auto____1.call(this,state_50382);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__45510__auto____0;
shadow$dom$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__45510__auto____1;
return shadow$dom$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_50420 = f__45586__auto__();
(statearr_50420[(6)] = c__45585__auto___51014);

return statearr_50420;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));

} else {
}

return chan;
}));

(shadow.dom.event_chan.cljs$lang$maxFixedArity = 4);

Object.defineProperty(module.exports, "contains_QMARK_", { enumerable: false, get: function() { return shadow.dom.contains_QMARK_; } });
Object.defineProperty(module.exports, "eval_scripts", { enumerable: false, get: function() { return shadow.dom.eval_scripts; } });
Object.defineProperty(module.exports, "redirect", { enumerable: false, get: function() { return shadow.dom.redirect; } });
Object.defineProperty(module.exports, "native_coll", { enumerable: false, get: function() { return shadow.dom.native_coll; } });
Object.defineProperty(module.exports, "NativeColl", { enumerable: false, get: function() { return shadow.dom.NativeColl; } });
Object.defineProperty(module.exports, "query", { enumerable: false, get: function() { return shadow.dom.query; } });
Object.defineProperty(module.exports, "make_svg_node", { enumerable: false, get: function() { return shadow.dom.make_svg_node; } });
Object.defineProperty(module.exports, "str__GT_fragment", { enumerable: false, get: function() { return shadow.dom.str__GT_fragment; } });
Object.defineProperty(module.exports, "_to_svg", { enumerable: false, get: function() { return shadow.dom._to_svg; } });
Object.defineProperty(module.exports, "child_nodes", { enumerable: false, get: function() { return shadow.dom.child_nodes; } });
Object.defineProperty(module.exports, "insert_before", { enumerable: false, get: function() { return shadow.dom.insert_before; } });
Object.defineProperty(module.exports, "merge_class_string", { enumerable: false, get: function() { return shadow.dom.merge_class_string; } });
Object.defineProperty(module.exports, "has_class_QMARK_", { enumerable: false, get: function() { return shadow.dom.has_class_QMARK_; } });
Object.defineProperty(module.exports, "map__GT_Size", { enumerable: false, get: function() { return shadow.dom.map__GT_Size; } });
Object.defineProperty(module.exports, "text", { enumerable: false, get: function() { return shadow.dom.text; } });
Object.defineProperty(module.exports, "data", { enumerable: false, get: function() { return shadow.dom.data; } });
Object.defineProperty(module.exports, "transition_supported_QMARK_", { enumerable: false, get: function() { return shadow.dom.transition_supported_QMARK_; } });
Object.defineProperty(module.exports, "attr", { enumerable: false, get: function() { return shadow.dom.attr; } });
Object.defineProperty(module.exports, "remove_styles", { enumerable: false, get: function() { return shadow.dom.remove_styles; } });
Object.defineProperty(module.exports, "remove_event_handler", { enumerable: false, get: function() { return shadow.dom.remove_event_handler; } });
Object.defineProperty(module.exports, "remove_class", { enumerable: false, get: function() { return shadow.dom.remove_class; } });
Object.defineProperty(module.exports, "select_option_values", { enumerable: false, get: function() { return shadow.dom.select_option_values; } });
Object.defineProperty(module.exports, "insert_after", { enumerable: false, get: function() { return shadow.dom.insert_after; } });
Object.defineProperty(module.exports, "svg_node", { enumerable: false, get: function() { return shadow.dom.svg_node; } });
Object.defineProperty(module.exports, "children", { enumerable: false, get: function() { return shadow.dom.children; } });
Object.defineProperty(module.exports, "first_child", { enumerable: false, get: function() { return shadow.dom.first_child; } });
Object.defineProperty(module.exports, "get_html", { enumerable: false, get: function() { return shadow.dom.get_html; } });
Object.defineProperty(module.exports, "remove", { enumerable: false, get: function() { return shadow.dom.remove; } });
Object.defineProperty(module.exports, "__GT_Coordinate", { enumerable: false, get: function() { return shadow.dom.__GT_Coordinate; } });
Object.defineProperty(module.exports, "dom_listen_remove", { enumerable: false, get: function() { return shadow.dom.dom_listen_remove; } });
Object.defineProperty(module.exports, "by_id", { enumerable: false, get: function() { return shadow.dom.by_id; } });
Object.defineProperty(module.exports, "checked_QMARK_", { enumerable: false, get: function() { return shadow.dom.checked_QMARK_; } });
Object.defineProperty(module.exports, "tag_name", { enumerable: false, get: function() { return shadow.dom.tag_name; } });
Object.defineProperty(module.exports, "get_size", { enumerable: false, get: function() { return shadow.dom.get_size; } });
Object.defineProperty(module.exports, "dom_listen", { enumerable: false, get: function() { return shadow.dom.dom_listen; } });
Object.defineProperty(module.exports, "get_viewport_size", { enumerable: false, get: function() { return shadow.dom.get_viewport_size; } });
Object.defineProperty(module.exports, "add_event_listeners", { enumerable: false, get: function() { return shadow.dom.add_event_listeners; } });
Object.defineProperty(module.exports, "set_attr", { enumerable: false, get: function() { return shadow.dom.set_attr; } });
Object.defineProperty(module.exports, "reset", { enumerable: false, get: function() { return shadow.dom.reset; } });
Object.defineProperty(module.exports, "IElement", { enumerable: false, get: function() { return shadow.dom.IElement; } });
Object.defineProperty(module.exports, "make_dom_node", { enumerable: false, get: function() { return shadow.dom.make_dom_node; } });
Object.defineProperty(module.exports, "SVGElement", { enumerable: false, get: function() { return shadow.dom.SVGElement; } });
Object.defineProperty(module.exports, "form_elements", { enumerable: false, get: function() { return shadow.dom.form_elements; } });
Object.defineProperty(module.exports, "Size", { enumerable: false, get: function() { return shadow.dom.Size; } });
Object.defineProperty(module.exports, "lazy_native_coll_seq", { enumerable: false, get: function() { return shadow.dom.lazy_native_coll_seq; } });
Object.defineProperty(module.exports, "get_parent", { enumerable: false, get: function() { return shadow.dom.get_parent; } });
Object.defineProperty(module.exports, "get_height", { enumerable: false, get: function() { return shadow.dom.get_height; } });
Object.defineProperty(module.exports, "event_chan", { enumerable: false, get: function() { return shadow.dom.event_chan; } });
Object.defineProperty(module.exports, "fragment", { enumerable: false, get: function() { return shadow.dom.fragment; } });
Object.defineProperty(module.exports, "check", { enumerable: false, get: function() { return shadow.dom.check; } });
Object.defineProperty(module.exports, "Coordinate", { enumerable: false, get: function() { return shadow.dom.Coordinate; } });
Object.defineProperty(module.exports, "parse_tag", { enumerable: false, get: function() { return shadow.dom.parse_tag; } });
Object.defineProperty(module.exports, "del_attr", { enumerable: false, get: function() { return shadow.dom.del_attr; } });
Object.defineProperty(module.exports, "reload_BANG_", { enumerable: false, get: function() { return shadow.dom.reload_BANG_; } });
Object.defineProperty(module.exports, "destructure_node", { enumerable: false, get: function() { return shadow.dom.destructure_node; } });
Object.defineProperty(module.exports, "remove_style", { enumerable: false, get: function() { return shadow.dom.remove_style; } });
Object.defineProperty(module.exports, "append", { enumerable: false, get: function() { return shadow.dom.append; } });
Object.defineProperty(module.exports, "px", { enumerable: false, get: function() { return shadow.dom.px; } });
Object.defineProperty(module.exports, "get_value", { enumerable: false, get: function() { return shadow.dom.get_value; } });
Object.defineProperty(module.exports, "ev_stop", { enumerable: false, get: function() { return shadow.dom.ev_stop; } });
Object.defineProperty(module.exports, "_to_dom", { enumerable: false, get: function() { return shadow.dom._to_dom; } });
Object.defineProperty(module.exports, "xmlns", { enumerable: false, get: function() { return shadow.dom.xmlns; } });
Object.defineProperty(module.exports, "matches", { enumerable: false, get: function() { return shadow.dom.matches; } });
Object.defineProperty(module.exports, "insert_first", { enumerable: false, get: function() { return shadow.dom.insert_first; } });
Object.defineProperty(module.exports, "map__GT_Coordinate", { enumerable: false, get: function() { return shadow.dom.map__GT_Coordinate; } });
Object.defineProperty(module.exports, "create_dom_node", { enumerable: false, get: function() { return shadow.dom.create_dom_node; } });
Object.defineProperty(module.exports, "__GT_NativeColl", { enumerable: false, get: function() { return shadow.dom.__GT_NativeColl; } });
Object.defineProperty(module.exports, "get_position", { enumerable: false, get: function() { return shadow.dom.get_position; } });
Object.defineProperty(module.exports, "dom_node", { enumerable: false, get: function() { return shadow.dom.dom_node; } });
Object.defineProperty(module.exports, "set_data", { enumerable: false, get: function() { return shadow.dom.set_data; } });
Object.defineProperty(module.exports, "get_client_position", { enumerable: false, get: function() { return shadow.dom.get_client_position; } });
Object.defineProperty(module.exports, "get_page_offset", { enumerable: false, get: function() { return shadow.dom.get_page_offset; } });
Object.defineProperty(module.exports, "query_one", { enumerable: false, get: function() { return shadow.dom.query_one; } });
Object.defineProperty(module.exports, "get_next_sibling", { enumerable: false, get: function() { return shadow.dom.get_next_sibling; } });
Object.defineProperty(module.exports, "set_style", { enumerable: false, get: function() { return shadow.dom.set_style; } });
Object.defineProperty(module.exports, "pct", { enumerable: false, get: function() { return shadow.dom.pct; } });
Object.defineProperty(module.exports, "get_previous_sibling", { enumerable: false, get: function() { return shadow.dom.get_previous_sibling; } });
Object.defineProperty(module.exports, "ancestor_by_tag", { enumerable: false, get: function() { return shadow.dom.ancestor_by_tag; } });
Object.defineProperty(module.exports, "build", { enumerable: false, get: function() { return shadow.dom.build; } });
Object.defineProperty(module.exports, "set_html", { enumerable: false, get: function() { return shadow.dom.set_html; } });
Object.defineProperty(module.exports, "build_url", { enumerable: false, get: function() { return shadow.dom.build_url; } });
Object.defineProperty(module.exports, "ancestor_by_class", { enumerable: false, get: function() { return shadow.dom.ancestor_by_class; } });
Object.defineProperty(module.exports, "__GT_Size", { enumerable: false, get: function() { return shadow.dom.__GT_Size; } });
Object.defineProperty(module.exports, "add_class", { enumerable: false, get: function() { return shadow.dom.add_class; } });
Object.defineProperty(module.exports, "node_name", { enumerable: false, get: function() { return shadow.dom.node_name; } });
Object.defineProperty(module.exports, "parents", { enumerable: false, get: function() { return shadow.dom.parents; } });
Object.defineProperty(module.exports, "on_query", { enumerable: false, get: function() { return shadow.dom.on_query; } });
Object.defineProperty(module.exports, "create_svg_node", { enumerable: false, get: function() { return shadow.dom.create_svg_node; } });
Object.defineProperty(module.exports, "set_attrs", { enumerable: false, get: function() { return shadow.dom.set_attrs; } });
Object.defineProperty(module.exports, "svg", { enumerable: false, get: function() { return shadow.dom.svg; } });
Object.defineProperty(module.exports, "index_of", { enumerable: false, get: function() { return shadow.dom.index_of; } });
Object.defineProperty(module.exports, "replace_node", { enumerable: false, get: function() { return shadow.dom.replace_node; } });
Object.defineProperty(module.exports, "size__GT_clj", { enumerable: false, get: function() { return shadow.dom.size__GT_clj; } });
Object.defineProperty(module.exports, "set_attr_STAR_", { enumerable: false, get: function() { return shadow.dom.set_attr_STAR_; } });
Object.defineProperty(module.exports, "on", { enumerable: false, get: function() { return shadow.dom.on; } });
Object.defineProperty(module.exports, "remove_style_STAR_", { enumerable: false, get: function() { return shadow.dom.remove_style_STAR_; } });
Object.defineProperty(module.exports, "toggle_class", { enumerable: false, get: function() { return shadow.dom.toggle_class; } });
Object.defineProperty(module.exports, "set_value", { enumerable: false, get: function() { return shadow.dom.set_value; } });
//# sourceMappingURL=shadow.dom.js.map

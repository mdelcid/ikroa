goog.provide('cljs.repl');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__74864){
var map__74865 = p__74864;
var map__74865__$1 = (((((!((map__74865 == null))))?(((((map__74865.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74865.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74865):map__74865);
var m = map__74865__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74865__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74865__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__4126__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return [(function (){var temp__5735__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__74871_75079 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__74872_75080 = null;
var count__74873_75081 = (0);
var i__74874_75082 = (0);
while(true){
if((i__74874_75082 < count__74873_75081)){
var f_75091 = chunk__74872_75080.cljs$core$IIndexed$_nth$arity$2(null,i__74874_75082);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_75091], 0));


var G__75092 = seq__74871_75079;
var G__75093 = chunk__74872_75080;
var G__75094 = count__74873_75081;
var G__75095 = (i__74874_75082 + (1));
seq__74871_75079 = G__75092;
chunk__74872_75080 = G__75093;
count__74873_75081 = G__75094;
i__74874_75082 = G__75095;
continue;
} else {
var temp__5735__auto___75096 = cljs.core.seq(seq__74871_75079);
if(temp__5735__auto___75096){
var seq__74871_75097__$1 = temp__5735__auto___75096;
if(cljs.core.chunked_seq_QMARK_(seq__74871_75097__$1)){
var c__4556__auto___75098 = cljs.core.chunk_first(seq__74871_75097__$1);
var G__75099 = cljs.core.chunk_rest(seq__74871_75097__$1);
var G__75100 = c__4556__auto___75098;
var G__75101 = cljs.core.count(c__4556__auto___75098);
var G__75102 = (0);
seq__74871_75079 = G__75099;
chunk__74872_75080 = G__75100;
count__74873_75081 = G__75101;
i__74874_75082 = G__75102;
continue;
} else {
var f_75103 = cljs.core.first(seq__74871_75097__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_75103], 0));


var G__75106 = cljs.core.next(seq__74871_75097__$1);
var G__75107 = null;
var G__75108 = (0);
var G__75109 = (0);
seq__74871_75079 = G__75106;
chunk__74872_75080 = G__75107;
count__74873_75081 = G__75108;
i__74874_75082 = G__75109;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_75116 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__4126__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_75116], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_75116)))?cljs.core.second(arglists_75116):arglists_75116)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__74879_75140 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__74880_75141 = null;
var count__74881_75142 = (0);
var i__74882_75143 = (0);
while(true){
if((i__74882_75143 < count__74881_75142)){
var vec__74893_75144 = chunk__74880_75141.cljs$core$IIndexed$_nth$arity$2(null,i__74882_75143);
var name_75145 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74893_75144,(0),null);
var map__74896_75146 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74893_75144,(1),null);
var map__74896_75147__$1 = (((((!((map__74896_75146 == null))))?(((((map__74896_75146.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74896_75146.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74896_75146):map__74896_75146);
var doc_75148 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74896_75147__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_75149 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74896_75147__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_75145], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_75149], 0));

if(cljs.core.truth_(doc_75148)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_75148], 0));
} else {
}


var G__75152 = seq__74879_75140;
var G__75153 = chunk__74880_75141;
var G__75154 = count__74881_75142;
var G__75155 = (i__74882_75143 + (1));
seq__74879_75140 = G__75152;
chunk__74880_75141 = G__75153;
count__74881_75142 = G__75154;
i__74882_75143 = G__75155;
continue;
} else {
var temp__5735__auto___75156 = cljs.core.seq(seq__74879_75140);
if(temp__5735__auto___75156){
var seq__74879_75158__$1 = temp__5735__auto___75156;
if(cljs.core.chunked_seq_QMARK_(seq__74879_75158__$1)){
var c__4556__auto___75159 = cljs.core.chunk_first(seq__74879_75158__$1);
var G__75160 = cljs.core.chunk_rest(seq__74879_75158__$1);
var G__75161 = c__4556__auto___75159;
var G__75162 = cljs.core.count(c__4556__auto___75159);
var G__75163 = (0);
seq__74879_75140 = G__75160;
chunk__74880_75141 = G__75161;
count__74881_75142 = G__75162;
i__74882_75143 = G__75163;
continue;
} else {
var vec__74901_75164 = cljs.core.first(seq__74879_75158__$1);
var name_75165 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74901_75164,(0),null);
var map__74904_75166 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74901_75164,(1),null);
var map__74904_75167__$1 = (((((!((map__74904_75166 == null))))?(((((map__74904_75166.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74904_75166.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74904_75166):map__74904_75166);
var doc_75168 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74904_75167__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_75169 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74904_75167__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_75165], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_75169], 0));

if(cljs.core.truth_(doc_75168)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_75168], 0));
} else {
}


var G__75170 = cljs.core.next(seq__74879_75158__$1);
var G__75171 = null;
var G__75172 = (0);
var G__75173 = (0);
seq__74879_75140 = G__75170;
chunk__74880_75141 = G__75171;
count__74881_75142 = G__75172;
i__74882_75143 = G__75173;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5735__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5735__auto__)){
var fnspec = temp__5735__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__74911 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__74912 = null;
var count__74913 = (0);
var i__74914 = (0);
while(true){
if((i__74914 < count__74913)){
var role = chunk__74912.cljs$core$IIndexed$_nth$arity$2(null,i__74914);
var temp__5735__auto___75175__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___75175__$1)){
var spec_75177 = temp__5735__auto___75175__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_75177)], 0));
} else {
}


var G__75179 = seq__74911;
var G__75180 = chunk__74912;
var G__75181 = count__74913;
var G__75182 = (i__74914 + (1));
seq__74911 = G__75179;
chunk__74912 = G__75180;
count__74913 = G__75181;
i__74914 = G__75182;
continue;
} else {
var temp__5735__auto____$1 = cljs.core.seq(seq__74911);
if(temp__5735__auto____$1){
var seq__74911__$1 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__74911__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__74911__$1);
var G__75184 = cljs.core.chunk_rest(seq__74911__$1);
var G__75185 = c__4556__auto__;
var G__75186 = cljs.core.count(c__4556__auto__);
var G__75187 = (0);
seq__74911 = G__75184;
chunk__74912 = G__75185;
count__74913 = G__75186;
i__74914 = G__75187;
continue;
} else {
var role = cljs.core.first(seq__74911__$1);
var temp__5735__auto___75189__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___75189__$2)){
var spec_75190 = temp__5735__auto___75189__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_75190)], 0));
} else {
}


var G__75193 = cljs.core.next(seq__74911__$1);
var G__75194 = null;
var G__75195 = (0);
var G__75196 = (0);
seq__74911 = G__75193;
chunk__74912 = G__75194;
count__74913 = G__75195;
i__74914 = G__75196;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol(null,"ExceptionInfo","ExceptionInfo",294935087,null):(((t instanceof Error))?cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("js",t.name):null
))], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(t);
if(cljs.core.truth_(temp__5735__auto__)){
var msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(t);
if(cljs.core.truth_(temp__5735__auto__)){
var ed = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})()], 0));
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__75201 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(via,t);
var G__75202 = cljs.core.ex_cause(t);
via = G__75201;
t = G__75202;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek(via);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(root);
if(cljs.core.truth_(temp__5735__auto__)){
var root_msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(root);
if(cljs.core.truth_(temp__5735__auto__)){
var data = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data(o));
if(cljs.core.truth_(temp__5735__auto__)){
var phase = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})()], 0));
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__74934 = datafied_throwable;
var map__74934__$1 = (((((!((map__74934 == null))))?(((((map__74934.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74934.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74934):map__74934);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74934__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74934__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__74934__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__74935 = cljs.core.last(via);
var map__74935__$1 = (((((!((map__74935 == null))))?(((((map__74935.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74935.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74935):map__74935);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74935__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74935__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74935__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__74936 = data;
var map__74936__$1 = (((((!((map__74936 == null))))?(((((map__74936.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74936.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74936):map__74936);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74936__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74936__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74936__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__74937 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__74937__$1 = (((((!((map__74937 == null))))?(((((map__74937.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74937.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74937):map__74937);
var top_data = map__74937__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74937__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__74950 = phase;
var G__74950__$1 = (((G__74950 instanceof cljs.core.Keyword))?G__74950.fqn:null);
switch (G__74950__$1) {
case "read-source":
var map__74951 = data;
var map__74951__$1 = (((((!((map__74951 == null))))?(((((map__74951.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74951.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74951):map__74951);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74951__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74951__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__74960 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__74960__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74960,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__74960);
var G__74960__$2 = (cljs.core.truth_((function (){var fexpr__74961 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__74961.cljs$core$IFn$_invoke$arity$1 ? fexpr__74961.cljs$core$IFn$_invoke$arity$1(source) : fexpr__74961.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__74960__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__74960__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74960__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__74960__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__74962 = top_data;
var G__74962__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74962,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__74962);
var G__74962__$2 = (cljs.core.truth_((function (){var fexpr__74963 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__74963.cljs$core$IFn$_invoke$arity$1 ? fexpr__74963.cljs$core$IFn$_invoke$arity$1(source) : fexpr__74963.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__74962__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__74962__$1);
var G__74962__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74962__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__74962__$2);
var G__74962__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74962__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__74962__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74962__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__74962__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__74966 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74966,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74966,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74966,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74966,(3),null);
var G__74969 = top_data;
var G__74969__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74969,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__74969);
var G__74969__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74969__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__74969__$1);
var G__74969__$3 = (cljs.core.truth_((function (){var and__4115__auto__ = source__$1;
if(cljs.core.truth_(and__4115__auto__)){
return method;
} else {
return and__4115__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74969__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__74969__$2);
var G__74969__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74969__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__74969__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74969__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__74969__$4;
}

break;
case "execution":
var vec__74972 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74972,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74972,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74972,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74972,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__74933_SHARP_){
var or__4126__auto__ = (p1__74933_SHARP_ == null);
if(or__4126__auto__){
return or__4126__auto__;
} else {
var fexpr__74976 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__74976.cljs$core$IFn$_invoke$arity$1 ? fexpr__74976.cljs$core$IFn$_invoke$arity$1(p1__74933_SHARP_) : fexpr__74976.call(null,p1__74933_SHARP_));
}
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__4126__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return line;
}
})();
var G__74980 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__74980__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74980,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__74980);
var G__74980__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74980__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__74980__$1);
var G__74980__$3 = (cljs.core.truth_((function (){var or__4126__auto__ = fn;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
var and__4115__auto__ = source__$1;
if(cljs.core.truth_(and__4115__auto__)){
return method;
} else {
return and__4115__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74980__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__4126__auto__ = fn;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__74980__$2);
var G__74980__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74980__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__74980__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__74980__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__74980__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__74950__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__74991){
var map__74992 = p__74991;
var map__74992__$1 = (((((!((map__74992 == null))))?(((((map__74992.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74992.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74992):map__74992);
var triage_data = map__74992__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74992__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74992__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74992__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74992__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74992__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74992__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74992__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74992__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4126__auto__ = source;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4126__auto__ = line;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__4126__auto__ = class$;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__74994 = phase;
var G__74994__$1 = (((G__74994 instanceof cljs.core.Keyword))?G__74994.fqn:null);
switch (G__74994__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__74995 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__74996 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__74997 = loc;
var G__74998 = (cljs.core.truth_(spec)?(function (){var sb__4667__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__74999_75227 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__75000_75228 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__75001_75229 = true;
var _STAR_print_fn_STAR__temp_val__75002_75230 = (function (x__4668__auto__){
return sb__4667__auto__.append(x__4668__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__75001_75229);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__75002_75230);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__74987_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__74987_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__75000_75228);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__74999_75227);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4667__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null,"%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__74995,G__74996,G__74997,G__74998) : format.call(null,G__74995,G__74996,G__74997,G__74998));

break;
case "macroexpansion":
var G__75003 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__75004 = cause_type;
var G__75005 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__75006 = loc;
var G__75007 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__75003,G__75004,G__75005,G__75006,G__75007) : format.call(null,G__75003,G__75004,G__75005,G__75006,G__75007));

break;
case "compile-syntax-check":
var G__75008 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__75009 = cause_type;
var G__75010 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__75011 = loc;
var G__75012 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__75008,G__75009,G__75010,G__75011,G__75012) : format.call(null,G__75008,G__75009,G__75010,G__75011,G__75012));

break;
case "compilation":
var G__75013 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__75014 = cause_type;
var G__75015 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__75016 = loc;
var G__75017 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__75013,G__75014,G__75015,G__75016,G__75017) : format.call(null,G__75013,G__75014,G__75015,G__75016,G__75017));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__75018 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__75019 = symbol;
var G__75020 = loc;
var G__75021 = (function (){var sb__4667__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__75022_75231 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__75023_75232 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__75024_75233 = true;
var _STAR_print_fn_STAR__temp_val__75025_75234 = (function (x__4668__auto__){
return sb__4667__auto__.append(x__4668__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__75024_75233);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__75025_75234);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__74989_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__74989_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__75023_75232);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__75022_75231);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4667__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__75018,G__75019,G__75020,G__75021) : format.call(null,G__75018,G__75019,G__75020,G__75021));
} else {
var G__75040 = "Execution error%s at %s(%s).\n%s\n";
var G__75041 = cause_type;
var G__75042 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__75043 = loc;
var G__75044 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__75040,G__75041,G__75042,G__75043,G__75044) : format.call(null,G__75040,G__75041,G__75042,G__75043,G__75044));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__74994__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map

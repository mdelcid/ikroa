var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./clojure.string.js");
require("./goog.dom.dom.js");
require("./goog.useragent.useragent.js");
require("./goog.useragent.product.js");
require("./goog.uri.uri.js");
require("./shadow.cljs.devtools.client.env.js");
require("./shadow.cljs.devtools.client.console.js");
require("./shadow.cljs.devtools.client.hud.js");
require("./shadow.cljs.devtools.client.websocket.js");
require("./shadow.cljs.devtools.client.shared.js");
require("./shadow.remote.runtime.api.js");
require("./shadow.remote.runtime.shared.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("shadow.cljs.devtools.client.browser.js");

goog.provide('shadow.cljs.devtools.client.browser');
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__4742__auto__ = [];
var len__4736__auto___53005 = arguments.length;
var i__4737__auto___53006 = (0);
while(true){
if((i__4737__auto___53006 < len__4736__auto___53005)){
args__4742__auto__.push((arguments[i__4737__auto___53006]));

var G__53007 = (i__4737__auto___53006 + (1));
i__4737__auto___53006 = G__53007;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
if(shadow.cljs.devtools.client.env.log){
if(cljs.core.seq(shadow.cljs.devtools.client.env.log_style)){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),shadow.cljs.devtools.client.env.log_style], null),args)));
} else {
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [["shadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join('')], null),args)));
}
} else {
return null;
}
}));

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq52517){
var G__52518 = cljs.core.first(seq52517);
var seq52517__$1 = cljs.core.next(seq52517);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__52518,seq52517__$1);
}));

shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__52534 = cljs.core.seq(sources);
var chunk__52536 = null;
var count__52537 = (0);
var i__52538 = (0);
while(true){
if((i__52538 < count__52537)){
var map__52579 = chunk__52536.cljs$core$IIndexed$_nth$arity$2(null,i__52538);
var map__52579__$1 = (((((!((map__52579 == null))))?(((((map__52579.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52579.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52579):map__52579);
var src = map__52579__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52579__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52579__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52579__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52579__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e52581){var e_53034 = e52581;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_53034);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_53034.message)].join('')));
}

var G__53039 = seq__52534;
var G__53040 = chunk__52536;
var G__53041 = count__52537;
var G__53042 = (i__52538 + (1));
seq__52534 = G__53039;
chunk__52536 = G__53040;
count__52537 = G__53041;
i__52538 = G__53042;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__52534);
if(temp__5735__auto__){
var seq__52534__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__52534__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__52534__$1);
var G__53047 = cljs.core.chunk_rest(seq__52534__$1);
var G__53048 = c__4556__auto__;
var G__53049 = cljs.core.count(c__4556__auto__);
var G__53050 = (0);
seq__52534 = G__53047;
chunk__52536 = G__53048;
count__52537 = G__53049;
i__52538 = G__53050;
continue;
} else {
var map__52586 = cljs.core.first(seq__52534__$1);
var map__52586__$1 = (((((!((map__52586 == null))))?(((((map__52586.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52586.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52586):map__52586);
var src = map__52586__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52586__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52586__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52586__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52586__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e52592){var e_53061 = e52592;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_53061);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_53061.message)].join('')));
}

var G__53062 = cljs.core.next(seq__52534__$1);
var G__53063 = null;
var G__53064 = (0);
var G__53065 = (0);
seq__52534 = G__53062;
chunk__52536 = G__53063;
count__52537 = G__53064;
i__52538 = G__53065;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return null;
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__52602 = cljs.core.seq(js_requires);
var chunk__52603 = null;
var count__52604 = (0);
var i__52605 = (0);
while(true){
if((i__52605 < count__52604)){
var js_ns = chunk__52603.cljs$core$IIndexed$_nth$arity$2(null,i__52605);
var require_str_53066 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_53066);


var G__53069 = seq__52602;
var G__53070 = chunk__52603;
var G__53071 = count__52604;
var G__53072 = (i__52605 + (1));
seq__52602 = G__53069;
chunk__52603 = G__53070;
count__52604 = G__53071;
i__52605 = G__53072;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__52602);
if(temp__5735__auto__){
var seq__52602__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__52602__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__52602__$1);
var G__53073 = cljs.core.chunk_rest(seq__52602__$1);
var G__53074 = c__4556__auto__;
var G__53075 = cljs.core.count(c__4556__auto__);
var G__53076 = (0);
seq__52602 = G__53073;
chunk__52603 = G__53074;
count__52604 = G__53075;
i__52605 = G__53076;
continue;
} else {
var js_ns = cljs.core.first(seq__52602__$1);
var require_str_53077 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_53077);


var G__53078 = cljs.core.next(seq__52602__$1);
var G__53079 = null;
var G__53080 = (0);
var G__53081 = (0);
seq__52602 = G__53078;
chunk__52603 = G__53079;
count__52604 = G__53080;
i__52605 = G__53081;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(runtime,p__52619){
var map__52620 = p__52619;
var map__52620__$1 = (((((!((map__52620 == null))))?(((((map__52620.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52620.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52620):map__52620);
var msg = map__52620__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52620__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52620__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__4529__auto__ = (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__52630(s__52632){
return (new cljs.core.LazySeq(null,(function (){
var s__52632__$1 = s__52632;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__52632__$1);
if(temp__5735__auto__){
var xs__6292__auto__ = temp__5735__auto__;
var map__52657 = cljs.core.first(xs__6292__auto__);
var map__52657__$1 = (((((!((map__52657 == null))))?(((((map__52657.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52657.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52657):map__52657);
var src = map__52657__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52657__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52657__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__4525__auto__ = ((function (s__52632__$1,map__52657,map__52657__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__52620,map__52620__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__52630_$_iter__52636(s__52637){
return (new cljs.core.LazySeq(null,((function (s__52632__$1,map__52657,map__52657__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__52620,map__52620__$1,msg,info,reload_info){
return (function (){
var s__52637__$1 = s__52637;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__52637__$1);
if(temp__5735__auto____$1){
var s__52637__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__52637__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__52637__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__52640 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__52638 = (0);
while(true){
if((i__52638 < size__4528__auto__)){
var warning = cljs.core._nth(c__4527__auto__,i__52638);
cljs.core.chunk_append(b__52640,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__53085 = (i__52638 + (1));
i__52638 = G__53085;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__52640),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__52630_$_iter__52636(cljs.core.chunk_rest(s__52637__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__52640),null);
}
} else {
var warning = cljs.core.first(s__52637__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__52630_$_iter__52636(cljs.core.rest(s__52637__$2)));
}
} else {
return null;
}
break;
}
});})(s__52632__$1,map__52657,map__52657__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__52620,map__52620__$1,msg,info,reload_info))
,null,null));
});})(s__52632__$1,map__52657,map__52657__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__52620,map__52620__$1,msg,info,reload_info))
;
var fs__4526__auto__ = cljs.core.seq(iterys__4525__auto__(warnings));
if(fs__4526__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4526__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__52630(cljs.core.rest(s__52632__$1)));
} else {
var G__53087 = cljs.core.rest(s__52632__$1);
s__52632__$1 = G__53087;
continue;
}
} else {
var G__53088 = cljs.core.rest(s__52632__$1);
s__52632__$1 = G__53088;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(new cljs.core.Keyword(null,"sources","sources",-321166424).cljs$core$IFn$_invoke$arity$1(info));
})()));
if(shadow.cljs.devtools.client.env.log){
var seq__52664_53089 = cljs.core.seq(warnings);
var chunk__52665_53090 = null;
var count__52666_53091 = (0);
var i__52667_53092 = (0);
while(true){
if((i__52667_53092 < count__52666_53091)){
var map__52676_53093 = chunk__52665_53090.cljs$core$IIndexed$_nth$arity$2(null,i__52667_53092);
var map__52676_53094__$1 = (((((!((map__52676_53093 == null))))?(((((map__52676_53093.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52676_53093.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52676_53093):map__52676_53093);
var w_53095 = map__52676_53094__$1;
var msg_53096__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52676_53094__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_53097 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52676_53094__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_53098 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52676_53094__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_53099 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52676_53094__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_53099)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_53097),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_53098),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_53096__$1)].join(''));


var G__53100 = seq__52664_53089;
var G__53101 = chunk__52665_53090;
var G__53102 = count__52666_53091;
var G__53103 = (i__52667_53092 + (1));
seq__52664_53089 = G__53100;
chunk__52665_53090 = G__53101;
count__52666_53091 = G__53102;
i__52667_53092 = G__53103;
continue;
} else {
var temp__5735__auto___53104 = cljs.core.seq(seq__52664_53089);
if(temp__5735__auto___53104){
var seq__52664_53105__$1 = temp__5735__auto___53104;
if(cljs.core.chunked_seq_QMARK_(seq__52664_53105__$1)){
var c__4556__auto___53107 = cljs.core.chunk_first(seq__52664_53105__$1);
var G__53108 = cljs.core.chunk_rest(seq__52664_53105__$1);
var G__53109 = c__4556__auto___53107;
var G__53110 = cljs.core.count(c__4556__auto___53107);
var G__53111 = (0);
seq__52664_53089 = G__53108;
chunk__52665_53090 = G__53109;
count__52666_53091 = G__53110;
i__52667_53092 = G__53111;
continue;
} else {
var map__52683_53113 = cljs.core.first(seq__52664_53105__$1);
var map__52683_53114__$1 = (((((!((map__52683_53113 == null))))?(((((map__52683_53113.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52683_53113.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52683_53113):map__52683_53113);
var w_53115 = map__52683_53114__$1;
var msg_53116__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52683_53114__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_53117 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52683_53114__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_53118 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52683_53114__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_53119 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52683_53114__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_53119)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_53117),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_53118),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_53116__$1)].join(''));


var G__53120 = cljs.core.next(seq__52664_53105__$1);
var G__53121 = null;
var G__53122 = (0);
var G__53123 = (0);
seq__52664_53089 = G__53120;
chunk__52665_53090 = G__53121;
count__52666_53091 = G__53122;
i__52667_53092 = G__53123;
continue;
}
} else {
}
}
break;
}
} else {
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = shadow.cljs.devtools.client.env.filter_reload_sources(info,reload_info);
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.shared.load_sources(runtime,sources_to_get,(function (p1__52618_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__52618_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
}));
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[rel_new,"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())))){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$)){
return new$;
} else {
return false;
}
} else {
return false;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_update = (function shadow$cljs$devtools$client$browser$handle_asset_update(p__52707){
var map__52708 = p__52707;
var map__52708__$1 = (((((!((map__52708 == null))))?(((((map__52708.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52708.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52708):map__52708);
var msg = map__52708__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52708__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var seq__52715 = cljs.core.seq(updates);
var chunk__52717 = null;
var count__52718 = (0);
var i__52719 = (0);
while(true){
if((i__52719 < count__52718)){
var path = chunk__52717.cljs$core$IIndexed$_nth$arity$2(null,i__52719);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__52791_53127 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__52795_53128 = null;
var count__52796_53129 = (0);
var i__52797_53130 = (0);
while(true){
if((i__52797_53130 < count__52796_53129)){
var node_53132 = chunk__52795_53128.cljs$core$IIndexed$_nth$arity$2(null,i__52797_53130);
if(cljs.core.not(node_53132.shadow$old)){
var path_match_53133 = shadow.cljs.devtools.client.browser.match_paths(node_53132.getAttribute("href"),path);
if(cljs.core.truth_(path_match_53133)){
var new_link_53134 = (function (){var G__52819 = node_53132.cloneNode(true);
G__52819.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_53133),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__52819;
})();
(node_53132.shadow$old = true);

(new_link_53134.onload = ((function (seq__52791_53127,chunk__52795_53128,count__52796_53129,i__52797_53130,seq__52715,chunk__52717,count__52718,i__52719,new_link_53134,path_match_53133,node_53132,path,map__52708,map__52708__$1,msg,updates){
return (function (e){
return goog.dom.removeNode(node_53132);
});})(seq__52791_53127,chunk__52795_53128,count__52796_53129,i__52797_53130,seq__52715,chunk__52717,count__52718,i__52719,new_link_53134,path_match_53133,node_53132,path,map__52708,map__52708__$1,msg,updates))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_53133], 0));

goog.dom.insertSiblingAfter(new_link_53134,node_53132);


var G__53135 = seq__52791_53127;
var G__53136 = chunk__52795_53128;
var G__53137 = count__52796_53129;
var G__53138 = (i__52797_53130 + (1));
seq__52791_53127 = G__53135;
chunk__52795_53128 = G__53136;
count__52796_53129 = G__53137;
i__52797_53130 = G__53138;
continue;
} else {
var G__53139 = seq__52791_53127;
var G__53140 = chunk__52795_53128;
var G__53141 = count__52796_53129;
var G__53142 = (i__52797_53130 + (1));
seq__52791_53127 = G__53139;
chunk__52795_53128 = G__53140;
count__52796_53129 = G__53141;
i__52797_53130 = G__53142;
continue;
}
} else {
var G__53143 = seq__52791_53127;
var G__53144 = chunk__52795_53128;
var G__53145 = count__52796_53129;
var G__53146 = (i__52797_53130 + (1));
seq__52791_53127 = G__53143;
chunk__52795_53128 = G__53144;
count__52796_53129 = G__53145;
i__52797_53130 = G__53146;
continue;
}
} else {
var temp__5735__auto___53147 = cljs.core.seq(seq__52791_53127);
if(temp__5735__auto___53147){
var seq__52791_53148__$1 = temp__5735__auto___53147;
if(cljs.core.chunked_seq_QMARK_(seq__52791_53148__$1)){
var c__4556__auto___53150 = cljs.core.chunk_first(seq__52791_53148__$1);
var G__53151 = cljs.core.chunk_rest(seq__52791_53148__$1);
var G__53152 = c__4556__auto___53150;
var G__53153 = cljs.core.count(c__4556__auto___53150);
var G__53154 = (0);
seq__52791_53127 = G__53151;
chunk__52795_53128 = G__53152;
count__52796_53129 = G__53153;
i__52797_53130 = G__53154;
continue;
} else {
var node_53155 = cljs.core.first(seq__52791_53148__$1);
if(cljs.core.not(node_53155.shadow$old)){
var path_match_53156 = shadow.cljs.devtools.client.browser.match_paths(node_53155.getAttribute("href"),path);
if(cljs.core.truth_(path_match_53156)){
var new_link_53157 = (function (){var G__52827 = node_53155.cloneNode(true);
G__52827.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_53156),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__52827;
})();
(node_53155.shadow$old = true);

(new_link_53157.onload = ((function (seq__52791_53127,chunk__52795_53128,count__52796_53129,i__52797_53130,seq__52715,chunk__52717,count__52718,i__52719,new_link_53157,path_match_53156,node_53155,seq__52791_53148__$1,temp__5735__auto___53147,path,map__52708,map__52708__$1,msg,updates){
return (function (e){
return goog.dom.removeNode(node_53155);
});})(seq__52791_53127,chunk__52795_53128,count__52796_53129,i__52797_53130,seq__52715,chunk__52717,count__52718,i__52719,new_link_53157,path_match_53156,node_53155,seq__52791_53148__$1,temp__5735__auto___53147,path,map__52708,map__52708__$1,msg,updates))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_53156], 0));

goog.dom.insertSiblingAfter(new_link_53157,node_53155);


var G__53158 = cljs.core.next(seq__52791_53148__$1);
var G__53159 = null;
var G__53160 = (0);
var G__53161 = (0);
seq__52791_53127 = G__53158;
chunk__52795_53128 = G__53159;
count__52796_53129 = G__53160;
i__52797_53130 = G__53161;
continue;
} else {
var G__53162 = cljs.core.next(seq__52791_53148__$1);
var G__53163 = null;
var G__53164 = (0);
var G__53165 = (0);
seq__52791_53127 = G__53162;
chunk__52795_53128 = G__53163;
count__52796_53129 = G__53164;
i__52797_53130 = G__53165;
continue;
}
} else {
var G__53166 = cljs.core.next(seq__52791_53148__$1);
var G__53167 = null;
var G__53168 = (0);
var G__53169 = (0);
seq__52791_53127 = G__53166;
chunk__52795_53128 = G__53167;
count__52796_53129 = G__53168;
i__52797_53130 = G__53169;
continue;
}
}
} else {
}
}
break;
}


var G__53170 = seq__52715;
var G__53171 = chunk__52717;
var G__53172 = count__52718;
var G__53173 = (i__52719 + (1));
seq__52715 = G__53170;
chunk__52717 = G__53171;
count__52718 = G__53172;
i__52719 = G__53173;
continue;
} else {
var G__53174 = seq__52715;
var G__53175 = chunk__52717;
var G__53176 = count__52718;
var G__53177 = (i__52719 + (1));
seq__52715 = G__53174;
chunk__52717 = G__53175;
count__52718 = G__53176;
i__52719 = G__53177;
continue;
}
} else {
var temp__5735__auto__ = cljs.core.seq(seq__52715);
if(temp__5735__auto__){
var seq__52715__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__52715__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__52715__$1);
var G__53178 = cljs.core.chunk_rest(seq__52715__$1);
var G__53179 = c__4556__auto__;
var G__53180 = cljs.core.count(c__4556__auto__);
var G__53181 = (0);
seq__52715 = G__53178;
chunk__52717 = G__53179;
count__52718 = G__53180;
i__52719 = G__53181;
continue;
} else {
var path = cljs.core.first(seq__52715__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__52837_53182 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__52841_53183 = null;
var count__52842_53184 = (0);
var i__52843_53185 = (0);
while(true){
if((i__52843_53185 < count__52842_53184)){
var node_53186 = chunk__52841_53183.cljs$core$IIndexed$_nth$arity$2(null,i__52843_53185);
if(cljs.core.not(node_53186.shadow$old)){
var path_match_53187 = shadow.cljs.devtools.client.browser.match_paths(node_53186.getAttribute("href"),path);
if(cljs.core.truth_(path_match_53187)){
var new_link_53188 = (function (){var G__52865 = node_53186.cloneNode(true);
G__52865.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_53187),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__52865;
})();
(node_53186.shadow$old = true);

(new_link_53188.onload = ((function (seq__52837_53182,chunk__52841_53183,count__52842_53184,i__52843_53185,seq__52715,chunk__52717,count__52718,i__52719,new_link_53188,path_match_53187,node_53186,path,seq__52715__$1,temp__5735__auto__,map__52708,map__52708__$1,msg,updates){
return (function (e){
return goog.dom.removeNode(node_53186);
});})(seq__52837_53182,chunk__52841_53183,count__52842_53184,i__52843_53185,seq__52715,chunk__52717,count__52718,i__52719,new_link_53188,path_match_53187,node_53186,path,seq__52715__$1,temp__5735__auto__,map__52708,map__52708__$1,msg,updates))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_53187], 0));

goog.dom.insertSiblingAfter(new_link_53188,node_53186);


var G__53189 = seq__52837_53182;
var G__53190 = chunk__52841_53183;
var G__53191 = count__52842_53184;
var G__53192 = (i__52843_53185 + (1));
seq__52837_53182 = G__53189;
chunk__52841_53183 = G__53190;
count__52842_53184 = G__53191;
i__52843_53185 = G__53192;
continue;
} else {
var G__53196 = seq__52837_53182;
var G__53197 = chunk__52841_53183;
var G__53198 = count__52842_53184;
var G__53199 = (i__52843_53185 + (1));
seq__52837_53182 = G__53196;
chunk__52841_53183 = G__53197;
count__52842_53184 = G__53198;
i__52843_53185 = G__53199;
continue;
}
} else {
var G__53200 = seq__52837_53182;
var G__53201 = chunk__52841_53183;
var G__53202 = count__52842_53184;
var G__53203 = (i__52843_53185 + (1));
seq__52837_53182 = G__53200;
chunk__52841_53183 = G__53201;
count__52842_53184 = G__53202;
i__52843_53185 = G__53203;
continue;
}
} else {
var temp__5735__auto___53204__$1 = cljs.core.seq(seq__52837_53182);
if(temp__5735__auto___53204__$1){
var seq__52837_53205__$1 = temp__5735__auto___53204__$1;
if(cljs.core.chunked_seq_QMARK_(seq__52837_53205__$1)){
var c__4556__auto___53206 = cljs.core.chunk_first(seq__52837_53205__$1);
var G__53207 = cljs.core.chunk_rest(seq__52837_53205__$1);
var G__53208 = c__4556__auto___53206;
var G__53209 = cljs.core.count(c__4556__auto___53206);
var G__53210 = (0);
seq__52837_53182 = G__53207;
chunk__52841_53183 = G__53208;
count__52842_53184 = G__53209;
i__52843_53185 = G__53210;
continue;
} else {
var node_53211 = cljs.core.first(seq__52837_53205__$1);
if(cljs.core.not(node_53211.shadow$old)){
var path_match_53212 = shadow.cljs.devtools.client.browser.match_paths(node_53211.getAttribute("href"),path);
if(cljs.core.truth_(path_match_53212)){
var new_link_53213 = (function (){var G__52873 = node_53211.cloneNode(true);
G__52873.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_53212),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__52873;
})();
(node_53211.shadow$old = true);

(new_link_53213.onload = ((function (seq__52837_53182,chunk__52841_53183,count__52842_53184,i__52843_53185,seq__52715,chunk__52717,count__52718,i__52719,new_link_53213,path_match_53212,node_53211,seq__52837_53205__$1,temp__5735__auto___53204__$1,path,seq__52715__$1,temp__5735__auto__,map__52708,map__52708__$1,msg,updates){
return (function (e){
return goog.dom.removeNode(node_53211);
});})(seq__52837_53182,chunk__52841_53183,count__52842_53184,i__52843_53185,seq__52715,chunk__52717,count__52718,i__52719,new_link_53213,path_match_53212,node_53211,seq__52837_53205__$1,temp__5735__auto___53204__$1,path,seq__52715__$1,temp__5735__auto__,map__52708,map__52708__$1,msg,updates))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_53212], 0));

goog.dom.insertSiblingAfter(new_link_53213,node_53211);


var G__53214 = cljs.core.next(seq__52837_53205__$1);
var G__53215 = null;
var G__53216 = (0);
var G__53217 = (0);
seq__52837_53182 = G__53214;
chunk__52841_53183 = G__53215;
count__52842_53184 = G__53216;
i__52843_53185 = G__53217;
continue;
} else {
var G__53218 = cljs.core.next(seq__52837_53205__$1);
var G__53219 = null;
var G__53220 = (0);
var G__53221 = (0);
seq__52837_53182 = G__53218;
chunk__52841_53183 = G__53219;
count__52842_53184 = G__53220;
i__52843_53185 = G__53221;
continue;
}
} else {
var G__53222 = cljs.core.next(seq__52837_53205__$1);
var G__53223 = null;
var G__53224 = (0);
var G__53225 = (0);
seq__52837_53182 = G__53222;
chunk__52841_53183 = G__53223;
count__52842_53184 = G__53224;
i__52843_53185 = G__53225;
continue;
}
}
} else {
}
}
break;
}


var G__53226 = cljs.core.next(seq__52715__$1);
var G__53227 = null;
var G__53228 = (0);
var G__53229 = (0);
seq__52715 = G__53226;
chunk__52717 = G__53227;
count__52718 = G__53228;
i__52719 = G__53229;
continue;
} else {
var G__53230 = cljs.core.next(seq__52715__$1);
var G__53231 = null;
var G__53232 = (0);
var G__53233 = (0);
seq__52715 = G__53230;
chunk__52717 = G__53231;
count__52718 = G__53232;
i__52719 = G__53233;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2("undefined",typeof(module))){
return eval(js);
} else {
return (0,eval)(js);;
}
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(runtime,p__52880){
var map__52881 = p__52880;
var map__52881__$1 = (((((!((map__52881 == null))))?(((((map__52881.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52881.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52881):map__52881);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52881__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
return shadow.cljs.devtools.client.shared.load_sources(runtime,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return shadow.cljs.devtools.client.browser.devtools_msg("ready!");
}));
});
shadow.cljs.devtools.client.browser.client_info = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"host","host",-1558485167),(cljs.core.truth_(goog.global.document)?new cljs.core.Keyword(null,"browser","browser",828191719):new cljs.core.Keyword(null,"browser-worker","browser-worker",1638998282)),new cljs.core.Keyword(null,"user-agent","user-agent",1220426212),[(cljs.core.truth_(goog.userAgent.OPERA)?"Opera":(cljs.core.truth_(goog.userAgent.product.CHROME)?"Chrome":(cljs.core.truth_(goog.userAgent.IE)?"MSIE":(cljs.core.truth_(goog.userAgent.EDGE)?"Edge":(cljs.core.truth_(goog.userAgent.GECKO)?"Firefox":(cljs.core.truth_(goog.userAgent.SAFARI)?"Safari":(cljs.core.truth_(goog.userAgent.WEBKIT)?"Webkit":null)))))))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.VERSION)," [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.PLATFORM),"]"].join(''),new cljs.core.Keyword(null,"dom","dom",-1236537922),(!((goog.global.document == null)))], null);
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.ws_was_welcome_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.ws_was_welcome_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
}
if(((shadow.cljs.devtools.client.env.enabled) && ((shadow.cljs.devtools.client.env.worker_client_id > (0))))){
(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$_js_eval$arity$2 = (function (this$,code){
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(code);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_invoke$arity$2 = (function (this$,p__52909){
var map__52910 = p__52909;
var map__52910__$1 = (((((!((map__52910 == null))))?(((((map__52910.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52910.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52910):map__52910);
var _ = map__52910__$1;
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52910__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(js);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_init$arity$4 = (function (runtime,p__52915,done,error){
var map__52916 = p__52915;
var map__52916__$1 = (((((!((map__52916 == null))))?(((((map__52916.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52916.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52916):map__52916);
var repl_sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52916__$1,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535));
var runtime__$1 = this;
return shadow.cljs.devtools.client.shared.load_sources(runtime__$1,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,repl_sources)),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}));
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_require$arity$4 = (function (runtime,p__52922,done,error){
var map__52923 = p__52922;
var map__52923__$1 = (((((!((map__52923 == null))))?(((((map__52923.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52923.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52923):map__52923);
var msg = map__52923__$1;
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52923__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52923__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52923__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var runtime__$1 = this;
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__52925){
var map__52926 = p__52925;
var map__52926__$1 = (((((!((map__52926 == null))))?(((((map__52926.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52926.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52926):map__52926);
var src = map__52926__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52926__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__4115__auto__ = shadow.cljs.devtools.client.env.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__4115__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__4115__auto__;
}
}),sources));
if(cljs.core.not(cljs.core.seq(sources_to_load))){
var G__52928 = cljs.core.PersistentVector.EMPTY;
return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(G__52928) : done.call(null,G__52928));
} else {
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3(runtime__$1,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"cljs-load-sources","cljs-load-sources",-1458295962),new cljs.core.Keyword(null,"to","to",192099007),shadow.cljs.devtools.client.env.worker_client_id,new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources_to_load)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cljs-sources","cljs-sources",31121610),(function (p__52929){
var map__52930 = p__52929;
var map__52930__$1 = (((((!((map__52930 == null))))?(((((map__52930.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52930.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52930):map__52930);
var msg__$1 = map__52930__$1;
var sources__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52930__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(sources_to_load) : done.call(null,sources_to_load));
}catch (e52936){var ex = e52936;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(ex) : error.call(null,ex));
}})], null));
}
}));

shadow.cljs.devtools.client.shared.add_plugin_BANG_(new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),cljs.core.PersistentHashSet.EMPTY,(function (p__52937){
var map__52938 = p__52937;
var map__52938__$1 = (((((!((map__52938 == null))))?(((((map__52938.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52938.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52938):map__52938);
var env = map__52938__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52938__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var svc = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125),(function (){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,true);

shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.env.patch_goog_BANG_();

return shadow.cljs.devtools.client.browser.devtools_msg(["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(new cljs.core.Keyword(null,"state-ref","state-ref",2127874952).cljs$core$IFn$_invoke$arity$1(runtime))))," ready!"].join(''));
}),new cljs.core.Keyword(null,"on-disconnect","on-disconnect",-809021814),(function (e){
if(cljs.core.truth_(cljs.core.deref(shadow.cljs.devtools.client.browser.ws_was_welcome_ref))){
shadow.cljs.devtools.client.hud.connection_error("The Websocket connection was closed!");

return cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);
} else {
return null;
}
}),new cljs.core.Keyword(null,"on-reconnect","on-reconnect",1239988702),(function (e){
return shadow.cljs.devtools.client.hud.connection_error("Reconnecting ...");
}),new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"access-denied","access-denied",959449406),(function (msg){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);

return shadow.cljs.devtools.client.hud.connection_error(["Stale Output! Your loaded JS was not produced by the running shadow-cljs instance."," Is the watch for this build running?"].join(''));
}),new cljs.core.Keyword(null,"cljs-runtime-init","cljs-runtime-init",1305890232),(function (msg){
return shadow.cljs.devtools.client.browser.repl_init(runtime,msg);
}),new cljs.core.Keyword(null,"cljs-asset-update","cljs-asset-update",1224093028),(function (p__52943){
var map__52945 = p__52943;
var map__52945__$1 = (((((!((map__52945 == null))))?(((((map__52945.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52945.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52945):map__52945);
var msg = map__52945__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52945__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
return shadow.cljs.devtools.client.browser.handle_asset_update(msg);
}),new cljs.core.Keyword(null,"cljs-build-configure","cljs-build-configure",-2089891268),(function (msg){
return null;
}),new cljs.core.Keyword(null,"cljs-build-start","cljs-build-start",-725781241),(function (msg){
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-start","build-start",-959649480)));
}),new cljs.core.Keyword(null,"cljs-build-complete","cljs-build-complete",273626153),(function (msg){
var msg__$1 = shadow.cljs.devtools.client.env.add_warnings_to_info(msg);
shadow.cljs.devtools.client.hud.hud_warnings(msg__$1);

shadow.cljs.devtools.client.browser.handle_build_complete(runtime,msg__$1);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg__$1,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-complete","build-complete",-501868472)));
}),new cljs.core.Keyword(null,"cljs-build-failure","cljs-build-failure",1718154990),(function (msg){
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-failure","build-failure",-2107487466)));
}),new cljs.core.Keyword("shadow.cljs.devtools.client.env","worker-notify","shadow.cljs.devtools.client.env/worker-notify",-1456820670),(function (p__52967){
var map__52968 = p__52967;
var map__52968__$1 = (((((!((map__52968 == null))))?(((((map__52968.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52968.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52968):map__52968);
var event_op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52968__$1,new cljs.core.Keyword(null,"event-op","event-op",200358057));
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52968__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-disconnect","client-disconnect",640227957),event_op)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(client_id,shadow.cljs.devtools.client.env.worker_client_id)))){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was stopped!");
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-connect","client-connect",-1113973888),event_op)){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was restarted. Reload required!");
} else {
return null;
}
}
})], null)], null));

return svc;
}),(function (p__52983){
var map__52985 = p__52983;
var map__52985__$1 = (((((!((map__52985 == null))))?(((((map__52985.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__52985.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__52985):map__52985);
var svc = map__52985__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__52985__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282));
}));

shadow.cljs.devtools.client.shared.init_runtime_BANG_(shadow.cljs.devtools.client.browser.client_info,shadow.cljs.devtools.client.websocket.start,shadow.cljs.devtools.client.websocket.send,shadow.cljs.devtools.client.websocket.stop);
} else {
}
Object.defineProperty(module.exports, "script_eval", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.script_eval; } });
Object.defineProperty(module.exports, "global_eval", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.global_eval; } });
Object.defineProperty(module.exports, "repl_init", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.repl_init; } });
Object.defineProperty(module.exports, "do_js_load", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.do_js_load; } });
Object.defineProperty(module.exports, "handle_asset_update", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.handle_asset_update; } });
Object.defineProperty(module.exports, "page_load_uri", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.page_load_uri; } });
Object.defineProperty(module.exports, "do_js_requires", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.do_js_requires; } });
Object.defineProperty(module.exports, "client_info", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.client_info; } });
Object.defineProperty(module.exports, "match_paths", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.match_paths; } });
Object.defineProperty(module.exports, "devtools_msg", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.devtools_msg; } });
Object.defineProperty(module.exports, "do_js_reload", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.do_js_reload; } });
Object.defineProperty(module.exports, "ws_was_welcome_ref", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.ws_was_welcome_ref; } });
Object.defineProperty(module.exports, "handle_build_complete", { enumerable: false, get: function() { return shadow.cljs.devtools.client.browser.handle_build_complete; } });
//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map

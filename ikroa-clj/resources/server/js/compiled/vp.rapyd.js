var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./cljs.core.async.js");
require("./vp.macros.js");
require("./shadow.js.shim.module$meteor$fetch.js");
require("./cljs.core.async.interop.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var camel_snake_kebab=$CLJS.camel_snake_kebab || ($CLJS.camel_snake_kebab = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("vp.rapyd.js");

goog.provide('vp.rapyd');
vp.rapyd.secret_key = "cfa6dc18129d96c404b540dc4a2d077b6c4093234e94a68f3b9b1a5c890d838e3e4501f6d5f8d2db";
vp.rapyd.access_key = "D301146F3586152707B1";
vp.rapyd.base_url = "https://sandboxapi.rapyd.net";
vp.rapyd.gensalt = (function vp$rapyd$gensalt(n){
var charseq = cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.char$,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(cljs.core.range.cljs$core$IFn$_invoke$arity$2((48),(58)),cljs.core.range.cljs$core$IFn$_invoke$arity$2((97),(123))));
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.take.cljs$core$IFn$_invoke$arity$2(n,cljs.core.repeatedly.cljs$core$IFn$_invoke$arity$1((function (){
return cljs.core.rand_nth(charseq);
}))));
});
vp.rapyd.methods_map = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"get","get",1683182755),"GET",new cljs.core.Keyword(null,"post","post",269697687),"POST"], null);
vp.rapyd.req = (function vp$rapyd$req(var_args){
var args__4742__auto__ = [];
var len__4736__auto___68599 = arguments.length;
var i__4737__auto___68601 = (0);
while(true){
if((i__4737__auto___68601 < len__4736__auto___68599)){
args__4742__auto__.push((arguments[i__4737__auto___68601]));

var G__68604 = (i__4737__auto___68601 + (1));
i__4737__auto___68601 = G__68604;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((2) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((2)),(0),null)):null);
return vp.rapyd.req.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4743__auto__);
});

(vp.rapyd.req.cljs$core$IFn$_invoke$arity$variadic = (function (method,uri,p__68299){
var vec__68304 = p__68299;
var body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__68304,(0),null);
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_68400){
var state_val_68402 = (state_68400[(1)]);
if((state_val_68402 === (7))){
var inst_68333 = (state_68400[(7)]);
var inst_68321 = (state_68400[(8)]);
var inst_68328 = (state_68400[(2)]);
var inst_68331 = signature(inst_68321,uri,inst_68328);
var inst_68333__$1 = cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(inst_68331,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",1310784252),true], 0));
var inst_68337 = (inst_68333__$1 == null);
var inst_68338 = cljs.core.not(inst_68337);
var state_68400__$1 = (function (){var statearr_68403 = state_68400;
(statearr_68403[(7)] = inst_68333__$1);

return statearr_68403;
})();
if(inst_68338){
var statearr_68406_68605 = state_68400__$1;
(statearr_68406_68605[(1)] = (8));

} else {
var statearr_68408_68607 = state_68400__$1;
(statearr_68408_68607[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (1))){
var state_68400__$1 = state_68400;
if(cljs.core.truth_(body)){
var statearr_68411_68611 = state_68400__$1;
(statearr_68411_68611[(1)] = (2));

} else {
var statearr_68412_68620 = state_68400__$1;
(statearr_68412_68620[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (4))){
var inst_68318 = (state_68400[(9)]);
var inst_68318__$1 = (state_68400[(2)]);
var inst_68321 = cljs.core.name(method);
var state_68400__$1 = (function (){var statearr_68414 = state_68400;
(statearr_68414[(8)] = inst_68321);

(statearr_68414[(9)] = inst_68318__$1);

return statearr_68414;
})();
if(cljs.core.truth_(inst_68318__$1)){
var statearr_68417_68624 = state_68400__$1;
(statearr_68417_68624[(1)] = (5));

} else {
var statearr_68418_68626 = state_68400__$1;
(statearr_68418_68626[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (15))){
var inst_68333 = (state_68400[(7)]);
var state_68400__$1 = state_68400;
var statearr_68420_68628 = state_68400__$1;
(statearr_68420_68628[(2)] = inst_68333);

(statearr_68420_68628[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (13))){
var inst_68357 = (state_68400[(2)]);
var state_68400__$1 = state_68400;
var statearr_68422_68630 = state_68400__$1;
(statearr_68422_68630[(2)] = inst_68357);

(statearr_68422_68630[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (6))){
var state_68400__$1 = state_68400;
var statearr_68424_68632 = state_68400__$1;
(statearr_68424_68632[(2)] = "");

(statearr_68424_68632[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (17))){
var inst_68395 = (state_68400[(2)]);
var state_68400__$1 = state_68400;
return cljs.core.async.impl.ioc_helpers.return_chan(state_68400__$1,inst_68395);
} else {
if((state_val_68402 === (3))){
var state_68400__$1 = state_68400;
var statearr_68426_68635 = state_68400__$1;
(statearr_68426_68635[(2)] = null);

(statearr_68426_68635[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (12))){
var state_68400__$1 = state_68400;
var statearr_68428_68637 = state_68400__$1;
(statearr_68428_68637[(2)] = false);

(statearr_68428_68637[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (2))){
var inst_68311 = cljs.core.clj__GT_js(body);
var inst_68312 = JSON.stringify(inst_68311);
var state_68400__$1 = state_68400;
var statearr_68430_68642 = state_68400__$1;
(statearr_68430_68642[(2)] = inst_68312);

(statearr_68430_68642[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (11))){
var state_68400__$1 = state_68400;
var statearr_68432_68644 = state_68400__$1;
(statearr_68432_68644[(2)] = true);

(statearr_68432_68644[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (9))){
var state_68400__$1 = state_68400;
var statearr_68434_68646 = state_68400__$1;
(statearr_68434_68646[(2)] = false);

(statearr_68434_68646[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (5))){
var inst_68318 = (state_68400[(9)]);
var state_68400__$1 = state_68400;
var statearr_68436_68655 = state_68400__$1;
(statearr_68436_68655[(2)] = inst_68318);

(statearr_68436_68655[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (14))){
var inst_68333 = (state_68400[(7)]);
var inst_68369 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_68333);
var state_68400__$1 = state_68400;
var statearr_68438_68671 = state_68400__$1;
(statearr_68438_68671[(2)] = inst_68369);

(statearr_68438_68671[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (16))){
var inst_68318 = (state_68400[(9)]);
var inst_68374 = (state_68400[(2)]);
var inst_68377 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_68374,new cljs.core.Keyword(null,"salt","salt",-587171712));
var inst_68379 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_68374,new cljs.core.Keyword(null,"signature","signature",1463754794));
var inst_68381 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_68374,new cljs.core.Keyword(null,"timestamp","timestamp",579478971));
var inst_68383 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var inst_68385 = (function (){var body_json_str = inst_68318;
var map__68307 = inst_68374;
var sigres = inst_68374;
var salt = inst_68377;
var signature = inst_68379;
var timestamp = inst_68381;
var chan__42104__auto__ = inst_68383;
return (function (){
var c__41437__auto____$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_68536){
var state_val_68538 = (state_68536[(1)]);
if((state_val_68538 === (7))){
var inst_68481 = (state_68536[(7)]);
var inst_68498 = (function(){throw inst_68481})();
var state_68536__$1 = state_68536;
var statearr_68539_68736 = state_68536__$1;
(statearr_68539_68736[(2)] = inst_68498);

(statearr_68539_68736[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68538 === (1))){
var inst_68440 = [vp.rapyd.base_url,cljs.core.str.cljs$core$IFn$_invoke$arity$1(uri)].join('');
var inst_68443 = [new cljs.core.Keyword(null,"method","method",55703592),new cljs.core.Keyword(null,"headers","headers",-835030129)];
var inst_68444 = (method.cljs$core$IFn$_invoke$arity$1 ? method.cljs$core$IFn$_invoke$arity$1(vp.rapyd.methods_map) : method.call(null,vp.rapyd.methods_map));
var inst_68446 = ["access_key","Content-Type","salt","timestamp","signature"];
var inst_68452 = cljs.core.str.cljs$core$IFn$_invoke$arity$1(timestamp);
var inst_68453 = [vp.rapyd.access_key,"application/json",salt,inst_68452,signature];
var inst_68454 = cljs.core.PersistentHashMap.fromArrays(inst_68446,inst_68453);
var inst_68455 = [inst_68444,inst_68454];
var inst_68457 = cljs.core.PersistentHashMap.fromArrays(inst_68443,inst_68455);
var state_68536__$1 = (function (){var statearr_68542 = state_68536;
(statearr_68542[(8)] = inst_68440);

(statearr_68542[(9)] = inst_68457);

return statearr_68542;
})();
if(cljs.core.truth_(body_json_str)){
var statearr_68545_68748 = state_68536__$1;
(statearr_68545_68748[(1)] = (4));

} else {
var statearr_68546_68751 = state_68536__$1;
(statearr_68546_68751[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68538 === (4))){
var inst_68462 = [new cljs.core.Keyword(null,"body","body",-2049205669)];
var inst_68468 = [body_json_str];
var inst_68470 = cljs.core.PersistentHashMap.fromArrays(inst_68462,inst_68468);
var state_68536__$1 = state_68536;
var statearr_68548_68754 = state_68536__$1;
(statearr_68548_68754[(2)] = inst_68470);

(statearr_68548_68754[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68538 === (6))){
var inst_68440 = (state_68536[(8)]);
var inst_68457 = (state_68536[(9)]);
var inst_68475 = (state_68536[(2)]);
var inst_68476 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([inst_68457,inst_68475], 0));
var inst_68477 = cljs.core.clj__GT_js(inst_68476);
var inst_68478 = shadow.js.shim.module$meteor$fetch.fetch(inst_68440,inst_68477);
var inst_68479 = cljs.core.async.interop.p__GT_c(inst_68478);
var state_68536__$1 = state_68536;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68536__$1,(3),inst_68479);
} else {
if((state_val_68538 === (3))){
var inst_68481 = (state_68536[(7)]);
var inst_68481__$1 = (state_68536[(2)]);
var inst_68484 = (inst_68481__$1 instanceof cljs.core.ExceptionInfo);
var inst_68487 = cljs.core.ex_data(inst_68481__$1);
var inst_68489 = new cljs.core.Keyword(null,"error","error",-978969032).cljs$core$IFn$_invoke$arity$1(inst_68487);
var inst_68491 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_68489,new cljs.core.Keyword(null,"promise-error","promise-error",-90673560));
var inst_68492 = ((inst_68484) && (inst_68491));
var state_68536__$1 = (function (){var statearr_68552 = state_68536;
(statearr_68552[(7)] = inst_68481__$1);

return statearr_68552;
})();
if(cljs.core.truth_(inst_68492)){
var statearr_68553_68772 = state_68536__$1;
(statearr_68553_68772[(1)] = (7));

} else {
var statearr_68554_68773 = state_68536__$1;
(statearr_68554_68773[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68538 === (12))){
var inst_68531 = (state_68536[(2)]);
var inst_68533 = cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(inst_68531,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",1310784252),true], 0));
var state_68536__$1 = state_68536;
return cljs.core.async.impl.ioc_helpers.return_chan(state_68536__$1,inst_68533);
} else {
if((state_val_68538 === (2))){
var inst_68510 = (state_68536[(10)]);
var inst_68510__$1 = (state_68536[(2)]);
var inst_68511 = (inst_68510__$1 instanceof cljs.core.ExceptionInfo);
var inst_68516 = cljs.core.ex_data(inst_68510__$1);
var inst_68518 = new cljs.core.Keyword(null,"error","error",-978969032).cljs$core$IFn$_invoke$arity$1(inst_68516);
var inst_68519 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_68518,new cljs.core.Keyword(null,"promise-error","promise-error",-90673560));
var inst_68520 = ((inst_68511) && (inst_68519));
var state_68536__$1 = (function (){var statearr_68558 = state_68536;
(statearr_68558[(10)] = inst_68510__$1);

return statearr_68558;
})();
if(cljs.core.truth_(inst_68520)){
var statearr_68560_68784 = state_68536__$1;
(statearr_68560_68784[(1)] = (10));

} else {
var statearr_68561_68786 = state_68536__$1;
(statearr_68561_68786[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68538 === (11))){
var inst_68510 = (state_68536[(10)]);
var state_68536__$1 = state_68536;
var statearr_68562_68790 = state_68536__$1;
(statearr_68562_68790[(2)] = inst_68510);

(statearr_68562_68790[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68538 === (9))){
var inst_68502 = (state_68536[(2)]);
var inst_68506 = inst_68502.json();
var inst_68507 = cljs.core.async.interop.p__GT_c(inst_68506);
var state_68536__$1 = state_68536;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68536__$1,(2),inst_68507);
} else {
if((state_val_68538 === (5))){
var state_68536__$1 = state_68536;
var statearr_68565_68799 = state_68536__$1;
(statearr_68565_68799[(2)] = null);

(statearr_68565_68799[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68538 === (10))){
var inst_68510 = (state_68536[(10)]);
var inst_68527 = (function(){throw inst_68510})();
var state_68536__$1 = state_68536;
var statearr_68567_68806 = state_68536__$1;
(statearr_68567_68806[(2)] = inst_68527);

(statearr_68567_68806[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68538 === (8))){
var inst_68481 = (state_68536[(7)]);
var state_68536__$1 = state_68536;
var statearr_68568_68811 = state_68536__$1;
(statearr_68568_68811[(2)] = inst_68481);

(statearr_68568_68811[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var vp$rapyd$state_machine__41292__auto__ = null;
var vp$rapyd$state_machine__41292__auto____0 = (function (){
var statearr_68571 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_68571[(0)] = vp$rapyd$state_machine__41292__auto__);

(statearr_68571[(1)] = (1));

return statearr_68571;
});
var vp$rapyd$state_machine__41292__auto____1 = (function (state_68536){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_68536);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e68574){var ex__41295__auto__ = e68574;
var statearr_68575_68818 = state_68536;
(statearr_68575_68818[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_68536[(4)]))){
var statearr_68576_68819 = state_68536;
(statearr_68576_68819[(1)] = cljs.core.first((state_68536[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__68822 = state_68536;
state_68536 = G__68822;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$rapyd$state_machine__41292__auto__ = function(state_68536){
switch(arguments.length){
case 0:
return vp$rapyd$state_machine__41292__auto____0.call(this);
case 1:
return vp$rapyd$state_machine__41292__auto____1.call(this,state_68536);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$rapyd$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$rapyd$state_machine__41292__auto____0;
vp$rapyd$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$rapyd$state_machine__41292__auto____1;
return vp$rapyd$state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_68578 = f__41438__auto__();
(statearr_68578[(6)] = c__41437__auto____$1);

return statearr_68578;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto____$1;
});
})();
var inst_68387 = (vp.meteor.bind_env.cljs$core$IFn$_invoke$arity$2 ? vp.meteor.bind_env.cljs$core$IFn$_invoke$arity$2(inst_68383,inst_68385) : vp.meteor.bind_env.call(null,inst_68383,inst_68385));
var state_68400__$1 = (function (){var statearr_68580 = state_68400;
(statearr_68580[(10)] = inst_68387);

return statearr_68580;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68400__$1,(18),inst_68383);
} else {
if((state_val_68402 === (10))){
var inst_68363 = (state_68400[(2)]);
var state_68400__$1 = state_68400;
if(cljs.core.truth_(inst_68363)){
var statearr_68583_68838 = state_68400__$1;
(statearr_68583_68838[(1)] = (14));

} else {
var statearr_68584_68846 = state_68400__$1;
(statearr_68584_68846[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_68402 === (18))){
var inst_68391 = (state_68400[(2)]);
var state_68400__$1 = state_68400;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68400__$1,(17),inst_68391);
} else {
if((state_val_68402 === (8))){
var inst_68333 = (state_68400[(7)]);
var inst_68343 = inst_68333.cljs$lang$protocol_mask$partition0$;
var inst_68344 = (inst_68343 & (64));
var inst_68347 = inst_68333.cljs$core$ISeq$;
var inst_68348 = (cljs.core.PROTOCOL_SENTINEL === inst_68347);
var inst_68349 = ((inst_68344) || (inst_68348));
var state_68400__$1 = state_68400;
if(cljs.core.truth_(inst_68349)){
var statearr_68587_68862 = state_68400__$1;
(statearr_68587_68862[(1)] = (11));

} else {
var statearr_68588_68864 = state_68400__$1;
(statearr_68588_68864[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var vp$rapyd$state_machine__41292__auto__ = null;
var vp$rapyd$state_machine__41292__auto____0 = (function (){
var statearr_68591 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_68591[(0)] = vp$rapyd$state_machine__41292__auto__);

(statearr_68591[(1)] = (1));

return statearr_68591;
});
var vp$rapyd$state_machine__41292__auto____1 = (function (state_68400){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_68400);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e68593){var ex__41295__auto__ = e68593;
var statearr_68595_68868 = state_68400;
(statearr_68595_68868[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_68400[(4)]))){
var statearr_68596_68869 = state_68400;
(statearr_68596_68869[(1)] = cljs.core.first((state_68400[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__68870 = state_68400;
state_68400 = G__68870;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$rapyd$state_machine__41292__auto__ = function(state_68400){
switch(arguments.length){
case 0:
return vp$rapyd$state_machine__41292__auto____0.call(this);
case 1:
return vp$rapyd$state_machine__41292__auto____1.call(this,state_68400);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$rapyd$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$rapyd$state_machine__41292__auto____0;
vp$rapyd$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$rapyd$state_machine__41292__auto____1;
return vp$rapyd$state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_68598 = f__41438__auto__();
(statearr_68598[(6)] = c__41437__auto__);

return statearr_68598;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
}));

(vp.rapyd.req.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(vp.rapyd.req.cljs$lang$applyTo = (function (seq68294){
var G__68295 = cljs.core.first(seq68294);
var seq68294__$1 = cljs.core.next(seq68294);
var G__68297 = cljs.core.first(seq68294__$1);
var seq68294__$2 = cljs.core.next(seq68294__$1);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__68295,G__68297,seq68294__$2);
}));

vp.rapyd.get_countries = (function vp$rapyd$get_countries(){
return vp.rapyd.req(new cljs.core.Keyword(null,"get","get",1683182755),"/v1/data/currencies");
});
vp.rapyd.get_currencies = (function vp$rapyd$get_currencies(){
return vp.rapyd.req(new cljs.core.Keyword(null,"get","get",1683182755),"/v1/data/currencies");
});
vp.rapyd.get_wallet = (function vp$rapyd$get_wallet(id){
return vp.rapyd.req(new cljs.core.Keyword(null,"get","get",1683182755),["/v1/user/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(id)].join(''));
});
vp.rapyd.create_company_wallet = (function vp$rapyd$create_company_wallet(data){
return vp.rapyd.req.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"post","post",269697687),"/v1/user",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([data], 0));
});
vp.rapyd.create_checkout_page = (function vp$rapyd$create_checkout_page(data){
return vp.rapyd.req.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"post","post",269697687),"/v1/checkout",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([data], 0));
});
vp.rapyd.get_checkout_page = (function vp$rapyd$get_checkout_page(id){
return vp.rapyd.req(new cljs.core.Keyword(null,"get","get",1683182755),["/v1/checkout/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(id)].join(''));
});
Object.defineProperty(module.exports, "get_countries", { enumerable: false, get: function() { return vp.rapyd.get_countries; } });
Object.defineProperty(module.exports, "get_checkout_page", { enumerable: false, get: function() { return vp.rapyd.get_checkout_page; } });
Object.defineProperty(module.exports, "methods_map", { enumerable: false, get: function() { return vp.rapyd.methods_map; } });
Object.defineProperty(module.exports, "access_key", { enumerable: false, get: function() { return vp.rapyd.access_key; } });
Object.defineProperty(module.exports, "secret_key", { enumerable: false, get: function() { return vp.rapyd.secret_key; } });
Object.defineProperty(module.exports, "base_url", { enumerable: false, get: function() { return vp.rapyd.base_url; } });
Object.defineProperty(module.exports, "get_currencies", { enumerable: false, get: function() { return vp.rapyd.get_currencies; } });
Object.defineProperty(module.exports, "create_company_wallet", { enumerable: false, get: function() { return vp.rapyd.create_company_wallet; } });
Object.defineProperty(module.exports, "req", { enumerable: false, get: function() { return vp.rapyd.req; } });
Object.defineProperty(module.exports, "create_checkout_page", { enumerable: false, get: function() { return vp.rapyd.create_checkout_page; } });
Object.defineProperty(module.exports, "get_wallet", { enumerable: false, get: function() { return vp.rapyd.get_wallet; } });
Object.defineProperty(module.exports, "gensalt", { enumerable: false, get: function() { return vp.rapyd.gensalt; } });
//# sourceMappingURL=vp.rapyd.js.map

var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./goog.history.history.js");
require("./goog.history.eventtype.js");
require("./secretary.core.js");
require("./goog.events.events.js");
require("./re_frame.core.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("vp.routes.js");

goog.provide('vp.routes');
vp.routes.hook_browser_navigation_BANG_ = (function vp$routes$hook_browser_navigation_BANG_(){
var G__73742 = (new goog.History());
goog.events.listen(G__73742,goog.history.EventType.NAVIGATE,(function (event){
return secretary.core.dispatch_BANG_(event.token);
}));

G__73742.setEnabled(true);

return G__73742;
});
vp.routes.app_routes = (function vp$routes$app_routes(){
secretary.core.set_config_BANG_(new cljs.core.Keyword(null,"prefix","prefix",-265908465),"#");

var action__42068__auto___73828 = (function (params__42069__auto__){
if(cljs.core.map_QMARK_(params__42069__auto__)){
var map__73744 = params__42069__auto__;
var map__73744__$1 = (((((!((map__73744 == null))))?(((((map__73744.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__73744.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__73744):map__73744);
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"home-page","home-page",1804156194)], null));
} else {
if(cljs.core.vector_QMARK_(params__42069__auto__)){
var vec__73747 = params__42069__auto__;
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"home-page","home-page",1804156194)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_("/",action__42068__auto___73828);

vp.routes.home_page = (function vp$routes$app_routes_$_home_page(var_args){
var args__4742__auto__ = [];
var len__4736__auto___73832 = arguments.length;
var i__4737__auto___73833 = (0);
while(true){
if((i__4737__auto___73833 < len__4736__auto___73832)){
args__4742__auto__.push((arguments[i__4737__auto___73833]));

var G__73834 = (i__4737__auto___73833 + (1));
i__4737__auto___73833 = G__73834;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return vp.routes.home_page.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(vp.routes.home_page.cljs$core$IFn$_invoke$arity$variadic = (function (args__42067__auto__){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(secretary.core.render_route_STAR_,"/",args__42067__auto__);
}));

(vp.routes.home_page.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(vp.routes.home_page.cljs$lang$applyTo = (function (seq73754){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq73754));
}));


var action__42068__auto___73836 = (function (params__42069__auto__){
if(cljs.core.map_QMARK_(params__42069__auto__)){
var map__73755 = params__42069__auto__;
var map__73755__$1 = (((((!((map__73755 == null))))?(((((map__73755.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__73755.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__73755):map__73755);
re_frame.core.dispatch(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"init-profile-page","init-profile-page",-832278911)], null));

return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"profile-page","profile-page",649028201)], null));
} else {
if(cljs.core.vector_QMARK_(params__42069__auto__)){
var vec__73762 = params__42069__auto__;
re_frame.core.dispatch(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"init-profile-page","init-profile-page",-832278911)], null));

return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"profile-page","profile-page",649028201)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_("/profile",action__42068__auto___73836);

vp.routes.profile_page = (function vp$routes$app_routes_$_profile_page(var_args){
var args__4742__auto__ = [];
var len__4736__auto___73840 = arguments.length;
var i__4737__auto___73841 = (0);
while(true){
if((i__4737__auto___73841 < len__4736__auto___73840)){
args__4742__auto__.push((arguments[i__4737__auto___73841]));

var G__73843 = (i__4737__auto___73841 + (1));
i__4737__auto___73841 = G__73843;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return vp.routes.profile_page.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(vp.routes.profile_page.cljs$core$IFn$_invoke$arity$variadic = (function (args__42067__auto__){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(secretary.core.render_route_STAR_,"/profile",args__42067__auto__);
}));

(vp.routes.profile_page.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(vp.routes.profile_page.cljs$lang$applyTo = (function (seq73766){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq73766));
}));


var action__42068__auto___73855 = (function (params__42069__auto__){
if(cljs.core.map_QMARK_(params__42069__auto__)){
var map__73769 = params__42069__auto__;
var map__73769__$1 = (((((!((map__73769 == null))))?(((((map__73769.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__73769.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__73769):map__73769);
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"company-contracts","company-contracts",-1709140333)], null));
} else {
if(cljs.core.vector_QMARK_(params__42069__auto__)){
var vec__73774 = params__42069__auto__;
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"company-contracts","company-contracts",-1709140333)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_("/company/contracts",action__42068__auto___73855);

vp.routes.company_contracts_page = (function vp$routes$app_routes_$_company_contracts_page(var_args){
var args__4742__auto__ = [];
var len__4736__auto___73900 = arguments.length;
var i__4737__auto___73903 = (0);
while(true){
if((i__4737__auto___73903 < len__4736__auto___73900)){
args__4742__auto__.push((arguments[i__4737__auto___73903]));

var G__73914 = (i__4737__auto___73903 + (1));
i__4737__auto___73903 = G__73914;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return vp.routes.company_contracts_page.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(vp.routes.company_contracts_page.cljs$core$IFn$_invoke$arity$variadic = (function (args__42067__auto__){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(secretary.core.render_route_STAR_,"/company/contracts",args__42067__auto__);
}));

(vp.routes.company_contracts_page.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(vp.routes.company_contracts_page.cljs$lang$applyTo = (function (seq73778){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq73778));
}));


var action__42068__auto___73927 = (function (params__42069__auto__){
if(cljs.core.map_QMARK_(params__42069__auto__)){
var map__73781 = params__42069__auto__;
var map__73781__$1 = (((((!((map__73781 == null))))?(((((map__73781.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__73781.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__73781):map__73781);
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"company-plans","company-plans",1232676027)], null));
} else {
if(cljs.core.vector_QMARK_(params__42069__auto__)){
var vec__73786 = params__42069__auto__;
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"company-plans","company-plans",1232676027)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_("/company/plans",action__42068__auto___73927);

vp.routes.company_plans_page = (function vp$routes$app_routes_$_company_plans_page(var_args){
var args__4742__auto__ = [];
var len__4736__auto___73978 = arguments.length;
var i__4737__auto___73986 = (0);
while(true){
if((i__4737__auto___73986 < len__4736__auto___73978)){
args__4742__auto__.push((arguments[i__4737__auto___73986]));

var G__73990 = (i__4737__auto___73986 + (1));
i__4737__auto___73986 = G__73990;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return vp.routes.company_plans_page.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(vp.routes.company_plans_page.cljs$core$IFn$_invoke$arity$variadic = (function (args__42067__auto__){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(secretary.core.render_route_STAR_,"/company/plans",args__42067__auto__);
}));

(vp.routes.company_plans_page.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(vp.routes.company_plans_page.cljs$lang$applyTo = (function (seq73790){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq73790));
}));


var action__42068__auto___74003 = (function (params__42069__auto__){
if(cljs.core.map_QMARK_(params__42069__auto__)){
var map__73793 = params__42069__auto__;
var map__73793__$1 = (((((!((map__73793 == null))))?(((((map__73793.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__73793.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__73793):map__73793);
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"company-payouts","company-payouts",-1997854563)], null));
} else {
if(cljs.core.vector_QMARK_(params__42069__auto__)){
var vec__73798 = params__42069__auto__;
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"company-payouts","company-payouts",-1997854563)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_("/company/payouts",action__42068__auto___74003);

vp.routes.company_payouts_page = (function vp$routes$app_routes_$_company_payouts_page(var_args){
var args__4742__auto__ = [];
var len__4736__auto___74058 = arguments.length;
var i__4737__auto___74059 = (0);
while(true){
if((i__4737__auto___74059 < len__4736__auto___74058)){
args__4742__auto__.push((arguments[i__4737__auto___74059]));

var G__74062 = (i__4737__auto___74059 + (1));
i__4737__auto___74059 = G__74062;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return vp.routes.company_payouts_page.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(vp.routes.company_payouts_page.cljs$core$IFn$_invoke$arity$variadic = (function (args__42067__auto__){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(secretary.core.render_route_STAR_,"/company/payouts",args__42067__auto__);
}));

(vp.routes.company_payouts_page.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(vp.routes.company_payouts_page.cljs$lang$applyTo = (function (seq73802){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq73802));
}));


var action__42068__auto___74121 = (function (params__42069__auto__){
if(cljs.core.map_QMARK_(params__42069__auto__)){
var map__73804 = params__42069__auto__;
var map__73804__$1 = (((((!((map__73804 == null))))?(((((map__73804.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__73804.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__73804):map__73804);
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"user-contracts","user-contracts",-2032527649)], null));
} else {
if(cljs.core.vector_QMARK_(params__42069__auto__)){
var vec__73810 = params__42069__auto__;
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"user-contracts","user-contracts",-2032527649)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_("/user/contracts",action__42068__auto___74121);

vp.routes.user_contracts_page = (function vp$routes$app_routes_$_user_contracts_page(var_args){
var args__4742__auto__ = [];
var len__4736__auto___74131 = arguments.length;
var i__4737__auto___74135 = (0);
while(true){
if((i__4737__auto___74135 < len__4736__auto___74131)){
args__4742__auto__.push((arguments[i__4737__auto___74135]));

var G__74138 = (i__4737__auto___74135 + (1));
i__4737__auto___74135 = G__74138;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return vp.routes.user_contracts_page.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(vp.routes.user_contracts_page.cljs$core$IFn$_invoke$arity$variadic = (function (args__42067__auto__){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(secretary.core.render_route_STAR_,"/user/contracts",args__42067__auto__);
}));

(vp.routes.user_contracts_page.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(vp.routes.user_contracts_page.cljs$lang$applyTo = (function (seq73814){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq73814));
}));


var action__42068__auto___74187 = (function (params__42069__auto__){
if(cljs.core.map_QMARK_(params__42069__auto__)){
var map__73817 = params__42069__auto__;
var map__73817__$1 = (((((!((map__73817 == null))))?(((((map__73817.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__73817.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__73817):map__73817);
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"user-payments","user-payments",1038214866)], null));
} else {
if(cljs.core.vector_QMARK_(params__42069__auto__)){
var vec__73822 = params__42069__auto__;
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"user-payments","user-payments",1038214866)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_("/user/payments",action__42068__auto___74187);

vp.routes.user_payments_page = (function vp$routes$app_routes_$_user_payments_page(var_args){
var args__4742__auto__ = [];
var len__4736__auto___74216 = arguments.length;
var i__4737__auto___74222 = (0);
while(true){
if((i__4737__auto___74222 < len__4736__auto___74216)){
args__4742__auto__.push((arguments[i__4737__auto___74222]));

var G__74234 = (i__4737__auto___74222 + (1));
i__4737__auto___74222 = G__74234;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return vp.routes.user_payments_page.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(vp.routes.user_payments_page.cljs$core$IFn$_invoke$arity$variadic = (function (args__42067__auto__){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(secretary.core.render_route_STAR_,"/user/payments",args__42067__auto__);
}));

(vp.routes.user_payments_page.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(vp.routes.user_payments_page.cljs$lang$applyTo = (function (seq73826){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq73826));
}));


return vp.routes.hook_browser_navigation_BANG_();
});
vp.routes.goto$ = (function vp$routes$goto(url){
return (window["location"]["href"] = url);
});
Object.defineProperty(module.exports, "app_routes", { enumerable: false, get: function() { return vp.routes.app_routes; } });
Object.defineProperty(module.exports, "company_contracts_page", { enumerable: false, get: function() { return vp.routes.company_contracts_page; } });
Object.defineProperty(module.exports, "goto$", { enumerable: false, get: function() { return vp.routes.goto$; } });
Object.defineProperty(module.exports, "home_page", { enumerable: false, get: function() { return vp.routes.home_page; } });
Object.defineProperty(module.exports, "profile_page", { enumerable: false, get: function() { return vp.routes.profile_page; } });
Object.defineProperty(module.exports, "company_plans_page", { enumerable: false, get: function() { return vp.routes.company_plans_page; } });
Object.defineProperty(module.exports, "user_contracts_page", { enumerable: false, get: function() { return vp.routes.user_contracts_page; } });
Object.defineProperty(module.exports, "company_payouts_page", { enumerable: false, get: function() { return vp.routes.company_payouts_page; } });
Object.defineProperty(module.exports, "hook_browser_navigation_BANG_", { enumerable: false, get: function() { return vp.routes.hook_browser_navigation_BANG_; } });
Object.defineProperty(module.exports, "user_payments_page", { enumerable: false, get: function() { return vp.routes.user_payments_page; } });
//# sourceMappingURL=vp.routes.js.map

var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./goog.object.object.js");
require("./cljs.tools.reader.js");
require("./cljs.tools.reader.edn.js");
require("./goog.string.stringbuffer.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("cljs.reader.js");

goog.provide('cljs.reader');
cljs.reader.zero_fill_right_and_truncate = (function cljs$reader$zero_fill_right_and_truncate(s,width){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(width,cljs.core.count(s))){
return s;
} else {
if((width < cljs.core.count(s))){
return cljs.core.subs.cljs$core$IFn$_invoke$arity$3(s,(0),width);
} else {
var b = (new goog.string.StringBuffer(s));
while(true){
if((b.getLength() < width)){
var G__46735 = b.append("0");
b = G__46735;
continue;
} else {
return b.toString();
}
break;
}

}
}
});
cljs.reader.divisible_QMARK_ = (function cljs$reader$divisible_QMARK_(num,div){
return (cljs.core.mod(num,div) === (0));
});
cljs.reader.indivisible_QMARK_ = (function cljs$reader$indivisible_QMARK_(num,div){
return (!(cljs.reader.divisible_QMARK_(num,div)));
});
cljs.reader.leap_year_QMARK_ = (function cljs$reader$leap_year_QMARK_(year){
return ((cljs.reader.divisible_QMARK_(year,(4))) && (((cljs.reader.indivisible_QMARK_(year,(100))) || (cljs.reader.divisible_QMARK_(year,(400))))));
});
cljs.reader.days_in_month = (function (){var dim_norm = new cljs.core.PersistentVector(null, 13, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,(31),(28),(31),(30),(31),(30),(31),(31),(30),(31),(30),(31)], null);
var dim_leap = new cljs.core.PersistentVector(null, 13, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,(31),(29),(31),(30),(31),(30),(31),(31),(30),(31),(30),(31)], null);
return (function (month,leap_year_QMARK_){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2((cljs.core.truth_(leap_year_QMARK_)?dim_leap:dim_norm),month);
});
})();
cljs.reader.timestamp_regex = /(\d\d\d\d)(?:-(\d\d)(?:-(\d\d)(?:[T](\d\d)(?::(\d\d)(?::(\d\d)(?:[.](\d+))?)?)?)?)?)?(?:[Z]|([-+])(\d\d):(\d\d))?/;
cljs.reader.parse_int = (function cljs$reader$parse_int(s){
var n = parseInt(s,(10));
if(cljs.core.not(isNaN(n))){
return n;
} else {
return null;
}
});
cljs.reader.check = (function cljs$reader$check(low,n,high,msg){
if((((low <= n)) && ((n <= high)))){
} else {
throw (new Error([cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)," Failed:  ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(low),"<=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(n),"<=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(high)].join('')));
}

return n;
});
cljs.reader.parse_and_validate_timestamp = (function cljs$reader$parse_and_validate_timestamp(s){
var vec__46533 = cljs.core.re_matches(cljs.reader.timestamp_regex,s);
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46533,(0),null);
var years = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46533,(1),null);
var months = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46533,(2),null);
var days = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46533,(3),null);
var hours = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46533,(4),null);
var minutes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46533,(5),null);
var seconds = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46533,(6),null);
var fraction = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46533,(7),null);
var offset_sign = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46533,(8),null);
var offset_hours = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46533,(9),null);
var offset_minutes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46533,(10),null);
var v = vec__46533;
if(cljs.core.not(v)){
throw (new Error(["Unrecognized date/time syntax: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(s)].join('')));
} else {
var years__$1 = cljs.reader.parse_int(years);
var months__$1 = (function (){var or__4126__auto__ = cljs.reader.parse_int(months);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (1);
}
})();
var days__$1 = (function (){var or__4126__auto__ = cljs.reader.parse_int(days);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (1);
}
})();
var hours__$1 = (function (){var or__4126__auto__ = cljs.reader.parse_int(hours);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (0);
}
})();
var minutes__$1 = (function (){var or__4126__auto__ = cljs.reader.parse_int(minutes);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (0);
}
})();
var seconds__$1 = (function (){var or__4126__auto__ = cljs.reader.parse_int(seconds);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (0);
}
})();
var fraction__$1 = (function (){var or__4126__auto__ = cljs.reader.parse_int(cljs.reader.zero_fill_right_and_truncate(fraction,(3)));
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (0);
}
})();
var offset_sign__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(offset_sign,"-"))?(-1):(1));
var offset_hours__$1 = (function (){var or__4126__auto__ = cljs.reader.parse_int(offset_hours);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (0);
}
})();
var offset_minutes__$1 = (function (){var or__4126__auto__ = cljs.reader.parse_int(offset_minutes);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (0);
}
})();
var offset = (offset_sign__$1 * ((offset_hours__$1 * (60)) + offset_minutes__$1));
return new cljs.core.PersistentVector(null, 8, 5, cljs.core.PersistentVector.EMPTY_NODE, [years__$1,cljs.reader.check((1),months__$1,(12),"timestamp month field must be in range 1..12"),cljs.reader.check((1),days__$1,cljs.reader.days_in_month(months__$1,cljs.reader.leap_year_QMARK_(years__$1)),"timestamp day field must be in range 1..last day in month"),cljs.reader.check((0),hours__$1,(23),"timestamp hour field must be in range 0..23"),cljs.reader.check((0),minutes__$1,(59),"timestamp minute field must be in range 0..59"),cljs.reader.check((0),seconds__$1,((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(minutes__$1,(59)))?(60):(59)),"timestamp second field must be in range 0..60"),cljs.reader.check((0),fraction__$1,(999),"timestamp millisecond field must be in range 0..999"),offset], null);
}
});
cljs.reader.parse_timestamp = (function cljs$reader$parse_timestamp(ts){
var temp__5733__auto__ = cljs.reader.parse_and_validate_timestamp(ts);
if(cljs.core.truth_(temp__5733__auto__)){
var vec__46571 = temp__5733__auto__;
var years = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46571,(0),null);
var months = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46571,(1),null);
var days = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46571,(2),null);
var hours = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46571,(3),null);
var minutes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46571,(4),null);
var seconds = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46571,(5),null);
var ms = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46571,(6),null);
var offset = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46571,(7),null);
return (new Date((Date.UTC(years,(months - (1)),days,hours,minutes,seconds,ms) - ((offset * (60)) * (1000)))));
} else {
throw (new Error(["Unrecognized date/time syntax: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ts)].join('')));
}
});
cljs.reader.read_date = (function cljs$reader$read_date(s){
if(typeof s === 'string'){
return cljs.reader.parse_timestamp(s);
} else {
throw (new Error("Instance literal expects a string for its timestamp."));
}
});
cljs.reader.read_queue = (function cljs$reader$read_queue(elems){
if(cljs.core.vector_QMARK_(elems)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentQueue.EMPTY,elems);
} else {
throw (new Error("Queue literal expects a vector for its elements."));
}
});
cljs.reader.read_js = (function cljs$reader$read_js(form){
if(cljs.core.vector_QMARK_(form)){
var arr = [];
var seq__46606_46760 = cljs.core.seq(form);
var chunk__46607_46761 = null;
var count__46608_46762 = (0);
var i__46609_46763 = (0);
while(true){
if((i__46609_46763 < count__46608_46762)){
var x_46764 = chunk__46607_46761.cljs$core$IIndexed$_nth$arity$2(null,i__46609_46763);
arr.push(x_46764);


var G__46765 = seq__46606_46760;
var G__46766 = chunk__46607_46761;
var G__46767 = count__46608_46762;
var G__46768 = (i__46609_46763 + (1));
seq__46606_46760 = G__46765;
chunk__46607_46761 = G__46766;
count__46608_46762 = G__46767;
i__46609_46763 = G__46768;
continue;
} else {
var temp__5735__auto___46769 = cljs.core.seq(seq__46606_46760);
if(temp__5735__auto___46769){
var seq__46606_46770__$1 = temp__5735__auto___46769;
if(cljs.core.chunked_seq_QMARK_(seq__46606_46770__$1)){
var c__4556__auto___46771 = cljs.core.chunk_first(seq__46606_46770__$1);
var G__46772 = cljs.core.chunk_rest(seq__46606_46770__$1);
var G__46773 = c__4556__auto___46771;
var G__46774 = cljs.core.count(c__4556__auto___46771);
var G__46775 = (0);
seq__46606_46760 = G__46772;
chunk__46607_46761 = G__46773;
count__46608_46762 = G__46774;
i__46609_46763 = G__46775;
continue;
} else {
var x_46779 = cljs.core.first(seq__46606_46770__$1);
arr.push(x_46779);


var G__46780 = cljs.core.next(seq__46606_46770__$1);
var G__46781 = null;
var G__46782 = (0);
var G__46783 = (0);
seq__46606_46760 = G__46780;
chunk__46607_46761 = G__46781;
count__46608_46762 = G__46782;
i__46609_46763 = G__46783;
continue;
}
} else {
}
}
break;
}

return arr;
} else {
if(cljs.core.map_QMARK_(form)){
var obj = ({});
var seq__46621_46789 = cljs.core.seq(form);
var chunk__46622_46790 = null;
var count__46623_46791 = (0);
var i__46624_46792 = (0);
while(true){
if((i__46624_46792 < count__46623_46791)){
var vec__46643_46795 = chunk__46622_46790.cljs$core$IIndexed$_nth$arity$2(null,i__46624_46792);
var k_46796 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46643_46795,(0),null);
var v_46797 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46643_46795,(1),null);
goog.object.set(obj,cljs.core.name(k_46796),v_46797);


var G__46813 = seq__46621_46789;
var G__46814 = chunk__46622_46790;
var G__46815 = count__46623_46791;
var G__46816 = (i__46624_46792 + (1));
seq__46621_46789 = G__46813;
chunk__46622_46790 = G__46814;
count__46623_46791 = G__46815;
i__46624_46792 = G__46816;
continue;
} else {
var temp__5735__auto___46819 = cljs.core.seq(seq__46621_46789);
if(temp__5735__auto___46819){
var seq__46621_46820__$1 = temp__5735__auto___46819;
if(cljs.core.chunked_seq_QMARK_(seq__46621_46820__$1)){
var c__4556__auto___46821 = cljs.core.chunk_first(seq__46621_46820__$1);
var G__46822 = cljs.core.chunk_rest(seq__46621_46820__$1);
var G__46823 = c__4556__auto___46821;
var G__46824 = cljs.core.count(c__4556__auto___46821);
var G__46825 = (0);
seq__46621_46789 = G__46822;
chunk__46622_46790 = G__46823;
count__46623_46791 = G__46824;
i__46624_46792 = G__46825;
continue;
} else {
var vec__46650_46826 = cljs.core.first(seq__46621_46820__$1);
var k_46827 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46650_46826,(0),null);
var v_46828 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46650_46826,(1),null);
goog.object.set(obj,cljs.core.name(k_46827),v_46828);


var G__46836 = cljs.core.next(seq__46621_46820__$1);
var G__46838 = null;
var G__46839 = (0);
var G__46840 = (0);
seq__46621_46789 = G__46836;
chunk__46622_46790 = G__46838;
count__46623_46791 = G__46839;
i__46624_46792 = G__46840;
continue;
}
} else {
}
}
break;
}

return obj;
} else {
throw (new Error(["JS literal expects a vector or map containing ","only string or unqualified keyword keys"].join('')));

}
}
});
cljs.reader.read_uuid = (function cljs$reader$read_uuid(uuid){
if(typeof uuid === 'string'){
return cljs.core.uuid(uuid);
} else {
throw (new Error("UUID literal expects a string as its representation."));
}
});
cljs.reader._STAR_default_data_reader_fn_STAR_ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
cljs.reader._STAR_tag_table_STAR_ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Symbol(null,"inst","inst",-2008473268,null),cljs.reader.read_date,new cljs.core.Symbol(null,"uuid","uuid",-504564192,null),cljs.reader.read_uuid,new cljs.core.Symbol(null,"queue","queue",-1198599890,null),cljs.reader.read_queue,new cljs.core.Symbol(null,"js","js",-886355190,null),cljs.reader.read_js], null),cljs.core.PersistentArrayMap.EMPTY], 0)));
/**
 * Reads the first object from an cljs.tools.reader.reader-types/IPushbackReader.
 * Returns the object read. If EOF, throws if eof-error? is true otherwise returns eof.
 * If no reader is provided, *in* will be used.
 * 
 * Reads data in the edn format (subset of Clojure data):
 * http://edn-format.org
 * 
 * cljs.tools.reader.edn/read doesn't depend on dynamic Vars, all configuration
 * is done by passing an opt map.
 * 
 * opts is a map that can include the following keys:
 * :eof - value to return on end-of-file. When not supplied, eof throws an exception.
 * :readers  - a map of tag symbols to data-reader functions to be considered before default-data-readers.
 *            When not supplied, only the default-data-readers will be used.
 * :default - A function of two args, that will, if present and no reader is found for a tag,
 *            be called with the tag and the value.
 */
cljs.reader.read = (function cljs$reader$read(var_args){
var G__46670 = arguments.length;
switch (G__46670) {
case 1:
return cljs.reader.read.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.reader.read.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.reader.read.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.reader.read.cljs$core$IFn$_invoke$arity$1 = (function (reader){
return cljs.tools.reader.edn.read.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"readers","readers",-2118263030),cljs.core.deref(cljs.reader._STAR_tag_table_STAR_),new cljs.core.Keyword(null,"default","default",-1987822328),cljs.core.deref(cljs.reader._STAR_default_data_reader_fn_STAR_),new cljs.core.Keyword(null,"eof","eof",-489063237),null], null),reader);
}));

(cljs.reader.read.cljs$core$IFn$_invoke$arity$2 = (function (p__46676,reader){
var map__46677 = p__46676;
var map__46677__$1 = (((((!((map__46677 == null))))?(((((map__46677.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46677.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__46677):map__46677);
var opts = map__46677__$1;
var eof = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46677__$1,new cljs.core.Keyword(null,"eof","eof",-489063237));
return cljs.tools.reader.edn.read.cljs$core$IFn$_invoke$arity$2(cljs.core.update.cljs$core$IFn$_invoke$arity$3(cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([opts,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"default","default",-1987822328),cljs.core.deref(cljs.reader._STAR_default_data_reader_fn_STAR_)], null)], 0)),new cljs.core.Keyword(null,"readers","readers",-2118263030),(function (m){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.deref(cljs.reader._STAR_tag_table_STAR_),m], 0));
})),reader);
}));

(cljs.reader.read.cljs$core$IFn$_invoke$arity$4 = (function (reader,eof_error_QMARK_,eof,opts){
return cljs.tools.reader.edn.read.cljs$core$IFn$_invoke$arity$4(reader,eof_error_QMARK_,eof,cljs.core.update.cljs$core$IFn$_invoke$arity$3(cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([opts,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"default","default",-1987822328),cljs.core.deref(cljs.reader._STAR_default_data_reader_fn_STAR_)], null)], 0)),new cljs.core.Keyword(null,"readers","readers",-2118263030),(function (m){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.deref(cljs.reader._STAR_tag_table_STAR_),m], 0));
})));
}));

(cljs.reader.read.cljs$lang$maxFixedArity = 4);

/**
 * Reads one object from the string s.
 * Returns nil when s is nil or empty.
 * 
 * Reads data in the edn format (subset of Clojure data):
 * http://edn-format.org
 * 
 * opts is a map as per cljs.tools.reader.edn/read
 */
cljs.reader.read_string = (function cljs$reader$read_string(var_args){
var G__46689 = arguments.length;
switch (G__46689) {
case 1:
return cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.reader.read_string.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1 = (function (s){
return cljs.tools.reader.edn.read_string.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"readers","readers",-2118263030),cljs.core.deref(cljs.reader._STAR_tag_table_STAR_),new cljs.core.Keyword(null,"default","default",-1987822328),cljs.core.deref(cljs.reader._STAR_default_data_reader_fn_STAR_),new cljs.core.Keyword(null,"eof","eof",-489063237),null], null),s);
}));

(cljs.reader.read_string.cljs$core$IFn$_invoke$arity$2 = (function (opts,s){
return cljs.tools.reader.edn.read_string.cljs$core$IFn$_invoke$arity$2(cljs.core.update.cljs$core$IFn$_invoke$arity$3(cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"default","default",-1987822328),cljs.core.deref(cljs.reader._STAR_default_data_reader_fn_STAR_)], null),opts], 0)),new cljs.core.Keyword(null,"readers","readers",-2118263030),(function (m){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.deref(cljs.reader._STAR_tag_table_STAR_),m], 0));
})),s);
}));

(cljs.reader.read_string.cljs$lang$maxFixedArity = 2);

cljs.reader.register_tag_parser_BANG_ = (function cljs$reader$register_tag_parser_BANG_(tag,f){
var old_parser = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(cljs.reader._STAR_tag_table_STAR_),tag);
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(cljs.reader._STAR_tag_table_STAR_,cljs.core.assoc,tag,f);

return old_parser;
});
cljs.reader.deregister_tag_parser_BANG_ = (function cljs$reader$deregister_tag_parser_BANG_(tag){
var old_parser = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(cljs.reader._STAR_tag_table_STAR_),tag);
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cljs.reader._STAR_tag_table_STAR_,cljs.core.dissoc,tag);

return old_parser;
});
cljs.reader.register_default_tag_parser_BANG_ = (function cljs$reader$register_default_tag_parser_BANG_(f){
var old_parser = cljs.core.deref(cljs.reader._STAR_default_data_reader_fn_STAR_);
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(cljs.reader._STAR_default_data_reader_fn_STAR_,(function (_){
return f;
}));

return old_parser;
});
cljs.reader.deregister_default_tag_parser_BANG_ = (function cljs$reader$deregister_default_tag_parser_BANG_(){
var old_parser = cljs.core.deref(cljs.reader._STAR_default_data_reader_fn_STAR_);
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(cljs.reader._STAR_default_data_reader_fn_STAR_,(function (_){
return null;
}));

return old_parser;
});
Object.defineProperty(module.exports, "read_uuid", { enumerable: false, get: function() { return cljs.reader.read_uuid; } });
Object.defineProperty(module.exports, "leap_year_QMARK_", { enumerable: false, get: function() { return cljs.reader.leap_year_QMARK_; } });
Object.defineProperty(module.exports, "_STAR_default_data_reader_fn_STAR_", { enumerable: false, get: function() { return cljs.reader._STAR_default_data_reader_fn_STAR_; } });
Object.defineProperty(module.exports, "read", { enumerable: false, get: function() { return cljs.reader.read; } });
Object.defineProperty(module.exports, "_STAR_tag_table_STAR_", { enumerable: false, get: function() { return cljs.reader._STAR_tag_table_STAR_; } });
Object.defineProperty(module.exports, "days_in_month", { enumerable: false, get: function() { return cljs.reader.days_in_month; } });
Object.defineProperty(module.exports, "parse_and_validate_timestamp", { enumerable: false, get: function() { return cljs.reader.parse_and_validate_timestamp; } });
Object.defineProperty(module.exports, "register_tag_parser_BANG_", { enumerable: false, get: function() { return cljs.reader.register_tag_parser_BANG_; } });
Object.defineProperty(module.exports, "check", { enumerable: false, get: function() { return cljs.reader.check; } });
Object.defineProperty(module.exports, "timestamp_regex", { enumerable: false, get: function() { return cljs.reader.timestamp_regex; } });
Object.defineProperty(module.exports, "register_default_tag_parser_BANG_", { enumerable: false, get: function() { return cljs.reader.register_default_tag_parser_BANG_; } });
Object.defineProperty(module.exports, "divisible_QMARK_", { enumerable: false, get: function() { return cljs.reader.divisible_QMARK_; } });
Object.defineProperty(module.exports, "zero_fill_right_and_truncate", { enumerable: false, get: function() { return cljs.reader.zero_fill_right_and_truncate; } });
Object.defineProperty(module.exports, "read_date", { enumerable: false, get: function() { return cljs.reader.read_date; } });
Object.defineProperty(module.exports, "deregister_default_tag_parser_BANG_", { enumerable: false, get: function() { return cljs.reader.deregister_default_tag_parser_BANG_; } });
Object.defineProperty(module.exports, "indivisible_QMARK_", { enumerable: false, get: function() { return cljs.reader.indivisible_QMARK_; } });
Object.defineProperty(module.exports, "read_queue", { enumerable: false, get: function() { return cljs.reader.read_queue; } });
Object.defineProperty(module.exports, "read_string", { enumerable: false, get: function() { return cljs.reader.read_string; } });
Object.defineProperty(module.exports, "parse_int", { enumerable: false, get: function() { return cljs.reader.parse_int; } });
Object.defineProperty(module.exports, "read_js", { enumerable: false, get: function() { return cljs.reader.read_js; } });
Object.defineProperty(module.exports, "deregister_tag_parser_BANG_", { enumerable: false, get: function() { return cljs.reader.deregister_tag_parser_BANG_; } });
Object.defineProperty(module.exports, "parse_timestamp", { enumerable: false, get: function() { return cljs.reader.parse_timestamp; } });
//# sourceMappingURL=cljs.reader.js.map

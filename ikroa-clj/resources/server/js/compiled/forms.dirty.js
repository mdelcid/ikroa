var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./clojure.data.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("forms.dirty.js");

goog.provide('forms.dirty');
forms.dirty.analyze_diff = (function forms$dirty$analyze_diff(var_args){
var G__42623 = arguments.length;
switch (G__42623) {
case 1:
return forms.dirty.analyze_diff.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 3:
return forms.dirty.analyze_diff.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(forms.dirty.analyze_diff.cljs$core$IFn$_invoke$arity$1 = (function (data){
return forms.dirty.analyze_diff.cljs$core$IFn$_invoke$arity$3(data,cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"results","results",-1134170113),cljs.core.PersistentVector.EMPTY,new cljs.core.Keyword(null,"lengths","lengths",-851104122),cljs.core.PersistentArrayMap.EMPTY], null));
}));

(forms.dirty.analyze_diff.cljs$core$IFn$_invoke$arity$3 = (function (data,path,results){
return cljs.core.reduce_kv((function (m,k,v){
if(((cljs.core.vector_QMARK_(v)) || (cljs.core.map_QMARK_(v)))){
var map__42636 = m;
var map__42636__$1 = (((((!((map__42636 == null))))?(((((map__42636.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__42636.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__42636):map__42636);
var results__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__42636__$1,new cljs.core.Keyword(null,"results","results",-1134170113));
var lengths = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__42636__$1,new cljs.core.Keyword(null,"lengths","lengths",-851104122));
var new_path = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(path,k);
var child_diff = forms.dirty.analyze_diff.cljs$core$IFn$_invoke$arity$3(v,new_path,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"results","results",-1134170113),cljs.core.PersistentVector.EMPTY,new cljs.core.Keyword(null,"lengths","lengths",-851104122),cljs.core.PersistentArrayMap.EMPTY], null));
var new_results = new cljs.core.Keyword(null,"results","results",-1134170113).cljs$core$IFn$_invoke$arity$1(child_diff);
var new_lengths = new cljs.core.Keyword(null,"lengths","lengths",-851104122).cljs$core$IFn$_invoke$arity$1(child_diff);
var lengths_with_current = ((cljs.core.vector_QMARK_(v))?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(lengths,new_path,cljs.core.count(v)):lengths);
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"results","results",-1134170113),cljs.core.into.cljs$core$IFn$_invoke$arity$2(results__$1,new_results),new cljs.core.Keyword(null,"lengths","lengths",-851104122),cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new_lengths,lengths_with_current], 0))], null);
} else {
if((v == null)){
return m;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m,new cljs.core.Keyword(null,"results","results",-1134170113),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"results","results",-1134170113).cljs$core$IFn$_invoke$arity$1(m),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(path,k)));
}
}
}),results,data);
}));

(forms.dirty.analyze_diff.cljs$lang$maxFixedArity = 3);

/**
 * Calculates the key paths that are dirty by diffing the initial and current form data.
 */
forms.dirty.calculate_dirty_fields = (function forms$dirty$calculate_dirty_fields(prev,current){
var vec__42640 = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,clojure.data.diff(prev,current));
var p_diff = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42640,(0),null);
var c_diff = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42640,(1),null);
var p_report = forms.dirty.analyze_diff.cljs$core$IFn$_invoke$arity$1(p_diff);
var c_report = forms.dirty.analyze_diff.cljs$core$IFn$_invoke$arity$1(c_diff);
var vec__42643 = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,clojure.data.diff(new cljs.core.Keyword(null,"lengths","lengths",-851104122).cljs$core$IFn$_invoke$arity$1(p_report),new cljs.core.Keyword(null,"lengths","lengths",-851104122).cljs$core$IFn$_invoke$arity$1(c_report)));
var p_lengths_diff = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42643,(0),null);
var c_lengths_diff = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42643,(1),null);
return cljs.core.set(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"results","results",-1134170113).cljs$core$IFn$_invoke$arity$1(p_report),new cljs.core.Keyword(null,"results","results",-1134170113).cljs$core$IFn$_invoke$arity$1(c_report),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.keys(p_lengths_diff),cljs.core.keys(c_lengths_diff)], 0)));
});
Object.defineProperty(module.exports, "analyze_diff", { enumerable: false, get: function() { return forms.dirty.analyze_diff; } });
Object.defineProperty(module.exports, "calculate_dirty_fields", { enumerable: false, get: function() { return forms.dirty.calculate_dirty_fields; } });
//# sourceMappingURL=forms.dirty.js.map

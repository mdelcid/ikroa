var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./clojure.walk.js");
require("./devtools.util.js");
require("./devtools.protocols.js");
require("./devtools.formatters.helpers.js");
require("./devtools.formatters.state.js");
require("./clojure.string.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("devtools.formatters.templating.js");

goog.provide('devtools.formatters.templating');
devtools.formatters.templating.mark_as_group_BANG_ = (function devtools$formatters$templating$mark_as_group_BANG_(value){
var x45828_46137 = value;
(x45828_46137.devtools$protocols$IGroup$ = cljs.core.PROTOCOL_SENTINEL);


return value;
});
devtools.formatters.templating.group_QMARK_ = (function devtools$formatters$templating$group_QMARK_(value){
if((!((value == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === value.devtools$protocols$IGroup$)))){
return true;
} else {
if((!value.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(devtools.protocols.IGroup,value);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(devtools.protocols.IGroup,value);
}
});
devtools.formatters.templating.mark_as_template_BANG_ = (function devtools$formatters$templating$mark_as_template_BANG_(value){
var x45844_46147 = value;
(x45844_46147.devtools$protocols$ITemplate$ = cljs.core.PROTOCOL_SENTINEL);


return value;
});
devtools.formatters.templating.template_QMARK_ = (function devtools$formatters$templating$template_QMARK_(value){
if((!((value == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === value.devtools$protocols$ITemplate$)))){
return true;
} else {
if((!value.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(devtools.protocols.ITemplate,value);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(devtools.protocols.ITemplate,value);
}
});
devtools.formatters.templating.mark_as_surrogate_BANG_ = (function devtools$formatters$templating$mark_as_surrogate_BANG_(value){
var x45854_46154 = value;
(x45854_46154.devtools$protocols$ISurrogate$ = cljs.core.PROTOCOL_SENTINEL);


return value;
});
devtools.formatters.templating.surrogate_QMARK_ = (function devtools$formatters$templating$surrogate_QMARK_(value){
if((!((value == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === value.devtools$protocols$ISurrogate$)))){
return true;
} else {
if((!value.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(devtools.protocols.ISurrogate,value);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(devtools.protocols.ISurrogate,value);
}
});
devtools.formatters.templating.reference_QMARK_ = (function devtools$formatters$templating$reference_QMARK_(value){
return ((devtools.formatters.templating.group_QMARK_(value)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((value[(0)]),"object")));
});
devtools.formatters.templating.make_group = (function devtools$formatters$templating$make_group(var_args){
var args__4742__auto__ = [];
var len__4736__auto___46161 = arguments.length;
var i__4737__auto___46162 = (0);
while(true){
if((i__4737__auto___46162 < len__4736__auto___46161)){
args__4742__auto__.push((arguments[i__4737__auto___46162]));

var G__46163 = (i__4737__auto___46162 + (1));
i__4737__auto___46162 = G__46163;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return devtools.formatters.templating.make_group.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(devtools.formatters.templating.make_group.cljs$core$IFn$_invoke$arity$variadic = (function (items){
var group = devtools.formatters.templating.mark_as_group_BANG_([]);
var seq__45884_46165 = cljs.core.seq(items);
var chunk__45885_46166 = null;
var count__45886_46167 = (0);
var i__45887_46168 = (0);
while(true){
if((i__45887_46168 < count__45886_46167)){
var item_46171 = chunk__45885_46166.cljs$core$IIndexed$_nth$arity$2(null,i__45887_46168);
if((!((item_46171 == null)))){
if(cljs.core.coll_QMARK_(item_46171)){
(group["push"]).apply(group,devtools.formatters.templating.mark_as_group_BANG_(cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(item_46171)));
} else {
group.push(devtools.formatters.helpers.pref(item_46171));
}
} else {
}


var G__46177 = seq__45884_46165;
var G__46178 = chunk__45885_46166;
var G__46179 = count__45886_46167;
var G__46180 = (i__45887_46168 + (1));
seq__45884_46165 = G__46177;
chunk__45885_46166 = G__46178;
count__45886_46167 = G__46179;
i__45887_46168 = G__46180;
continue;
} else {
var temp__5735__auto___46181 = cljs.core.seq(seq__45884_46165);
if(temp__5735__auto___46181){
var seq__45884_46182__$1 = temp__5735__auto___46181;
if(cljs.core.chunked_seq_QMARK_(seq__45884_46182__$1)){
var c__4556__auto___46187 = cljs.core.chunk_first(seq__45884_46182__$1);
var G__46190 = cljs.core.chunk_rest(seq__45884_46182__$1);
var G__46191 = c__4556__auto___46187;
var G__46192 = cljs.core.count(c__4556__auto___46187);
var G__46193 = (0);
seq__45884_46165 = G__46190;
chunk__45885_46166 = G__46191;
count__45886_46167 = G__46192;
i__45887_46168 = G__46193;
continue;
} else {
var item_46196 = cljs.core.first(seq__45884_46182__$1);
if((!((item_46196 == null)))){
if(cljs.core.coll_QMARK_(item_46196)){
(group["push"]).apply(group,devtools.formatters.templating.mark_as_group_BANG_(cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(item_46196)));
} else {
group.push(devtools.formatters.helpers.pref(item_46196));
}
} else {
}


var G__46200 = cljs.core.next(seq__45884_46182__$1);
var G__46201 = null;
var G__46202 = (0);
var G__46203 = (0);
seq__45884_46165 = G__46200;
chunk__45885_46166 = G__46201;
count__45886_46167 = G__46202;
i__45887_46168 = G__46203;
continue;
}
} else {
}
}
break;
}

return group;
}));

(devtools.formatters.templating.make_group.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(devtools.formatters.templating.make_group.cljs$lang$applyTo = (function (seq45878){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq45878));
}));

devtools.formatters.templating.make_template = (function devtools$formatters$templating$make_template(var_args){
var args__4742__auto__ = [];
var len__4736__auto___46207 = arguments.length;
var i__4737__auto___46208 = (0);
while(true){
if((i__4737__auto___46208 < len__4736__auto___46207)){
args__4742__auto__.push((arguments[i__4737__auto___46208]));

var G__46211 = (i__4737__auto___46208 + (1));
i__4737__auto___46208 = G__46211;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((2) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((2)),(0),null)):null);
return devtools.formatters.templating.make_template.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4743__auto__);
});

(devtools.formatters.templating.make_template.cljs$core$IFn$_invoke$arity$variadic = (function (tag,style,children){
var tag__$1 = devtools.formatters.helpers.pref(tag);
var style__$1 = devtools.formatters.helpers.pref(style);
var template = devtools.formatters.templating.mark_as_template_BANG_([tag__$1,((cljs.core.empty_QMARK_(style__$1))?({}):({"style": style__$1}))]);
var seq__45922_46220 = cljs.core.seq(children);
var chunk__45923_46221 = null;
var count__45924_46222 = (0);
var i__45925_46223 = (0);
while(true){
if((i__45925_46223 < count__45924_46222)){
var child_46226 = chunk__45923_46221.cljs$core$IIndexed$_nth$arity$2(null,i__45925_46223);
if((!((child_46226 == null)))){
if(cljs.core.coll_QMARK_(child_46226)){
(template["push"]).apply(template,devtools.formatters.templating.mark_as_template_BANG_(cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.keep.cljs$core$IFn$_invoke$arity$2(devtools.formatters.helpers.pref,child_46226))));
} else {
var temp__5733__auto___46229 = devtools.formatters.helpers.pref(child_46226);
if(cljs.core.truth_(temp__5733__auto___46229)){
var child_value_46230 = temp__5733__auto___46229;
template.push(child_value_46230);
} else {
}
}
} else {
}


var G__46231 = seq__45922_46220;
var G__46232 = chunk__45923_46221;
var G__46233 = count__45924_46222;
var G__46234 = (i__45925_46223 + (1));
seq__45922_46220 = G__46231;
chunk__45923_46221 = G__46232;
count__45924_46222 = G__46233;
i__45925_46223 = G__46234;
continue;
} else {
var temp__5735__auto___46237 = cljs.core.seq(seq__45922_46220);
if(temp__5735__auto___46237){
var seq__45922_46239__$1 = temp__5735__auto___46237;
if(cljs.core.chunked_seq_QMARK_(seq__45922_46239__$1)){
var c__4556__auto___46240 = cljs.core.chunk_first(seq__45922_46239__$1);
var G__46242 = cljs.core.chunk_rest(seq__45922_46239__$1);
var G__46243 = c__4556__auto___46240;
var G__46244 = cljs.core.count(c__4556__auto___46240);
var G__46245 = (0);
seq__45922_46220 = G__46242;
chunk__45923_46221 = G__46243;
count__45924_46222 = G__46244;
i__45925_46223 = G__46245;
continue;
} else {
var child_46248 = cljs.core.first(seq__45922_46239__$1);
if((!((child_46248 == null)))){
if(cljs.core.coll_QMARK_(child_46248)){
(template["push"]).apply(template,devtools.formatters.templating.mark_as_template_BANG_(cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.keep.cljs$core$IFn$_invoke$arity$2(devtools.formatters.helpers.pref,child_46248))));
} else {
var temp__5733__auto___46256 = devtools.formatters.helpers.pref(child_46248);
if(cljs.core.truth_(temp__5733__auto___46256)){
var child_value_46259 = temp__5733__auto___46256;
template.push(child_value_46259);
} else {
}
}
} else {
}


var G__46270 = cljs.core.next(seq__45922_46239__$1);
var G__46271 = null;
var G__46272 = (0);
var G__46273 = (0);
seq__45922_46220 = G__46270;
chunk__45923_46221 = G__46271;
count__45924_46222 = G__46272;
i__45925_46223 = G__46273;
continue;
}
} else {
}
}
break;
}

return template;
}));

(devtools.formatters.templating.make_template.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(devtools.formatters.templating.make_template.cljs$lang$applyTo = (function (seq45910){
var G__45911 = cljs.core.first(seq45910);
var seq45910__$1 = cljs.core.next(seq45910);
var G__45912 = cljs.core.first(seq45910__$1);
var seq45910__$2 = cljs.core.next(seq45910__$1);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__45911,G__45912,seq45910__$2);
}));

devtools.formatters.templating.concat_templates_BANG_ = (function devtools$formatters$templating$concat_templates_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___46280 = arguments.length;
var i__4737__auto___46281 = (0);
while(true){
if((i__4737__auto___46281 < len__4736__auto___46280)){
args__4742__auto__.push((arguments[i__4737__auto___46281]));

var G__46282 = (i__4737__auto___46281 + (1));
i__4737__auto___46281 = G__46282;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return devtools.formatters.templating.concat_templates_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(devtools.formatters.templating.concat_templates_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (template,templates){
return devtools.formatters.templating.mark_as_template_BANG_((template["concat"]).apply(template,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.into_array,cljs.core.keep.cljs$core$IFn$_invoke$arity$2(devtools.formatters.helpers.pref,templates)))));
}));

(devtools.formatters.templating.concat_templates_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(devtools.formatters.templating.concat_templates_BANG_.cljs$lang$applyTo = (function (seq45951){
var G__45952 = cljs.core.first(seq45951);
var seq45951__$1 = cljs.core.next(seq45951);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__45952,seq45951__$1);
}));

devtools.formatters.templating.extend_template_BANG_ = (function devtools$formatters$templating$extend_template_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___46298 = arguments.length;
var i__4737__auto___46301 = (0);
while(true){
if((i__4737__auto___46301 < len__4736__auto___46298)){
args__4742__auto__.push((arguments[i__4737__auto___46301]));

var G__46302 = (i__4737__auto___46301 + (1));
i__4737__auto___46301 = G__46302;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return devtools.formatters.templating.extend_template_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(devtools.formatters.templating.extend_template_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (template,args){
return devtools.formatters.templating.concat_templates_BANG_.cljs$core$IFn$_invoke$arity$variadic(template,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([args], 0));
}));

(devtools.formatters.templating.extend_template_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(devtools.formatters.templating.extend_template_BANG_.cljs$lang$applyTo = (function (seq45962){
var G__45963 = cljs.core.first(seq45962);
var seq45962__$1 = cljs.core.next(seq45962);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__45963,seq45962__$1);
}));

devtools.formatters.templating.make_surrogate = (function devtools$formatters$templating$make_surrogate(var_args){
var G__45975 = arguments.length;
switch (G__45975) {
case 1:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$1 = (function (object){
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$2(object,null);
}));

(devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$2 = (function (object,header){
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$3(object,header,null);
}));

(devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$3 = (function (object,header,body){
return devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$4(object,header,body,(0));
}));

(devtools.formatters.templating.make_surrogate.cljs$core$IFn$_invoke$arity$4 = (function (object,header,body,start_index){
return devtools.formatters.templating.mark_as_surrogate_BANG_((function (){var obj45981 = ({"target":object,"header":header,"body":body,"startIndex":(function (){var or__4126__auto__ = start_index;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (0);
}
})()});
return obj45981;
})());
}));

(devtools.formatters.templating.make_surrogate.cljs$lang$maxFixedArity = 4);

devtools.formatters.templating.get_surrogate_target = (function devtools$formatters$templating$get_surrogate_target(surrogate){
if(devtools.formatters.templating.surrogate_QMARK_(surrogate)){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return (surrogate["target"]);
});
devtools.formatters.templating.get_surrogate_header = (function devtools$formatters$templating$get_surrogate_header(surrogate){
if(devtools.formatters.templating.surrogate_QMARK_(surrogate)){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return (surrogate["header"]);
});
devtools.formatters.templating.get_surrogate_body = (function devtools$formatters$templating$get_surrogate_body(surrogate){
if(devtools.formatters.templating.surrogate_QMARK_(surrogate)){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return (surrogate["body"]);
});
devtools.formatters.templating.get_surrogate_start_index = (function devtools$formatters$templating$get_surrogate_start_index(surrogate){
if(devtools.formatters.templating.surrogate_QMARK_(surrogate)){
} else {
throw (new Error("Assert failed: (surrogate? surrogate)"));
}

return (surrogate["startIndex"]);
});
devtools.formatters.templating.make_reference = (function devtools$formatters$templating$make_reference(var_args){
var args__4742__auto__ = [];
var len__4736__auto___46340 = arguments.length;
var i__4737__auto___46341 = (0);
while(true){
if((i__4737__auto___46341 < len__4736__auto___46340)){
args__4742__auto__.push((arguments[i__4737__auto___46341]));

var G__46348 = (i__4737__auto___46341 + (1));
i__4737__auto___46341 = G__46348;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return devtools.formatters.templating.make_reference.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(devtools.formatters.templating.make_reference.cljs$core$IFn$_invoke$arity$variadic = (function (object,p__46009){
var vec__46010 = p__46009;
var state_override_fn = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46010,(0),null);
if((((state_override_fn == null)) || (cljs.core.fn_QMARK_(state_override_fn)))){
} else {
throw (new Error("Assert failed: (or (nil? state-override-fn) (fn? state-override-fn))"));
}

if((object == null)){
return devtools.formatters.templating.make_template.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"span","span",1394872991),new cljs.core.Keyword(null,"nil-style","nil-style",-1505044832),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"nil-label","nil-label",-587789203)], 0));
} else {
var sub_state = (((!((state_override_fn == null))))?(function (){var G__46015 = devtools.formatters.state.get_current_state();
return (state_override_fn.cljs$core$IFn$_invoke$arity$1 ? state_override_fn.cljs$core$IFn$_invoke$arity$1(G__46015) : state_override_fn.call(null,G__46015));
})():devtools.formatters.state.get_current_state());
return devtools.formatters.templating.make_group.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["object",({"object": object, "config": sub_state})], 0));
}
}));

(devtools.formatters.templating.make_reference.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(devtools.formatters.templating.make_reference.cljs$lang$applyTo = (function (seq46005){
var G__46006 = cljs.core.first(seq46005);
var seq46005__$1 = cljs.core.next(seq46005);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__46006,seq46005__$1);
}));

devtools.formatters.templating._STAR_current_render_stack_STAR_ = cljs.core.PersistentVector.EMPTY;
devtools.formatters.templating._STAR_current_render_path_STAR_ = cljs.core.PersistentVector.EMPTY;
devtools.formatters.templating.print_preview = (function devtools$formatters$templating$print_preview(markup){
var _STAR_print_level_STAR__orig_val__46016 = cljs.core._STAR_print_level_STAR_;
var _STAR_print_level_STAR__temp_val__46017 = (1);
(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__temp_val__46017);

try{return cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([markup], 0));
}finally {(cljs.core._STAR_print_level_STAR_ = _STAR_print_level_STAR__orig_val__46016);
}});
devtools.formatters.templating.add_stack_separators = (function devtools$formatters$templating$add_stack_separators(stack){
return cljs.core.interpose.cljs$core$IFn$_invoke$arity$2("-------------",stack);
});
devtools.formatters.templating.replace_fns_with_markers = (function devtools$formatters$templating$replace_fns_with_markers(stack){
var f = (function (v){
if(cljs.core.fn_QMARK_(v)){
return "##fn##";
} else {
return v;
}
});
return clojure.walk.prewalk(f,stack);
});
devtools.formatters.templating.pprint_render_calls = (function devtools$formatters$templating$pprint_render_calls(stack){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(devtools.util.pprint_str,stack);
});
devtools.formatters.templating.pprint_render_stack = (function devtools$formatters$templating$pprint_render_stack(stack){
return clojure.string.join.cljs$core$IFn$_invoke$arity$2("\n",devtools.formatters.templating.add_stack_separators(devtools.formatters.templating.pprint_render_calls(devtools.formatters.templating.replace_fns_with_markers(cljs.core.reverse(stack)))));
});
devtools.formatters.templating.pprint_render_path = (function devtools$formatters$templating$pprint_render_path(path){
return devtools.util.pprint_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path], 0));
});
devtools.formatters.templating.assert_markup_error = (function devtools$formatters$templating$assert_markup_error(msg){
throw (new Error(["Assert failed: ",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg),"\n","Render path: ",devtools.formatters.templating.pprint_render_path(devtools.formatters.templating._STAR_current_render_path_STAR_),"\n","Render stack:\n",devtools.formatters.templating.pprint_render_stack(devtools.formatters.templating._STAR_current_render_stack_STAR_)].join(''),"\n","false"].join('')));

});
devtools.formatters.templating.surrogate_markup_QMARK_ = (function devtools$formatters$templating$surrogate_markup_QMARK_(markup){
return ((cljs.core.sequential_QMARK_(markup)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.first(markup),"surrogate")));
});
devtools.formatters.templating.render_special = (function devtools$formatters$templating$render_special(name,args){
var G__46052 = name;
switch (G__46052) {
case "surrogate":
var obj = cljs.core.first(args);
var converted_args = cljs.core.map.cljs$core$IFn$_invoke$arity$2(devtools.formatters.templating.render_json_ml_STAR_,cljs.core.rest(args));
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(devtools.formatters.templating.make_surrogate,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [obj], null),converted_args));

break;
case "reference":
var obj = cljs.core.first(args);
var converted_obj = ((devtools.formatters.templating.surrogate_markup_QMARK_(obj))?(devtools.formatters.templating.render_json_ml_STAR_.cljs$core$IFn$_invoke$arity$1 ? devtools.formatters.templating.render_json_ml_STAR_.cljs$core$IFn$_invoke$arity$1(obj) : devtools.formatters.templating.render_json_ml_STAR_.call(null,obj)):obj);
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(devtools.formatters.templating.make_reference,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [converted_obj], null),cljs.core.rest(args)));

break;
default:
return devtools.formatters.templating.assert_markup_error(["no matching special tag name: '",cljs.core.str.cljs$core$IFn$_invoke$arity$1(name),"'"].join(''));

}
});
devtools.formatters.templating.emptyish_QMARK_ = (function devtools$formatters$templating$emptyish_QMARK_(v){
if(((cljs.core.seqable_QMARK_(v)) || (cljs.core.array_QMARK_(v)) || (typeof v === 'string'))){
return cljs.core.empty_QMARK_(v);
} else {
return false;
}
});
devtools.formatters.templating.render_subtree = (function devtools$formatters$templating$render_subtree(tag,children){
var vec__46059 = tag;
var html_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46059,(0),null);
var style = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46059,(1),null);
return cljs.core.apply.cljs$core$IFn$_invoke$arity$4(devtools.formatters.templating.make_template,html_tag,style,cljs.core.map.cljs$core$IFn$_invoke$arity$2(devtools.formatters.templating.render_json_ml_STAR_,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(devtools.formatters.templating.emptyish_QMARK_,cljs.core.map.cljs$core$IFn$_invoke$arity$2(devtools.formatters.helpers.pref,children))));
});
devtools.formatters.templating.render_json_ml_STAR_ = (function devtools$formatters$templating$render_json_ml_STAR_(markup){
if((!(cljs.core.sequential_QMARK_(markup)))){
return markup;
} else {
var _STAR_current_render_path_STAR__orig_val__46072 = devtools.formatters.templating._STAR_current_render_path_STAR_;
var _STAR_current_render_path_STAR__temp_val__46073 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(devtools.formatters.templating._STAR_current_render_path_STAR_,cljs.core.first(markup));
(devtools.formatters.templating._STAR_current_render_path_STAR_ = _STAR_current_render_path_STAR__temp_val__46073);

try{var tag = devtools.formatters.helpers.pref(cljs.core.first(markup));
if(typeof tag === 'string'){
return devtools.formatters.templating.render_special(tag,cljs.core.rest(markup));
} else {
if(cljs.core.sequential_QMARK_(tag)){
return devtools.formatters.templating.render_subtree(tag,cljs.core.rest(markup));
} else {
return devtools.formatters.templating.assert_markup_error(["invalid json-ml markup at ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(devtools.formatters.templating.print_preview(markup)),":"].join(''));

}
}
}finally {(devtools.formatters.templating._STAR_current_render_path_STAR_ = _STAR_current_render_path_STAR__orig_val__46072);
}}
});
devtools.formatters.templating.render_json_ml = (function devtools$formatters$templating$render_json_ml(markup){
var _STAR_current_render_stack_STAR__orig_val__46080 = devtools.formatters.templating._STAR_current_render_stack_STAR_;
var _STAR_current_render_path_STAR__orig_val__46081 = devtools.formatters.templating._STAR_current_render_path_STAR_;
var _STAR_current_render_stack_STAR__temp_val__46082 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(devtools.formatters.templating._STAR_current_render_stack_STAR_,markup);
var _STAR_current_render_path_STAR__temp_val__46083 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(devtools.formatters.templating._STAR_current_render_path_STAR_,"<render-json-ml>");
(devtools.formatters.templating._STAR_current_render_stack_STAR_ = _STAR_current_render_stack_STAR__temp_val__46082);

(devtools.formatters.templating._STAR_current_render_path_STAR_ = _STAR_current_render_path_STAR__temp_val__46083);

try{return devtools.formatters.templating.render_json_ml_STAR_(markup);
}finally {(devtools.formatters.templating._STAR_current_render_path_STAR_ = _STAR_current_render_path_STAR__orig_val__46081);

(devtools.formatters.templating._STAR_current_render_stack_STAR_ = _STAR_current_render_stack_STAR__orig_val__46080);
}});
devtools.formatters.templating.assert_failed_markup_rendering = (function devtools$formatters$templating$assert_failed_markup_rendering(initial_value,value){
throw (new Error(["Assert failed: ",["result of markup rendering must be a template,\n","resolved to ",devtools.util.pprint_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([value], 0)),"initial value: ",devtools.util.pprint_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([initial_value], 0))].join(''),"\n","false"].join('')));

});
devtools.formatters.templating.render_markup_STAR_ = (function devtools$formatters$templating$render_markup_STAR_(initial_value,value){
while(true){
if(cljs.core.fn_QMARK_(value)){
var G__46436 = initial_value;
var G__46437 = (value.cljs$core$IFn$_invoke$arity$0 ? value.cljs$core$IFn$_invoke$arity$0() : value.call(null));
initial_value = G__46436;
value = G__46437;
continue;
} else {
if((value instanceof cljs.core.Keyword)){
var G__46438 = initial_value;
var G__46439 = devtools.formatters.helpers.pref(value);
initial_value = G__46438;
value = G__46439;
continue;
} else {
if(cljs.core.sequential_QMARK_(value)){
var G__46444 = initial_value;
var G__46445 = devtools.formatters.templating.render_json_ml(value);
initial_value = G__46444;
value = G__46445;
continue;
} else {
if(devtools.formatters.templating.template_QMARK_(value)){
return value;
} else {
if(devtools.formatters.templating.surrogate_QMARK_(value)){
return value;
} else {
if(devtools.formatters.templating.reference_QMARK_(value)){
return value;
} else {
return devtools.formatters.templating.assert_failed_markup_rendering.call(null,initial_value,value);

}
}
}
}
}
}
break;
}
});
devtools.formatters.templating.render_markup = (function devtools$formatters$templating$render_markup(value){
return devtools.formatters.templating.render_markup_STAR_(value,value);
});
Object.defineProperty(module.exports, "make_template", { enumerable: false, get: function() { return devtools.formatters.templating.make_template; } });
Object.defineProperty(module.exports, "mark_as_surrogate_BANG_", { enumerable: false, get: function() { return devtools.formatters.templating.mark_as_surrogate_BANG_; } });
Object.defineProperty(module.exports, "concat_templates_BANG_", { enumerable: false, get: function() { return devtools.formatters.templating.concat_templates_BANG_; } });
Object.defineProperty(module.exports, "make_reference", { enumerable: false, get: function() { return devtools.formatters.templating.make_reference; } });
Object.defineProperty(module.exports, "make_surrogate", { enumerable: false, get: function() { return devtools.formatters.templating.make_surrogate; } });
Object.defineProperty(module.exports, "pprint_render_path", { enumerable: false, get: function() { return devtools.formatters.templating.pprint_render_path; } });
Object.defineProperty(module.exports, "get_surrogate_body", { enumerable: false, get: function() { return devtools.formatters.templating.get_surrogate_body; } });
Object.defineProperty(module.exports, "reference_QMARK_", { enumerable: false, get: function() { return devtools.formatters.templating.reference_QMARK_; } });
Object.defineProperty(module.exports, "replace_fns_with_markers", { enumerable: false, get: function() { return devtools.formatters.templating.replace_fns_with_markers; } });
Object.defineProperty(module.exports, "_STAR_current_render_path_STAR_", { enumerable: false, get: function() { return devtools.formatters.templating._STAR_current_render_path_STAR_; } });
Object.defineProperty(module.exports, "render_subtree", { enumerable: false, get: function() { return devtools.formatters.templating.render_subtree; } });
Object.defineProperty(module.exports, "emptyish_QMARK_", { enumerable: false, get: function() { return devtools.formatters.templating.emptyish_QMARK_; } });
Object.defineProperty(module.exports, "assert_failed_markup_rendering", { enumerable: false, get: function() { return devtools.formatters.templating.assert_failed_markup_rendering; } });
Object.defineProperty(module.exports, "pprint_render_calls", { enumerable: false, get: function() { return devtools.formatters.templating.pprint_render_calls; } });
Object.defineProperty(module.exports, "render_markup", { enumerable: false, get: function() { return devtools.formatters.templating.render_markup; } });
Object.defineProperty(module.exports, "assert_markup_error", { enumerable: false, get: function() { return devtools.formatters.templating.assert_markup_error; } });
Object.defineProperty(module.exports, "surrogate_markup_QMARK_", { enumerable: false, get: function() { return devtools.formatters.templating.surrogate_markup_QMARK_; } });
Object.defineProperty(module.exports, "render_markup_STAR_", { enumerable: false, get: function() { return devtools.formatters.templating.render_markup_STAR_; } });
Object.defineProperty(module.exports, "add_stack_separators", { enumerable: false, get: function() { return devtools.formatters.templating.add_stack_separators; } });
Object.defineProperty(module.exports, "make_group", { enumerable: false, get: function() { return devtools.formatters.templating.make_group; } });
Object.defineProperty(module.exports, "get_surrogate_header", { enumerable: false, get: function() { return devtools.formatters.templating.get_surrogate_header; } });
Object.defineProperty(module.exports, "render_json_ml_STAR_", { enumerable: false, get: function() { return devtools.formatters.templating.render_json_ml_STAR_; } });
Object.defineProperty(module.exports, "surrogate_QMARK_", { enumerable: false, get: function() { return devtools.formatters.templating.surrogate_QMARK_; } });
Object.defineProperty(module.exports, "render_json_ml", { enumerable: false, get: function() { return devtools.formatters.templating.render_json_ml; } });
Object.defineProperty(module.exports, "_STAR_current_render_stack_STAR_", { enumerable: false, get: function() { return devtools.formatters.templating._STAR_current_render_stack_STAR_; } });
Object.defineProperty(module.exports, "mark_as_group_BANG_", { enumerable: false, get: function() { return devtools.formatters.templating.mark_as_group_BANG_; } });
Object.defineProperty(module.exports, "get_surrogate_target", { enumerable: false, get: function() { return devtools.formatters.templating.get_surrogate_target; } });
Object.defineProperty(module.exports, "get_surrogate_start_index", { enumerable: false, get: function() { return devtools.formatters.templating.get_surrogate_start_index; } });
Object.defineProperty(module.exports, "mark_as_template_BANG_", { enumerable: false, get: function() { return devtools.formatters.templating.mark_as_template_BANG_; } });
Object.defineProperty(module.exports, "pprint_render_stack", { enumerable: false, get: function() { return devtools.formatters.templating.pprint_render_stack; } });
Object.defineProperty(module.exports, "template_QMARK_", { enumerable: false, get: function() { return devtools.formatters.templating.template_QMARK_; } });
Object.defineProperty(module.exports, "render_special", { enumerable: false, get: function() { return devtools.formatters.templating.render_special; } });
Object.defineProperty(module.exports, "print_preview", { enumerable: false, get: function() { return devtools.formatters.templating.print_preview; } });
Object.defineProperty(module.exports, "group_QMARK_", { enumerable: false, get: function() { return devtools.formatters.templating.group_QMARK_; } });
Object.defineProperty(module.exports, "extend_template_BANG_", { enumerable: false, get: function() { return devtools.formatters.templating.extend_template_BANG_; } });
//# sourceMappingURL=devtools.formatters.templating.js.map

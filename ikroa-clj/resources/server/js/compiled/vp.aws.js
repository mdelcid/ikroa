var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./vp.macros.js");
require("./cljs.core.async.js");
require("./clojure.string.js");
require("./shadow.js.shim.module$meteor$meteor.js");
require("./shadow.js.shim.module$meteor$fetch.js");
require("./cljs.core.async.interop.js");
require("./vp.rapyd.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var camel_snake_kebab=$CLJS.camel_snake_kebab || ($CLJS.camel_snake_kebab = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("vp.aws.js");

goog.provide('vp.aws');
vp.aws.upload_url = (function vp$aws$upload_url(file){
if(cljs.core.truth_(Meteor.isServer)){
var access_key = "H040OPAMO1Q8765FDYB2";
var secret_key = "ymCTunLyv3uaPQIuMNYASERzJViz1dvz7PcDoNGk";
var client = (1);
var fname_parts = clojure.string.split.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(file),/\./);
var fname = clojure.string.join.cljs$core$IFn$_invoke$arity$2("",cljs.core.butlast(fname_parts));
var ext = cljs.core.last(fname_parts);
return null;
} else {
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_68622){
var state_val_68623 = (state_68622[(1)]);
if((state_val_68623 === (1))){
var inst_68609 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_68610 = [file];
var inst_68612 = (new cljs.core.PersistentVector(null,1,(5),inst_68609,inst_68610,null));
var inst_68613 = vp.meteor.t_write(inst_68612);
var inst_68614 = vp.meteor.call("vp.aws.upload-url",inst_68613);
var state_68622__$1 = state_68622;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68622__$1,(2),inst_68614);
} else {
if((state_val_68623 === (2))){
var inst_68617 = (state_68622[(2)]);
var inst_68618 = vp.meteor.t_read(inst_68617);
var state_68622__$1 = state_68622;
return cljs.core.async.impl.ioc_helpers.return_chan(state_68622__$1,inst_68618);
} else {
return null;
}
}
});
return (function() {
var vp$aws$upload_url_$_state_machine__41292__auto__ = null;
var vp$aws$upload_url_$_state_machine__41292__auto____0 = (function (){
var statearr_68634 = [null,null,null,null,null,null,null];
(statearr_68634[(0)] = vp$aws$upload_url_$_state_machine__41292__auto__);

(statearr_68634[(1)] = (1));

return statearr_68634;
});
var vp$aws$upload_url_$_state_machine__41292__auto____1 = (function (state_68622){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_68622);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e68638){var ex__41295__auto__ = e68638;
var statearr_68640_68793 = state_68622;
(statearr_68640_68793[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_68622[(4)]))){
var statearr_68641_68796 = state_68622;
(statearr_68641_68796[(1)] = cljs.core.first((state_68622[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__68801 = state_68622;
state_68622 = G__68801;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$aws$upload_url_$_state_machine__41292__auto__ = function(state_68622){
switch(arguments.length){
case 0:
return vp$aws$upload_url_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$aws$upload_url_$_state_machine__41292__auto____1.call(this,state_68622);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$aws$upload_url_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$aws$upload_url_$_state_machine__41292__auto____0;
vp$aws$upload_url_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$aws$upload_url_$_state_machine__41292__auto____1;
return vp$aws$upload_url_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_68647 = f__41438__auto__();
(statearr_68647[(6)] = c__41437__auto__);

return statearr_68647;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
}
});
goog.exportSymbol('vp.aws.upload_url', vp.aws.upload_url);

if(cljs.core.truth_(Meteor.isServer)){
Meteor["methods"](cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 1, ["vp.aws.upload-url",(function (args__42089__auto__){
return vp.meteor.to_promise((function (){var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_68704){
var state_val_68705 = (state_68704[(1)]);
if((state_val_68705 === (1))){
var inst_68692 = vp.meteor.t_read(args__42089__auto__);
var inst_68696 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(vp.aws.upload_url,inst_68692);
var state_68704__$1 = state_68704;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_68704__$1,(2),inst_68696);
} else {
if((state_val_68705 === (2))){
var inst_68699 = (state_68704[(2)]);
var inst_68700 = vp.meteor.t_write(inst_68699);
var state_68704__$1 = state_68704;
return cljs.core.async.impl.ioc_helpers.return_chan(state_68704__$1,inst_68700);
} else {
return null;
}
}
});
return (function() {
var vp$aws$state_machine__41292__auto__ = null;
var vp$aws$state_machine__41292__auto____0 = (function (){
var statearr_68740 = [null,null,null,null,null,null,null];
(statearr_68740[(0)] = vp$aws$state_machine__41292__auto__);

(statearr_68740[(1)] = (1));

return statearr_68740;
});
var vp$aws$state_machine__41292__auto____1 = (function (state_68704){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_68704);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e68742){var ex__41295__auto__ = e68742;
var statearr_68743_68824 = state_68704;
(statearr_68743_68824[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_68704[(4)]))){
var statearr_68747_68825 = state_68704;
(statearr_68747_68825[(1)] = cljs.core.first((state_68704[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__68843 = state_68704;
state_68704 = G__68843;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$aws$state_machine__41292__auto__ = function(state_68704){
switch(arguments.length){
case 0:
return vp$aws$state_machine__41292__auto____0.call(this);
case 1:
return vp$aws$state_machine__41292__auto____1.call(this,state_68704);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$aws$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$aws$state_machine__41292__auto____0;
vp$aws$state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$aws$state_machine__41292__auto____1;
return vp$aws$state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_68755 = f__41438__auto__();
(statearr_68755[(6)] = c__41437__auto__);

return statearr_68755;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
})());
})], null)));
} else {
}
Object.defineProperty(module.exports, "upload_url", { enumerable: true, get: function() { return vp.aws.upload_url; } });
//# sourceMappingURL=vp.aws.js.map

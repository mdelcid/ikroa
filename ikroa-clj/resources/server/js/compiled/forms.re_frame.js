var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./clojure.string.js");
require("./forms.util.js");
require("./forms.dirty.js");
require("./re_frame.core.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("forms.re_frame.js");

goog.provide('forms.re_frame');
forms.re_frame.init_state = (function forms$re_frame$init_state(data,validator,opts){
return new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"errors","errors",-908790718),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"init-data","init-data",372811248),data,new cljs.core.Keyword(null,"data","data",-232669377),(function (){var or__4126__auto__ = data;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
})(),new cljs.core.Keyword(null,"cached-dirty-key-paths","cached-dirty-key-paths",-611573122),cljs.core.PersistentHashSet.EMPTY,new cljs.core.Keyword(null,"dirty-key-paths","dirty-key-paths",-818120204),cljs.core.PersistentHashSet.EMPTY,new cljs.core.Keyword(null,"validator","validator",-1966190681),validator,new cljs.core.Keyword(null,"opts","opts",155075701),opts], null);
});
/**
 * Calculates the error key paths from the error map. It is used to mark
 *   all invalid key paths as dirty
 */
forms.re_frame.errors_keypaths = (function forms$re_frame$errors_keypaths(var_args){
var G__45335 = arguments.length;
switch (G__45335) {
case 1:
return forms.re_frame.errors_keypaths.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 3:
return forms.re_frame.errors_keypaths.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(forms.re_frame.errors_keypaths.cljs$core$IFn$_invoke$arity$1 = (function (data){
return cljs.core.distinct.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"results","results",-1134170113).cljs$core$IFn$_invoke$arity$1(forms.re_frame.errors_keypaths.cljs$core$IFn$_invoke$arity$3(data,cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"results","results",-1134170113),cljs.core.PersistentVector.EMPTY], null))));
}));

(forms.re_frame.errors_keypaths.cljs$core$IFn$_invoke$arity$3 = (function (data,path,results){
return cljs.core.reduce_kv((function (m,k,v){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(k,new cljs.core.Keyword(null,"$errors$","$errors$",634888771))){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m,new cljs.core.Keyword(null,"results","results",-1134170113),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"results","results",-1134170113).cljs$core$IFn$_invoke$arity$1(m),path));
} else {
if(((cljs.core.vector_QMARK_(v)) || (cljs.core.map_QMARK_(v)))){
var map__45343 = m;
var map__45343__$1 = (((((!((map__45343 == null))))?(((((map__45343.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45343.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__45343):map__45343);
var results__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__45343__$1,new cljs.core.Keyword(null,"results","results",-1134170113));
var lengths = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__45343__$1,new cljs.core.Keyword(null,"lengths","lengths",-851104122));
var new_path = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(path,k);
var child_paths = forms.re_frame.errors_keypaths.cljs$core$IFn$_invoke$arity$3(v,new_path,m);
var new_results = new cljs.core.Keyword(null,"results","results",-1134170113).cljs$core$IFn$_invoke$arity$1(child_paths);
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"results","results",-1134170113),cljs.core.concat.cljs$core$IFn$_invoke$arity$2(results__$1,new_results)], null);
} else {
if((v == null)){
return m;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m,new cljs.core.Keyword(null,"results","results",-1134170113),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"results","results",-1134170113).cljs$core$IFn$_invoke$arity$1(m),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(path,k)));
}
}
}
}),results,data);
}));

(forms.re_frame.errors_keypaths.cljs$lang$maxFixedArity = 3);

forms.re_frame.form_path__GT_db_path = (function forms$re_frame$form_path__GT_db_path(form_path){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(form_path,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","form","forms.re-frame/form",-1853455825)], null));
});
re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("forms.re-frame","form","forms.re-frame/form",-1853455825),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function forms$re_frame$form(db,p__45349){
var vec__45350 = p__45349;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45350,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45350,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,forms.re_frame.form_path__GT_db_path(form_path));
})], 0));
re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("forms.re-frame","data","forms.re-frame/data",9831181),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function forms$re_frame$data(db,p__45358){
var vec__45359 = p__45358;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45359,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45359,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,cljs.core.conj.cljs$core$IFn$_invoke$arity$2(forms.re_frame.form_path__GT_db_path(form_path),new cljs.core.Keyword(null,"data","data",-232669377)));
})], 0));
re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("forms.re-frame","cached-dirty-key-paths","forms.re-frame/cached-dirty-key-paths",-1439146416),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function forms$re_frame$data(db,p__45362){
var vec__45365 = p__45362;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45365,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45365,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,cljs.core.conj.cljs$core$IFn$_invoke$arity$2(forms.re_frame.form_path__GT_db_path(form_path),new cljs.core.Keyword(null,"cached-dirty-key-paths","cached-dirty-key-paths",-611573122)));
})], 0));
re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("forms.re-frame","dirty-key-paths","forms.re-frame/dirty-key-paths",1596997286),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function forms$re_frame$data(db,p__45369){
var vec__45370 = p__45369;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45370,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45370,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,cljs.core.conj.cljs$core$IFn$_invoke$arity$2(forms.re_frame.form_path__GT_db_path(form_path),new cljs.core.Keyword(null,"dirty-key-paths","dirty-key-paths",-818120204)));
})], 0));
re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("forms.re-frame","data-for-path","forms.re-frame/data-for-path",49205032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function forms$re_frame$data_for_path(db,p__45375){
var vec__45376 = p__45375;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45376,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45376,(1),null);
var key_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45376,(2),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.conj.cljs$core$IFn$_invoke$arity$2(forms.re_frame.form_path__GT_db_path(form_path),new cljs.core.Keyword(null,"data","data",-232669377)),forms.util.key_to_path(key_path)));
})], 0));
re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("forms.re-frame","errors","forms.re-frame/errors",1998665856),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function forms$re_frame$errors(db,p__45385){
var vec__45387 = p__45385;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45387,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45387,(1),null);
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,cljs.core.conj.cljs$core$IFn$_invoke$arity$2(forms.re_frame.form_path__GT_db_path(form_path),new cljs.core.Keyword(null,"errors","errors",-908790718)));
})], 0));
forms.re_frame.errors_for_path = (function forms$re_frame$errors_for_path(db,p__45390){
var vec__45391 = p__45390;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45391,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45391,(1),null);
var key_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45391,(2),null);
var path = forms.util.key_to_path(key_path);
var is_dirty_QMARK_ = cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"dirty-key-paths","dirty-key-paths",-818120204).cljs$core$IFn$_invoke$arity$1(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,forms.re_frame.form_path__GT_db_path(form_path))),path);
if(is_dirty_QMARK_){
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.conj.cljs$core$IFn$_invoke$arity$2(forms.re_frame.form_path__GT_db_path(form_path),new cljs.core.Keyword(null,"errors","errors",-908790718)),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(path,new cljs.core.Keyword(null,"$errors$","$errors$",634888771))));
} else {
return null;
}
});
re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("forms.re-frame","errors-for-path","forms.re-frame/errors-for-path",-1106864696),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([forms.re_frame.errors_for_path], 0));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("forms.re-frame","init!","forms.re-frame/init!",-672020802),(function forms$re_frame$init_BANG_(db,p__45395){
var vec__45398 = p__45395;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45398,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45398,(1),null);
var form_data = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45398,(2),null);
return cljs.core.assoc_in(db,forms.re_frame.form_path__GT_db_path(form_path),form_data);
}));
forms.re_frame.mark_dirty_BANG_ = (function forms$re_frame$mark_dirty_BANG_(form_state){
var validator = new cljs.core.Keyword(null,"validator","validator",-1966190681).cljs$core$IFn$_invoke$arity$1(form_state);
var errors = (function (){var G__45408 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(form_state);
return (validator.cljs$core$IFn$_invoke$arity$1 ? validator.cljs$core$IFn$_invoke$arity$1(G__45408) : validator.call(null,G__45408));
})();
var errors_keypaths = forms.re_frame.errors_keypaths.cljs$core$IFn$_invoke$arity$1(errors);
var current_dirty_paths = new cljs.core.Keyword(null,"dirty-key-paths","dirty-key-paths",-818120204).cljs$core$IFn$_invoke$arity$1(form_state);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(form_state,new cljs.core.Keyword(null,"cached-dirty-key-paths","cached-dirty-key-paths",-611573122),cljs.core.set(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"cached-dirty-key-paths","cached-dirty-key-paths",-611573122).cljs$core$IFn$_invoke$arity$1(form_state),errors_keypaths)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"dirty-key-paths","dirty-key-paths",-818120204),cljs.core.set(errors_keypaths)], 0));
});
forms.re_frame.mark_dirty_paths_BANG_ = (function forms$re_frame$mark_dirty_paths_BANG_(form_state){
var dirty_paths = forms.dirty.calculate_dirty_fields(new cljs.core.Keyword(null,"init-data","init-data",372811248).cljs$core$IFn$_invoke$arity$1(form_state),new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(form_state));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(form_state,new cljs.core.Keyword(null,"dirty-key-paths","dirty-key-paths",-818120204),cljs.core.set(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(dirty_paths,new cljs.core.Keyword(null,"cached-dirty-key-paths","cached-dirty-key-paths",-611573122).cljs$core$IFn$_invoke$arity$1(form_state))));
});
forms.re_frame.validate_BANG_ = (function forms$re_frame$validate_BANG_(db,p__45442){
var vec__45443 = p__45442;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45443,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45443,(1),null);
var dirty_only_QMARK_ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45443,(2),null);
var dirty_db = (cljs.core.truth_(dirty_only_QMARK_)?cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,forms.re_frame.form_path__GT_db_path(form_path),forms.re_frame.mark_dirty_paths_BANG_):cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,forms.re_frame.form_path__GT_db_path(form_path),forms.re_frame.mark_dirty_BANG_));
var validator = new cljs.core.Keyword(null,"validator","validator",-1966190681).cljs$core$IFn$_invoke$arity$1(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(dirty_db,forms.re_frame.form_path__GT_db_path(form_path)));
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$5(dirty_db,cljs.core.conj.cljs$core$IFn$_invoke$arity$1(forms.re_frame.form_path__GT_db_path(form_path)),cljs.core.assoc,new cljs.core.Keyword(null,"errors","errors",-908790718),(function (){var G__45451 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(dirty_db,forms.re_frame.form_path__GT_db_path(form_path)));
return (validator.cljs$core$IFn$_invoke$arity$1 ? validator.cljs$core$IFn$_invoke$arity$1(G__45451) : validator.call(null,G__45451));
})());
});
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("forms.re-frame","validate!","forms.re-frame/validate!",-1266339907),forms.re_frame.validate_BANG_);
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("forms.re-frame","commit!","forms.re-frame/commit!",823025996),(function forms$re_frame$commit_BANG_(db,p__45455){
var vec__45457 = p__45455;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45457,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45457,(1),null);
var form_state = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,forms.re_frame.form_path__GT_db_path(form_path));
var commit_fn = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"opts","opts",155075701),new cljs.core.Keyword(null,"on-commit","on-commit",551275063)], null));
var dirty_db = cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,forms.re_frame.form_path__GT_db_path(form_path),forms.re_frame.mark_dirty_BANG_);
var validated_db = forms.re_frame.validate_BANG_(dirty_db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,form_path], null));
var new_form_state = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(validated_db,forms.re_frame.form_path__GT_db_path(form_path));
(commit_fn.cljs$core$IFn$_invoke$arity$1 ? commit_fn.cljs$core$IFn$_invoke$arity$1(new_form_state) : commit_fn.call(null,new_form_state));

return validated_db;
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("forms.re-frame","update!","forms.re-frame/update!",-1164872664),(function forms$re_frame$update_BANG_(db,p__45468){
var vec__45470 = p__45468;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45470,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45470,(1),null);
var data = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45470,(2),null);
var updated_db = cljs.core.update_in.cljs$core$IFn$_invoke$arity$5(db,forms.re_frame.form_path__GT_db_path(form_path),cljs.core.assoc,new cljs.core.Keyword(null,"data","data",-232669377),data);
var dirty_db = cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(updated_db,forms.re_frame.form_path__GT_db_path(form_path),forms.re_frame.mark_dirty_paths_BANG_);
return forms.re_frame.validate_BANG_(dirty_db,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,form_path,true], null));
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("forms.re-frame","mark-dirty!","forms.re-frame/mark-dirty!",-393473751),(function forms$re_frame$mark_dirty_BANG__ev(db,p__45476){
var vec__45477 = p__45476;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45477,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45477,(1),null);
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,forms.re_frame.form_path__GT_db_path(form_path),forms.re_frame.mark_dirty_BANG_);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("forms.re-frame","mark-dirty-paths!","forms.re-frame/mark-dirty-paths!",1362620376),(function forms$re_frame$mark_dirty_paths_BANG__ev(db,p__45485){
var vec__45491 = p__45485;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45491,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45491,(1),null);
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(db,forms.re_frame.form_path__GT_db_path(form_path),forms.re_frame.mark_dirty_paths_BANG_);
}));
re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("forms.re-frame","dirty-paths-valid?","forms.re-frame/dirty-paths-valid?",1749864658),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function forms$re_frame$dirty_paths_valid_QMARK_(db,p__45502){
var vec__45508 = p__45502;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45508,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45508,(1),null);
var form_state = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,forms.re_frame.form_path__GT_db_path(form_path));
var errors = new cljs.core.Keyword(null,"errors","errors",-908790718).cljs$core$IFn$_invoke$arity$1(form_state);
var dirty_paths = new cljs.core.Keyword(null,"dirty-key-paths","dirty-key-paths",-818120204).cljs$core$IFn$_invoke$arity$1(form_state);
var valid_paths = cljs.core.take_while.cljs$core$IFn$_invoke$arity$2((function (path){
return (cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(errors,path) == null);
}),dirty_paths);
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.count(valid_paths),cljs.core.count(dirty_paths));
})], 0));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("forms.re-frame","clear-cached-dirty-key-paths!","forms.re-frame/clear-cached-dirty-key-paths!",-755330760),(function forms$re_frame$mark_dirty_paths_BANG__ev(db,p__45522){
var vec__45526 = p__45522;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45526,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45526,(1),null);
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$5(db,forms.re_frame.form_path__GT_db_path(form_path),cljs.core.assoc,new cljs.core.Keyword(null,"cached-dirty-key-paths","cached-dirty-key-paths",-611573122),cljs.core.PersistentHashSet.EMPTY);
}));
forms.re_frame.is_valid_QMARK_ = (function forms$re_frame$is_valid_QMARK_(form_state){
var errors = new cljs.core.Keyword(null,"errors","errors",-908790718).cljs$core$IFn$_invoke$arity$1(form_state);
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(errors,cljs.core.PersistentArrayMap.EMPTY);
});
re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("forms.re-frame","is-valid?","forms.re-frame/is-valid?",13167751),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function forms$re_frame$is_valid_QMARK__ev(db,p__45550){
var vec__45551 = p__45550;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45551,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45551,(1),null);
var form_state = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,forms.re_frame.form_path__GT_db_path(form_path));
return forms.re_frame.is_valid_QMARK_(form_state);
})], 0));
re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword("forms.re-frame","is-valid-path?","forms.re-frame/is-valid-path?",-781879906),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function forms$re_frame$is_valid_path_QMARK_(db,p__45556){
var vec__45558 = p__45556;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45558,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45558,(1),null);
var key_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45558,(2),null);
return (forms.re_frame.errors_for_path(db,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,form_path,key_path], null)) == null);
})], 0));
forms.re_frame.with_default_opts = (function forms$re_frame$with_default_opts(opts){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"on-commit","on-commit",551275063),(function forms$re_frame$with_default_opts_$_on_commit_placeholder(_){
return null;
}),new cljs.core.Keyword(null,"auto-validate?","auto-validate?",866667103),false], null),opts], 0));
});
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("forms.re-frame","reset-form!","forms.re-frame/reset-form!",-1253227900),(function forms$re_frame$reset_form_BANG_(db,p__45572){
var vec__45574 = p__45572;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45574,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45574,(1),null);
var init_data_STAR_ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45574,(2),null);
var map__45580 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,forms.re_frame.form_path__GT_db_path(form_path));
var map__45580__$1 = (((((!((map__45580 == null))))?(((((map__45580.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45580.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__45580):map__45580);
var init_data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__45580__$1,new cljs.core.Keyword(null,"init-data","init-data",372811248));
var validator = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__45580__$1,new cljs.core.Keyword(null,"validator","validator",-1966190681));
var opts = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__45580__$1,new cljs.core.Keyword(null,"opts","opts",155075701));
if(cljs.core.truth_(init_data_STAR_)){
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(db,forms.re_frame.form_path__GT_db_path(form_path),cljs.core.merge,forms.re_frame.init_state(init_data_STAR_,validator,forms.re_frame.with_default_opts(opts)));
} else {
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(db,forms.re_frame.form_path__GT_db_path(form_path),cljs.core.merge,forms.re_frame.init_state(init_data,validator,forms.re_frame.with_default_opts(opts)));
}
}));
forms.re_frame.set_value = (function forms$re_frame$set_value(db,p__45607){
var vec__45608 = p__45607;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45608,(0),null);
var form_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45608,(1),null);
var key_path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45608,(2),null);
var value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45608,(3),null);
var form_state = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,forms.re_frame.form_path__GT_db_path(form_path));
var auto_validate_QMARK_ = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(form_state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"opts","opts",155075701),new cljs.core.Keyword(null,"auto-validate?","auto-validate?",866667103)], null));
var setted_db = cljs.core.assoc_in(db,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.conj.cljs$core$IFn$_invoke$arity$2(forms.re_frame.form_path__GT_db_path(form_path),new cljs.core.Keyword(null,"data","data",-232669377)),forms.util.key_to_path(key_path)),value);
if(cljs.core.truth_(auto_validate_QMARK_)){
var old_value = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.conj.cljs$core$IFn$_invoke$arity$2(forms.re_frame.form_path__GT_db_path(form_path),new cljs.core.Keyword(null,"data","data",-232669377)),forms.util.key_to_path(key_path)));
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(value,old_value)){
return setted_db;
} else {
return forms.re_frame.validate_BANG_(cljs.core.update_in.cljs$core$IFn$_invoke$arity$3(setted_db,forms.re_frame.form_path__GT_db_path(form_path),forms.re_frame.mark_dirty_paths_BANG_),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,form_path,true], null));
}
} else {
return setted_db;
}
});
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("forms.re-frame","set!","forms.re-frame/set!",498933512),forms.re_frame.set_value);
/**
 * Form constructor. It accepts the following arguments:
 * 
 *   - `validator` - returned either by the `form.validator/validator` or `form.validator/comp-validators` function
 *   - `path` - path in the db where to put the form
 *   - `data` - initial data map
 *   - `opts` - map with the form options:
 *    + `:on-commit` - function to be called when the form is commited (by calling `(commit! form)`)
 *    + `:auto-validate?` - should the form be validated on any data change
 */
forms.re_frame.constructor$ = (function forms$re_frame$constructor(var_args){
var G__45630 = arguments.length;
switch (G__45630) {
case 1:
return forms.re_frame.constructor$.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return forms.re_frame.constructor$.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return forms.re_frame.constructor$.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return forms.re_frame.constructor$.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(forms.re_frame.constructor$.cljs$core$IFn$_invoke$arity$1 = (function (validator){
return cljs.core.partial.cljs$core$IFn$_invoke$arity$2(forms.re_frame.constructor$,validator);
}));

(forms.re_frame.constructor$.cljs$core$IFn$_invoke$arity$2 = (function (validator,path){
return cljs.core.partial.cljs$core$IFn$_invoke$arity$3(forms.re_frame.constructor$,validator,path);
}));

(forms.re_frame.constructor$.cljs$core$IFn$_invoke$arity$3 = (function (validator,path,data){
return forms.re_frame.constructor$.cljs$core$IFn$_invoke$arity$4(validator,path,data,cljs.core.PersistentArrayMap.EMPTY);
}));

(forms.re_frame.constructor$.cljs$core$IFn$_invoke$arity$4 = (function (validator,path,data,opts){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("forms.re-frame","init!","forms.re-frame/init!",-672020802),path,forms.re_frame.init_state(data,validator,forms.re_frame.with_default_opts(opts))], null));
}));

(forms.re_frame.constructor$.cljs$lang$maxFixedArity = 4);

Object.defineProperty(module.exports, "errors_keypaths", { enumerable: false, get: function() { return forms.re_frame.errors_keypaths; } });
Object.defineProperty(module.exports, "validate_BANG_", { enumerable: false, get: function() { return forms.re_frame.validate_BANG_; } });
Object.defineProperty(module.exports, "mark_dirty_paths_BANG_", { enumerable: false, get: function() { return forms.re_frame.mark_dirty_paths_BANG_; } });
Object.defineProperty(module.exports, "mark_dirty_BANG_", { enumerable: false, get: function() { return forms.re_frame.mark_dirty_BANG_; } });
Object.defineProperty(module.exports, "errors_for_path", { enumerable: false, get: function() { return forms.re_frame.errors_for_path; } });
Object.defineProperty(module.exports, "init_state", { enumerable: false, get: function() { return forms.re_frame.init_state; } });
Object.defineProperty(module.exports, "is_valid_QMARK_", { enumerable: false, get: function() { return forms.re_frame.is_valid_QMARK_; } });
Object.defineProperty(module.exports, "constructor$", { enumerable: false, get: function() { return forms.re_frame.constructor$; } });
Object.defineProperty(module.exports, "form_path__GT_db_path", { enumerable: false, get: function() { return forms.re_frame.form_path__GT_db_path; } });
Object.defineProperty(module.exports, "with_default_opts", { enumerable: false, get: function() { return forms.re_frame.with_default_opts; } });
Object.defineProperty(module.exports, "set_value", { enumerable: false, get: function() { return forms.re_frame.set_value; } });
//# sourceMappingURL=forms.re_frame.js.map

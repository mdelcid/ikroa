var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./cljs.core.async.impl.protocols.js");
require("./cljs.core.async.impl.channels.js");
require("./cljs.core.async.impl.buffers.js");
require("./cljs.core.async.impl.timers.js");
require("./cljs.core.async.impl.dispatch.js");
require("./cljs.core.async.impl.ioc_helpers.js");
require("./goog.array.array.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("cljs.core.async.js");

goog.provide('cljs.core.async');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__45745 = arguments.length;
switch (G__45745) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
}));

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async45755 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async45755 = (function (f,blockable,meta45756){
this.f = f;
this.blockable = blockable;
this.meta45756 = meta45756;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async45755.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_45757,meta45756__$1){
var self__ = this;
var _45757__$1 = this;
return (new cljs.core.async.t_cljs$core$async45755(self__.f,self__.blockable,meta45756__$1));
}));

(cljs.core.async.t_cljs$core$async45755.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_45757){
var self__ = this;
var _45757__$1 = this;
return self__.meta45756;
}));

(cljs.core.async.t_cljs$core$async45755.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async45755.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async45755.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
}));

(cljs.core.async.t_cljs$core$async45755.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
}));

(cljs.core.async.t_cljs$core$async45755.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta45756","meta45756",2124404334,null)], null);
}));

(cljs.core.async.t_cljs$core$async45755.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async45755.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async45755");

(cljs.core.async.t_cljs$core$async45755.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async45755");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async45755.
 */
cljs.core.async.__GT_t_cljs$core$async45755 = (function cljs$core$async$__GT_t_cljs$core$async45755(f__$1,blockable__$1,meta45756){
return (new cljs.core.async.t_cljs$core$async45755(f__$1,blockable__$1,meta45756));
});

}

return (new cljs.core.async.t_cljs$core$async45755(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
}));

(cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2);

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__45810 = arguments.length;
switch (G__45810) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
}));

(cljs.core.async.chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__45840 = arguments.length;
switch (G__45840) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
}));

(cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__45861 = arguments.length;
switch (G__45861) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
}));

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_49474 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_49474) : fn1.call(null,val_49474));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_49474) : fn1.call(null,val_49474));
}));
}
} else {
}

return null;
}));

(cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3);

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__45889 = arguments.length;
switch (G__45889) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5733__auto__)){
var ret = temp__5733__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5733__auto__)){
var retb = temp__5733__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
}));
}

return ret;
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4);

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4613__auto___49478 = n;
var x_49479 = (0);
while(true){
if((x_49479 < n__4613__auto___49478)){
(a[x_49479] = x_49479);

var G__49480 = (x_49479 + (1));
x_49479 = G__49480;
continue;
} else {
}
break;
}

goog.array.shuffle(a);

return a;
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async45915 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async45915 = (function (flag,meta45916){
this.flag = flag;
this.meta45916 = meta45916;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async45915.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_45917,meta45916__$1){
var self__ = this;
var _45917__$1 = this;
return (new cljs.core.async.t_cljs$core$async45915(self__.flag,meta45916__$1));
}));

(cljs.core.async.t_cljs$core$async45915.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_45917){
var self__ = this;
var _45917__$1 = this;
return self__.meta45916;
}));

(cljs.core.async.t_cljs$core$async45915.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async45915.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
}));

(cljs.core.async.t_cljs$core$async45915.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async45915.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
}));

(cljs.core.async.t_cljs$core$async45915.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta45916","meta45916",-2021303477,null)], null);
}));

(cljs.core.async.t_cljs$core$async45915.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async45915.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async45915");

(cljs.core.async.t_cljs$core$async45915.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async45915");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async45915.
 */
cljs.core.async.__GT_t_cljs$core$async45915 = (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async45915(flag__$1,meta45916){
return (new cljs.core.async.t_cljs$core$async45915(flag__$1,meta45916));
});

}

return (new cljs.core.async.t_cljs$core$async45915(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async45940 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async45940 = (function (flag,cb,meta45941){
this.flag = flag;
this.cb = cb;
this.meta45941 = meta45941;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async45940.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_45942,meta45941__$1){
var self__ = this;
var _45942__$1 = this;
return (new cljs.core.async.t_cljs$core$async45940(self__.flag,self__.cb,meta45941__$1));
}));

(cljs.core.async.t_cljs$core$async45940.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_45942){
var self__ = this;
var _45942__$1 = this;
return self__.meta45941;
}));

(cljs.core.async.t_cljs$core$async45940.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async45940.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
}));

(cljs.core.async.t_cljs$core$async45940.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async45940.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
}));

(cljs.core.async.t_cljs$core$async45940.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta45941","meta45941",410930980,null)], null);
}));

(cljs.core.async.t_cljs$core$async45940.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async45940.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async45940");

(cljs.core.async.t_cljs$core$async45940.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async45940");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async45940.
 */
cljs.core.async.__GT_t_cljs$core$async45940 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async45940(flag__$1,cb__$1,meta45941){
return (new cljs.core.async.t_cljs$core$async45940(flag__$1,cb__$1,meta45941));
});

}

return (new cljs.core.async.t_cljs$core$async45940(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__45958_SHARP_){
var G__45971 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__45958_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__45971) : fret.call(null,G__45971));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__45959_SHARP_){
var G__45972 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__45959_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__45972) : fret.call(null,G__45972));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__4126__auto__ = wport;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return port;
}
})()], null));
} else {
var G__49498 = (i + (1));
i = G__49498;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4126__auto__ = ret;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5735__auto__ = (function (){var and__4115__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__4115__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__4115__auto__;
}
})();
if(cljs.core.truth_(temp__5735__auto__)){
var got = temp__5735__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___49501 = arguments.length;
var i__4737__auto___49502 = (0);
while(true){
if((i__4737__auto___49502 < len__4736__auto___49501)){
args__4742__auto__.push((arguments[i__4737__auto___49502]));

var G__49505 = (i__4737__auto___49502 + (1));
i__4737__auto___49502 = G__49505;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__45984){
var map__45987 = p__45984;
var map__45987__$1 = (((((!((map__45987 == null))))?(((((map__45987.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45987.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__45987):map__45987);
var opts = map__45987__$1;
throw (new Error("alts! used not in (go ...) block"));
}));

(cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq45978){
var G__45979 = cljs.core.first(seq45978);
var seq45978__$1 = cljs.core.next(seq45978);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__45979,seq45978__$1);
}));

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__46008 = arguments.length;
switch (G__46008) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
}));

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__45585__auto___49522 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_46050){
var state_val_46051 = (state_46050[(1)]);
if((state_val_46051 === (7))){
var inst_46046 = (state_46050[(2)]);
var state_46050__$1 = state_46050;
var statearr_46058_49527 = state_46050__$1;
(statearr_46058_49527[(2)] = inst_46046);

(statearr_46058_49527[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46051 === (1))){
var state_46050__$1 = state_46050;
var statearr_46063_49529 = state_46050__$1;
(statearr_46063_49529[(2)] = null);

(statearr_46063_49529[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46051 === (4))){
var inst_46027 = (state_46050[(7)]);
var inst_46027__$1 = (state_46050[(2)]);
var inst_46029 = (inst_46027__$1 == null);
var state_46050__$1 = (function (){var statearr_46069 = state_46050;
(statearr_46069[(7)] = inst_46027__$1);

return statearr_46069;
})();
if(cljs.core.truth_(inst_46029)){
var statearr_46071_49533 = state_46050__$1;
(statearr_46071_49533[(1)] = (5));

} else {
var statearr_46074_49534 = state_46050__$1;
(statearr_46074_49534[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46051 === (13))){
var state_46050__$1 = state_46050;
var statearr_46076_49535 = state_46050__$1;
(statearr_46076_49535[(2)] = null);

(statearr_46076_49535[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46051 === (6))){
var inst_46027 = (state_46050[(7)]);
var state_46050__$1 = state_46050;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_46050__$1,(11),to,inst_46027);
} else {
if((state_val_46051 === (3))){
var inst_46048 = (state_46050[(2)]);
var state_46050__$1 = state_46050;
return cljs.core.async.impl.ioc_helpers.return_chan(state_46050__$1,inst_46048);
} else {
if((state_val_46051 === (12))){
var state_46050__$1 = state_46050;
var statearr_46085_49538 = state_46050__$1;
(statearr_46085_49538[(2)] = null);

(statearr_46085_49538[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46051 === (2))){
var state_46050__$1 = state_46050;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_46050__$1,(4),from);
} else {
if((state_val_46051 === (11))){
var inst_46039 = (state_46050[(2)]);
var state_46050__$1 = state_46050;
if(cljs.core.truth_(inst_46039)){
var statearr_46099_49542 = state_46050__$1;
(statearr_46099_49542[(1)] = (12));

} else {
var statearr_46100_49543 = state_46050__$1;
(statearr_46100_49543[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46051 === (9))){
var state_46050__$1 = state_46050;
var statearr_46104_49546 = state_46050__$1;
(statearr_46104_49546[(2)] = null);

(statearr_46104_49546[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46051 === (5))){
var state_46050__$1 = state_46050;
if(cljs.core.truth_(close_QMARK_)){
var statearr_46105_49548 = state_46050__$1;
(statearr_46105_49548[(1)] = (8));

} else {
var statearr_46107_49549 = state_46050__$1;
(statearr_46107_49549[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46051 === (14))){
var inst_46044 = (state_46050[(2)]);
var state_46050__$1 = state_46050;
var statearr_46115_49551 = state_46050__$1;
(statearr_46115_49551[(2)] = inst_46044);

(statearr_46115_49551[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46051 === (10))){
var inst_46036 = (state_46050[(2)]);
var state_46050__$1 = state_46050;
var statearr_46118_49555 = state_46050__$1;
(statearr_46118_49555[(2)] = inst_46036);

(statearr_46118_49555[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46051 === (8))){
var inst_46032 = cljs.core.async.close_BANG_(to);
var state_46050__$1 = state_46050;
var statearr_46122_49556 = state_46050__$1;
(statearr_46122_49556[(2)] = inst_46032);

(statearr_46122_49556[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__45510__auto__ = null;
var cljs$core$async$state_machine__45510__auto____0 = (function (){
var statearr_46128 = [null,null,null,null,null,null,null,null];
(statearr_46128[(0)] = cljs$core$async$state_machine__45510__auto__);

(statearr_46128[(1)] = (1));

return statearr_46128;
});
var cljs$core$async$state_machine__45510__auto____1 = (function (state_46050){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_46050);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e46129){var ex__45513__auto__ = e46129;
var statearr_46133_49563 = state_46050;
(statearr_46133_49563[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_46050[(4)]))){
var statearr_46134_49565 = state_46050;
(statearr_46134_49565[(1)] = cljs.core.first((state_46050[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__49569 = state_46050;
state_46050 = G__49569;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$state_machine__45510__auto__ = function(state_46050){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__45510__auto____1.call(this,state_46050);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__45510__auto____0;
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__45510__auto____1;
return cljs$core$async$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_46141 = f__45586__auto__();
(statearr_46141[(6)] = c__45585__auto___49522);

return statearr_46141;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return to;
}));

(cljs.core.async.pipe.cljs$lang$maxFixedArity = 3);

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process = (function (p__46170){
var vec__46172 = p__46170;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46172,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46172,(1),null);
var job = vec__46172;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__45585__auto___49577 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_46188){
var state_val_46189 = (state_46188[(1)]);
if((state_val_46189 === (1))){
var state_46188__$1 = state_46188;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_46188__$1,(2),res,v);
} else {
if((state_val_46189 === (2))){
var inst_46184 = (state_46188[(2)]);
var inst_46185 = cljs.core.async.close_BANG_(res);
var state_46188__$1 = (function (){var statearr_46209 = state_46188;
(statearr_46209[(7)] = inst_46184);

return statearr_46209;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_46188__$1,inst_46185);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0 = (function (){
var statearr_46219 = [null,null,null,null,null,null,null,null];
(statearr_46219[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__);

(statearr_46219[(1)] = (1));

return statearr_46219;
});
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1 = (function (state_46188){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_46188);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e46224){var ex__45513__auto__ = e46224;
var statearr_46225_49589 = state_46188;
(statearr_46225_49589[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_46188[(4)]))){
var statearr_46228_49590 = state_46188;
(statearr_46228_49590[(1)] = cljs.core.first((state_46188[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__49591 = state_46188;
state_46188 = G__49591;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__ = function(state_46188){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1.call(this,state_46188);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_46247 = f__45586__auto__();
(statearr_46247[(6)] = c__45585__auto___49577);

return statearr_46247;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var async = (function (p__46274){
var vec__46276 = p__46274;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46276,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46276,(1),null);
var job = vec__46276;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var n__4613__auto___49597 = n;
var __49600 = (0);
while(true){
if((__49600 < n__4613__auto___49597)){
var G__46283_49603 = type;
var G__46283_49604__$1 = (((G__46283_49603 instanceof cljs.core.Keyword))?G__46283_49603.fqn:null);
switch (G__46283_49604__$1) {
case "compute":
var c__45585__auto___49607 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__49600,c__45585__auto___49607,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async){
return (function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = ((function (__49600,c__45585__auto___49607,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async){
return (function (state_46299){
var state_val_46300 = (state_46299[(1)]);
if((state_val_46300 === (1))){
var state_46299__$1 = state_46299;
var statearr_46312_49613 = state_46299__$1;
(statearr_46312_49613[(2)] = null);

(statearr_46312_49613[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46300 === (2))){
var state_46299__$1 = state_46299;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_46299__$1,(4),jobs);
} else {
if((state_val_46300 === (3))){
var inst_46296 = (state_46299[(2)]);
var state_46299__$1 = state_46299;
return cljs.core.async.impl.ioc_helpers.return_chan(state_46299__$1,inst_46296);
} else {
if((state_val_46300 === (4))){
var inst_46288 = (state_46299[(2)]);
var inst_46289 = process(inst_46288);
var state_46299__$1 = state_46299;
if(cljs.core.truth_(inst_46289)){
var statearr_46329_49617 = state_46299__$1;
(statearr_46329_49617[(1)] = (5));

} else {
var statearr_46336_49618 = state_46299__$1;
(statearr_46336_49618[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46300 === (5))){
var state_46299__$1 = state_46299;
var statearr_46337_49620 = state_46299__$1;
(statearr_46337_49620[(2)] = null);

(statearr_46337_49620[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46300 === (6))){
var state_46299__$1 = state_46299;
var statearr_46338_49622 = state_46299__$1;
(statearr_46338_49622[(2)] = null);

(statearr_46338_49622[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46300 === (7))){
var inst_46294 = (state_46299[(2)]);
var state_46299__$1 = state_46299;
var statearr_46339_49624 = state_46299__$1;
(statearr_46339_49624[(2)] = inst_46294);

(statearr_46339_49624[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__49600,c__45585__auto___49607,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async))
;
return ((function (__49600,switch__45509__auto__,c__45585__auto___49607,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0 = (function (){
var statearr_46349 = [null,null,null,null,null,null,null];
(statearr_46349[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__);

(statearr_46349[(1)] = (1));

return statearr_46349;
});
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1 = (function (state_46299){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_46299);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e46350){var ex__45513__auto__ = e46350;
var statearr_46352_49628 = state_46299;
(statearr_46352_49628[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_46299[(4)]))){
var statearr_46354_49630 = state_46299;
(statearr_46354_49630[(1)] = cljs.core.first((state_46299[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__49631 = state_46299;
state_46299 = G__49631;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__ = function(state_46299){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1.call(this,state_46299);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__;
})()
;})(__49600,switch__45509__auto__,c__45585__auto___49607,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async))
})();
var state__45587__auto__ = (function (){var statearr_46357 = f__45586__auto__();
(statearr_46357[(6)] = c__45585__auto___49607);

return statearr_46357;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
});})(__49600,c__45585__auto___49607,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async))
);


break;
case "async":
var c__45585__auto___49632 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__49600,c__45585__auto___49632,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async){
return (function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = ((function (__49600,c__45585__auto___49632,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async){
return (function (state_46378){
var state_val_46379 = (state_46378[(1)]);
if((state_val_46379 === (1))){
var state_46378__$1 = state_46378;
var statearr_46392_49636 = state_46378__$1;
(statearr_46392_49636[(2)] = null);

(statearr_46392_49636[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46379 === (2))){
var state_46378__$1 = state_46378;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_46378__$1,(4),jobs);
} else {
if((state_val_46379 === (3))){
var inst_46376 = (state_46378[(2)]);
var state_46378__$1 = state_46378;
return cljs.core.async.impl.ioc_helpers.return_chan(state_46378__$1,inst_46376);
} else {
if((state_val_46379 === (4))){
var inst_46368 = (state_46378[(2)]);
var inst_46369 = async(inst_46368);
var state_46378__$1 = state_46378;
if(cljs.core.truth_(inst_46369)){
var statearr_46399_49644 = state_46378__$1;
(statearr_46399_49644[(1)] = (5));

} else {
var statearr_46406_49645 = state_46378__$1;
(statearr_46406_49645[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46379 === (5))){
var state_46378__$1 = state_46378;
var statearr_46408_49646 = state_46378__$1;
(statearr_46408_49646[(2)] = null);

(statearr_46408_49646[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46379 === (6))){
var state_46378__$1 = state_46378;
var statearr_46421_49647 = state_46378__$1;
(statearr_46421_49647[(2)] = null);

(statearr_46421_49647[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46379 === (7))){
var inst_46374 = (state_46378[(2)]);
var state_46378__$1 = state_46378;
var statearr_46425_49648 = state_46378__$1;
(statearr_46425_49648[(2)] = inst_46374);

(statearr_46425_49648[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__49600,c__45585__auto___49632,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async))
;
return ((function (__49600,switch__45509__auto__,c__45585__auto___49632,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0 = (function (){
var statearr_46431 = [null,null,null,null,null,null,null];
(statearr_46431[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__);

(statearr_46431[(1)] = (1));

return statearr_46431;
});
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1 = (function (state_46378){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_46378);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e46433){var ex__45513__auto__ = e46433;
var statearr_46435_49650 = state_46378;
(statearr_46435_49650[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_46378[(4)]))){
var statearr_46440_49651 = state_46378;
(statearr_46440_49651[(1)] = cljs.core.first((state_46378[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__49652 = state_46378;
state_46378 = G__49652;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__ = function(state_46378){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1.call(this,state_46378);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__;
})()
;})(__49600,switch__45509__auto__,c__45585__auto___49632,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async))
})();
var state__45587__auto__ = (function (){var statearr_46451 = f__45586__auto__();
(statearr_46451[(6)] = c__45585__auto___49632);

return statearr_46451;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
});})(__49600,c__45585__auto___49632,G__46283_49603,G__46283_49604__$1,n__4613__auto___49597,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__46283_49604__$1)].join('')));

}

var G__49653 = (__49600 + (1));
__49600 = G__49653;
continue;
} else {
}
break;
}

var c__45585__auto___49654 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_46487){
var state_val_46488 = (state_46487[(1)]);
if((state_val_46488 === (7))){
var inst_46483 = (state_46487[(2)]);
var state_46487__$1 = state_46487;
var statearr_46519_49659 = state_46487__$1;
(statearr_46519_49659[(2)] = inst_46483);

(statearr_46519_49659[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46488 === (1))){
var state_46487__$1 = state_46487;
var statearr_46521_49661 = state_46487__$1;
(statearr_46521_49661[(2)] = null);

(statearr_46521_49661[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46488 === (4))){
var inst_46461 = (state_46487[(7)]);
var inst_46461__$1 = (state_46487[(2)]);
var inst_46462 = (inst_46461__$1 == null);
var state_46487__$1 = (function (){var statearr_46522 = state_46487;
(statearr_46522[(7)] = inst_46461__$1);

return statearr_46522;
})();
if(cljs.core.truth_(inst_46462)){
var statearr_46523_49663 = state_46487__$1;
(statearr_46523_49663[(1)] = (5));

} else {
var statearr_46524_49664 = state_46487__$1;
(statearr_46524_49664[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46488 === (6))){
var inst_46461 = (state_46487[(7)]);
var inst_46467 = (state_46487[(8)]);
var inst_46467__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_46473 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_46474 = [inst_46461,inst_46467__$1];
var inst_46475 = (new cljs.core.PersistentVector(null,2,(5),inst_46473,inst_46474,null));
var state_46487__$1 = (function (){var statearr_46527 = state_46487;
(statearr_46527[(8)] = inst_46467__$1);

return statearr_46527;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_46487__$1,(8),jobs,inst_46475);
} else {
if((state_val_46488 === (3))){
var inst_46485 = (state_46487[(2)]);
var state_46487__$1 = state_46487;
return cljs.core.async.impl.ioc_helpers.return_chan(state_46487__$1,inst_46485);
} else {
if((state_val_46488 === (2))){
var state_46487__$1 = state_46487;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_46487__$1,(4),from);
} else {
if((state_val_46488 === (9))){
var inst_46479 = (state_46487[(2)]);
var state_46487__$1 = (function (){var statearr_46538 = state_46487;
(statearr_46538[(9)] = inst_46479);

return statearr_46538;
})();
var statearr_46539_49665 = state_46487__$1;
(statearr_46539_49665[(2)] = null);

(statearr_46539_49665[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46488 === (5))){
var inst_46464 = cljs.core.async.close_BANG_(jobs);
var state_46487__$1 = state_46487;
var statearr_46540_49666 = state_46487__$1;
(statearr_46540_49666[(2)] = inst_46464);

(statearr_46540_49666[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46488 === (8))){
var inst_46467 = (state_46487[(8)]);
var inst_46477 = (state_46487[(2)]);
var state_46487__$1 = (function (){var statearr_46546 = state_46487;
(statearr_46546[(10)] = inst_46477);

return statearr_46546;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_46487__$1,(9),results,inst_46467);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0 = (function (){
var statearr_46548 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_46548[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__);

(statearr_46548[(1)] = (1));

return statearr_46548;
});
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1 = (function (state_46487){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_46487);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e46549){var ex__45513__auto__ = e46549;
var statearr_46552_49676 = state_46487;
(statearr_46552_49676[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_46487[(4)]))){
var statearr_46554_49677 = state_46487;
(statearr_46554_49677[(1)] = cljs.core.first((state_46487[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__49678 = state_46487;
state_46487 = G__49678;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__ = function(state_46487){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1.call(this,state_46487);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_46556 = f__45586__auto__();
(statearr_46556[(6)] = c__45585__auto___49654);

return statearr_46556;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


var c__45585__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_46600){
var state_val_46601 = (state_46600[(1)]);
if((state_val_46601 === (7))){
var inst_46596 = (state_46600[(2)]);
var state_46600__$1 = state_46600;
var statearr_46603_49682 = state_46600__$1;
(statearr_46603_49682[(2)] = inst_46596);

(statearr_46603_49682[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (20))){
var state_46600__$1 = state_46600;
var statearr_46604_49687 = state_46600__$1;
(statearr_46604_49687[(2)] = null);

(statearr_46604_49687[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (1))){
var state_46600__$1 = state_46600;
var statearr_46605_49690 = state_46600__$1;
(statearr_46605_49690[(2)] = null);

(statearr_46605_49690[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (4))){
var inst_46560 = (state_46600[(7)]);
var inst_46560__$1 = (state_46600[(2)]);
var inst_46561 = (inst_46560__$1 == null);
var state_46600__$1 = (function (){var statearr_46610 = state_46600;
(statearr_46610[(7)] = inst_46560__$1);

return statearr_46610;
})();
if(cljs.core.truth_(inst_46561)){
var statearr_46611_49695 = state_46600__$1;
(statearr_46611_49695[(1)] = (5));

} else {
var statearr_46612_49696 = state_46600__$1;
(statearr_46612_49696[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (15))){
var inst_46577 = (state_46600[(8)]);
var state_46600__$1 = state_46600;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_46600__$1,(18),to,inst_46577);
} else {
if((state_val_46601 === (21))){
var inst_46591 = (state_46600[(2)]);
var state_46600__$1 = state_46600;
var statearr_46613_49698 = state_46600__$1;
(statearr_46613_49698[(2)] = inst_46591);

(statearr_46613_49698[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (13))){
var inst_46593 = (state_46600[(2)]);
var state_46600__$1 = (function (){var statearr_46615 = state_46600;
(statearr_46615[(9)] = inst_46593);

return statearr_46615;
})();
var statearr_46616_49700 = state_46600__$1;
(statearr_46616_49700[(2)] = null);

(statearr_46616_49700[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (6))){
var inst_46560 = (state_46600[(7)]);
var state_46600__$1 = state_46600;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_46600__$1,(11),inst_46560);
} else {
if((state_val_46601 === (17))){
var inst_46586 = (state_46600[(2)]);
var state_46600__$1 = state_46600;
if(cljs.core.truth_(inst_46586)){
var statearr_46617_49707 = state_46600__$1;
(statearr_46617_49707[(1)] = (19));

} else {
var statearr_46618_49708 = state_46600__$1;
(statearr_46618_49708[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (3))){
var inst_46598 = (state_46600[(2)]);
var state_46600__$1 = state_46600;
return cljs.core.async.impl.ioc_helpers.return_chan(state_46600__$1,inst_46598);
} else {
if((state_val_46601 === (12))){
var inst_46570 = (state_46600[(10)]);
var state_46600__$1 = state_46600;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_46600__$1,(14),inst_46570);
} else {
if((state_val_46601 === (2))){
var state_46600__$1 = state_46600;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_46600__$1,(4),results);
} else {
if((state_val_46601 === (19))){
var state_46600__$1 = state_46600;
var statearr_46632_49709 = state_46600__$1;
(statearr_46632_49709[(2)] = null);

(statearr_46632_49709[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (11))){
var inst_46570 = (state_46600[(2)]);
var state_46600__$1 = (function (){var statearr_46633 = state_46600;
(statearr_46633[(10)] = inst_46570);

return statearr_46633;
})();
var statearr_46634_49713 = state_46600__$1;
(statearr_46634_49713[(2)] = null);

(statearr_46634_49713[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (9))){
var state_46600__$1 = state_46600;
var statearr_46638_49714 = state_46600__$1;
(statearr_46638_49714[(2)] = null);

(statearr_46638_49714[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (5))){
var state_46600__$1 = state_46600;
if(cljs.core.truth_(close_QMARK_)){
var statearr_46639_49718 = state_46600__$1;
(statearr_46639_49718[(1)] = (8));

} else {
var statearr_46640_49722 = state_46600__$1;
(statearr_46640_49722[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (14))){
var inst_46577 = (state_46600[(8)]);
var inst_46577__$1 = (state_46600[(2)]);
var inst_46579 = (inst_46577__$1 == null);
var inst_46580 = cljs.core.not(inst_46579);
var state_46600__$1 = (function (){var statearr_46646 = state_46600;
(statearr_46646[(8)] = inst_46577__$1);

return statearr_46646;
})();
if(inst_46580){
var statearr_46647_49725 = state_46600__$1;
(statearr_46647_49725[(1)] = (15));

} else {
var statearr_46648_49726 = state_46600__$1;
(statearr_46648_49726[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (16))){
var state_46600__$1 = state_46600;
var statearr_46649_49728 = state_46600__$1;
(statearr_46649_49728[(2)] = false);

(statearr_46649_49728[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (10))){
var inst_46567 = (state_46600[(2)]);
var state_46600__$1 = state_46600;
var statearr_46654_49732 = state_46600__$1;
(statearr_46654_49732[(2)] = inst_46567);

(statearr_46654_49732[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (18))){
var inst_46583 = (state_46600[(2)]);
var state_46600__$1 = state_46600;
var statearr_46659_49734 = state_46600__$1;
(statearr_46659_49734[(2)] = inst_46583);

(statearr_46659_49734[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46601 === (8))){
var inst_46564 = cljs.core.async.close_BANG_(to);
var state_46600__$1 = state_46600;
var statearr_46660_49735 = state_46600__$1;
(statearr_46660_49735[(2)] = inst_46564);

(statearr_46660_49735[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0 = (function (){
var statearr_46662 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_46662[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__);

(statearr_46662[(1)] = (1));

return statearr_46662;
});
var cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1 = (function (state_46600){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_46600);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e46663){var ex__45513__auto__ = e46663;
var statearr_46666_49739 = state_46600;
(statearr_46666_49739[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_46600[(4)]))){
var statearr_46667_49740 = state_46600;
(statearr_46667_49740[(1)] = cljs.core.first((state_46600[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__49741 = state_46600;
state_46600 = G__49741;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__ = function(state_46600){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1.call(this,state_46600);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__45510__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_46669 = f__45586__auto__();
(statearr_46669[(6)] = c__45585__auto__);

return statearr_46669;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));

return c__45585__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__46675 = arguments.length;
switch (G__46675) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
}));

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
}));

(cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5);

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__46682 = arguments.length;
switch (G__46682) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
}));

(cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6);

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__46699 = arguments.length;
switch (G__46699) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
}));

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__45585__auto___49749 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_46728){
var state_val_46729 = (state_46728[(1)]);
if((state_val_46729 === (7))){
var inst_46724 = (state_46728[(2)]);
var state_46728__$1 = state_46728;
var statearr_46730_49751 = state_46728__$1;
(statearr_46730_49751[(2)] = inst_46724);

(statearr_46730_49751[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46729 === (1))){
var state_46728__$1 = state_46728;
var statearr_46731_49754 = state_46728__$1;
(statearr_46731_49754[(2)] = null);

(statearr_46731_49754[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46729 === (4))){
var inst_46704 = (state_46728[(7)]);
var inst_46704__$1 = (state_46728[(2)]);
var inst_46705 = (inst_46704__$1 == null);
var state_46728__$1 = (function (){var statearr_46732 = state_46728;
(statearr_46732[(7)] = inst_46704__$1);

return statearr_46732;
})();
if(cljs.core.truth_(inst_46705)){
var statearr_46733_49755 = state_46728__$1;
(statearr_46733_49755[(1)] = (5));

} else {
var statearr_46734_49757 = state_46728__$1;
(statearr_46734_49757[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46729 === (13))){
var state_46728__$1 = state_46728;
var statearr_46736_49759 = state_46728__$1;
(statearr_46736_49759[(2)] = null);

(statearr_46736_49759[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46729 === (6))){
var inst_46704 = (state_46728[(7)]);
var inst_46711 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_46704) : p.call(null,inst_46704));
var state_46728__$1 = state_46728;
if(cljs.core.truth_(inst_46711)){
var statearr_46737_49763 = state_46728__$1;
(statearr_46737_49763[(1)] = (9));

} else {
var statearr_46738_49764 = state_46728__$1;
(statearr_46738_49764[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46729 === (3))){
var inst_46726 = (state_46728[(2)]);
var state_46728__$1 = state_46728;
return cljs.core.async.impl.ioc_helpers.return_chan(state_46728__$1,inst_46726);
} else {
if((state_val_46729 === (12))){
var state_46728__$1 = state_46728;
var statearr_46739_49765 = state_46728__$1;
(statearr_46739_49765[(2)] = null);

(statearr_46739_49765[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46729 === (2))){
var state_46728__$1 = state_46728;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_46728__$1,(4),ch);
} else {
if((state_val_46729 === (11))){
var inst_46704 = (state_46728[(7)]);
var inst_46715 = (state_46728[(2)]);
var state_46728__$1 = state_46728;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_46728__$1,(8),inst_46715,inst_46704);
} else {
if((state_val_46729 === (9))){
var state_46728__$1 = state_46728;
var statearr_46740_49771 = state_46728__$1;
(statearr_46740_49771[(2)] = tc);

(statearr_46740_49771[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46729 === (5))){
var inst_46708 = cljs.core.async.close_BANG_(tc);
var inst_46709 = cljs.core.async.close_BANG_(fc);
var state_46728__$1 = (function (){var statearr_46741 = state_46728;
(statearr_46741[(8)] = inst_46708);

return statearr_46741;
})();
var statearr_46742_49773 = state_46728__$1;
(statearr_46742_49773[(2)] = inst_46709);

(statearr_46742_49773[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46729 === (14))){
var inst_46722 = (state_46728[(2)]);
var state_46728__$1 = state_46728;
var statearr_46746_49775 = state_46728__$1;
(statearr_46746_49775[(2)] = inst_46722);

(statearr_46746_49775[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46729 === (10))){
var state_46728__$1 = state_46728;
var statearr_46747_49777 = state_46728__$1;
(statearr_46747_49777[(2)] = fc);

(statearr_46747_49777[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46729 === (8))){
var inst_46717 = (state_46728[(2)]);
var state_46728__$1 = state_46728;
if(cljs.core.truth_(inst_46717)){
var statearr_46748_49779 = state_46728__$1;
(statearr_46748_49779[(1)] = (12));

} else {
var statearr_46749_49780 = state_46728__$1;
(statearr_46749_49780[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__45510__auto__ = null;
var cljs$core$async$state_machine__45510__auto____0 = (function (){
var statearr_46751 = [null,null,null,null,null,null,null,null,null];
(statearr_46751[(0)] = cljs$core$async$state_machine__45510__auto__);

(statearr_46751[(1)] = (1));

return statearr_46751;
});
var cljs$core$async$state_machine__45510__auto____1 = (function (state_46728){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_46728);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e46756){var ex__45513__auto__ = e46756;
var statearr_46757_49781 = state_46728;
(statearr_46757_49781[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_46728[(4)]))){
var statearr_46758_49782 = state_46728;
(statearr_46758_49782[(1)] = cljs.core.first((state_46728[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__49783 = state_46728;
state_46728 = G__49783;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$state_machine__45510__auto__ = function(state_46728){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__45510__auto____1.call(this,state_46728);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__45510__auto____0;
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__45510__auto____1;
return cljs$core$async$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_46759 = f__45586__auto__();
(statearr_46759[(6)] = c__45585__auto___49749);

return statearr_46759;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
}));

(cljs.core.async.split.cljs$lang$maxFixedArity = 4);

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__45585__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_46817){
var state_val_46818 = (state_46817[(1)]);
if((state_val_46818 === (7))){
var inst_46805 = (state_46817[(2)]);
var state_46817__$1 = state_46817;
var statearr_46837_49788 = state_46817__$1;
(statearr_46837_49788[(2)] = inst_46805);

(statearr_46837_49788[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46818 === (1))){
var inst_46776 = init;
var inst_46777 = inst_46776;
var state_46817__$1 = (function (){var statearr_46841 = state_46817;
(statearr_46841[(7)] = inst_46777);

return statearr_46841;
})();
var statearr_46842_49796 = state_46817__$1;
(statearr_46842_49796[(2)] = null);

(statearr_46842_49796[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46818 === (4))){
var inst_46785 = (state_46817[(8)]);
var inst_46785__$1 = (state_46817[(2)]);
var inst_46786 = (inst_46785__$1 == null);
var state_46817__$1 = (function (){var statearr_46843 = state_46817;
(statearr_46843[(8)] = inst_46785__$1);

return statearr_46843;
})();
if(cljs.core.truth_(inst_46786)){
var statearr_46844_49804 = state_46817__$1;
(statearr_46844_49804[(1)] = (5));

} else {
var statearr_46845_49805 = state_46817__$1;
(statearr_46845_49805[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46818 === (6))){
var inst_46785 = (state_46817[(8)]);
var inst_46777 = (state_46817[(7)]);
var inst_46793 = (state_46817[(9)]);
var inst_46793__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_46777,inst_46785) : f.call(null,inst_46777,inst_46785));
var inst_46794 = cljs.core.reduced_QMARK_(inst_46793__$1);
var state_46817__$1 = (function (){var statearr_46846 = state_46817;
(statearr_46846[(9)] = inst_46793__$1);

return statearr_46846;
})();
if(inst_46794){
var statearr_46848_49808 = state_46817__$1;
(statearr_46848_49808[(1)] = (8));

} else {
var statearr_46849_49813 = state_46817__$1;
(statearr_46849_49813[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46818 === (3))){
var inst_46811 = (state_46817[(2)]);
var state_46817__$1 = state_46817;
return cljs.core.async.impl.ioc_helpers.return_chan(state_46817__$1,inst_46811);
} else {
if((state_val_46818 === (2))){
var state_46817__$1 = state_46817;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_46817__$1,(4),ch);
} else {
if((state_val_46818 === (9))){
var inst_46793 = (state_46817[(9)]);
var inst_46777 = inst_46793;
var state_46817__$1 = (function (){var statearr_46854 = state_46817;
(statearr_46854[(7)] = inst_46777);

return statearr_46854;
})();
var statearr_46855_49821 = state_46817__$1;
(statearr_46855_49821[(2)] = null);

(statearr_46855_49821[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46818 === (5))){
var inst_46777 = (state_46817[(7)]);
var state_46817__$1 = state_46817;
var statearr_46856_49828 = state_46817__$1;
(statearr_46856_49828[(2)] = inst_46777);

(statearr_46856_49828[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46818 === (10))){
var inst_46803 = (state_46817[(2)]);
var state_46817__$1 = state_46817;
var statearr_46857_49829 = state_46817__$1;
(statearr_46857_49829[(2)] = inst_46803);

(statearr_46857_49829[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46818 === (8))){
var inst_46793 = (state_46817[(9)]);
var inst_46799 = cljs.core.deref(inst_46793);
var state_46817__$1 = state_46817;
var statearr_46858_49830 = state_46817__$1;
(statearr_46858_49830[(2)] = inst_46799);

(statearr_46858_49830[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$reduce_$_state_machine__45510__auto__ = null;
var cljs$core$async$reduce_$_state_machine__45510__auto____0 = (function (){
var statearr_46859 = [null,null,null,null,null,null,null,null,null,null];
(statearr_46859[(0)] = cljs$core$async$reduce_$_state_machine__45510__auto__);

(statearr_46859[(1)] = (1));

return statearr_46859;
});
var cljs$core$async$reduce_$_state_machine__45510__auto____1 = (function (state_46817){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_46817);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e46861){var ex__45513__auto__ = e46861;
var statearr_46862_49831 = state_46817;
(statearr_46862_49831[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_46817[(4)]))){
var statearr_46863_49832 = state_46817;
(statearr_46863_49832[(1)] = cljs.core.first((state_46817[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__49833 = state_46817;
state_46817 = G__49833;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__45510__auto__ = function(state_46817){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__45510__auto____1.call(this,state_46817);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__45510__auto____0;
cljs$core$async$reduce_$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__45510__auto____1;
return cljs$core$async$reduce_$_state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_46864 = f__45586__auto__();
(statearr_46864[(6)] = c__45585__auto__);

return statearr_46864;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));

return c__45585__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__45585__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_46881){
var state_val_46882 = (state_46881[(1)]);
if((state_val_46882 === (1))){
var inst_46872 = cljs.core.async.reduce(f__$1,init,ch);
var state_46881__$1 = state_46881;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_46881__$1,(2),inst_46872);
} else {
if((state_val_46882 === (2))){
var inst_46874 = (state_46881[(2)]);
var inst_46875 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_46874) : f__$1.call(null,inst_46874));
var state_46881__$1 = state_46881;
return cljs.core.async.impl.ioc_helpers.return_chan(state_46881__$1,inst_46875);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$transduce_$_state_machine__45510__auto__ = null;
var cljs$core$async$transduce_$_state_machine__45510__auto____0 = (function (){
var statearr_46887 = [null,null,null,null,null,null,null];
(statearr_46887[(0)] = cljs$core$async$transduce_$_state_machine__45510__auto__);

(statearr_46887[(1)] = (1));

return statearr_46887;
});
var cljs$core$async$transduce_$_state_machine__45510__auto____1 = (function (state_46881){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_46881);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e46888){var ex__45513__auto__ = e46888;
var statearr_46889_49846 = state_46881;
(statearr_46889_49846[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_46881[(4)]))){
var statearr_46892_49847 = state_46881;
(statearr_46892_49847[(1)] = cljs.core.first((state_46881[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__49848 = state_46881;
state_46881 = G__49848;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__45510__auto__ = function(state_46881){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__45510__auto____1.call(this,state_46881);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__45510__auto____0;
cljs$core$async$transduce_$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__45510__auto____1;
return cljs$core$async$transduce_$_state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_46893 = f__45586__auto__();
(statearr_46893[(6)] = c__45585__auto__);

return statearr_46893;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));

return c__45585__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan_BANG_ = (function cljs$core$async$onto_chan_BANG_(var_args){
var G__46897 = arguments.length;
switch (G__46897) {
case 2:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__45585__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_46929){
var state_val_46930 = (state_46929[(1)]);
if((state_val_46930 === (7))){
var inst_46911 = (state_46929[(2)]);
var state_46929__$1 = state_46929;
var statearr_46933_49854 = state_46929__$1;
(statearr_46933_49854[(2)] = inst_46911);

(statearr_46933_49854[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46930 === (1))){
var inst_46902 = cljs.core.seq(coll);
var inst_46903 = inst_46902;
var state_46929__$1 = (function (){var statearr_46937 = state_46929;
(statearr_46937[(7)] = inst_46903);

return statearr_46937;
})();
var statearr_46938_49858 = state_46929__$1;
(statearr_46938_49858[(2)] = null);

(statearr_46938_49858[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46930 === (4))){
var inst_46903 = (state_46929[(7)]);
var inst_46909 = cljs.core.first(inst_46903);
var state_46929__$1 = state_46929;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_46929__$1,(7),ch,inst_46909);
} else {
if((state_val_46930 === (13))){
var inst_46923 = (state_46929[(2)]);
var state_46929__$1 = state_46929;
var statearr_46946_49862 = state_46929__$1;
(statearr_46946_49862[(2)] = inst_46923);

(statearr_46946_49862[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46930 === (6))){
var inst_46914 = (state_46929[(2)]);
var state_46929__$1 = state_46929;
if(cljs.core.truth_(inst_46914)){
var statearr_46949_49869 = state_46929__$1;
(statearr_46949_49869[(1)] = (8));

} else {
var statearr_46951_49870 = state_46929__$1;
(statearr_46951_49870[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46930 === (3))){
var inst_46927 = (state_46929[(2)]);
var state_46929__$1 = state_46929;
return cljs.core.async.impl.ioc_helpers.return_chan(state_46929__$1,inst_46927);
} else {
if((state_val_46930 === (12))){
var state_46929__$1 = state_46929;
var statearr_46962_49874 = state_46929__$1;
(statearr_46962_49874[(2)] = null);

(statearr_46962_49874[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46930 === (2))){
var inst_46903 = (state_46929[(7)]);
var state_46929__$1 = state_46929;
if(cljs.core.truth_(inst_46903)){
var statearr_46965_49875 = state_46929__$1;
(statearr_46965_49875[(1)] = (4));

} else {
var statearr_46966_49876 = state_46929__$1;
(statearr_46966_49876[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46930 === (11))){
var inst_46920 = cljs.core.async.close_BANG_(ch);
var state_46929__$1 = state_46929;
var statearr_46967_49877 = state_46929__$1;
(statearr_46967_49877[(2)] = inst_46920);

(statearr_46967_49877[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46930 === (9))){
var state_46929__$1 = state_46929;
if(cljs.core.truth_(close_QMARK_)){
var statearr_46970_49878 = state_46929__$1;
(statearr_46970_49878[(1)] = (11));

} else {
var statearr_46971_49879 = state_46929__$1;
(statearr_46971_49879[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46930 === (5))){
var inst_46903 = (state_46929[(7)]);
var state_46929__$1 = state_46929;
var statearr_46975_49880 = state_46929__$1;
(statearr_46975_49880[(2)] = inst_46903);

(statearr_46975_49880[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46930 === (10))){
var inst_46925 = (state_46929[(2)]);
var state_46929__$1 = state_46929;
var statearr_46979_49881 = state_46929__$1;
(statearr_46979_49881[(2)] = inst_46925);

(statearr_46979_49881[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46930 === (8))){
var inst_46903 = (state_46929[(7)]);
var inst_46916 = cljs.core.next(inst_46903);
var inst_46903__$1 = inst_46916;
var state_46929__$1 = (function (){var statearr_46985 = state_46929;
(statearr_46985[(7)] = inst_46903__$1);

return statearr_46985;
})();
var statearr_46987_49885 = state_46929__$1;
(statearr_46987_49885[(2)] = null);

(statearr_46987_49885[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__45510__auto__ = null;
var cljs$core$async$state_machine__45510__auto____0 = (function (){
var statearr_46988 = [null,null,null,null,null,null,null,null];
(statearr_46988[(0)] = cljs$core$async$state_machine__45510__auto__);

(statearr_46988[(1)] = (1));

return statearr_46988;
});
var cljs$core$async$state_machine__45510__auto____1 = (function (state_46929){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_46929);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e46991){var ex__45513__auto__ = e46991;
var statearr_46993_49886 = state_46929;
(statearr_46993_49886[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_46929[(4)]))){
var statearr_46994_49889 = state_46929;
(statearr_46994_49889[(1)] = cljs.core.first((state_46929[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__49896 = state_46929;
state_46929 = G__49896;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$state_machine__45510__auto__ = function(state_46929){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__45510__auto____1.call(this,state_46929);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__45510__auto____0;
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__45510__auto____1;
return cljs$core$async$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_46998 = f__45586__auto__();
(statearr_46998[(6)] = c__45585__auto__);

return statearr_46998;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));

return c__45585__auto__;
}));

(cljs.core.async.onto_chan_BANG_.cljs$lang$maxFixedArity = 3);

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan_BANG_ = (function cljs$core$async$to_chan_BANG_(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});
/**
 * Deprecated - use onto-chan!
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__47010 = arguments.length;
switch (G__47010) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,close_QMARK_);
}));

(cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - use to-chan!
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
return cljs.core.async.to_chan_BANG_(coll);
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

var cljs$core$async$Mux$muxch_STAR_$dyn_49901 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
});
cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
return cljs$core$async$Mux$muxch_STAR_$dyn_49901(_);
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

var cljs$core$async$Mult$tap_STAR_$dyn_49905 = (function (m,ch,close_QMARK_){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4429__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__4426__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4426__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
});
cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
return cljs$core$async$Mult$tap_STAR_$dyn_49905(m,ch,close_QMARK_);
}
});

var cljs$core$async$Mult$untap_STAR_$dyn_49907 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
});
cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mult$untap_STAR_$dyn_49907(m,ch);
}
});

var cljs$core$async$Mult$untap_all_STAR_$dyn_49909 = (function (m){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4429__auto__.call(null,m));
} else {
var m__4426__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4426__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
});
cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mult$untap_all_STAR_$dyn_49909(m);
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async47034 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async47034 = (function (ch,cs,meta47035){
this.ch = ch;
this.cs = cs;
this.meta47035 = meta47035;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async47034.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_47036,meta47035__$1){
var self__ = this;
var _47036__$1 = this;
return (new cljs.core.async.t_cljs$core$async47034(self__.ch,self__.cs,meta47035__$1));
}));

(cljs.core.async.t_cljs$core$async47034.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_47036){
var self__ = this;
var _47036__$1 = this;
return self__.meta47035;
}));

(cljs.core.async.t_cljs$core$async47034.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async47034.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async47034.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async47034.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
}));

(cljs.core.async.t_cljs$core$async47034.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
}));

(cljs.core.async.t_cljs$core$async47034.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
}));

(cljs.core.async.t_cljs$core$async47034.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta47035","meta47035",809637774,null)], null);
}));

(cljs.core.async.t_cljs$core$async47034.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async47034.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async47034");

(cljs.core.async.t_cljs$core$async47034.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async47034");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async47034.
 */
cljs.core.async.__GT_t_cljs$core$async47034 = (function cljs$core$async$mult_$___GT_t_cljs$core$async47034(ch__$1,cs__$1,meta47035){
return (new cljs.core.async.t_cljs$core$async47034(ch__$1,cs__$1,meta47035));
});

}

return (new cljs.core.async.t_cljs$core$async47034(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});
var c__45585__auto___49918 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_47211){
var state_val_47212 = (state_47211[(1)]);
if((state_val_47212 === (7))){
var inst_47207 = (state_47211[(2)]);
var state_47211__$1 = state_47211;
var statearr_47213_49920 = state_47211__$1;
(statearr_47213_49920[(2)] = inst_47207);

(statearr_47213_49920[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (20))){
var inst_47092 = (state_47211[(7)]);
var inst_47108 = cljs.core.first(inst_47092);
var inst_47109 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_47108,(0),null);
var inst_47110 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_47108,(1),null);
var state_47211__$1 = (function (){var statearr_47214 = state_47211;
(statearr_47214[(8)] = inst_47109);

return statearr_47214;
})();
if(cljs.core.truth_(inst_47110)){
var statearr_47215_49921 = state_47211__$1;
(statearr_47215_49921[(1)] = (22));

} else {
var statearr_47216_49922 = state_47211__$1;
(statearr_47216_49922[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (27))){
var inst_47149 = (state_47211[(9)]);
var inst_47140 = (state_47211[(10)]);
var inst_47059 = (state_47211[(11)]);
var inst_47138 = (state_47211[(12)]);
var inst_47149__$1 = cljs.core._nth(inst_47138,inst_47140);
var inst_47150 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_47149__$1,inst_47059,done);
var state_47211__$1 = (function (){var statearr_47218 = state_47211;
(statearr_47218[(9)] = inst_47149__$1);

return statearr_47218;
})();
if(cljs.core.truth_(inst_47150)){
var statearr_47219_49923 = state_47211__$1;
(statearr_47219_49923[(1)] = (30));

} else {
var statearr_47220_49926 = state_47211__$1;
(statearr_47220_49926[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (1))){
var state_47211__$1 = state_47211;
var statearr_47221_49929 = state_47211__$1;
(statearr_47221_49929[(2)] = null);

(statearr_47221_49929[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (24))){
var inst_47092 = (state_47211[(7)]);
var inst_47115 = (state_47211[(2)]);
var inst_47116 = cljs.core.next(inst_47092);
var inst_47068 = inst_47116;
var inst_47069 = null;
var inst_47070 = (0);
var inst_47071 = (0);
var state_47211__$1 = (function (){var statearr_47222 = state_47211;
(statearr_47222[(13)] = inst_47070);

(statearr_47222[(14)] = inst_47115);

(statearr_47222[(15)] = inst_47068);

(statearr_47222[(16)] = inst_47069);

(statearr_47222[(17)] = inst_47071);

return statearr_47222;
})();
var statearr_47223_49937 = state_47211__$1;
(statearr_47223_49937[(2)] = null);

(statearr_47223_49937[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (39))){
var state_47211__$1 = state_47211;
var statearr_47229_49939 = state_47211__$1;
(statearr_47229_49939[(2)] = null);

(statearr_47229_49939[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (4))){
var inst_47059 = (state_47211[(11)]);
var inst_47059__$1 = (state_47211[(2)]);
var inst_47060 = (inst_47059__$1 == null);
var state_47211__$1 = (function (){var statearr_47234 = state_47211;
(statearr_47234[(11)] = inst_47059__$1);

return statearr_47234;
})();
if(cljs.core.truth_(inst_47060)){
var statearr_47235_49940 = state_47211__$1;
(statearr_47235_49940[(1)] = (5));

} else {
var statearr_47238_49941 = state_47211__$1;
(statearr_47238_49941[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (15))){
var inst_47070 = (state_47211[(13)]);
var inst_47068 = (state_47211[(15)]);
var inst_47069 = (state_47211[(16)]);
var inst_47071 = (state_47211[(17)]);
var inst_47088 = (state_47211[(2)]);
var inst_47089 = (inst_47071 + (1));
var tmp47226 = inst_47070;
var tmp47227 = inst_47068;
var tmp47228 = inst_47069;
var inst_47068__$1 = tmp47227;
var inst_47069__$1 = tmp47228;
var inst_47070__$1 = tmp47226;
var inst_47071__$1 = inst_47089;
var state_47211__$1 = (function (){var statearr_47242 = state_47211;
(statearr_47242[(13)] = inst_47070__$1);

(statearr_47242[(15)] = inst_47068__$1);

(statearr_47242[(18)] = inst_47088);

(statearr_47242[(16)] = inst_47069__$1);

(statearr_47242[(17)] = inst_47071__$1);

return statearr_47242;
})();
var statearr_47244_49948 = state_47211__$1;
(statearr_47244_49948[(2)] = null);

(statearr_47244_49948[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (21))){
var inst_47119 = (state_47211[(2)]);
var state_47211__$1 = state_47211;
var statearr_47248_49950 = state_47211__$1;
(statearr_47248_49950[(2)] = inst_47119);

(statearr_47248_49950[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (31))){
var inst_47149 = (state_47211[(9)]);
var inst_47157 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_47149);
var state_47211__$1 = state_47211;
var statearr_47249_49951 = state_47211__$1;
(statearr_47249_49951[(2)] = inst_47157);

(statearr_47249_49951[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (32))){
var inst_47140 = (state_47211[(10)]);
var inst_47139 = (state_47211[(19)]);
var inst_47137 = (state_47211[(20)]);
var inst_47138 = (state_47211[(12)]);
var inst_47159 = (state_47211[(2)]);
var inst_47160 = (inst_47140 + (1));
var tmp47245 = inst_47139;
var tmp47246 = inst_47137;
var tmp47247 = inst_47138;
var inst_47137__$1 = tmp47246;
var inst_47138__$1 = tmp47247;
var inst_47139__$1 = tmp47245;
var inst_47140__$1 = inst_47160;
var state_47211__$1 = (function (){var statearr_47253 = state_47211;
(statearr_47253[(21)] = inst_47159);

(statearr_47253[(10)] = inst_47140__$1);

(statearr_47253[(19)] = inst_47139__$1);

(statearr_47253[(20)] = inst_47137__$1);

(statearr_47253[(12)] = inst_47138__$1);

return statearr_47253;
})();
var statearr_47255_49952 = state_47211__$1;
(statearr_47255_49952[(2)] = null);

(statearr_47255_49952[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (40))){
var inst_47180 = (state_47211[(22)]);
var inst_47184 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_47180);
var state_47211__$1 = state_47211;
var statearr_47257_49954 = state_47211__$1;
(statearr_47257_49954[(2)] = inst_47184);

(statearr_47257_49954[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (33))){
var inst_47163 = (state_47211[(23)]);
var inst_47169 = cljs.core.chunked_seq_QMARK_(inst_47163);
var state_47211__$1 = state_47211;
if(inst_47169){
var statearr_47263_49956 = state_47211__$1;
(statearr_47263_49956[(1)] = (36));

} else {
var statearr_47266_49957 = state_47211__$1;
(statearr_47266_49957[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (13))){
var inst_47082 = (state_47211[(24)]);
var inst_47085 = cljs.core.async.close_BANG_(inst_47082);
var state_47211__$1 = state_47211;
var statearr_47272_49958 = state_47211__$1;
(statearr_47272_49958[(2)] = inst_47085);

(statearr_47272_49958[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (22))){
var inst_47109 = (state_47211[(8)]);
var inst_47112 = cljs.core.async.close_BANG_(inst_47109);
var state_47211__$1 = state_47211;
var statearr_47273_49959 = state_47211__$1;
(statearr_47273_49959[(2)] = inst_47112);

(statearr_47273_49959[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (36))){
var inst_47163 = (state_47211[(23)]);
var inst_47171 = cljs.core.chunk_first(inst_47163);
var inst_47172 = cljs.core.chunk_rest(inst_47163);
var inst_47173 = cljs.core.count(inst_47171);
var inst_47137 = inst_47172;
var inst_47138 = inst_47171;
var inst_47139 = inst_47173;
var inst_47140 = (0);
var state_47211__$1 = (function (){var statearr_47282 = state_47211;
(statearr_47282[(10)] = inst_47140);

(statearr_47282[(19)] = inst_47139);

(statearr_47282[(20)] = inst_47137);

(statearr_47282[(12)] = inst_47138);

return statearr_47282;
})();
var statearr_47288_49961 = state_47211__$1;
(statearr_47288_49961[(2)] = null);

(statearr_47288_49961[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (41))){
var inst_47163 = (state_47211[(23)]);
var inst_47186 = (state_47211[(2)]);
var inst_47187 = cljs.core.next(inst_47163);
var inst_47137 = inst_47187;
var inst_47138 = null;
var inst_47139 = (0);
var inst_47140 = (0);
var state_47211__$1 = (function (){var statearr_47295 = state_47211;
(statearr_47295[(10)] = inst_47140);

(statearr_47295[(19)] = inst_47139);

(statearr_47295[(20)] = inst_47137);

(statearr_47295[(25)] = inst_47186);

(statearr_47295[(12)] = inst_47138);

return statearr_47295;
})();
var statearr_47304_49963 = state_47211__$1;
(statearr_47304_49963[(2)] = null);

(statearr_47304_49963[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (43))){
var state_47211__$1 = state_47211;
var statearr_47307_49964 = state_47211__$1;
(statearr_47307_49964[(2)] = null);

(statearr_47307_49964[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (29))){
var inst_47195 = (state_47211[(2)]);
var state_47211__$1 = state_47211;
var statearr_47312_49965 = state_47211__$1;
(statearr_47312_49965[(2)] = inst_47195);

(statearr_47312_49965[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (44))){
var inst_47204 = (state_47211[(2)]);
var state_47211__$1 = (function (){var statearr_47315 = state_47211;
(statearr_47315[(26)] = inst_47204);

return statearr_47315;
})();
var statearr_47322_49966 = state_47211__$1;
(statearr_47322_49966[(2)] = null);

(statearr_47322_49966[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (6))){
var inst_47129 = (state_47211[(27)]);
var inst_47128 = cljs.core.deref(cs);
var inst_47129__$1 = cljs.core.keys(inst_47128);
var inst_47130 = cljs.core.count(inst_47129__$1);
var inst_47131 = cljs.core.reset_BANG_(dctr,inst_47130);
var inst_47136 = cljs.core.seq(inst_47129__$1);
var inst_47137 = inst_47136;
var inst_47138 = null;
var inst_47139 = (0);
var inst_47140 = (0);
var state_47211__$1 = (function (){var statearr_47334 = state_47211;
(statearr_47334[(10)] = inst_47140);

(statearr_47334[(27)] = inst_47129__$1);

(statearr_47334[(19)] = inst_47139);

(statearr_47334[(28)] = inst_47131);

(statearr_47334[(20)] = inst_47137);

(statearr_47334[(12)] = inst_47138);

return statearr_47334;
})();
var statearr_47336_49969 = state_47211__$1;
(statearr_47336_49969[(2)] = null);

(statearr_47336_49969[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (28))){
var inst_47137 = (state_47211[(20)]);
var inst_47163 = (state_47211[(23)]);
var inst_47163__$1 = cljs.core.seq(inst_47137);
var state_47211__$1 = (function (){var statearr_47341 = state_47211;
(statearr_47341[(23)] = inst_47163__$1);

return statearr_47341;
})();
if(inst_47163__$1){
var statearr_47344_49970 = state_47211__$1;
(statearr_47344_49970[(1)] = (33));

} else {
var statearr_47345_49971 = state_47211__$1;
(statearr_47345_49971[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (25))){
var inst_47140 = (state_47211[(10)]);
var inst_47139 = (state_47211[(19)]);
var inst_47146 = (inst_47140 < inst_47139);
var inst_47147 = inst_47146;
var state_47211__$1 = state_47211;
if(cljs.core.truth_(inst_47147)){
var statearr_47352_49972 = state_47211__$1;
(statearr_47352_49972[(1)] = (27));

} else {
var statearr_47354_49973 = state_47211__$1;
(statearr_47354_49973[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (34))){
var state_47211__$1 = state_47211;
var statearr_47355_49974 = state_47211__$1;
(statearr_47355_49974[(2)] = null);

(statearr_47355_49974[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (17))){
var state_47211__$1 = state_47211;
var statearr_47360_49975 = state_47211__$1;
(statearr_47360_49975[(2)] = null);

(statearr_47360_49975[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (3))){
var inst_47209 = (state_47211[(2)]);
var state_47211__$1 = state_47211;
return cljs.core.async.impl.ioc_helpers.return_chan(state_47211__$1,inst_47209);
} else {
if((state_val_47212 === (12))){
var inst_47124 = (state_47211[(2)]);
var state_47211__$1 = state_47211;
var statearr_47370_49976 = state_47211__$1;
(statearr_47370_49976[(2)] = inst_47124);

(statearr_47370_49976[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (2))){
var state_47211__$1 = state_47211;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_47211__$1,(4),ch);
} else {
if((state_val_47212 === (23))){
var state_47211__$1 = state_47211;
var statearr_47375_49977 = state_47211__$1;
(statearr_47375_49977[(2)] = null);

(statearr_47375_49977[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (35))){
var inst_47193 = (state_47211[(2)]);
var state_47211__$1 = state_47211;
var statearr_47382_49979 = state_47211__$1;
(statearr_47382_49979[(2)] = inst_47193);

(statearr_47382_49979[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (19))){
var inst_47092 = (state_47211[(7)]);
var inst_47099 = cljs.core.chunk_first(inst_47092);
var inst_47100 = cljs.core.chunk_rest(inst_47092);
var inst_47101 = cljs.core.count(inst_47099);
var inst_47068 = inst_47100;
var inst_47069 = inst_47099;
var inst_47070 = inst_47101;
var inst_47071 = (0);
var state_47211__$1 = (function (){var statearr_47394 = state_47211;
(statearr_47394[(13)] = inst_47070);

(statearr_47394[(15)] = inst_47068);

(statearr_47394[(16)] = inst_47069);

(statearr_47394[(17)] = inst_47071);

return statearr_47394;
})();
var statearr_47395_49980 = state_47211__$1;
(statearr_47395_49980[(2)] = null);

(statearr_47395_49980[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (11))){
var inst_47068 = (state_47211[(15)]);
var inst_47092 = (state_47211[(7)]);
var inst_47092__$1 = cljs.core.seq(inst_47068);
var state_47211__$1 = (function (){var statearr_47406 = state_47211;
(statearr_47406[(7)] = inst_47092__$1);

return statearr_47406;
})();
if(inst_47092__$1){
var statearr_47407_49985 = state_47211__$1;
(statearr_47407_49985[(1)] = (16));

} else {
var statearr_47410_49986 = state_47211__$1;
(statearr_47410_49986[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (9))){
var inst_47126 = (state_47211[(2)]);
var state_47211__$1 = state_47211;
var statearr_47414_49987 = state_47211__$1;
(statearr_47414_49987[(2)] = inst_47126);

(statearr_47414_49987[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (5))){
var inst_47066 = cljs.core.deref(cs);
var inst_47067 = cljs.core.seq(inst_47066);
var inst_47068 = inst_47067;
var inst_47069 = null;
var inst_47070 = (0);
var inst_47071 = (0);
var state_47211__$1 = (function (){var statearr_47415 = state_47211;
(statearr_47415[(13)] = inst_47070);

(statearr_47415[(15)] = inst_47068);

(statearr_47415[(16)] = inst_47069);

(statearr_47415[(17)] = inst_47071);

return statearr_47415;
})();
var statearr_47416_49989 = state_47211__$1;
(statearr_47416_49989[(2)] = null);

(statearr_47416_49989[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (14))){
var state_47211__$1 = state_47211;
var statearr_47424_49990 = state_47211__$1;
(statearr_47424_49990[(2)] = null);

(statearr_47424_49990[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (45))){
var inst_47201 = (state_47211[(2)]);
var state_47211__$1 = state_47211;
var statearr_47428_49991 = state_47211__$1;
(statearr_47428_49991[(2)] = inst_47201);

(statearr_47428_49991[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (26))){
var inst_47129 = (state_47211[(27)]);
var inst_47197 = (state_47211[(2)]);
var inst_47198 = cljs.core.seq(inst_47129);
var state_47211__$1 = (function (){var statearr_47433 = state_47211;
(statearr_47433[(29)] = inst_47197);

return statearr_47433;
})();
if(inst_47198){
var statearr_47435_49992 = state_47211__$1;
(statearr_47435_49992[(1)] = (42));

} else {
var statearr_47437_49993 = state_47211__$1;
(statearr_47437_49993[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (16))){
var inst_47092 = (state_47211[(7)]);
var inst_47094 = cljs.core.chunked_seq_QMARK_(inst_47092);
var state_47211__$1 = state_47211;
if(inst_47094){
var statearr_47444_49994 = state_47211__$1;
(statearr_47444_49994[(1)] = (19));

} else {
var statearr_47445_49995 = state_47211__$1;
(statearr_47445_49995[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (38))){
var inst_47190 = (state_47211[(2)]);
var state_47211__$1 = state_47211;
var statearr_47454_49996 = state_47211__$1;
(statearr_47454_49996[(2)] = inst_47190);

(statearr_47454_49996[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (30))){
var state_47211__$1 = state_47211;
var statearr_47462_49997 = state_47211__$1;
(statearr_47462_49997[(2)] = null);

(statearr_47462_49997[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (10))){
var inst_47069 = (state_47211[(16)]);
var inst_47071 = (state_47211[(17)]);
var inst_47081 = cljs.core._nth(inst_47069,inst_47071);
var inst_47082 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_47081,(0),null);
var inst_47083 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_47081,(1),null);
var state_47211__$1 = (function (){var statearr_47463 = state_47211;
(statearr_47463[(24)] = inst_47082);

return statearr_47463;
})();
if(cljs.core.truth_(inst_47083)){
var statearr_47464_50005 = state_47211__$1;
(statearr_47464_50005[(1)] = (13));

} else {
var statearr_47465_50006 = state_47211__$1;
(statearr_47465_50006[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (18))){
var inst_47122 = (state_47211[(2)]);
var state_47211__$1 = state_47211;
var statearr_47467_50007 = state_47211__$1;
(statearr_47467_50007[(2)] = inst_47122);

(statearr_47467_50007[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (42))){
var state_47211__$1 = state_47211;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_47211__$1,(45),dchan);
} else {
if((state_val_47212 === (37))){
var inst_47180 = (state_47211[(22)]);
var inst_47059 = (state_47211[(11)]);
var inst_47163 = (state_47211[(23)]);
var inst_47180__$1 = cljs.core.first(inst_47163);
var inst_47181 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_47180__$1,inst_47059,done);
var state_47211__$1 = (function (){var statearr_47475 = state_47211;
(statearr_47475[(22)] = inst_47180__$1);

return statearr_47475;
})();
if(cljs.core.truth_(inst_47181)){
var statearr_47478_50011 = state_47211__$1;
(statearr_47478_50011[(1)] = (39));

} else {
var statearr_47480_50012 = state_47211__$1;
(statearr_47480_50012[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47212 === (8))){
var inst_47070 = (state_47211[(13)]);
var inst_47071 = (state_47211[(17)]);
var inst_47075 = (inst_47071 < inst_47070);
var inst_47076 = inst_47075;
var state_47211__$1 = state_47211;
if(cljs.core.truth_(inst_47076)){
var statearr_47483_50016 = state_47211__$1;
(statearr_47483_50016[(1)] = (10));

} else {
var statearr_47488_50017 = state_47211__$1;
(statearr_47488_50017[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mult_$_state_machine__45510__auto__ = null;
var cljs$core$async$mult_$_state_machine__45510__auto____0 = (function (){
var statearr_47497 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_47497[(0)] = cljs$core$async$mult_$_state_machine__45510__auto__);

(statearr_47497[(1)] = (1));

return statearr_47497;
});
var cljs$core$async$mult_$_state_machine__45510__auto____1 = (function (state_47211){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_47211);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e47501){var ex__45513__auto__ = e47501;
var statearr_47502_50018 = state_47211;
(statearr_47502_50018[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_47211[(4)]))){
var statearr_47505_50022 = state_47211;
(statearr_47505_50022[(1)] = cljs.core.first((state_47211[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__50023 = state_47211;
state_47211 = G__50023;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__45510__auto__ = function(state_47211){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__45510__auto____1.call(this,state_47211);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__45510__auto____0;
cljs$core$async$mult_$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__45510__auto____1;
return cljs$core$async$mult_$_state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_47515 = f__45586__auto__();
(statearr_47515[(6)] = c__45585__auto___49918);

return statearr_47515;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__47525 = arguments.length;
switch (G__47525) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
}));

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
}));

(cljs.core.async.tap.cljs$lang$maxFixedArity = 3);

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

var cljs$core$async$Mix$admix_STAR_$dyn_50031 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
});
cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$admix_STAR_$dyn_50031(m,ch);
}
});

var cljs$core$async$Mix$unmix_STAR_$dyn_50032 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
});
cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$unmix_STAR_$dyn_50032(m,ch);
}
});

var cljs$core$async$Mix$unmix_all_STAR_$dyn_50037 = (function (m){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4429__auto__.call(null,m));
} else {
var m__4426__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4426__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
});
cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mix$unmix_all_STAR_$dyn_50037(m);
}
});

var cljs$core$async$Mix$toggle_STAR_$dyn_50040 = (function (m,state_map){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4429__auto__.call(null,m,state_map));
} else {
var m__4426__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4426__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
});
cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
return cljs$core$async$Mix$toggle_STAR_$dyn_50040(m,state_map);
}
});

var cljs$core$async$Mix$solo_mode_STAR_$dyn_50045 = (function (m,mode){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4429__auto__.call(null,m,mode));
} else {
var m__4426__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4426__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
});
cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
return cljs$core$async$Mix$solo_mode_STAR_$dyn_50045(m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___50046 = arguments.length;
var i__4737__auto___50047 = (0);
while(true){
if((i__4737__auto___50047 < len__4736__auto___50046)){
args__4742__auto__.push((arguments[i__4737__auto___50047]));

var G__50048 = (i__4737__auto___50047 + (1));
i__4737__auto___50047 = G__50048;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((3) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4743__auto__);
});

(cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__47649){
var map__47652 = p__47649;
var map__47652__$1 = (((((!((map__47652 == null))))?(((((map__47652.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__47652.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__47652):map__47652);
var opts = map__47652__$1;
var statearr_47660_50049 = state;
(statearr_47660_50049[(1)] = cont_block);


var temp__5735__auto__ = cljs.core.async.do_alts((function (val){
var statearr_47665_50050 = state;
(statearr_47665_50050[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
}),ports,opts);
if(cljs.core.truth_(temp__5735__auto__)){
var cb = temp__5735__auto__;
var statearr_47668_50051 = state;
(statearr_47668_50051[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}));

(cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq47635){
var G__47636 = cljs.core.first(seq47635);
var seq47635__$1 = cljs.core.next(seq47635);
var G__47637 = cljs.core.first(seq47635__$1);
var seq47635__$2 = cljs.core.next(seq47635__$1);
var G__47638 = cljs.core.first(seq47635__$2);
var seq47635__$3 = cljs.core.next(seq47635__$2);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__47636,G__47637,G__47638,seq47635__$3);
}));

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.async.sliding_buffer((1)));
var changed = (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});
var pick = (function (attr,chs){
return cljs.core.reduce_kv((function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
}),cljs.core.PersistentHashSet.EMPTY,chs);
});
var calc_state = (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async47716 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async47716 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta47717){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta47717 = meta47717;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async47716.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_47718,meta47717__$1){
var self__ = this;
var _47718__$1 = this;
return (new cljs.core.async.t_cljs$core$async47716(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta47717__$1));
}));

(cljs.core.async.t_cljs$core$async47716.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_47718){
var self__ = this;
var _47718__$1 = this;
return self__.meta47717;
}));

(cljs.core.async.t_cljs$core$async47716.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async47716.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
}));

(cljs.core.async.t_cljs$core$async47716.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async47716.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async47716.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async47716.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async47716.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async47716.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async47716.getBasis = (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta47717","meta47717",84365761,null)], null);
}));

(cljs.core.async.t_cljs$core$async47716.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async47716.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async47716");

(cljs.core.async.t_cljs$core$async47716.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async47716");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async47716.
 */
cljs.core.async.__GT_t_cljs$core$async47716 = (function cljs$core$async$mix_$___GT_t_cljs$core$async47716(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta47717){
return (new cljs.core.async.t_cljs$core$async47716(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta47717));
});

}

return (new cljs.core.async.t_cljs$core$async47716(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__45585__auto___50076 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_47852){
var state_val_47853 = (state_47852[(1)]);
if((state_val_47853 === (7))){
var inst_47759 = (state_47852[(2)]);
var state_47852__$1 = state_47852;
var statearr_47855_50077 = state_47852__$1;
(statearr_47855_50077[(2)] = inst_47759);

(statearr_47855_50077[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (20))){
var inst_47771 = (state_47852[(7)]);
var state_47852__$1 = state_47852;
var statearr_47857_50078 = state_47852__$1;
(statearr_47857_50078[(2)] = inst_47771);

(statearr_47857_50078[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (27))){
var state_47852__$1 = state_47852;
var statearr_47858_50079 = state_47852__$1;
(statearr_47858_50079[(2)] = null);

(statearr_47858_50079[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (1))){
var inst_47746 = (state_47852[(8)]);
var inst_47746__$1 = calc_state();
var inst_47748 = (inst_47746__$1 == null);
var inst_47749 = cljs.core.not(inst_47748);
var state_47852__$1 = (function (){var statearr_47859 = state_47852;
(statearr_47859[(8)] = inst_47746__$1);

return statearr_47859;
})();
if(inst_47749){
var statearr_47860_50080 = state_47852__$1;
(statearr_47860_50080[(1)] = (2));

} else {
var statearr_47861_50081 = state_47852__$1;
(statearr_47861_50081[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (24))){
var inst_47806 = (state_47852[(9)]);
var inst_47823 = (state_47852[(10)]);
var inst_47797 = (state_47852[(11)]);
var inst_47823__$1 = (inst_47797.cljs$core$IFn$_invoke$arity$1 ? inst_47797.cljs$core$IFn$_invoke$arity$1(inst_47806) : inst_47797.call(null,inst_47806));
var state_47852__$1 = (function (){var statearr_47868 = state_47852;
(statearr_47868[(10)] = inst_47823__$1);

return statearr_47868;
})();
if(cljs.core.truth_(inst_47823__$1)){
var statearr_47873_50083 = state_47852__$1;
(statearr_47873_50083[(1)] = (29));

} else {
var statearr_47874_50084 = state_47852__$1;
(statearr_47874_50084[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (4))){
var inst_47762 = (state_47852[(2)]);
var state_47852__$1 = state_47852;
if(cljs.core.truth_(inst_47762)){
var statearr_47879_50085 = state_47852__$1;
(statearr_47879_50085[(1)] = (8));

} else {
var statearr_47880_50086 = state_47852__$1;
(statearr_47880_50086[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (15))){
var inst_47790 = (state_47852[(2)]);
var state_47852__$1 = state_47852;
if(cljs.core.truth_(inst_47790)){
var statearr_47881_50090 = state_47852__$1;
(statearr_47881_50090[(1)] = (19));

} else {
var statearr_47882_50091 = state_47852__$1;
(statearr_47882_50091[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (21))){
var inst_47795 = (state_47852[(12)]);
var inst_47795__$1 = (state_47852[(2)]);
var inst_47797 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_47795__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_47798 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_47795__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_47799 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_47795__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_47852__$1 = (function (){var statearr_47884 = state_47852;
(statearr_47884[(12)] = inst_47795__$1);

(statearr_47884[(13)] = inst_47798);

(statearr_47884[(11)] = inst_47797);

return statearr_47884;
})();
return cljs.core.async.ioc_alts_BANG_(state_47852__$1,(22),inst_47799);
} else {
if((state_val_47853 === (31))){
var inst_47831 = (state_47852[(2)]);
var state_47852__$1 = state_47852;
if(cljs.core.truth_(inst_47831)){
var statearr_47888_50093 = state_47852__$1;
(statearr_47888_50093[(1)] = (32));

} else {
var statearr_47889_50094 = state_47852__$1;
(statearr_47889_50094[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (32))){
var inst_47805 = (state_47852[(14)]);
var state_47852__$1 = state_47852;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_47852__$1,(35),out,inst_47805);
} else {
if((state_val_47853 === (33))){
var inst_47795 = (state_47852[(12)]);
var inst_47771 = inst_47795;
var state_47852__$1 = (function (){var statearr_47891 = state_47852;
(statearr_47891[(7)] = inst_47771);

return statearr_47891;
})();
var statearr_47892_50097 = state_47852__$1;
(statearr_47892_50097[(2)] = null);

(statearr_47892_50097[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (13))){
var inst_47771 = (state_47852[(7)]);
var inst_47779 = inst_47771.cljs$lang$protocol_mask$partition0$;
var inst_47780 = (inst_47779 & (64));
var inst_47781 = inst_47771.cljs$core$ISeq$;
var inst_47782 = (cljs.core.PROTOCOL_SENTINEL === inst_47781);
var inst_47783 = ((inst_47780) || (inst_47782));
var state_47852__$1 = state_47852;
if(cljs.core.truth_(inst_47783)){
var statearr_47896_50101 = state_47852__$1;
(statearr_47896_50101[(1)] = (16));

} else {
var statearr_47899_50102 = state_47852__$1;
(statearr_47899_50102[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (22))){
var inst_47806 = (state_47852[(9)]);
var inst_47805 = (state_47852[(14)]);
var inst_47804 = (state_47852[(2)]);
var inst_47805__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_47804,(0),null);
var inst_47806__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_47804,(1),null);
var inst_47807 = (inst_47805__$1 == null);
var inst_47808 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_47806__$1,change);
var inst_47809 = ((inst_47807) || (inst_47808));
var state_47852__$1 = (function (){var statearr_47905 = state_47852;
(statearr_47905[(9)] = inst_47806__$1);

(statearr_47905[(14)] = inst_47805__$1);

return statearr_47905;
})();
if(cljs.core.truth_(inst_47809)){
var statearr_47906_50104 = state_47852__$1;
(statearr_47906_50104[(1)] = (23));

} else {
var statearr_47907_50105 = state_47852__$1;
(statearr_47907_50105[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (36))){
var inst_47795 = (state_47852[(12)]);
var inst_47771 = inst_47795;
var state_47852__$1 = (function (){var statearr_47908 = state_47852;
(statearr_47908[(7)] = inst_47771);

return statearr_47908;
})();
var statearr_47909_50106 = state_47852__$1;
(statearr_47909_50106[(2)] = null);

(statearr_47909_50106[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (29))){
var inst_47823 = (state_47852[(10)]);
var state_47852__$1 = state_47852;
var statearr_47911_50107 = state_47852__$1;
(statearr_47911_50107[(2)] = inst_47823);

(statearr_47911_50107[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (6))){
var state_47852__$1 = state_47852;
var statearr_47913_50108 = state_47852__$1;
(statearr_47913_50108[(2)] = false);

(statearr_47913_50108[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (28))){
var inst_47817 = (state_47852[(2)]);
var inst_47818 = calc_state();
var inst_47771 = inst_47818;
var state_47852__$1 = (function (){var statearr_47916 = state_47852;
(statearr_47916[(15)] = inst_47817);

(statearr_47916[(7)] = inst_47771);

return statearr_47916;
})();
var statearr_47918_50109 = state_47852__$1;
(statearr_47918_50109[(2)] = null);

(statearr_47918_50109[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (25))){
var inst_47846 = (state_47852[(2)]);
var state_47852__$1 = state_47852;
var statearr_47922_50110 = state_47852__$1;
(statearr_47922_50110[(2)] = inst_47846);

(statearr_47922_50110[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (34))){
var inst_47844 = (state_47852[(2)]);
var state_47852__$1 = state_47852;
var statearr_47925_50115 = state_47852__$1;
(statearr_47925_50115[(2)] = inst_47844);

(statearr_47925_50115[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (17))){
var state_47852__$1 = state_47852;
var statearr_47927_50117 = state_47852__$1;
(statearr_47927_50117[(2)] = false);

(statearr_47927_50117[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (3))){
var state_47852__$1 = state_47852;
var statearr_47928_50118 = state_47852__$1;
(statearr_47928_50118[(2)] = false);

(statearr_47928_50118[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (12))){
var inst_47849 = (state_47852[(2)]);
var state_47852__$1 = state_47852;
return cljs.core.async.impl.ioc_helpers.return_chan(state_47852__$1,inst_47849);
} else {
if((state_val_47853 === (2))){
var inst_47746 = (state_47852[(8)]);
var inst_47751 = inst_47746.cljs$lang$protocol_mask$partition0$;
var inst_47752 = (inst_47751 & (64));
var inst_47753 = inst_47746.cljs$core$ISeq$;
var inst_47754 = (cljs.core.PROTOCOL_SENTINEL === inst_47753);
var inst_47755 = ((inst_47752) || (inst_47754));
var state_47852__$1 = state_47852;
if(cljs.core.truth_(inst_47755)){
var statearr_47931_50120 = state_47852__$1;
(statearr_47931_50120[(1)] = (5));

} else {
var statearr_47932_50121 = state_47852__$1;
(statearr_47932_50121[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (23))){
var inst_47805 = (state_47852[(14)]);
var inst_47812 = (inst_47805 == null);
var state_47852__$1 = state_47852;
if(cljs.core.truth_(inst_47812)){
var statearr_47936_50122 = state_47852__$1;
(statearr_47936_50122[(1)] = (26));

} else {
var statearr_47939_50123 = state_47852__$1;
(statearr_47939_50123[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (35))){
var inst_47834 = (state_47852[(2)]);
var state_47852__$1 = state_47852;
if(cljs.core.truth_(inst_47834)){
var statearr_47940_50125 = state_47852__$1;
(statearr_47940_50125[(1)] = (36));

} else {
var statearr_47942_50129 = state_47852__$1;
(statearr_47942_50129[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (19))){
var inst_47771 = (state_47852[(7)]);
var inst_47792 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_47771);
var state_47852__$1 = state_47852;
var statearr_47944_50130 = state_47852__$1;
(statearr_47944_50130[(2)] = inst_47792);

(statearr_47944_50130[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (11))){
var inst_47771 = (state_47852[(7)]);
var inst_47776 = (inst_47771 == null);
var inst_47777 = cljs.core.not(inst_47776);
var state_47852__$1 = state_47852;
if(inst_47777){
var statearr_47945_50133 = state_47852__$1;
(statearr_47945_50133[(1)] = (13));

} else {
var statearr_47947_50134 = state_47852__$1;
(statearr_47947_50134[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (9))){
var inst_47746 = (state_47852[(8)]);
var state_47852__$1 = state_47852;
var statearr_47950_50135 = state_47852__$1;
(statearr_47950_50135[(2)] = inst_47746);

(statearr_47950_50135[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (5))){
var state_47852__$1 = state_47852;
var statearr_47953_50136 = state_47852__$1;
(statearr_47953_50136[(2)] = true);

(statearr_47953_50136[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (14))){
var state_47852__$1 = state_47852;
var statearr_47958_50139 = state_47852__$1;
(statearr_47958_50139[(2)] = false);

(statearr_47958_50139[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (26))){
var inst_47806 = (state_47852[(9)]);
var inst_47814 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_47806);
var state_47852__$1 = state_47852;
var statearr_47959_50141 = state_47852__$1;
(statearr_47959_50141[(2)] = inst_47814);

(statearr_47959_50141[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (16))){
var state_47852__$1 = state_47852;
var statearr_47961_50142 = state_47852__$1;
(statearr_47961_50142[(2)] = true);

(statearr_47961_50142[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (38))){
var inst_47840 = (state_47852[(2)]);
var state_47852__$1 = state_47852;
var statearr_47969_50143 = state_47852__$1;
(statearr_47969_50143[(2)] = inst_47840);

(statearr_47969_50143[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (30))){
var inst_47806 = (state_47852[(9)]);
var inst_47798 = (state_47852[(13)]);
var inst_47797 = (state_47852[(11)]);
var inst_47826 = cljs.core.empty_QMARK_(inst_47797);
var inst_47827 = (inst_47798.cljs$core$IFn$_invoke$arity$1 ? inst_47798.cljs$core$IFn$_invoke$arity$1(inst_47806) : inst_47798.call(null,inst_47806));
var inst_47828 = cljs.core.not(inst_47827);
var inst_47829 = ((inst_47826) && (inst_47828));
var state_47852__$1 = state_47852;
var statearr_47988_50144 = state_47852__$1;
(statearr_47988_50144[(2)] = inst_47829);

(statearr_47988_50144[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (10))){
var inst_47746 = (state_47852[(8)]);
var inst_47767 = (state_47852[(2)]);
var inst_47768 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_47767,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_47769 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_47767,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_47770 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_47767,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_47771 = inst_47746;
var state_47852__$1 = (function (){var statearr_47998 = state_47852;
(statearr_47998[(16)] = inst_47769);

(statearr_47998[(7)] = inst_47771);

(statearr_47998[(17)] = inst_47768);

(statearr_47998[(18)] = inst_47770);

return statearr_47998;
})();
var statearr_48000_50145 = state_47852__$1;
(statearr_48000_50145[(2)] = null);

(statearr_48000_50145[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (18))){
var inst_47787 = (state_47852[(2)]);
var state_47852__$1 = state_47852;
var statearr_48002_50146 = state_47852__$1;
(statearr_48002_50146[(2)] = inst_47787);

(statearr_48002_50146[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (37))){
var state_47852__$1 = state_47852;
var statearr_48003_50150 = state_47852__$1;
(statearr_48003_50150[(2)] = null);

(statearr_48003_50150[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_47853 === (8))){
var inst_47746 = (state_47852[(8)]);
var inst_47764 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_47746);
var state_47852__$1 = state_47852;
var statearr_48004_50151 = state_47852__$1;
(statearr_48004_50151[(2)] = inst_47764);

(statearr_48004_50151[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mix_$_state_machine__45510__auto__ = null;
var cljs$core$async$mix_$_state_machine__45510__auto____0 = (function (){
var statearr_48005 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_48005[(0)] = cljs$core$async$mix_$_state_machine__45510__auto__);

(statearr_48005[(1)] = (1));

return statearr_48005;
});
var cljs$core$async$mix_$_state_machine__45510__auto____1 = (function (state_47852){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_47852);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e48008){var ex__45513__auto__ = e48008;
var statearr_48009_50154 = state_47852;
(statearr_48009_50154[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_47852[(4)]))){
var statearr_48010_50155 = state_47852;
(statearr_48010_50155[(1)] = cljs.core.first((state_47852[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__50156 = state_47852;
state_47852 = G__50156;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__45510__auto__ = function(state_47852){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__45510__auto____1.call(this,state_47852);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__45510__auto____0;
cljs$core$async$mix_$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__45510__auto____1;
return cljs$core$async$mix_$_state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_48012 = f__45586__auto__();
(statearr_48012[(6)] = c__45585__auto___50076);

return statearr_48012;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

var cljs$core$async$Pub$sub_STAR_$dyn_50158 = (function (p,v,ch,close_QMARK_){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4429__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__4426__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4426__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
});
cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
return cljs$core$async$Pub$sub_STAR_$dyn_50158(p,v,ch,close_QMARK_);
}
});

var cljs$core$async$Pub$unsub_STAR_$dyn_50159 = (function (p,v,ch){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4429__auto__.call(null,p,v,ch));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4426__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
});
cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
return cljs$core$async$Pub$unsub_STAR_$dyn_50159(p,v,ch);
}
});

var cljs$core$async$Pub$unsub_all_STAR_$dyn_50160 = (function() {
var G__50161 = null;
var G__50161__1 = (function (p){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4429__auto__.call(null,p));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4426__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
var G__50161__2 = (function (p,v){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4429__auto__.call(null,p,v));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4426__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
G__50161 = function(p,v){
switch(arguments.length){
case 1:
return G__50161__1.call(this,p);
case 2:
return G__50161__2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__50161.cljs$core$IFn$_invoke$arity$1 = G__50161__1;
G__50161.cljs$core$IFn$_invoke$arity$2 = G__50161__2;
return G__50161;
})()
;
cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__48117 = arguments.length;
switch (G__48117) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_50160(p);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_50160(p,v);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2);


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__48133 = arguments.length;
switch (G__48133) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
}));

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = (function (topic){
var or__4126__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,(function (p1__48118_SHARP_){
if(cljs.core.truth_((p1__48118_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__48118_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__48118_SHARP_.call(null,topic)))){
return p1__48118_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__48118_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
})),topic);
}
});
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async48139 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async48139 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta48140){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta48140 = meta48140;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async48139.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_48141,meta48140__$1){
var self__ = this;
var _48141__$1 = this;
return (new cljs.core.async.t_cljs$core$async48139(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta48140__$1));
}));

(cljs.core.async.t_cljs$core$async48139.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_48141){
var self__ = this;
var _48141__$1 = this;
return self__.meta48140;
}));

(cljs.core.async.t_cljs$core$async48139.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48139.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async48139.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48139.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
}));

(cljs.core.async.t_cljs$core$async48139.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5735__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5735__auto__)){
var m = temp__5735__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
}));

(cljs.core.async.t_cljs$core$async48139.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
}));

(cljs.core.async.t_cljs$core$async48139.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
}));

(cljs.core.async.t_cljs$core$async48139.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta48140","meta48140",2023522099,null)], null);
}));

(cljs.core.async.t_cljs$core$async48139.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async48139.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async48139");

(cljs.core.async.t_cljs$core$async48139.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async48139");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async48139.
 */
cljs.core.async.__GT_t_cljs$core$async48139 = (function cljs$core$async$__GT_t_cljs$core$async48139(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta48140){
return (new cljs.core.async.t_cljs$core$async48139(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta48140));
});

}

return (new cljs.core.async.t_cljs$core$async48139(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__45585__auto___50174 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_48246){
var state_val_48247 = (state_48246[(1)]);
if((state_val_48247 === (7))){
var inst_48242 = (state_48246[(2)]);
var state_48246__$1 = state_48246;
var statearr_48257_50175 = state_48246__$1;
(statearr_48257_50175[(2)] = inst_48242);

(statearr_48257_50175[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (20))){
var state_48246__$1 = state_48246;
var statearr_48258_50176 = state_48246__$1;
(statearr_48258_50176[(2)] = null);

(statearr_48258_50176[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (1))){
var state_48246__$1 = state_48246;
var statearr_48259_50177 = state_48246__$1;
(statearr_48259_50177[(2)] = null);

(statearr_48259_50177[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (24))){
var inst_48225 = (state_48246[(7)]);
var inst_48234 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_48225);
var state_48246__$1 = state_48246;
var statearr_48260_50178 = state_48246__$1;
(statearr_48260_50178[(2)] = inst_48234);

(statearr_48260_50178[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (4))){
var inst_48175 = (state_48246[(8)]);
var inst_48175__$1 = (state_48246[(2)]);
var inst_48177 = (inst_48175__$1 == null);
var state_48246__$1 = (function (){var statearr_48261 = state_48246;
(statearr_48261[(8)] = inst_48175__$1);

return statearr_48261;
})();
if(cljs.core.truth_(inst_48177)){
var statearr_48262_50179 = state_48246__$1;
(statearr_48262_50179[(1)] = (5));

} else {
var statearr_48263_50180 = state_48246__$1;
(statearr_48263_50180[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (15))){
var inst_48219 = (state_48246[(2)]);
var state_48246__$1 = state_48246;
var statearr_48264_50181 = state_48246__$1;
(statearr_48264_50181[(2)] = inst_48219);

(statearr_48264_50181[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (21))){
var inst_48239 = (state_48246[(2)]);
var state_48246__$1 = (function (){var statearr_48265 = state_48246;
(statearr_48265[(9)] = inst_48239);

return statearr_48265;
})();
var statearr_48266_50182 = state_48246__$1;
(statearr_48266_50182[(2)] = null);

(statearr_48266_50182[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (13))){
var inst_48201 = (state_48246[(10)]);
var inst_48203 = cljs.core.chunked_seq_QMARK_(inst_48201);
var state_48246__$1 = state_48246;
if(inst_48203){
var statearr_48267_50183 = state_48246__$1;
(statearr_48267_50183[(1)] = (16));

} else {
var statearr_48268_50185 = state_48246__$1;
(statearr_48268_50185[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (22))){
var inst_48231 = (state_48246[(2)]);
var state_48246__$1 = state_48246;
if(cljs.core.truth_(inst_48231)){
var statearr_48270_50186 = state_48246__$1;
(statearr_48270_50186[(1)] = (23));

} else {
var statearr_48272_50187 = state_48246__$1;
(statearr_48272_50187[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (6))){
var inst_48227 = (state_48246[(11)]);
var inst_48175 = (state_48246[(8)]);
var inst_48225 = (state_48246[(7)]);
var inst_48225__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_48175) : topic_fn.call(null,inst_48175));
var inst_48226 = cljs.core.deref(mults);
var inst_48227__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_48226,inst_48225__$1);
var state_48246__$1 = (function (){var statearr_48274 = state_48246;
(statearr_48274[(11)] = inst_48227__$1);

(statearr_48274[(7)] = inst_48225__$1);

return statearr_48274;
})();
if(cljs.core.truth_(inst_48227__$1)){
var statearr_48275_50188 = state_48246__$1;
(statearr_48275_50188[(1)] = (19));

} else {
var statearr_48277_50189 = state_48246__$1;
(statearr_48277_50189[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (25))){
var inst_48236 = (state_48246[(2)]);
var state_48246__$1 = state_48246;
var statearr_48278_50190 = state_48246__$1;
(statearr_48278_50190[(2)] = inst_48236);

(statearr_48278_50190[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (17))){
var inst_48201 = (state_48246[(10)]);
var inst_48210 = cljs.core.first(inst_48201);
var inst_48211 = cljs.core.async.muxch_STAR_(inst_48210);
var inst_48212 = cljs.core.async.close_BANG_(inst_48211);
var inst_48213 = cljs.core.next(inst_48201);
var inst_48187 = inst_48213;
var inst_48188 = null;
var inst_48189 = (0);
var inst_48190 = (0);
var state_48246__$1 = (function (){var statearr_48279 = state_48246;
(statearr_48279[(12)] = inst_48190);

(statearr_48279[(13)] = inst_48212);

(statearr_48279[(14)] = inst_48188);

(statearr_48279[(15)] = inst_48189);

(statearr_48279[(16)] = inst_48187);

return statearr_48279;
})();
var statearr_48281_50201 = state_48246__$1;
(statearr_48281_50201[(2)] = null);

(statearr_48281_50201[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (3))){
var inst_48244 = (state_48246[(2)]);
var state_48246__$1 = state_48246;
return cljs.core.async.impl.ioc_helpers.return_chan(state_48246__$1,inst_48244);
} else {
if((state_val_48247 === (12))){
var inst_48221 = (state_48246[(2)]);
var state_48246__$1 = state_48246;
var statearr_48283_50202 = state_48246__$1;
(statearr_48283_50202[(2)] = inst_48221);

(statearr_48283_50202[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (2))){
var state_48246__$1 = state_48246;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_48246__$1,(4),ch);
} else {
if((state_val_48247 === (23))){
var state_48246__$1 = state_48246;
var statearr_48284_50209 = state_48246__$1;
(statearr_48284_50209[(2)] = null);

(statearr_48284_50209[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (19))){
var inst_48227 = (state_48246[(11)]);
var inst_48175 = (state_48246[(8)]);
var inst_48229 = cljs.core.async.muxch_STAR_(inst_48227);
var state_48246__$1 = state_48246;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_48246__$1,(22),inst_48229,inst_48175);
} else {
if((state_val_48247 === (11))){
var inst_48201 = (state_48246[(10)]);
var inst_48187 = (state_48246[(16)]);
var inst_48201__$1 = cljs.core.seq(inst_48187);
var state_48246__$1 = (function (){var statearr_48288 = state_48246;
(statearr_48288[(10)] = inst_48201__$1);

return statearr_48288;
})();
if(inst_48201__$1){
var statearr_48290_50210 = state_48246__$1;
(statearr_48290_50210[(1)] = (13));

} else {
var statearr_48291_50214 = state_48246__$1;
(statearr_48291_50214[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (9))){
var inst_48223 = (state_48246[(2)]);
var state_48246__$1 = state_48246;
var statearr_48292_50215 = state_48246__$1;
(statearr_48292_50215[(2)] = inst_48223);

(statearr_48292_50215[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (5))){
var inst_48184 = cljs.core.deref(mults);
var inst_48185 = cljs.core.vals(inst_48184);
var inst_48186 = cljs.core.seq(inst_48185);
var inst_48187 = inst_48186;
var inst_48188 = null;
var inst_48189 = (0);
var inst_48190 = (0);
var state_48246__$1 = (function (){var statearr_48296 = state_48246;
(statearr_48296[(12)] = inst_48190);

(statearr_48296[(14)] = inst_48188);

(statearr_48296[(15)] = inst_48189);

(statearr_48296[(16)] = inst_48187);

return statearr_48296;
})();
var statearr_48297_50221 = state_48246__$1;
(statearr_48297_50221[(2)] = null);

(statearr_48297_50221[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (14))){
var state_48246__$1 = state_48246;
var statearr_48301_50225 = state_48246__$1;
(statearr_48301_50225[(2)] = null);

(statearr_48301_50225[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (16))){
var inst_48201 = (state_48246[(10)]);
var inst_48205 = cljs.core.chunk_first(inst_48201);
var inst_48206 = cljs.core.chunk_rest(inst_48201);
var inst_48207 = cljs.core.count(inst_48205);
var inst_48187 = inst_48206;
var inst_48188 = inst_48205;
var inst_48189 = inst_48207;
var inst_48190 = (0);
var state_48246__$1 = (function (){var statearr_48305 = state_48246;
(statearr_48305[(12)] = inst_48190);

(statearr_48305[(14)] = inst_48188);

(statearr_48305[(15)] = inst_48189);

(statearr_48305[(16)] = inst_48187);

return statearr_48305;
})();
var statearr_48306_50227 = state_48246__$1;
(statearr_48306_50227[(2)] = null);

(statearr_48306_50227[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (10))){
var inst_48190 = (state_48246[(12)]);
var inst_48188 = (state_48246[(14)]);
var inst_48189 = (state_48246[(15)]);
var inst_48187 = (state_48246[(16)]);
var inst_48195 = cljs.core._nth(inst_48188,inst_48190);
var inst_48196 = cljs.core.async.muxch_STAR_(inst_48195);
var inst_48197 = cljs.core.async.close_BANG_(inst_48196);
var inst_48198 = (inst_48190 + (1));
var tmp48298 = inst_48188;
var tmp48299 = inst_48189;
var tmp48300 = inst_48187;
var inst_48187__$1 = tmp48300;
var inst_48188__$1 = tmp48298;
var inst_48189__$1 = tmp48299;
var inst_48190__$1 = inst_48198;
var state_48246__$1 = (function (){var statearr_48315 = state_48246;
(statearr_48315[(12)] = inst_48190__$1);

(statearr_48315[(14)] = inst_48188__$1);

(statearr_48315[(17)] = inst_48197);

(statearr_48315[(15)] = inst_48189__$1);

(statearr_48315[(16)] = inst_48187__$1);

return statearr_48315;
})();
var statearr_48316_50230 = state_48246__$1;
(statearr_48316_50230[(2)] = null);

(statearr_48316_50230[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (18))){
var inst_48216 = (state_48246[(2)]);
var state_48246__$1 = state_48246;
var statearr_48317_50231 = state_48246__$1;
(statearr_48317_50231[(2)] = inst_48216);

(statearr_48317_50231[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48247 === (8))){
var inst_48190 = (state_48246[(12)]);
var inst_48189 = (state_48246[(15)]);
var inst_48192 = (inst_48190 < inst_48189);
var inst_48193 = inst_48192;
var state_48246__$1 = state_48246;
if(cljs.core.truth_(inst_48193)){
var statearr_48318_50232 = state_48246__$1;
(statearr_48318_50232[(1)] = (10));

} else {
var statearr_48319_50236 = state_48246__$1;
(statearr_48319_50236[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__45510__auto__ = null;
var cljs$core$async$state_machine__45510__auto____0 = (function (){
var statearr_48320 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_48320[(0)] = cljs$core$async$state_machine__45510__auto__);

(statearr_48320[(1)] = (1));

return statearr_48320;
});
var cljs$core$async$state_machine__45510__auto____1 = (function (state_48246){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_48246);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e48321){var ex__45513__auto__ = e48321;
var statearr_48322_50243 = state_48246;
(statearr_48322_50243[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_48246[(4)]))){
var statearr_48323_50244 = state_48246;
(statearr_48323_50244[(1)] = cljs.core.first((state_48246[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__50245 = state_48246;
state_48246 = G__50245;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$state_machine__45510__auto__ = function(state_48246){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__45510__auto____1.call(this,state_48246);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__45510__auto____0;
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__45510__auto____1;
return cljs$core$async$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_48324 = f__45586__auto__();
(statearr_48324[(6)] = c__45585__auto___50174);

return statearr_48324;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return p;
}));

(cljs.core.async.pub.cljs$lang$maxFixedArity = 3);

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__48326 = arguments.length;
switch (G__48326) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
}));

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
}));

(cljs.core.async.sub.cljs$lang$maxFixedArity = 4);

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__48336 = arguments.length;
switch (G__48336) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_(p);
}));

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_(p,topic);
}));

(cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2);

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__48341 = arguments.length;
switch (G__48341) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
}));

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (i){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
var c__45585__auto___50268 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_48398){
var state_val_48399 = (state_48398[(1)]);
if((state_val_48399 === (7))){
var state_48398__$1 = state_48398;
var statearr_48401_50269 = state_48398__$1;
(statearr_48401_50269[(2)] = null);

(statearr_48401_50269[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (1))){
var state_48398__$1 = state_48398;
var statearr_48405_50270 = state_48398__$1;
(statearr_48405_50270[(2)] = null);

(statearr_48405_50270[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (4))){
var inst_48351 = (state_48398[(7)]);
var inst_48352 = (state_48398[(8)]);
var inst_48354 = (inst_48352 < inst_48351);
var state_48398__$1 = state_48398;
if(cljs.core.truth_(inst_48354)){
var statearr_48407_50272 = state_48398__$1;
(statearr_48407_50272[(1)] = (6));

} else {
var statearr_48408_50273 = state_48398__$1;
(statearr_48408_50273[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (15))){
var inst_48383 = (state_48398[(9)]);
var inst_48388 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_48383);
var state_48398__$1 = state_48398;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_48398__$1,(17),out,inst_48388);
} else {
if((state_val_48399 === (13))){
var inst_48383 = (state_48398[(9)]);
var inst_48383__$1 = (state_48398[(2)]);
var inst_48384 = cljs.core.some(cljs.core.nil_QMARK_,inst_48383__$1);
var state_48398__$1 = (function (){var statearr_48414 = state_48398;
(statearr_48414[(9)] = inst_48383__$1);

return statearr_48414;
})();
if(cljs.core.truth_(inst_48384)){
var statearr_48416_50277 = state_48398__$1;
(statearr_48416_50277[(1)] = (14));

} else {
var statearr_48417_50278 = state_48398__$1;
(statearr_48417_50278[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (6))){
var state_48398__$1 = state_48398;
var statearr_48419_50279 = state_48398__$1;
(statearr_48419_50279[(2)] = null);

(statearr_48419_50279[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (17))){
var inst_48390 = (state_48398[(2)]);
var state_48398__$1 = (function (){var statearr_48427 = state_48398;
(statearr_48427[(10)] = inst_48390);

return statearr_48427;
})();
var statearr_48428_50281 = state_48398__$1;
(statearr_48428_50281[(2)] = null);

(statearr_48428_50281[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (3))){
var inst_48395 = (state_48398[(2)]);
var state_48398__$1 = state_48398;
return cljs.core.async.impl.ioc_helpers.return_chan(state_48398__$1,inst_48395);
} else {
if((state_val_48399 === (12))){
var _ = (function (){var statearr_48432 = state_48398;
(statearr_48432[(4)] = cljs.core.rest((state_48398[(4)])));

return statearr_48432;
})();
var state_48398__$1 = state_48398;
var ex48426 = (state_48398__$1[(2)]);
var statearr_48433_50287 = state_48398__$1;
(statearr_48433_50287[(5)] = ex48426);


if((ex48426 instanceof Object)){
var statearr_48434_50294 = state_48398__$1;
(statearr_48434_50294[(1)] = (11));

(statearr_48434_50294[(5)] = null);

} else {
throw ex48426;

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (2))){
var inst_48350 = cljs.core.reset_BANG_(dctr,cnt);
var inst_48351 = cnt;
var inst_48352 = (0);
var state_48398__$1 = (function (){var statearr_48443 = state_48398;
(statearr_48443[(7)] = inst_48351);

(statearr_48443[(8)] = inst_48352);

(statearr_48443[(11)] = inst_48350);

return statearr_48443;
})();
var statearr_48444_50295 = state_48398__$1;
(statearr_48444_50295[(2)] = null);

(statearr_48444_50295[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (11))){
var inst_48361 = (state_48398[(2)]);
var inst_48362 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_48398__$1 = (function (){var statearr_48447 = state_48398;
(statearr_48447[(12)] = inst_48361);

return statearr_48447;
})();
var statearr_48448_50299 = state_48398__$1;
(statearr_48448_50299[(2)] = inst_48362);

(statearr_48448_50299[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (9))){
var inst_48352 = (state_48398[(8)]);
var _ = (function (){var statearr_48449 = state_48398;
(statearr_48449[(4)] = cljs.core.cons((12),(state_48398[(4)])));

return statearr_48449;
})();
var inst_48368 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_48352) : chs__$1.call(null,inst_48352));
var inst_48369 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_48352) : done.call(null,inst_48352));
var inst_48370 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_48368,inst_48369);
var ___$1 = (function (){var statearr_48450 = state_48398;
(statearr_48450[(4)] = cljs.core.rest((state_48398[(4)])));

return statearr_48450;
})();
var state_48398__$1 = state_48398;
var statearr_48452_50303 = state_48398__$1;
(statearr_48452_50303[(2)] = inst_48370);

(statearr_48452_50303[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (5))){
var inst_48380 = (state_48398[(2)]);
var state_48398__$1 = (function (){var statearr_48453 = state_48398;
(statearr_48453[(13)] = inst_48380);

return statearr_48453;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_48398__$1,(13),dchan);
} else {
if((state_val_48399 === (14))){
var inst_48386 = cljs.core.async.close_BANG_(out);
var state_48398__$1 = state_48398;
var statearr_48454_50305 = state_48398__$1;
(statearr_48454_50305[(2)] = inst_48386);

(statearr_48454_50305[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (16))){
var inst_48393 = (state_48398[(2)]);
var state_48398__$1 = state_48398;
var statearr_48455_50306 = state_48398__$1;
(statearr_48455_50306[(2)] = inst_48393);

(statearr_48455_50306[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (10))){
var inst_48352 = (state_48398[(8)]);
var inst_48373 = (state_48398[(2)]);
var inst_48374 = (inst_48352 + (1));
var inst_48352__$1 = inst_48374;
var state_48398__$1 = (function (){var statearr_48457 = state_48398;
(statearr_48457[(14)] = inst_48373);

(statearr_48457[(8)] = inst_48352__$1);

return statearr_48457;
})();
var statearr_48464_50307 = state_48398__$1;
(statearr_48464_50307[(2)] = null);

(statearr_48464_50307[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48399 === (8))){
var inst_48378 = (state_48398[(2)]);
var state_48398__$1 = state_48398;
var statearr_48465_50314 = state_48398__$1;
(statearr_48465_50314[(2)] = inst_48378);

(statearr_48465_50314[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__45510__auto__ = null;
var cljs$core$async$state_machine__45510__auto____0 = (function (){
var statearr_48468 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_48468[(0)] = cljs$core$async$state_machine__45510__auto__);

(statearr_48468[(1)] = (1));

return statearr_48468;
});
var cljs$core$async$state_machine__45510__auto____1 = (function (state_48398){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_48398);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e48472){var ex__45513__auto__ = e48472;
var statearr_48473_50315 = state_48398;
(statearr_48473_50315[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_48398[(4)]))){
var statearr_48474_50316 = state_48398;
(statearr_48474_50316[(1)] = cljs.core.first((state_48398[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__50317 = state_48398;
state_48398 = G__50317;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$state_machine__45510__auto__ = function(state_48398){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__45510__auto____1.call(this,state_48398);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__45510__auto____0;
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__45510__auto____1;
return cljs$core$async$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_48477 = f__45586__auto__();
(statearr_48477[(6)] = c__45585__auto___50268);

return statearr_48477;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return out;
}));

(cljs.core.async.map.cljs$lang$maxFixedArity = 3);

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__48507 = arguments.length;
switch (G__48507) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
}));

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__45585__auto___50321 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_48567){
var state_val_48568 = (state_48567[(1)]);
if((state_val_48568 === (7))){
var inst_48542 = (state_48567[(7)]);
var inst_48543 = (state_48567[(8)]);
var inst_48542__$1 = (state_48567[(2)]);
var inst_48543__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_48542__$1,(0),null);
var inst_48544 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_48542__$1,(1),null);
var inst_48545 = (inst_48543__$1 == null);
var state_48567__$1 = (function (){var statearr_48569 = state_48567;
(statearr_48569[(7)] = inst_48542__$1);

(statearr_48569[(8)] = inst_48543__$1);

(statearr_48569[(9)] = inst_48544);

return statearr_48569;
})();
if(cljs.core.truth_(inst_48545)){
var statearr_48570_50322 = state_48567__$1;
(statearr_48570_50322[(1)] = (8));

} else {
var statearr_48571_50323 = state_48567__$1;
(statearr_48571_50323[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48568 === (1))){
var inst_48532 = cljs.core.vec(chs);
var inst_48533 = inst_48532;
var state_48567__$1 = (function (){var statearr_48574 = state_48567;
(statearr_48574[(10)] = inst_48533);

return statearr_48574;
})();
var statearr_48575_50324 = state_48567__$1;
(statearr_48575_50324[(2)] = null);

(statearr_48575_50324[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48568 === (4))){
var inst_48533 = (state_48567[(10)]);
var state_48567__$1 = state_48567;
return cljs.core.async.ioc_alts_BANG_(state_48567__$1,(7),inst_48533);
} else {
if((state_val_48568 === (6))){
var inst_48563 = (state_48567[(2)]);
var state_48567__$1 = state_48567;
var statearr_48583_50327 = state_48567__$1;
(statearr_48583_50327[(2)] = inst_48563);

(statearr_48583_50327[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48568 === (3))){
var inst_48565 = (state_48567[(2)]);
var state_48567__$1 = state_48567;
return cljs.core.async.impl.ioc_helpers.return_chan(state_48567__$1,inst_48565);
} else {
if((state_val_48568 === (2))){
var inst_48533 = (state_48567[(10)]);
var inst_48535 = cljs.core.count(inst_48533);
var inst_48536 = (inst_48535 > (0));
var state_48567__$1 = state_48567;
if(cljs.core.truth_(inst_48536)){
var statearr_48593_50331 = state_48567__$1;
(statearr_48593_50331[(1)] = (4));

} else {
var statearr_48594_50332 = state_48567__$1;
(statearr_48594_50332[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48568 === (11))){
var inst_48533 = (state_48567[(10)]);
var inst_48555 = (state_48567[(2)]);
var tmp48584 = inst_48533;
var inst_48533__$1 = tmp48584;
var state_48567__$1 = (function (){var statearr_48604 = state_48567;
(statearr_48604[(10)] = inst_48533__$1);

(statearr_48604[(11)] = inst_48555);

return statearr_48604;
})();
var statearr_48605_50333 = state_48567__$1;
(statearr_48605_50333[(2)] = null);

(statearr_48605_50333[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48568 === (9))){
var inst_48543 = (state_48567[(8)]);
var state_48567__$1 = state_48567;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_48567__$1,(11),out,inst_48543);
} else {
if((state_val_48568 === (5))){
var inst_48561 = cljs.core.async.close_BANG_(out);
var state_48567__$1 = state_48567;
var statearr_48622_50335 = state_48567__$1;
(statearr_48622_50335[(2)] = inst_48561);

(statearr_48622_50335[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48568 === (10))){
var inst_48558 = (state_48567[(2)]);
var state_48567__$1 = state_48567;
var statearr_48623_50336 = state_48567__$1;
(statearr_48623_50336[(2)] = inst_48558);

(statearr_48623_50336[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48568 === (8))){
var inst_48533 = (state_48567[(10)]);
var inst_48542 = (state_48567[(7)]);
var inst_48543 = (state_48567[(8)]);
var inst_48544 = (state_48567[(9)]);
var inst_48548 = (function (){var cs = inst_48533;
var vec__48538 = inst_48542;
var v = inst_48543;
var c = inst_48544;
return (function (p1__48489_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__48489_SHARP_);
});
})();
var inst_48550 = cljs.core.filterv(inst_48548,inst_48533);
var inst_48533__$1 = inst_48550;
var state_48567__$1 = (function (){var statearr_48629 = state_48567;
(statearr_48629[(10)] = inst_48533__$1);

return statearr_48629;
})();
var statearr_48630_50337 = state_48567__$1;
(statearr_48630_50337[(2)] = null);

(statearr_48630_50337[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__45510__auto__ = null;
var cljs$core$async$state_machine__45510__auto____0 = (function (){
var statearr_48634 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_48634[(0)] = cljs$core$async$state_machine__45510__auto__);

(statearr_48634[(1)] = (1));

return statearr_48634;
});
var cljs$core$async$state_machine__45510__auto____1 = (function (state_48567){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_48567);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e48635){var ex__45513__auto__ = e48635;
var statearr_48636_50340 = state_48567;
(statearr_48636_50340[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_48567[(4)]))){
var statearr_48638_50341 = state_48567;
(statearr_48638_50341[(1)] = cljs.core.first((state_48567[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__50346 = state_48567;
state_48567 = G__50346;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$state_machine__45510__auto__ = function(state_48567){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__45510__auto____1.call(this,state_48567);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__45510__auto____0;
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__45510__auto____1;
return cljs$core$async$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_48640 = f__45586__auto__();
(statearr_48640[(6)] = c__45585__auto___50321);

return statearr_48640;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return out;
}));

(cljs.core.async.merge.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__48646 = arguments.length;
switch (G__48646) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__45585__auto___50377 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_48676){
var state_val_48677 = (state_48676[(1)]);
if((state_val_48677 === (7))){
var inst_48655 = (state_48676[(7)]);
var inst_48655__$1 = (state_48676[(2)]);
var inst_48656 = (inst_48655__$1 == null);
var inst_48657 = cljs.core.not(inst_48656);
var state_48676__$1 = (function (){var statearr_48680 = state_48676;
(statearr_48680[(7)] = inst_48655__$1);

return statearr_48680;
})();
if(inst_48657){
var statearr_48681_50385 = state_48676__$1;
(statearr_48681_50385[(1)] = (8));

} else {
var statearr_48682_50389 = state_48676__$1;
(statearr_48682_50389[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48677 === (1))){
var inst_48650 = (0);
var state_48676__$1 = (function (){var statearr_48683 = state_48676;
(statearr_48683[(8)] = inst_48650);

return statearr_48683;
})();
var statearr_48684_50390 = state_48676__$1;
(statearr_48684_50390[(2)] = null);

(statearr_48684_50390[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48677 === (4))){
var state_48676__$1 = state_48676;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_48676__$1,(7),ch);
} else {
if((state_val_48677 === (6))){
var inst_48671 = (state_48676[(2)]);
var state_48676__$1 = state_48676;
var statearr_48685_50392 = state_48676__$1;
(statearr_48685_50392[(2)] = inst_48671);

(statearr_48685_50392[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48677 === (3))){
var inst_48673 = (state_48676[(2)]);
var inst_48674 = cljs.core.async.close_BANG_(out);
var state_48676__$1 = (function (){var statearr_48686 = state_48676;
(statearr_48686[(9)] = inst_48673);

return statearr_48686;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_48676__$1,inst_48674);
} else {
if((state_val_48677 === (2))){
var inst_48650 = (state_48676[(8)]);
var inst_48652 = (inst_48650 < n);
var state_48676__$1 = state_48676;
if(cljs.core.truth_(inst_48652)){
var statearr_48688_50397 = state_48676__$1;
(statearr_48688_50397[(1)] = (4));

} else {
var statearr_48690_50398 = state_48676__$1;
(statearr_48690_50398[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48677 === (11))){
var inst_48650 = (state_48676[(8)]);
var inst_48660 = (state_48676[(2)]);
var inst_48664 = (inst_48650 + (1));
var inst_48650__$1 = inst_48664;
var state_48676__$1 = (function (){var statearr_48693 = state_48676;
(statearr_48693[(8)] = inst_48650__$1);

(statearr_48693[(10)] = inst_48660);

return statearr_48693;
})();
var statearr_48694_50411 = state_48676__$1;
(statearr_48694_50411[(2)] = null);

(statearr_48694_50411[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48677 === (9))){
var state_48676__$1 = state_48676;
var statearr_48696_50415 = state_48676__$1;
(statearr_48696_50415[(2)] = null);

(statearr_48696_50415[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48677 === (5))){
var state_48676__$1 = state_48676;
var statearr_48702_50422 = state_48676__$1;
(statearr_48702_50422[(2)] = null);

(statearr_48702_50422[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48677 === (10))){
var inst_48668 = (state_48676[(2)]);
var state_48676__$1 = state_48676;
var statearr_48709_50427 = state_48676__$1;
(statearr_48709_50427[(2)] = inst_48668);

(statearr_48709_50427[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48677 === (8))){
var inst_48655 = (state_48676[(7)]);
var state_48676__$1 = state_48676;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_48676__$1,(11),out,inst_48655);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__45510__auto__ = null;
var cljs$core$async$state_machine__45510__auto____0 = (function (){
var statearr_48722 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_48722[(0)] = cljs$core$async$state_machine__45510__auto__);

(statearr_48722[(1)] = (1));

return statearr_48722;
});
var cljs$core$async$state_machine__45510__auto____1 = (function (state_48676){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_48676);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e48728){var ex__45513__auto__ = e48728;
var statearr_48729_50430 = state_48676;
(statearr_48729_50430[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_48676[(4)]))){
var statearr_48731_50431 = state_48676;
(statearr_48731_50431[(1)] = cljs.core.first((state_48676[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__50435 = state_48676;
state_48676 = G__50435;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$state_machine__45510__auto__ = function(state_48676){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__45510__auto____1.call(this,state_48676);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__45510__auto____0;
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__45510__auto____1;
return cljs$core$async$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_48735 = f__45586__auto__();
(statearr_48735[(6)] = c__45585__auto___50377);

return statearr_48735;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return out;
}));

(cljs.core.async.take.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async48739 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async48739 = (function (f,ch,meta48740){
this.f = f;
this.ch = ch;
this.meta48740 = meta48740;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async48739.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_48741,meta48740__$1){
var self__ = this;
var _48741__$1 = this;
return (new cljs.core.async.t_cljs$core$async48739(self__.f,self__.ch,meta48740__$1));
}));

(cljs.core.async.t_cljs$core$async48739.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_48741){
var self__ = this;
var _48741__$1 = this;
return self__.meta48740;
}));

(cljs.core.async.t_cljs$core$async48739.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48739.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async48739.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async48739.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48739.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async48752 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async48752 = (function (f,ch,meta48740,_,fn1,meta48753){
this.f = f;
this.ch = ch;
this.meta48740 = meta48740;
this._ = _;
this.fn1 = fn1;
this.meta48753 = meta48753;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async48752.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_48754,meta48753__$1){
var self__ = this;
var _48754__$1 = this;
return (new cljs.core.async.t_cljs$core$async48752(self__.f,self__.ch,self__.meta48740,self__._,self__.fn1,meta48753__$1));
}));

(cljs.core.async.t_cljs$core$async48752.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_48754){
var self__ = this;
var _48754__$1 = this;
return self__.meta48753;
}));

(cljs.core.async.t_cljs$core$async48752.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48752.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
}));

(cljs.core.async.t_cljs$core$async48752.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async48752.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return (function (p1__48737_SHARP_){
var G__48759 = (((p1__48737_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__48737_SHARP_) : self__.f.call(null,p1__48737_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__48759) : f1.call(null,G__48759));
});
}));

(cljs.core.async.t_cljs$core$async48752.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta48740","meta48740",1257217821,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async48739","cljs.core.async/t_cljs$core$async48739",746338523,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta48753","meta48753",-325263136,null)], null);
}));

(cljs.core.async.t_cljs$core$async48752.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async48752.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async48752");

(cljs.core.async.t_cljs$core$async48752.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async48752");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async48752.
 */
cljs.core.async.__GT_t_cljs$core$async48752 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async48752(f__$1,ch__$1,meta48740__$1,___$2,fn1__$1,meta48753){
return (new cljs.core.async.t_cljs$core$async48752(f__$1,ch__$1,meta48740__$1,___$2,fn1__$1,meta48753));
});

}

return (new cljs.core.async.t_cljs$core$async48752(self__.f,self__.ch,self__.meta48740,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4115__auto__ = ret;
if(cljs.core.truth_(and__4115__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__4115__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__48767 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__48767) : self__.f.call(null,G__48767));
})());
} else {
return ret;
}
}));

(cljs.core.async.t_cljs$core$async48739.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48739.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
}));

(cljs.core.async.t_cljs$core$async48739.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta48740","meta48740",1257217821,null)], null);
}));

(cljs.core.async.t_cljs$core$async48739.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async48739.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async48739");

(cljs.core.async.t_cljs$core$async48739.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async48739");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async48739.
 */
cljs.core.async.__GT_t_cljs$core$async48739 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async48739(f__$1,ch__$1,meta48740){
return (new cljs.core.async.t_cljs$core$async48739(f__$1,ch__$1,meta48740));
});

}

return (new cljs.core.async.t_cljs$core$async48739(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async48773 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async48773 = (function (f,ch,meta48774){
this.f = f;
this.ch = ch;
this.meta48774 = meta48774;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async48773.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_48775,meta48774__$1){
var self__ = this;
var _48775__$1 = this;
return (new cljs.core.async.t_cljs$core$async48773(self__.f,self__.ch,meta48774__$1));
}));

(cljs.core.async.t_cljs$core$async48773.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_48775){
var self__ = this;
var _48775__$1 = this;
return self__.meta48774;
}));

(cljs.core.async.t_cljs$core$async48773.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48773.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async48773.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48773.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async48773.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48773.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
}));

(cljs.core.async.t_cljs$core$async48773.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta48774","meta48774",-1493250670,null)], null);
}));

(cljs.core.async.t_cljs$core$async48773.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async48773.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async48773");

(cljs.core.async.t_cljs$core$async48773.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async48773");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async48773.
 */
cljs.core.async.__GT_t_cljs$core$async48773 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async48773(f__$1,ch__$1,meta48774){
return (new cljs.core.async.t_cljs$core$async48773(f__$1,ch__$1,meta48774));
});

}

return (new cljs.core.async.t_cljs$core$async48773(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async48789 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async48789 = (function (p,ch,meta48790){
this.p = p;
this.ch = ch;
this.meta48790 = meta48790;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async48789.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_48791,meta48790__$1){
var self__ = this;
var _48791__$1 = this;
return (new cljs.core.async.t_cljs$core$async48789(self__.p,self__.ch,meta48790__$1));
}));

(cljs.core.async.t_cljs$core$async48789.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_48791){
var self__ = this;
var _48791__$1 = this;
return self__.meta48790;
}));

(cljs.core.async.t_cljs$core$async48789.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48789.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async48789.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async48789.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48789.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async48789.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async48789.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
}));

(cljs.core.async.t_cljs$core$async48789.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta48790","meta48790",1317542749,null)], null);
}));

(cljs.core.async.t_cljs$core$async48789.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async48789.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async48789");

(cljs.core.async.t_cljs$core$async48789.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async48789");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async48789.
 */
cljs.core.async.__GT_t_cljs$core$async48789 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async48789(p__$1,ch__$1,meta48790){
return (new cljs.core.async.t_cljs$core$async48789(p__$1,ch__$1,meta48790));
});

}

return (new cljs.core.async.t_cljs$core$async48789(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__48812 = arguments.length;
switch (G__48812) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__45585__auto___50459 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_48833){
var state_val_48834 = (state_48833[(1)]);
if((state_val_48834 === (7))){
var inst_48829 = (state_48833[(2)]);
var state_48833__$1 = state_48833;
var statearr_48835_50460 = state_48833__$1;
(statearr_48835_50460[(2)] = inst_48829);

(statearr_48835_50460[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48834 === (1))){
var state_48833__$1 = state_48833;
var statearr_48839_50462 = state_48833__$1;
(statearr_48839_50462[(2)] = null);

(statearr_48839_50462[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48834 === (4))){
var inst_48815 = (state_48833[(7)]);
var inst_48815__$1 = (state_48833[(2)]);
var inst_48816 = (inst_48815__$1 == null);
var state_48833__$1 = (function (){var statearr_48840 = state_48833;
(statearr_48840[(7)] = inst_48815__$1);

return statearr_48840;
})();
if(cljs.core.truth_(inst_48816)){
var statearr_48841_50468 = state_48833__$1;
(statearr_48841_50468[(1)] = (5));

} else {
var statearr_48842_50473 = state_48833__$1;
(statearr_48842_50473[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48834 === (6))){
var inst_48815 = (state_48833[(7)]);
var inst_48820 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_48815) : p.call(null,inst_48815));
var state_48833__$1 = state_48833;
if(cljs.core.truth_(inst_48820)){
var statearr_48848_50475 = state_48833__$1;
(statearr_48848_50475[(1)] = (8));

} else {
var statearr_48849_50476 = state_48833__$1;
(statearr_48849_50476[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48834 === (3))){
var inst_48831 = (state_48833[(2)]);
var state_48833__$1 = state_48833;
return cljs.core.async.impl.ioc_helpers.return_chan(state_48833__$1,inst_48831);
} else {
if((state_val_48834 === (2))){
var state_48833__$1 = state_48833;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_48833__$1,(4),ch);
} else {
if((state_val_48834 === (11))){
var inst_48823 = (state_48833[(2)]);
var state_48833__$1 = state_48833;
var statearr_48850_50482 = state_48833__$1;
(statearr_48850_50482[(2)] = inst_48823);

(statearr_48850_50482[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48834 === (9))){
var state_48833__$1 = state_48833;
var statearr_48851_50483 = state_48833__$1;
(statearr_48851_50483[(2)] = null);

(statearr_48851_50483[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48834 === (5))){
var inst_48818 = cljs.core.async.close_BANG_(out);
var state_48833__$1 = state_48833;
var statearr_48852_50484 = state_48833__$1;
(statearr_48852_50484[(2)] = inst_48818);

(statearr_48852_50484[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48834 === (10))){
var inst_48826 = (state_48833[(2)]);
var state_48833__$1 = (function (){var statearr_48853 = state_48833;
(statearr_48853[(8)] = inst_48826);

return statearr_48853;
})();
var statearr_48854_50485 = state_48833__$1;
(statearr_48854_50485[(2)] = null);

(statearr_48854_50485[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48834 === (8))){
var inst_48815 = (state_48833[(7)]);
var state_48833__$1 = state_48833;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_48833__$1,(11),out,inst_48815);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__45510__auto__ = null;
var cljs$core$async$state_machine__45510__auto____0 = (function (){
var statearr_48855 = [null,null,null,null,null,null,null,null,null];
(statearr_48855[(0)] = cljs$core$async$state_machine__45510__auto__);

(statearr_48855[(1)] = (1));

return statearr_48855;
});
var cljs$core$async$state_machine__45510__auto____1 = (function (state_48833){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_48833);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e48856){var ex__45513__auto__ = e48856;
var statearr_48857_50492 = state_48833;
(statearr_48857_50492[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_48833[(4)]))){
var statearr_48858_50497 = state_48833;
(statearr_48858_50497[(1)] = cljs.core.first((state_48833[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__50499 = state_48833;
state_48833 = G__50499;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$state_machine__45510__auto__ = function(state_48833){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__45510__auto____1.call(this,state_48833);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__45510__auto____0;
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__45510__auto____1;
return cljs$core$async$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_48859 = f__45586__auto__();
(statearr_48859[(6)] = c__45585__auto___50459);

return statearr_48859;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return out;
}));

(cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__48867 = arguments.length;
switch (G__48867) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
}));

(cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3);

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__45585__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_48948){
var state_val_48949 = (state_48948[(1)]);
if((state_val_48949 === (7))){
var inst_48941 = (state_48948[(2)]);
var state_48948__$1 = state_48948;
var statearr_48959_50516 = state_48948__$1;
(statearr_48959_50516[(2)] = inst_48941);

(statearr_48959_50516[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (20))){
var inst_48908 = (state_48948[(7)]);
var inst_48922 = (state_48948[(2)]);
var inst_48923 = cljs.core.next(inst_48908);
var inst_48889 = inst_48923;
var inst_48890 = null;
var inst_48891 = (0);
var inst_48892 = (0);
var state_48948__$1 = (function (){var statearr_48963 = state_48948;
(statearr_48963[(8)] = inst_48889);

(statearr_48963[(9)] = inst_48892);

(statearr_48963[(10)] = inst_48890);

(statearr_48963[(11)] = inst_48922);

(statearr_48963[(12)] = inst_48891);

return statearr_48963;
})();
var statearr_48964_50524 = state_48948__$1;
(statearr_48964_50524[(2)] = null);

(statearr_48964_50524[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (1))){
var state_48948__$1 = state_48948;
var statearr_48967_50525 = state_48948__$1;
(statearr_48967_50525[(2)] = null);

(statearr_48967_50525[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (4))){
var inst_48877 = (state_48948[(13)]);
var inst_48877__$1 = (state_48948[(2)]);
var inst_48878 = (inst_48877__$1 == null);
var state_48948__$1 = (function (){var statearr_48972 = state_48948;
(statearr_48972[(13)] = inst_48877__$1);

return statearr_48972;
})();
if(cljs.core.truth_(inst_48878)){
var statearr_48974_50529 = state_48948__$1;
(statearr_48974_50529[(1)] = (5));

} else {
var statearr_48975_50530 = state_48948__$1;
(statearr_48975_50530[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (15))){
var state_48948__$1 = state_48948;
var statearr_48980_50531 = state_48948__$1;
(statearr_48980_50531[(2)] = null);

(statearr_48980_50531[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (21))){
var state_48948__$1 = state_48948;
var statearr_48983_50533 = state_48948__$1;
(statearr_48983_50533[(2)] = null);

(statearr_48983_50533[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (13))){
var inst_48889 = (state_48948[(8)]);
var inst_48892 = (state_48948[(9)]);
var inst_48890 = (state_48948[(10)]);
var inst_48891 = (state_48948[(12)]);
var inst_48900 = (state_48948[(2)]);
var inst_48902 = (inst_48892 + (1));
var tmp48977 = inst_48889;
var tmp48978 = inst_48890;
var tmp48979 = inst_48891;
var inst_48889__$1 = tmp48977;
var inst_48890__$1 = tmp48978;
var inst_48891__$1 = tmp48979;
var inst_48892__$1 = inst_48902;
var state_48948__$1 = (function (){var statearr_48989 = state_48948;
(statearr_48989[(8)] = inst_48889__$1);

(statearr_48989[(9)] = inst_48892__$1);

(statearr_48989[(10)] = inst_48890__$1);

(statearr_48989[(12)] = inst_48891__$1);

(statearr_48989[(14)] = inst_48900);

return statearr_48989;
})();
var statearr_48993_50536 = state_48948__$1;
(statearr_48993_50536[(2)] = null);

(statearr_48993_50536[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (22))){
var state_48948__$1 = state_48948;
var statearr_48995_50537 = state_48948__$1;
(statearr_48995_50537[(2)] = null);

(statearr_48995_50537[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (6))){
var inst_48877 = (state_48948[(13)]);
var inst_48886 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_48877) : f.call(null,inst_48877));
var inst_48887 = cljs.core.seq(inst_48886);
var inst_48889 = inst_48887;
var inst_48890 = null;
var inst_48891 = (0);
var inst_48892 = (0);
var state_48948__$1 = (function (){var statearr_48996 = state_48948;
(statearr_48996[(8)] = inst_48889);

(statearr_48996[(9)] = inst_48892);

(statearr_48996[(10)] = inst_48890);

(statearr_48996[(12)] = inst_48891);

return statearr_48996;
})();
var statearr_48997_50538 = state_48948__$1;
(statearr_48997_50538[(2)] = null);

(statearr_48997_50538[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (17))){
var inst_48908 = (state_48948[(7)]);
var inst_48914 = cljs.core.chunk_first(inst_48908);
var inst_48915 = cljs.core.chunk_rest(inst_48908);
var inst_48916 = cljs.core.count(inst_48914);
var inst_48889 = inst_48915;
var inst_48890 = inst_48914;
var inst_48891 = inst_48916;
var inst_48892 = (0);
var state_48948__$1 = (function (){var statearr_48998 = state_48948;
(statearr_48998[(8)] = inst_48889);

(statearr_48998[(9)] = inst_48892);

(statearr_48998[(10)] = inst_48890);

(statearr_48998[(12)] = inst_48891);

return statearr_48998;
})();
var statearr_49001_50541 = state_48948__$1;
(statearr_49001_50541[(2)] = null);

(statearr_49001_50541[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (3))){
var inst_48943 = (state_48948[(2)]);
var state_48948__$1 = state_48948;
return cljs.core.async.impl.ioc_helpers.return_chan(state_48948__$1,inst_48943);
} else {
if((state_val_48949 === (12))){
var inst_48931 = (state_48948[(2)]);
var state_48948__$1 = state_48948;
var statearr_49010_50543 = state_48948__$1;
(statearr_49010_50543[(2)] = inst_48931);

(statearr_49010_50543[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (2))){
var state_48948__$1 = state_48948;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_48948__$1,(4),in$);
} else {
if((state_val_48949 === (23))){
var inst_48939 = (state_48948[(2)]);
var state_48948__$1 = state_48948;
var statearr_49013_50545 = state_48948__$1;
(statearr_49013_50545[(2)] = inst_48939);

(statearr_49013_50545[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (19))){
var inst_48926 = (state_48948[(2)]);
var state_48948__$1 = state_48948;
var statearr_49015_50547 = state_48948__$1;
(statearr_49015_50547[(2)] = inst_48926);

(statearr_49015_50547[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (11))){
var inst_48889 = (state_48948[(8)]);
var inst_48908 = (state_48948[(7)]);
var inst_48908__$1 = cljs.core.seq(inst_48889);
var state_48948__$1 = (function (){var statearr_49018 = state_48948;
(statearr_49018[(7)] = inst_48908__$1);

return statearr_49018;
})();
if(inst_48908__$1){
var statearr_49021_50549 = state_48948__$1;
(statearr_49021_50549[(1)] = (14));

} else {
var statearr_49023_50550 = state_48948__$1;
(statearr_49023_50550[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (9))){
var inst_48933 = (state_48948[(2)]);
var inst_48934 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_48948__$1 = (function (){var statearr_49027 = state_48948;
(statearr_49027[(15)] = inst_48933);

return statearr_49027;
})();
if(cljs.core.truth_(inst_48934)){
var statearr_49028_50552 = state_48948__$1;
(statearr_49028_50552[(1)] = (21));

} else {
var statearr_49030_50553 = state_48948__$1;
(statearr_49030_50553[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (5))){
var inst_48880 = cljs.core.async.close_BANG_(out);
var state_48948__$1 = state_48948;
var statearr_49034_50554 = state_48948__$1;
(statearr_49034_50554[(2)] = inst_48880);

(statearr_49034_50554[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (14))){
var inst_48908 = (state_48948[(7)]);
var inst_48911 = cljs.core.chunked_seq_QMARK_(inst_48908);
var state_48948__$1 = state_48948;
if(inst_48911){
var statearr_49037_50558 = state_48948__$1;
(statearr_49037_50558[(1)] = (17));

} else {
var statearr_49039_50563 = state_48948__$1;
(statearr_49039_50563[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (16))){
var inst_48929 = (state_48948[(2)]);
var state_48948__$1 = state_48948;
var statearr_49044_50567 = state_48948__$1;
(statearr_49044_50567[(2)] = inst_48929);

(statearr_49044_50567[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_48949 === (10))){
var inst_48892 = (state_48948[(9)]);
var inst_48890 = (state_48948[(10)]);
var inst_48898 = cljs.core._nth(inst_48890,inst_48892);
var state_48948__$1 = state_48948;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_48948__$1,(13),out,inst_48898);
} else {
if((state_val_48949 === (18))){
var inst_48908 = (state_48948[(7)]);
var inst_48920 = cljs.core.first(inst_48908);
var state_48948__$1 = state_48948;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_48948__$1,(20),out,inst_48920);
} else {
if((state_val_48949 === (8))){
var inst_48892 = (state_48948[(9)]);
var inst_48891 = (state_48948[(12)]);
var inst_48895 = (inst_48892 < inst_48891);
var inst_48896 = inst_48895;
var state_48948__$1 = state_48948;
if(cljs.core.truth_(inst_48896)){
var statearr_49052_50589 = state_48948__$1;
(statearr_49052_50589[(1)] = (10));

} else {
var statearr_49053_50594 = state_48948__$1;
(statearr_49053_50594[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__45510__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__45510__auto____0 = (function (){
var statearr_49055 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_49055[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__45510__auto__);

(statearr_49055[(1)] = (1));

return statearr_49055;
});
var cljs$core$async$mapcat_STAR__$_state_machine__45510__auto____1 = (function (state_48948){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_48948);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e49059){var ex__45513__auto__ = e49059;
var statearr_49062_50609 = state_48948;
(statearr_49062_50609[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_48948[(4)]))){
var statearr_49064_50612 = state_48948;
(statearr_49064_50612[(1)] = cljs.core.first((state_48948[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__50618 = state_48948;
state_48948 = G__50618;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__45510__auto__ = function(state_48948){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__45510__auto____1.call(this,state_48948);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__45510__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__45510__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_49068 = f__45586__auto__();
(statearr_49068[(6)] = c__45585__auto__);

return statearr_49068;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));

return c__45585__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__49076 = arguments.length;
switch (G__49076) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
}));

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
}));

(cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__49089 = arguments.length;
switch (G__49089) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
}));

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
}));

(cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__49105 = arguments.length;
switch (G__49105) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
}));

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__45585__auto___50664 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_49136){
var state_val_49137 = (state_49136[(1)]);
if((state_val_49137 === (7))){
var inst_49129 = (state_49136[(2)]);
var state_49136__$1 = state_49136;
var statearr_49154_50665 = state_49136__$1;
(statearr_49154_50665[(2)] = inst_49129);

(statearr_49154_50665[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49137 === (1))){
var inst_49109 = null;
var state_49136__$1 = (function (){var statearr_49157 = state_49136;
(statearr_49157[(7)] = inst_49109);

return statearr_49157;
})();
var statearr_49159_50670 = state_49136__$1;
(statearr_49159_50670[(2)] = null);

(statearr_49159_50670[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49137 === (4))){
var inst_49114 = (state_49136[(8)]);
var inst_49114__$1 = (state_49136[(2)]);
var inst_49115 = (inst_49114__$1 == null);
var inst_49116 = cljs.core.not(inst_49115);
var state_49136__$1 = (function (){var statearr_49164 = state_49136;
(statearr_49164[(8)] = inst_49114__$1);

return statearr_49164;
})();
if(inst_49116){
var statearr_49165_50676 = state_49136__$1;
(statearr_49165_50676[(1)] = (5));

} else {
var statearr_49167_50677 = state_49136__$1;
(statearr_49167_50677[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49137 === (6))){
var state_49136__$1 = state_49136;
var statearr_49169_50682 = state_49136__$1;
(statearr_49169_50682[(2)] = null);

(statearr_49169_50682[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49137 === (3))){
var inst_49131 = (state_49136[(2)]);
var inst_49132 = cljs.core.async.close_BANG_(out);
var state_49136__$1 = (function (){var statearr_49173 = state_49136;
(statearr_49173[(9)] = inst_49131);

return statearr_49173;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_49136__$1,inst_49132);
} else {
if((state_val_49137 === (2))){
var state_49136__$1 = state_49136;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_49136__$1,(4),ch);
} else {
if((state_val_49137 === (11))){
var inst_49114 = (state_49136[(8)]);
var inst_49123 = (state_49136[(2)]);
var inst_49109 = inst_49114;
var state_49136__$1 = (function (){var statearr_49175 = state_49136;
(statearr_49175[(7)] = inst_49109);

(statearr_49175[(10)] = inst_49123);

return statearr_49175;
})();
var statearr_49176_50684 = state_49136__$1;
(statearr_49176_50684[(2)] = null);

(statearr_49176_50684[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49137 === (9))){
var inst_49114 = (state_49136[(8)]);
var state_49136__$1 = state_49136;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_49136__$1,(11),out,inst_49114);
} else {
if((state_val_49137 === (5))){
var inst_49109 = (state_49136[(7)]);
var inst_49114 = (state_49136[(8)]);
var inst_49118 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_49114,inst_49109);
var state_49136__$1 = state_49136;
if(inst_49118){
var statearr_49186_50686 = state_49136__$1;
(statearr_49186_50686[(1)] = (8));

} else {
var statearr_49187_50687 = state_49136__$1;
(statearr_49187_50687[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49137 === (10))){
var inst_49126 = (state_49136[(2)]);
var state_49136__$1 = state_49136;
var statearr_49188_50688 = state_49136__$1;
(statearr_49188_50688[(2)] = inst_49126);

(statearr_49188_50688[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49137 === (8))){
var inst_49109 = (state_49136[(7)]);
var tmp49181 = inst_49109;
var inst_49109__$1 = tmp49181;
var state_49136__$1 = (function (){var statearr_49189 = state_49136;
(statearr_49189[(7)] = inst_49109__$1);

return statearr_49189;
})();
var statearr_49190_50693 = state_49136__$1;
(statearr_49190_50693[(2)] = null);

(statearr_49190_50693[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__45510__auto__ = null;
var cljs$core$async$state_machine__45510__auto____0 = (function (){
var statearr_49196 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_49196[(0)] = cljs$core$async$state_machine__45510__auto__);

(statearr_49196[(1)] = (1));

return statearr_49196;
});
var cljs$core$async$state_machine__45510__auto____1 = (function (state_49136){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_49136);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e49197){var ex__45513__auto__ = e49197;
var statearr_49199_50695 = state_49136;
(statearr_49199_50695[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_49136[(4)]))){
var statearr_49200_50699 = state_49136;
(statearr_49200_50699[(1)] = cljs.core.first((state_49136[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__50700 = state_49136;
state_49136 = G__50700;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$state_machine__45510__auto__ = function(state_49136){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__45510__auto____1.call(this,state_49136);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__45510__auto____0;
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__45510__auto____1;
return cljs$core$async$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_49202 = f__45586__auto__();
(statearr_49202[(6)] = c__45585__auto___50664);

return statearr_49202;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return out;
}));

(cljs.core.async.unique.cljs$lang$maxFixedArity = 2);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__49218 = arguments.length;
switch (G__49218) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__45585__auto___50713 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_49282){
var state_val_49283 = (state_49282[(1)]);
if((state_val_49283 === (7))){
var inst_49275 = (state_49282[(2)]);
var state_49282__$1 = state_49282;
var statearr_49289_50722 = state_49282__$1;
(statearr_49289_50722[(2)] = inst_49275);

(statearr_49289_50722[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49283 === (1))){
var inst_49236 = (new Array(n));
var inst_49237 = inst_49236;
var inst_49238 = (0);
var state_49282__$1 = (function (){var statearr_49296 = state_49282;
(statearr_49296[(7)] = inst_49237);

(statearr_49296[(8)] = inst_49238);

return statearr_49296;
})();
var statearr_49297_50732 = state_49282__$1;
(statearr_49297_50732[(2)] = null);

(statearr_49297_50732[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49283 === (4))){
var inst_49244 = (state_49282[(9)]);
var inst_49244__$1 = (state_49282[(2)]);
var inst_49245 = (inst_49244__$1 == null);
var inst_49246 = cljs.core.not(inst_49245);
var state_49282__$1 = (function (){var statearr_49298 = state_49282;
(statearr_49298[(9)] = inst_49244__$1);

return statearr_49298;
})();
if(inst_49246){
var statearr_49299_50744 = state_49282__$1;
(statearr_49299_50744[(1)] = (5));

} else {
var statearr_49300_50746 = state_49282__$1;
(statearr_49300_50746[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49283 === (15))){
var inst_49269 = (state_49282[(2)]);
var state_49282__$1 = state_49282;
var statearr_49305_50753 = state_49282__$1;
(statearr_49305_50753[(2)] = inst_49269);

(statearr_49305_50753[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49283 === (13))){
var state_49282__$1 = state_49282;
var statearr_49309_50757 = state_49282__$1;
(statearr_49309_50757[(2)] = null);

(statearr_49309_50757[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49283 === (6))){
var inst_49238 = (state_49282[(8)]);
var inst_49265 = (inst_49238 > (0));
var state_49282__$1 = state_49282;
if(cljs.core.truth_(inst_49265)){
var statearr_49315_50765 = state_49282__$1;
(statearr_49315_50765[(1)] = (12));

} else {
var statearr_49316_50766 = state_49282__$1;
(statearr_49316_50766[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49283 === (3))){
var inst_49277 = (state_49282[(2)]);
var state_49282__$1 = state_49282;
return cljs.core.async.impl.ioc_helpers.return_chan(state_49282__$1,inst_49277);
} else {
if((state_val_49283 === (12))){
var inst_49237 = (state_49282[(7)]);
var inst_49267 = cljs.core.vec(inst_49237);
var state_49282__$1 = state_49282;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_49282__$1,(15),out,inst_49267);
} else {
if((state_val_49283 === (2))){
var state_49282__$1 = state_49282;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_49282__$1,(4),ch);
} else {
if((state_val_49283 === (11))){
var inst_49257 = (state_49282[(2)]);
var inst_49258 = (new Array(n));
var inst_49237 = inst_49258;
var inst_49238 = (0);
var state_49282__$1 = (function (){var statearr_49328 = state_49282;
(statearr_49328[(7)] = inst_49237);

(statearr_49328[(10)] = inst_49257);

(statearr_49328[(8)] = inst_49238);

return statearr_49328;
})();
var statearr_49331_50768 = state_49282__$1;
(statearr_49331_50768[(2)] = null);

(statearr_49331_50768[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49283 === (9))){
var inst_49237 = (state_49282[(7)]);
var inst_49255 = cljs.core.vec(inst_49237);
var state_49282__$1 = state_49282;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_49282__$1,(11),out,inst_49255);
} else {
if((state_val_49283 === (5))){
var inst_49237 = (state_49282[(7)]);
var inst_49238 = (state_49282[(8)]);
var inst_49244 = (state_49282[(9)]);
var inst_49249 = (state_49282[(11)]);
var inst_49248 = (inst_49237[inst_49238] = inst_49244);
var inst_49249__$1 = (inst_49238 + (1));
var inst_49250 = (inst_49249__$1 < n);
var state_49282__$1 = (function (){var statearr_49332 = state_49282;
(statearr_49332[(12)] = inst_49248);

(statearr_49332[(11)] = inst_49249__$1);

return statearr_49332;
})();
if(cljs.core.truth_(inst_49250)){
var statearr_49335_50772 = state_49282__$1;
(statearr_49335_50772[(1)] = (8));

} else {
var statearr_49338_50773 = state_49282__$1;
(statearr_49338_50773[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49283 === (14))){
var inst_49272 = (state_49282[(2)]);
var inst_49273 = cljs.core.async.close_BANG_(out);
var state_49282__$1 = (function (){var statearr_49342 = state_49282;
(statearr_49342[(13)] = inst_49272);

return statearr_49342;
})();
var statearr_49343_50774 = state_49282__$1;
(statearr_49343_50774[(2)] = inst_49273);

(statearr_49343_50774[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49283 === (10))){
var inst_49262 = (state_49282[(2)]);
var state_49282__$1 = state_49282;
var statearr_49344_50779 = state_49282__$1;
(statearr_49344_50779[(2)] = inst_49262);

(statearr_49344_50779[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49283 === (8))){
var inst_49237 = (state_49282[(7)]);
var inst_49249 = (state_49282[(11)]);
var tmp49341 = inst_49237;
var inst_49237__$1 = tmp49341;
var inst_49238 = inst_49249;
var state_49282__$1 = (function (){var statearr_49345 = state_49282;
(statearr_49345[(7)] = inst_49237__$1);

(statearr_49345[(8)] = inst_49238);

return statearr_49345;
})();
var statearr_49346_50788 = state_49282__$1;
(statearr_49346_50788[(2)] = null);

(statearr_49346_50788[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__45510__auto__ = null;
var cljs$core$async$state_machine__45510__auto____0 = (function (){
var statearr_49347 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_49347[(0)] = cljs$core$async$state_machine__45510__auto__);

(statearr_49347[(1)] = (1));

return statearr_49347;
});
var cljs$core$async$state_machine__45510__auto____1 = (function (state_49282){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_49282);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e49348){var ex__45513__auto__ = e49348;
var statearr_49349_50791 = state_49282;
(statearr_49349_50791[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_49282[(4)]))){
var statearr_49350_50792 = state_49282;
(statearr_49350_50792[(1)] = cljs.core.first((state_49282[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__50793 = state_49282;
state_49282 = G__50793;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$state_machine__45510__auto__ = function(state_49282){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__45510__auto____1.call(this,state_49282);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__45510__auto____0;
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__45510__auto____1;
return cljs$core$async$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_49352 = f__45586__auto__();
(statearr_49352[(6)] = c__45585__auto___50713);

return statearr_49352;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return out;
}));

(cljs.core.async.partition.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__49354 = arguments.length;
switch (G__49354) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
}));

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__45585__auto___50796 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__45586__auto__ = (function (){var switch__45509__auto__ = (function (state_49408){
var state_val_49409 = (state_49408[(1)]);
if((state_val_49409 === (7))){
var inst_49404 = (state_49408[(2)]);
var state_49408__$1 = state_49408;
var statearr_49410_50798 = state_49408__$1;
(statearr_49410_50798[(2)] = inst_49404);

(statearr_49410_50798[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49409 === (1))){
var inst_49355 = [];
var inst_49356 = inst_49355;
var inst_49357 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_49408__$1 = (function (){var statearr_49411 = state_49408;
(statearr_49411[(7)] = inst_49356);

(statearr_49411[(8)] = inst_49357);

return statearr_49411;
})();
var statearr_49412_50801 = state_49408__$1;
(statearr_49412_50801[(2)] = null);

(statearr_49412_50801[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49409 === (4))){
var inst_49360 = (state_49408[(9)]);
var inst_49360__$1 = (state_49408[(2)]);
var inst_49361 = (inst_49360__$1 == null);
var inst_49362 = cljs.core.not(inst_49361);
var state_49408__$1 = (function (){var statearr_49416 = state_49408;
(statearr_49416[(9)] = inst_49360__$1);

return statearr_49416;
})();
if(inst_49362){
var statearr_49417_50806 = state_49408__$1;
(statearr_49417_50806[(1)] = (5));

} else {
var statearr_49418_50807 = state_49408__$1;
(statearr_49418_50807[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49409 === (15))){
var inst_49394 = (state_49408[(2)]);
var state_49408__$1 = state_49408;
var statearr_49419_50808 = state_49408__$1;
(statearr_49419_50808[(2)] = inst_49394);

(statearr_49419_50808[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49409 === (13))){
var state_49408__$1 = state_49408;
var statearr_49420_50809 = state_49408__$1;
(statearr_49420_50809[(2)] = null);

(statearr_49420_50809[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49409 === (6))){
var inst_49356 = (state_49408[(7)]);
var inst_49389 = inst_49356.length;
var inst_49390 = (inst_49389 > (0));
var state_49408__$1 = state_49408;
if(cljs.core.truth_(inst_49390)){
var statearr_49421_50810 = state_49408__$1;
(statearr_49421_50810[(1)] = (12));

} else {
var statearr_49422_50811 = state_49408__$1;
(statearr_49422_50811[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49409 === (3))){
var inst_49406 = (state_49408[(2)]);
var state_49408__$1 = state_49408;
return cljs.core.async.impl.ioc_helpers.return_chan(state_49408__$1,inst_49406);
} else {
if((state_val_49409 === (12))){
var inst_49356 = (state_49408[(7)]);
var inst_49392 = cljs.core.vec(inst_49356);
var state_49408__$1 = state_49408;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_49408__$1,(15),out,inst_49392);
} else {
if((state_val_49409 === (2))){
var state_49408__$1 = state_49408;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_49408__$1,(4),ch);
} else {
if((state_val_49409 === (11))){
var inst_49364 = (state_49408[(10)]);
var inst_49360 = (state_49408[(9)]);
var inst_49382 = (state_49408[(2)]);
var inst_49383 = [];
var inst_49384 = inst_49383.push(inst_49360);
var inst_49356 = inst_49383;
var inst_49357 = inst_49364;
var state_49408__$1 = (function (){var statearr_49423 = state_49408;
(statearr_49423[(11)] = inst_49382);

(statearr_49423[(7)] = inst_49356);

(statearr_49423[(12)] = inst_49384);

(statearr_49423[(8)] = inst_49357);

return statearr_49423;
})();
var statearr_49424_50816 = state_49408__$1;
(statearr_49424_50816[(2)] = null);

(statearr_49424_50816[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49409 === (9))){
var inst_49356 = (state_49408[(7)]);
var inst_49380 = cljs.core.vec(inst_49356);
var state_49408__$1 = state_49408;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_49408__$1,(11),out,inst_49380);
} else {
if((state_val_49409 === (5))){
var inst_49364 = (state_49408[(10)]);
var inst_49357 = (state_49408[(8)]);
var inst_49360 = (state_49408[(9)]);
var inst_49364__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_49360) : f.call(null,inst_49360));
var inst_49369 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_49364__$1,inst_49357);
var inst_49370 = cljs.core.keyword_identical_QMARK_(inst_49357,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_49371 = ((inst_49369) || (inst_49370));
var state_49408__$1 = (function (){var statearr_49425 = state_49408;
(statearr_49425[(10)] = inst_49364__$1);

return statearr_49425;
})();
if(cljs.core.truth_(inst_49371)){
var statearr_49426_50820 = state_49408__$1;
(statearr_49426_50820[(1)] = (8));

} else {
var statearr_49427_50821 = state_49408__$1;
(statearr_49427_50821[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49409 === (14))){
var inst_49401 = (state_49408[(2)]);
var inst_49402 = cljs.core.async.close_BANG_(out);
var state_49408__$1 = (function (){var statearr_49429 = state_49408;
(statearr_49429[(13)] = inst_49401);

return statearr_49429;
})();
var statearr_49430_50822 = state_49408__$1;
(statearr_49430_50822[(2)] = inst_49402);

(statearr_49430_50822[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49409 === (10))){
var inst_49387 = (state_49408[(2)]);
var state_49408__$1 = state_49408;
var statearr_49431_50824 = state_49408__$1;
(statearr_49431_50824[(2)] = inst_49387);

(statearr_49431_50824[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_49409 === (8))){
var inst_49356 = (state_49408[(7)]);
var inst_49364 = (state_49408[(10)]);
var inst_49360 = (state_49408[(9)]);
var inst_49377 = inst_49356.push(inst_49360);
var tmp49428 = inst_49356;
var inst_49356__$1 = tmp49428;
var inst_49357 = inst_49364;
var state_49408__$1 = (function (){var statearr_49432 = state_49408;
(statearr_49432[(7)] = inst_49356__$1);

(statearr_49432[(8)] = inst_49357);

(statearr_49432[(14)] = inst_49377);

return statearr_49432;
})();
var statearr_49433_50825 = state_49408__$1;
(statearr_49433_50825[(2)] = null);

(statearr_49433_50825[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__45510__auto__ = null;
var cljs$core$async$state_machine__45510__auto____0 = (function (){
var statearr_49434 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_49434[(0)] = cljs$core$async$state_machine__45510__auto__);

(statearr_49434[(1)] = (1));

return statearr_49434;
});
var cljs$core$async$state_machine__45510__auto____1 = (function (state_49408){
while(true){
var ret_value__45511__auto__ = (function (){try{while(true){
var result__45512__auto__ = switch__45509__auto__(state_49408);
if(cljs.core.keyword_identical_QMARK_(result__45512__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__45512__auto__;
}
break;
}
}catch (e49435){var ex__45513__auto__ = e49435;
var statearr_49436_50829 = state_49408;
(statearr_49436_50829[(2)] = ex__45513__auto__);


if(cljs.core.seq((state_49408[(4)]))){
var statearr_49437_50831 = state_49408;
(statearr_49437_50831[(1)] = cljs.core.first((state_49408[(4)])));

} else {
throw ex__45513__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__45511__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__50832 = state_49408;
state_49408 = G__50832;
continue;
} else {
return ret_value__45511__auto__;
}
break;
}
});
cljs$core$async$state_machine__45510__auto__ = function(state_49408){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__45510__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__45510__auto____1.call(this,state_49408);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__45510__auto____0;
cljs$core$async$state_machine__45510__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__45510__auto____1;
return cljs$core$async$state_machine__45510__auto__;
})()
})();
var state__45587__auto__ = (function (){var statearr_49439 = f__45586__auto__();
(statearr_49439[(6)] = c__45585__auto___50796);

return statearr_49439;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__45587__auto__);
}));


return out;
}));

(cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3);

Object.defineProperty(module.exports, "Pub", { enumerable: false, get: function() { return cljs.core.async.Pub; } });
Object.defineProperty(module.exports, "reduce", { enumerable: false, get: function() { return cljs.core.async.reduce; } });
Object.defineProperty(module.exports, "remove_GT_", { enumerable: false, get: function() { return cljs.core.async.remove_GT_; } });
Object.defineProperty(module.exports, "timeout", { enumerable: false, get: function() { return cljs.core.async.timeout; } });
Object.defineProperty(module.exports, "unsub_STAR_", { enumerable: false, get: function() { return cljs.core.async.unsub_STAR_; } });
Object.defineProperty(module.exports, "admix_STAR_", { enumerable: false, get: function() { return cljs.core.async.admix_STAR_; } });
Object.defineProperty(module.exports, "unmix_STAR_", { enumerable: false, get: function() { return cljs.core.async.unmix_STAR_; } });
Object.defineProperty(module.exports, "mapcat_STAR_", { enumerable: false, get: function() { return cljs.core.async.mapcat_STAR_; } });
Object.defineProperty(module.exports, "__GT_t_cljs$core$async45755", { enumerable: false, get: function() { return cljs.core.async.__GT_t_cljs$core$async45755; } });
Object.defineProperty(module.exports, "mix", { enumerable: false, get: function() { return cljs.core.async.mix; } });
Object.defineProperty(module.exports, "pub", { enumerable: false, get: function() { return cljs.core.async.pub; } });
Object.defineProperty(module.exports, "take", { enumerable: false, get: function() { return cljs.core.async.take; } });
Object.defineProperty(module.exports, "unsub_all_STAR_", { enumerable: false, get: function() { return cljs.core.async.unsub_all_STAR_; } });
Object.defineProperty(module.exports, "_LT__BANG_", { enumerable: false, get: function() { return cljs.core.async._LT__BANG_; } });
Object.defineProperty(module.exports, "map", { enumerable: false, get: function() { return cljs.core.async.map; } });
Object.defineProperty(module.exports, "Mux", { enumerable: false, get: function() { return cljs.core.async.Mux; } });
Object.defineProperty(module.exports, "t_cljs$core$async48139", { enumerable: false, get: function() { return cljs.core.async.t_cljs$core$async48139; } });
Object.defineProperty(module.exports, "mapcat_GT_", { enumerable: false, get: function() { return cljs.core.async.mapcat_GT_; } });
Object.defineProperty(module.exports, "fhnop", { enumerable: false, get: function() { return cljs.core.async.fhnop; } });
Object.defineProperty(module.exports, "t_cljs$core$async47716", { enumerable: false, get: function() { return cljs.core.async.t_cljs$core$async47716; } });
Object.defineProperty(module.exports, "__GT_t_cljs$core$async48752", { enumerable: false, get: function() { return cljs.core.async.__GT_t_cljs$core$async48752; } });
Object.defineProperty(module.exports, "buffer", { enumerable: false, get: function() { return cljs.core.async.buffer; } });
Object.defineProperty(module.exports, "close_BANG_", { enumerable: false, get: function() { return cljs.core.async.close_BANG_; } });
Object.defineProperty(module.exports, "t_cljs$core$async48773", { enumerable: false, get: function() { return cljs.core.async.t_cljs$core$async48773; } });
Object.defineProperty(module.exports, "t_cljs$core$async45940", { enumerable: false, get: function() { return cljs.core.async.t_cljs$core$async45940; } });
Object.defineProperty(module.exports, "offer_BANG_", { enumerable: false, get: function() { return cljs.core.async.offer_BANG_; } });
Object.defineProperty(module.exports, "chan", { enumerable: false, get: function() { return cljs.core.async.chan; } });
Object.defineProperty(module.exports, "solo_mode_STAR_", { enumerable: false, get: function() { return cljs.core.async.solo_mode_STAR_; } });
Object.defineProperty(module.exports, "onto_chan_BANG_", { enumerable: false, get: function() { return cljs.core.async.onto_chan_BANG_; } });
Object.defineProperty(module.exports, "tap", { enumerable: false, get: function() { return cljs.core.async.tap; } });
Object.defineProperty(module.exports, "admix", { enumerable: false, get: function() { return cljs.core.async.admix; } });
Object.defineProperty(module.exports, "promise_chan", { enumerable: false, get: function() { return cljs.core.async.promise_chan; } });
Object.defineProperty(module.exports, "unique", { enumerable: false, get: function() { return cljs.core.async.unique; } });
Object.defineProperty(module.exports, "muxch_STAR_", { enumerable: false, get: function() { return cljs.core.async.muxch_STAR_; } });
Object.defineProperty(module.exports, "solo_mode", { enumerable: false, get: function() { return cljs.core.async.solo_mode; } });
Object.defineProperty(module.exports, "transduce", { enumerable: false, get: function() { return cljs.core.async.transduce; } });
Object.defineProperty(module.exports, "onto_chan", { enumerable: false, get: function() { return cljs.core.async.onto_chan; } });
Object.defineProperty(module.exports, "to_chan", { enumerable: false, get: function() { return cljs.core.async.to_chan; } });
Object.defineProperty(module.exports, "dropping_buffer", { enumerable: false, get: function() { return cljs.core.async.dropping_buffer; } });
Object.defineProperty(module.exports, "untap_all", { enumerable: false, get: function() { return cljs.core.async.untap_all; } });
Object.defineProperty(module.exports, "__GT_t_cljs$core$async45940", { enumerable: false, get: function() { return cljs.core.async.__GT_t_cljs$core$async45940; } });
Object.defineProperty(module.exports, "__GT_t_cljs$core$async48739", { enumerable: false, get: function() { return cljs.core.async.__GT_t_cljs$core$async48739; } });
Object.defineProperty(module.exports, "into", { enumerable: false, get: function() { return cljs.core.async.into; } });
Object.defineProperty(module.exports, "to_chan_BANG_", { enumerable: false, get: function() { return cljs.core.async.to_chan_BANG_; } });
Object.defineProperty(module.exports, "pipeline", { enumerable: false, get: function() { return cljs.core.async.pipeline; } });
Object.defineProperty(module.exports, "t_cljs$core$async45915", { enumerable: false, get: function() { return cljs.core.async.t_cljs$core$async45915; } });
Object.defineProperty(module.exports, "sub", { enumerable: false, get: function() { return cljs.core.async.sub; } });
Object.defineProperty(module.exports, "__GT_t_cljs$core$async48789", { enumerable: false, get: function() { return cljs.core.async.__GT_t_cljs$core$async48789; } });
Object.defineProperty(module.exports, "__GT_t_cljs$core$async47034", { enumerable: false, get: function() { return cljs.core.async.__GT_t_cljs$core$async47034; } });
Object.defineProperty(module.exports, "alt_flag", { enumerable: false, get: function() { return cljs.core.async.alt_flag; } });
Object.defineProperty(module.exports, "map_GT_", { enumerable: false, get: function() { return cljs.core.async.map_GT_; } });
Object.defineProperty(module.exports, "pipeline_STAR_", { enumerable: false, get: function() { return cljs.core.async.pipeline_STAR_; } });
Object.defineProperty(module.exports, "t_cljs$core$async48789", { enumerable: false, get: function() { return cljs.core.async.t_cljs$core$async48789; } });
Object.defineProperty(module.exports, "t_cljs$core$async48739", { enumerable: false, get: function() { return cljs.core.async.t_cljs$core$async48739; } });
Object.defineProperty(module.exports, "pipe", { enumerable: false, get: function() { return cljs.core.async.pipe; } });
Object.defineProperty(module.exports, "__GT_t_cljs$core$async48773", { enumerable: false, get: function() { return cljs.core.async.__GT_t_cljs$core$async48773; } });
Object.defineProperty(module.exports, "unmix", { enumerable: false, get: function() { return cljs.core.async.unmix; } });
Object.defineProperty(module.exports, "filter_LT_", { enumerable: false, get: function() { return cljs.core.async.filter_LT_; } });
Object.defineProperty(module.exports, "sub_STAR_", { enumerable: false, get: function() { return cljs.core.async.sub_STAR_; } });
Object.defineProperty(module.exports, "remove_LT_", { enumerable: false, get: function() { return cljs.core.async.remove_LT_; } });
Object.defineProperty(module.exports, "__GT_t_cljs$core$async48139", { enumerable: false, get: function() { return cljs.core.async.__GT_t_cljs$core$async48139; } });
Object.defineProperty(module.exports, "untap_STAR_", { enumerable: false, get: function() { return cljs.core.async.untap_STAR_; } });
Object.defineProperty(module.exports, "toggle", { enumerable: false, get: function() { return cljs.core.async.toggle; } });
Object.defineProperty(module.exports, "untap_all_STAR_", { enumerable: false, get: function() { return cljs.core.async.untap_all_STAR_; } });
Object.defineProperty(module.exports, "__GT_t_cljs$core$async47716", { enumerable: false, get: function() { return cljs.core.async.__GT_t_cljs$core$async47716; } });
Object.defineProperty(module.exports, "sliding_buffer", { enumerable: false, get: function() { return cljs.core.async.sliding_buffer; } });
Object.defineProperty(module.exports, "partition", { enumerable: false, get: function() { return cljs.core.async.partition; } });
Object.defineProperty(module.exports, "Mult", { enumerable: false, get: function() { return cljs.core.async.Mult; } });
Object.defineProperty(module.exports, "merge", { enumerable: false, get: function() { return cljs.core.async.merge; } });
Object.defineProperty(module.exports, "t_cljs$core$async45755", { enumerable: false, get: function() { return cljs.core.async.t_cljs$core$async45755; } });
Object.defineProperty(module.exports, "partition_by", { enumerable: false, get: function() { return cljs.core.async.partition_by; } });
Object.defineProperty(module.exports, "unsub_all", { enumerable: false, get: function() { return cljs.core.async.unsub_all; } });
Object.defineProperty(module.exports, "_GT__BANG_", { enumerable: false, get: function() { return cljs.core.async._GT__BANG_; } });
Object.defineProperty(module.exports, "unmix_all_STAR_", { enumerable: false, get: function() { return cljs.core.async.unmix_all_STAR_; } });
Object.defineProperty(module.exports, "nop", { enumerable: false, get: function() { return cljs.core.async.nop; } });
Object.defineProperty(module.exports, "split", { enumerable: false, get: function() { return cljs.core.async.split; } });
Object.defineProperty(module.exports, "__GT_t_cljs$core$async45915", { enumerable: false, get: function() { return cljs.core.async.__GT_t_cljs$core$async45915; } });
Object.defineProperty(module.exports, "unmix_all", { enumerable: false, get: function() { return cljs.core.async.unmix_all; } });
Object.defineProperty(module.exports, "filter_GT_", { enumerable: false, get: function() { return cljs.core.async.filter_GT_; } });
Object.defineProperty(module.exports, "tap_STAR_", { enumerable: false, get: function() { return cljs.core.async.tap_STAR_; } });
Object.defineProperty(module.exports, "untap", { enumerable: false, get: function() { return cljs.core.async.untap; } });
Object.defineProperty(module.exports, "alt_handler", { enumerable: false, get: function() { return cljs.core.async.alt_handler; } });
Object.defineProperty(module.exports, "alts_BANG_", { enumerable: false, get: function() { return cljs.core.async.alts_BANG_; } });
Object.defineProperty(module.exports, "unsub", { enumerable: false, get: function() { return cljs.core.async.unsub; } });
Object.defineProperty(module.exports, "t_cljs$core$async47034", { enumerable: false, get: function() { return cljs.core.async.t_cljs$core$async47034; } });
Object.defineProperty(module.exports, "poll_BANG_", { enumerable: false, get: function() { return cljs.core.async.poll_BANG_; } });
Object.defineProperty(module.exports, "map_LT_", { enumerable: false, get: function() { return cljs.core.async.map_LT_; } });
Object.defineProperty(module.exports, "fn_handler", { enumerable: false, get: function() { return cljs.core.async.fn_handler; } });
Object.defineProperty(module.exports, "do_alts", { enumerable: false, get: function() { return cljs.core.async.do_alts; } });
Object.defineProperty(module.exports, "random_array", { enumerable: false, get: function() { return cljs.core.async.random_array; } });
Object.defineProperty(module.exports, "pipeline_async", { enumerable: false, get: function() { return cljs.core.async.pipeline_async; } });
Object.defineProperty(module.exports, "Mix", { enumerable: false, get: function() { return cljs.core.async.Mix; } });
Object.defineProperty(module.exports, "toggle_STAR_", { enumerable: false, get: function() { return cljs.core.async.toggle_STAR_; } });
Object.defineProperty(module.exports, "mult", { enumerable: false, get: function() { return cljs.core.async.mult; } });
Object.defineProperty(module.exports, "mapcat_LT_", { enumerable: false, get: function() { return cljs.core.async.mapcat_LT_; } });
Object.defineProperty(module.exports, "ioc_alts_BANG_", { enumerable: false, get: function() { return cljs.core.async.ioc_alts_BANG_; } });
Object.defineProperty(module.exports, "t_cljs$core$async48752", { enumerable: false, get: function() { return cljs.core.async.t_cljs$core$async48752; } });
Object.defineProperty(module.exports, "unblocking_buffer_QMARK_", { enumerable: false, get: function() { return cljs.core.async.unblocking_buffer_QMARK_; } });
Object.defineProperty(module.exports, "put_BANG_", { enumerable: false, get: function() { return cljs.core.async.put_BANG_; } });
Object.defineProperty(module.exports, "take_BANG_", { enumerable: false, get: function() { return cljs.core.async.take_BANG_; } });
//# sourceMappingURL=cljs.core.async.js.map

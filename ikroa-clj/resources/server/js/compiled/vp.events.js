var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./re_frame.core.js");
require("./vp.db.js");
require("./vp.util.js");
require("./cljs.core.async.js");
require("./vp.macros.js");
require("./vp.toastr.js");
require("./vp.swal.js");
require("./vp.routes.js");
require("./shadow.js.shim.module$meteor$accounts_base.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("vp.events.js");

goog.provide('vp.events');
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("vp.events","initialize-db","vp.events/initialize-db",204435043),(function (_,___$1){
return vp.db.default_db;
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),(function (db,p__73844){
var vec__73845 = p__73844;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__73845,(0),null);
var active_panel = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__73845,(1),null);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"active-panel","active-panel",-1802545994),active_panel);
}));
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"toastr","toastr",696071399),(function (p__73858,p__73859){
var map__73865 = p__73858;
var map__73865__$1 = (((((!((map__73865 == null))))?(((((map__73865.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__73865.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__73865):map__73865);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__73865__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__73867 = p__73859;
var seq__73868 = cljs.core.seq(vec__73867);
var first__73869 = cljs.core.first(seq__73868);
var seq__73868__$1 = cljs.core.next(seq__73868);
var _ = first__73869;
var first__73869__$1 = cljs.core.first(seq__73868__$1);
var seq__73868__$2 = cljs.core.next(seq__73868__$1);
var k = first__73869__$1;
var args = seq__73868__$2;
cljs.core.apply.cljs$core$IFn$_invoke$arity$2((vp.toastr.keyword__GT_fn.cljs$core$IFn$_invoke$arity$1 ? vp.toastr.keyword__GT_fn.cljs$core$IFn$_invoke$arity$1(k) : vp.toastr.keyword__GT_fn.call(null,k)),args);

return cljs.core.PersistentArrayMap.EMPTY;
}));
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"alert","alert",-571950580),(function (p__73928,p__73929){
var map__73930 = p__73928;
var map__73930__$1 = (((((!((map__73930 == null))))?(((((map__73930.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__73930.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__73930):map__73930);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__73930__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__73931 = p__73929;
var seq__73932 = cljs.core.seq(vec__73931);
var first__73933 = cljs.core.first(seq__73932);
var seq__73932__$1 = cljs.core.next(seq__73932);
var _ = first__73933;
var args = seq__73932__$1;
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.count(args),(2))){
var vec__73991_74347 = args;
var icon_74348 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__73991_74347,(0),null);
var text_74349 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__73991_74347,(1),null);
vp.swal.fire(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"icon","icon",1679606541),cljs.core.name(icon_74348),new cljs.core.Keyword(null,"text","text",-1790561697),text_74349], null));
} else {
vp.swal.fire(cljs.core.first(args));
}

return cljs.core.PersistentArrayMap.EMPTY;
}));
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"confirm","confirm",-2004000608),(function (p__74017,p__74018){
var map__74039 = p__74017;
var map__74039__$1 = (((((!((map__74039 == null))))?(((((map__74039.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74039.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74039):map__74039);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74039__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__74040 = p__74018;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74040,(0),null);
var args = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74040,(1),null);
var cb = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74040,(2),null);
vp.swal.confirm(args,cb);

return cljs.core.PersistentArrayMap.EMPTY;
}));
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"redirect","redirect",-1975673286),(function (p__74087,p__74088){
var map__74098 = p__74087;
var map__74098__$1 = (((((!((map__74098 == null))))?(((((map__74098.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74098.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74098):map__74098);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74098__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__74102 = p__74088;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74102,(0),null);
var url = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74102,(1),null);
vp.routes.goto$(url);

return cljs.core.PersistentArrayMap.EMPTY;
}));
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"set-user","set-user",444398487),(function (p__74137,p__74139){
var map__74144 = p__74137;
var map__74144__$1 = (((((!((map__74144 == null))))?(((((map__74144.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74144.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74144):map__74144);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74144__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__74145 = p__74139;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74145,(0),null);
var user = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74145,(1),null);
Sentry.setUser(cljs.core.clj__GT_js(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"email","email",1415816706),new cljs.core.Keyword(null,"email","email",1415816706).cljs$core$IFn$_invoke$arity$1(user)], null)));

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"db","db",993250759),cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"user","user",1532431356),user),new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"get-init-info","get-init-info",2011244391)], null)], null)], null);
}));
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"logout","logout",1418564329),(function (p__74190,p__74191){
var map__74201 = p__74190;
var map__74201__$1 = (((((!((map__74201 == null))))?(((((map__74201.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74201.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74201):map__74201);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74201__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__74202 = p__74191;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74202,(0),null);
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"db","db",993250759),cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(db,new cljs.core.Keyword(null,"init-info","init-info",-1290973133))], null);
}));
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"init","init",-1875481434),(function (p__74247,p__74248){
var map__74249 = p__74247;
var map__74249__$1 = (((((!((map__74249 == null))))?(((((map__74249.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74249.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74249):map__74249);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74249__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__74250 = p__74248;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74250,(0),null);
shadow.js.shim.module$meteor$accounts_base.Accounts.onLogin((function (){
return re_frame.core.dispatch(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"toastr","toastr",696071399),new cljs.core.Keyword(null,"success","success",1890645906),"Welcome!"], null));
}));

return cljs.core.PersistentArrayMap.EMPTY;
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"set-db","set-db",-547855905),(function (db,p__74261){
var vec__74262 = p__74261;
var seq__74263 = cljs.core.seq(vec__74262);
var first__74264 = cljs.core.first(seq__74263);
var seq__74263__$1 = cljs.core.next(seq__74263);
var _ = first__74264;
var path_n_value = seq__74263__$1;
return cljs.core.assoc_in(db,cljs.core.butlast(path_n_value),cljs.core.last(path_n_value));
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"swap-db","swap-db",-1033930255),(function (db,p__74266){
var vec__74270 = p__74266;
var seq__74271 = cljs.core.seq(vec__74270);
var first__74272 = cljs.core.first(seq__74271);
var seq__74271__$1 = cljs.core.next(seq__74271);
var _ = first__74272;
var first__74272__$1 = cljs.core.first(seq__74271__$1);
var seq__74271__$2 = cljs.core.next(seq__74271__$1);
var path = first__74272__$1;
var args = seq__74271__$2;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$4(cljs.core.update_in,db,path,args);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"update-in-db","update-in-db",1790429121),(function (db,p__74282){
var vec__74286 = p__74282;
var seq__74287 = cljs.core.seq(vec__74286);
var first__74288 = cljs.core.first(seq__74287);
var seq__74287__$1 = cljs.core.next(seq__74287);
var _ = first__74288;
var args = seq__74287__$1;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(cljs.core.update_in,db,args);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"dissoc-db","dissoc-db",1603600317),(function (db,p__74289){
var vec__74291 = p__74289;
var seq__74292 = cljs.core.seq(vec__74291);
var first__74293 = cljs.core.first(seq__74292);
var seq__74292__$1 = cljs.core.next(seq__74292);
var _ = first__74293;
var path = seq__74292__$1;
return vp.util.dissoc_in(db,path);
}));
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"login-redirect","login-redirect",-2132232884),(function (p__74297,p__74298){
var map__74300 = p__74297;
var map__74300__$1 = (((((!((map__74300 == null))))?(((((map__74300.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74300.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74300):map__74300);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74300__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__74301 = p__74298;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74301,(0),null);
var after_login_url = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74301,(1),null);
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"redirect","redirect",-1975673286),vp.routes.home_page()], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"toastr","toastr",696071399),new cljs.core.Keyword(null,"info","info",-317069002),"Please login first"], null),(cljs.core.truth_(after_login_url)?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-db","set-db",-547855905),new cljs.core.Keyword(null,"after-login-url","after-login-url",-1521298820),after_login_url], null):null)], null)], null);
}));
re_frame.core.reg_event_fx.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"login","login",55217519),(function (p__74316,p__74317){
var map__74318 = p__74316;
var map__74318__$1 = (((((!((map__74318 == null))))?(((((map__74318.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__74318.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__74318):map__74318);
var db = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__74318__$1,new cljs.core.Keyword(null,"db","db",993250759));
var vec__74319 = p__74317;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74319,(0),null);
var values = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74319,(1),null);
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"db","db",993250759),cljs.core.update.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"after-login-url","after-login-url",-1521298820),(function (p1__74310_SHARP_){
var or__4126__auto__ = p1__74310_SHARP_;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return vp.routes.home_page();
}
}))], null);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"show-modal","show-modal",-11429385),(function (db,p__74331){
var vec__74332 = p__74331;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74332,(0),null);
var cmp = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74332,(1),null);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(db,new cljs.core.Keyword(null,"modal-cmp","modal-cmp",-699741929),cmp);
}));
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"close-modal","close-modal",-1882189985),(function (db,p__74335){
var vec__74336 = p__74335;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74336,(0),null);
var cmp = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__74336,(1),null);
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(db,new cljs.core.Keyword(null,"modal-cmp","modal-cmp",-699741929));
}));

//# sourceMappingURL=vp.events.js.map

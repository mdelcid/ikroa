var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./goog.promise.promise.js");
require("./goog.asserts.asserts.js");
require("./goog.debug.error.js");
require("./goog.net.httpstatus.js");
require("./goog.net.xmlhttp.js");
require("./goog.object.object.js");
require("./goog.string.string.js");
require("./goog.uri.utils.js");
require("./goog.useragent.useragent.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("goog.labs.net.xhr.js");

goog.provide("goog.labs.net.xhr");
goog.provide("goog.labs.net.xhr.Error");
goog.provide("goog.labs.net.xhr.HttpError");
goog.provide("goog.labs.net.xhr.Options");
goog.provide("goog.labs.net.xhr.PostData");
goog.provide("goog.labs.net.xhr.ResponseType");
goog.provide("goog.labs.net.xhr.TimeoutError");
goog.require("goog.Promise");
goog.require("goog.asserts");
goog.require("goog.debug.Error");
goog.require("goog.net.HttpStatus");
goog.require("goog.net.XmlHttp");
goog.require("goog.object");
goog.require("goog.string");
goog.require("goog.uri.utils");
goog.require("goog.userAgent");
goog.scope(function() {
  var userAgent = goog.userAgent;
  var xhr = goog.labs.net.xhr;
  var HttpStatus = goog.net.HttpStatus;
  xhr.Options;
  xhr.PostData;
  xhr.CONTENT_TYPE_HEADER = "Content-Type";
  xhr.FORM_CONTENT_TYPE = "application/x-www-form-urlencoded;charset\x3dutf-8";
  xhr.ResponseType = {ARRAYBUFFER:"arraybuffer", BLOB:"blob", DOCUMENT:"document", JSON:"json", TEXT:"text"};
  xhr.get = function(url, opt_options) {
    return xhr.send("GET", url, null, opt_options).then(function(request) {
      return request.responseText;
    });
  };
  xhr.post = function(url, data, opt_options) {
    return xhr.send("POST", url, data, opt_options).then(function(request) {
      return request.responseText;
    });
  };
  xhr.getJson = function(url, opt_options) {
    return xhr.send("GET", url, null, opt_options).then(function(request) {
      return xhr.parseJson_(request.responseText, opt_options);
    });
  };
  xhr.getBlob = function(url, opt_options) {
    goog.asserts.assert("Blob" in goog.global, "getBlob is not supported in this browser.");
    var options = opt_options ? goog.object.clone(opt_options) : {};
    options.responseType = xhr.ResponseType.BLOB;
    return xhr.send("GET", url, null, options).then(function(request) {
      return request.response;
    });
  };
  xhr.getBytes = function(url, opt_options) {
    goog.asserts.assert(!userAgent.IE || userAgent.isDocumentModeOrHigher(9), "getBytes is not supported in this browser.");
    var options = opt_options ? goog.object.clone(opt_options) : {};
    options.responseType = xhr.ResponseType.ARRAYBUFFER;
    return xhr.send("GET", url, null, options).then(function(request) {
      if (request.response) {
        return new Uint8Array(request.response);
      }
      if (goog.global["VBArray"]) {
        return (new goog.global["VBArray"](request["responseBody"])).toArray();
      }
      throw new xhr.Error("getBytes is not supported in this browser.", url, request);
    });
  };
  xhr.postJson = function(url, data, opt_options) {
    return xhr.send("POST", url, data, opt_options).then(function(request) {
      return xhr.parseJson_(request.responseText, opt_options);
    });
  };
  xhr.send = function(method, url, data, opt_options) {
    var options = opt_options || {};
    var request = options.xmlHttpFactory ? options.xmlHttpFactory.createInstance() : goog.net.XmlHttp();
    var result = new goog.Promise(function(resolve, reject) {
      var timer;
      try {
        request.open(method, url, true);
      } catch (e) {
        reject(new xhr.Error("Error opening XHR: " + e.message, url, request));
      }
      request.onreadystatechange = function() {
        if (request.readyState == goog.net.XmlHttp.ReadyState.COMPLETE) {
          goog.global.clearTimeout(timer);
          if (HttpStatus.isSuccess(request.status) || request.status === 0 && !xhr.isEffectiveSchemeHttp_(url)) {
            resolve(request);
          } else {
            reject(new xhr.HttpError(request.status, url, request));
          }
        }
      };
      request.onerror = function() {
        reject(new xhr.Error("Network error", url, request));
      };
      var contentType;
      if (options.headers) {
        for (var key in options.headers) {
          var value = options.headers[key];
          if (value != null) {
            request.setRequestHeader(key, value);
          }
        }
        contentType = options.headers[xhr.CONTENT_TYPE_HEADER];
      }
      var dataIsFormData = goog.global["FormData"] && data instanceof goog.global["FormData"];
      if (method == "POST" && contentType === undefined && !dataIsFormData) {
        request.setRequestHeader(xhr.CONTENT_TYPE_HEADER, xhr.FORM_CONTENT_TYPE);
      }
      if (options.withCredentials) {
        request.withCredentials = options.withCredentials;
      }
      if (options.responseType) {
        request.responseType = options.responseType;
      }
      if (options.mimeType) {
        request.overrideMimeType(options.mimeType);
      }
      if (options.timeoutMs > 0) {
        timer = goog.global.setTimeout(function() {
          request.onreadystatechange = goog.nullFunction;
          request.abort();
          reject(new xhr.TimeoutError(url, request));
        }, options.timeoutMs);
      }
      try {
        request.send(data);
      } catch (e$3) {
        request.onreadystatechange = goog.nullFunction;
        goog.global.clearTimeout(timer);
        reject(new xhr.Error("Error sending XHR: " + e$3.message, url, request));
      }
    });
    return result.thenCatch(function(error) {
      if (error instanceof goog.Promise.CancellationError) {
        request.abort();
      }
      throw error;
    });
  };
  xhr.isEffectiveSchemeHttp_ = function(url) {
    var scheme = goog.uri.utils.getEffectiveScheme(url);
    return scheme == "http" || scheme == "https" || scheme == "";
  };
  xhr.parseJson = function(responseText, opt_xssiPrefix) {
    return xhr.parseJson_(responseText, {xssiPrefix:opt_xssiPrefix});
  };
  xhr.parseJson_ = function(responseText, options) {
    var prefixStrippedResult = responseText;
    if (options && options.xssiPrefix) {
      prefixStrippedResult = xhr.stripXssiPrefix_(options.xssiPrefix, prefixStrippedResult);
    }
    return JSON.parse(prefixStrippedResult);
  };
  xhr.stripXssiPrefix_ = function(prefix, string) {
    if (goog.string.startsWith(string, prefix)) {
      string = string.substring(prefix.length);
    }
    return string;
  };
  xhr.Error = function(message, url, request) {
    xhr.Error.base(this, "constructor", message + ", url\x3d" + url);
    this.url = url;
    this.xhr = request;
  };
  goog.inherits(xhr.Error, goog.debug.Error);
  xhr.Error.prototype.name = "XhrError";
  xhr.HttpError = function(status, url, request) {
    xhr.HttpError.base(this, "constructor", "Request Failed, status\x3d" + status, url, request);
    this.status = status;
  };
  goog.inherits(xhr.HttpError, xhr.Error);
  xhr.HttpError.prototype.name = "XhrHttpError";
  xhr.TimeoutError = function(url, request) {
    xhr.TimeoutError.base(this, "constructor", "Request timed out", url, request);
  };
  goog.inherits(xhr.TimeoutError, xhr.Error);
  xhr.TimeoutError.prototype.name = "XhrTimeoutError";
});

module.exports = goog.labs.net.xhr;

//# sourceMappingURL=goog.labs.net.xhr.js.map

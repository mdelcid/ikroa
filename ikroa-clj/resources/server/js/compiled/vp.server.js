var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./vp.config.js");
require("./shadow.js.shim.module$meteor$mongo.js");
require("./shadow.js.shim.module$meteor$meteor.js");
require("./shadow.js.shim.module$meteor$tracker.js");
require("./cljs.core.async.js");
require("./shadow.js.shim.module$meteor$fetch.js");
require("./cljs.core.async.interop.js");
require("./vp.collections.js");
require("./vp.services.js");
require("./vp.macros.js");
require("./vp.meteor.js");
require("./vp.rapyd.js");
require("./camel_snake_kebab.core.js");
require("./camel_snake_kebab.extras.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var camel_snake_kebab=$CLJS.camel_snake_kebab || ($CLJS.camel_snake_kebab = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("vp.server.js");

goog.provide('vp.server');
vp.server.init = (function vp$server$init(){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["init server"], 0));

if((vp.meteor.cnt.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"countries","countries",863192750),cljs.core.PersistentArrayMap.EMPTY) === (0))){
var c__41437__auto___76269 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_75940){
var state_val_75941 = (state_75940[(1)]);
if((state_val_75941 === (7))){
var inst_75936 = (state_75940[(2)]);
var state_75940__$1 = state_75940;
var statearr_75942_76270 = state_75940__$1;
(statearr_75942_76270[(2)] = inst_75936);

(statearr_75942_76270[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_75941 === (1))){
var inst_75895 = vp.rapyd.get_countries();
var state_75940__$1 = state_75940;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_75940__$1,(2),inst_75895);
} else {
if((state_val_75941 === (4))){
var inst_75938 = (state_75940[(2)]);
var state_75940__$1 = state_75940;
return cljs.core.async.impl.ioc_helpers.return_chan(state_75940__$1,inst_75938);
} else {
if((state_val_75941 === (13))){
var inst_75931 = (state_75940[(2)]);
var state_75940__$1 = state_75940;
var statearr_75943_76271 = state_75940__$1;
(statearr_75943_76271[(2)] = inst_75931);

(statearr_75943_76271[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_75941 === (6))){
var inst_75917 = (state_75940[(7)]);
var inst_75904 = (state_75940[(8)]);
var inst_75917__$1 = cljs.core.seq(inst_75904);
var state_75940__$1 = (function (){var statearr_75944 = state_75940;
(statearr_75944[(7)] = inst_75917__$1);

return statearr_75944;
})();
if(inst_75917__$1){
var statearr_75945_76272 = state_75940__$1;
(statearr_75945_76272[(1)] = (8));

} else {
var statearr_75946_76273 = state_75940__$1;
(statearr_75946_76273[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_75941 === (3))){
var inst_75906 = (state_75940[(9)]);
var inst_75907 = (state_75940[(10)]);
var inst_75909 = (inst_75907 < inst_75906);
var inst_75910 = inst_75909;
var state_75940__$1 = state_75940;
if(cljs.core.truth_(inst_75910)){
var statearr_75947_76274 = state_75940__$1;
(statearr_75947_76274[(1)] = (5));

} else {
var statearr_75948_76275 = state_75940__$1;
(statearr_75948_76275[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_75941 === (12))){
var inst_75917 = (state_75940[(7)]);
var inst_75926 = cljs.core.first(inst_75917);
var inst_75927 = vp.meteor.cre(new cljs.core.Keyword(null,"countries","countries",863192750),inst_75926);
var inst_75928 = cljs.core.next(inst_75917);
var inst_75904 = inst_75928;
var inst_75905 = null;
var inst_75906 = (0);
var inst_75907 = (0);
var state_75940__$1 = (function (){var statearr_75949 = state_75940;
(statearr_75949[(9)] = inst_75906);

(statearr_75949[(10)] = inst_75907);

(statearr_75949[(8)] = inst_75904);

(statearr_75949[(11)] = inst_75927);

(statearr_75949[(12)] = inst_75905);

return statearr_75949;
})();
var statearr_75950_76276 = state_75940__$1;
(statearr_75950_76276[(2)] = null);

(statearr_75950_76276[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_75941 === (2))){
var inst_75897 = (state_75940[(2)]);
var inst_75898 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(inst_75897);
var inst_75903 = cljs.core.seq(inst_75898);
var inst_75904 = inst_75903;
var inst_75905 = null;
var inst_75906 = (0);
var inst_75907 = (0);
var state_75940__$1 = (function (){var statearr_75951 = state_75940;
(statearr_75951[(9)] = inst_75906);

(statearr_75951[(10)] = inst_75907);

(statearr_75951[(8)] = inst_75904);

(statearr_75951[(12)] = inst_75905);

return statearr_75951;
})();
var statearr_75952_76277 = state_75940__$1;
(statearr_75952_76277[(2)] = null);

(statearr_75952_76277[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_75941 === (11))){
var inst_75917 = (state_75940[(7)]);
var inst_75921 = cljs.core.chunk_first(inst_75917);
var inst_75922 = cljs.core.chunk_rest(inst_75917);
var inst_75923 = cljs.core.count(inst_75921);
var inst_75904 = inst_75922;
var inst_75905 = inst_75921;
var inst_75906 = inst_75923;
var inst_75907 = (0);
var state_75940__$1 = (function (){var statearr_75956 = state_75940;
(statearr_75956[(9)] = inst_75906);

(statearr_75956[(10)] = inst_75907);

(statearr_75956[(8)] = inst_75904);

(statearr_75956[(12)] = inst_75905);

return statearr_75956;
})();
var statearr_75957_76278 = state_75940__$1;
(statearr_75957_76278[(2)] = null);

(statearr_75957_76278[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_75941 === (9))){
var state_75940__$1 = state_75940;
var statearr_75958_76279 = state_75940__$1;
(statearr_75958_76279[(2)] = null);

(statearr_75958_76279[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_75941 === (5))){
var inst_75906 = (state_75940[(9)]);
var inst_75907 = (state_75940[(10)]);
var inst_75904 = (state_75940[(8)]);
var inst_75905 = (state_75940[(12)]);
var inst_75912 = cljs.core._nth(inst_75905,inst_75907);
var inst_75913 = vp.meteor.cre(new cljs.core.Keyword(null,"countries","countries",863192750),inst_75912);
var inst_75914 = (inst_75907 + (1));
var tmp75953 = inst_75906;
var tmp75954 = inst_75904;
var tmp75955 = inst_75905;
var inst_75904__$1 = tmp75954;
var inst_75905__$1 = tmp75955;
var inst_75906__$1 = tmp75953;
var inst_75907__$1 = inst_75914;
var state_75940__$1 = (function (){var statearr_75959 = state_75940;
(statearr_75959[(9)] = inst_75906__$1);

(statearr_75959[(10)] = inst_75907__$1);

(statearr_75959[(8)] = inst_75904__$1);

(statearr_75959[(13)] = inst_75913);

(statearr_75959[(12)] = inst_75905__$1);

return statearr_75959;
})();
var statearr_75960_76280 = state_75940__$1;
(statearr_75960_76280[(2)] = null);

(statearr_75960_76280[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_75941 === (10))){
var inst_75934 = (state_75940[(2)]);
var state_75940__$1 = state_75940;
var statearr_75961_76281 = state_75940__$1;
(statearr_75961_76281[(2)] = inst_75934);

(statearr_75961_76281[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_75941 === (8))){
var inst_75917 = (state_75940[(7)]);
var inst_75919 = cljs.core.chunked_seq_QMARK_(inst_75917);
var state_75940__$1 = state_75940;
if(inst_75919){
var statearr_75962_76282 = state_75940__$1;
(statearr_75962_76282[(1)] = (11));

} else {
var statearr_75963_76283 = state_75940__$1;
(statearr_75963_76283[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var vp$server$init_$_state_machine__41292__auto__ = null;
var vp$server$init_$_state_machine__41292__auto____0 = (function (){
var statearr_75964 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_75964[(0)] = vp$server$init_$_state_machine__41292__auto__);

(statearr_75964[(1)] = (1));

return statearr_75964;
});
var vp$server$init_$_state_machine__41292__auto____1 = (function (state_75940){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_75940);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e75965){var ex__41295__auto__ = e75965;
var statearr_75966_76284 = state_75940;
(statearr_75966_76284[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_75940[(4)]))){
var statearr_75967_76285 = state_75940;
(statearr_75967_76285[(1)] = cljs.core.first((state_75940[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__76286 = state_75940;
state_75940 = G__76286;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$server$init_$_state_machine__41292__auto__ = function(state_75940){
switch(arguments.length){
case 0:
return vp$server$init_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$server$init_$_state_machine__41292__auto____1.call(this,state_75940);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$server$init_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$server$init_$_state_machine__41292__auto____0;
vp$server$init_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$server$init_$_state_machine__41292__auto____1;
return vp$server$init_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_75968 = f__41438__auto__();
(statearr_75968[(6)] = c__41437__auto___76269);

return statearr_75968;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

} else {
}

if((vp.meteor.cnt.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"currencies","currencies",2003336181),cljs.core.PersistentArrayMap.EMPTY) === (0))){
var c__41437__auto___76287 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_76014){
var state_val_76015 = (state_76014[(1)]);
if((state_val_76015 === (7))){
var inst_76010 = (state_76014[(2)]);
var state_76014__$1 = state_76014;
var statearr_76016_76288 = state_76014__$1;
(statearr_76016_76288[(2)] = inst_76010);

(statearr_76016_76288[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76015 === (1))){
var inst_75969 = vp.rapyd.get_currencies();
var state_76014__$1 = state_76014;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_76014__$1,(2),inst_75969);
} else {
if((state_val_76015 === (4))){
var inst_76012 = (state_76014[(2)]);
var state_76014__$1 = state_76014;
return cljs.core.async.impl.ioc_helpers.return_chan(state_76014__$1,inst_76012);
} else {
if((state_val_76015 === (13))){
var inst_76005 = (state_76014[(2)]);
var state_76014__$1 = state_76014;
var statearr_76017_76289 = state_76014__$1;
(statearr_76017_76289[(2)] = inst_76005);

(statearr_76017_76289[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76015 === (6))){
var inst_75991 = (state_76014[(7)]);
var inst_75978 = (state_76014[(8)]);
var inst_75991__$1 = cljs.core.seq(inst_75978);
var state_76014__$1 = (function (){var statearr_76018 = state_76014;
(statearr_76018[(7)] = inst_75991__$1);

return statearr_76018;
})();
if(inst_75991__$1){
var statearr_76019_76290 = state_76014__$1;
(statearr_76019_76290[(1)] = (8));

} else {
var statearr_76020_76291 = state_76014__$1;
(statearr_76020_76291[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76015 === (3))){
var inst_75981 = (state_76014[(9)]);
var inst_75980 = (state_76014[(10)]);
var inst_75983 = (inst_75981 < inst_75980);
var inst_75984 = inst_75983;
var state_76014__$1 = state_76014;
if(cljs.core.truth_(inst_75984)){
var statearr_76021_76292 = state_76014__$1;
(statearr_76021_76292[(1)] = (5));

} else {
var statearr_76022_76293 = state_76014__$1;
(statearr_76022_76293[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76015 === (12))){
var inst_75991 = (state_76014[(7)]);
var inst_76000 = cljs.core.first(inst_75991);
var inst_76001 = vp.meteor.cre(new cljs.core.Keyword(null,"currencies","currencies",2003336181),inst_76000);
var inst_76002 = cljs.core.next(inst_75991);
var inst_75978 = inst_76002;
var inst_75979 = null;
var inst_75980 = (0);
var inst_75981 = (0);
var state_76014__$1 = (function (){var statearr_76023 = state_76014;
(statearr_76023[(11)] = inst_75979);

(statearr_76023[(9)] = inst_75981);

(statearr_76023[(12)] = inst_76001);

(statearr_76023[(10)] = inst_75980);

(statearr_76023[(8)] = inst_75978);

return statearr_76023;
})();
var statearr_76024_76294 = state_76014__$1;
(statearr_76024_76294[(2)] = null);

(statearr_76024_76294[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76015 === (2))){
var inst_75971 = (state_76014[(2)]);
var inst_75972 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(inst_75971);
var inst_75977 = cljs.core.seq(inst_75972);
var inst_75978 = inst_75977;
var inst_75979 = null;
var inst_75980 = (0);
var inst_75981 = (0);
var state_76014__$1 = (function (){var statearr_76025 = state_76014;
(statearr_76025[(11)] = inst_75979);

(statearr_76025[(9)] = inst_75981);

(statearr_76025[(10)] = inst_75980);

(statearr_76025[(8)] = inst_75978);

return statearr_76025;
})();
var statearr_76026_76295 = state_76014__$1;
(statearr_76026_76295[(2)] = null);

(statearr_76026_76295[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76015 === (11))){
var inst_75991 = (state_76014[(7)]);
var inst_75995 = cljs.core.chunk_first(inst_75991);
var inst_75996 = cljs.core.chunk_rest(inst_75991);
var inst_75997 = cljs.core.count(inst_75995);
var inst_75978 = inst_75996;
var inst_75979 = inst_75995;
var inst_75980 = inst_75997;
var inst_75981 = (0);
var state_76014__$1 = (function (){var statearr_76030 = state_76014;
(statearr_76030[(11)] = inst_75979);

(statearr_76030[(9)] = inst_75981);

(statearr_76030[(10)] = inst_75980);

(statearr_76030[(8)] = inst_75978);

return statearr_76030;
})();
var statearr_76031_76296 = state_76014__$1;
(statearr_76031_76296[(2)] = null);

(statearr_76031_76296[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76015 === (9))){
var state_76014__$1 = state_76014;
var statearr_76032_76297 = state_76014__$1;
(statearr_76032_76297[(2)] = null);

(statearr_76032_76297[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76015 === (5))){
var inst_75979 = (state_76014[(11)]);
var inst_75981 = (state_76014[(9)]);
var inst_75980 = (state_76014[(10)]);
var inst_75978 = (state_76014[(8)]);
var inst_75986 = cljs.core._nth(inst_75979,inst_75981);
var inst_75987 = vp.meteor.cre(new cljs.core.Keyword(null,"currencies","currencies",2003336181),inst_75986);
var inst_75988 = (inst_75981 + (1));
var tmp76027 = inst_75979;
var tmp76028 = inst_75980;
var tmp76029 = inst_75978;
var inst_75978__$1 = tmp76029;
var inst_75979__$1 = tmp76027;
var inst_75980__$1 = tmp76028;
var inst_75981__$1 = inst_75988;
var state_76014__$1 = (function (){var statearr_76033 = state_76014;
(statearr_76033[(13)] = inst_75987);

(statearr_76033[(11)] = inst_75979__$1);

(statearr_76033[(9)] = inst_75981__$1);

(statearr_76033[(10)] = inst_75980__$1);

(statearr_76033[(8)] = inst_75978__$1);

return statearr_76033;
})();
var statearr_76034_76298 = state_76014__$1;
(statearr_76034_76298[(2)] = null);

(statearr_76034_76298[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76015 === (10))){
var inst_76008 = (state_76014[(2)]);
var state_76014__$1 = state_76014;
var statearr_76035_76299 = state_76014__$1;
(statearr_76035_76299[(2)] = inst_76008);

(statearr_76035_76299[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76015 === (8))){
var inst_75991 = (state_76014[(7)]);
var inst_75993 = cljs.core.chunked_seq_QMARK_(inst_75991);
var state_76014__$1 = state_76014;
if(inst_75993){
var statearr_76036_76300 = state_76014__$1;
(statearr_76036_76300[(1)] = (11));

} else {
var statearr_76037_76301 = state_76014__$1;
(statearr_76037_76301[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var vp$server$init_$_state_machine__41292__auto__ = null;
var vp$server$init_$_state_machine__41292__auto____0 = (function (){
var statearr_76038 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_76038[(0)] = vp$server$init_$_state_machine__41292__auto__);

(statearr_76038[(1)] = (1));

return statearr_76038;
});
var vp$server$init_$_state_machine__41292__auto____1 = (function (state_76014){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_76014);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e76039){var ex__41295__auto__ = e76039;
var statearr_76040_76302 = state_76014;
(statearr_76040_76302[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_76014[(4)]))){
var statearr_76041_76303 = state_76014;
(statearr_76041_76303[(1)] = cljs.core.first((state_76014[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__76304 = state_76014;
state_76014 = G__76304;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$server$init_$_state_machine__41292__auto__ = function(state_76014){
switch(arguments.length){
case 0:
return vp$server$init_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$server$init_$_state_machine__41292__auto____1.call(this,state_76014);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$server$init_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$server$init_$_state_machine__41292__auto____0;
vp$server$init_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$server$init_$_state_machine__41292__auto____1;
return vp$server$init_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_76042 = f__41438__auto__();
(statearr_76042[(6)] = c__41437__auto___76287);

return statearr_76042;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

} else {
}

vp.meteor.get_coll(new cljs.core.Keyword(null,"users","users",-713552705)).after.update((function (user_id,doc_aux){
var doc = cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(doc_aux,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",1310784252),true], 0));
if(cljs.core.truth_((function (){var and__4115__auto__ = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(doc,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"profile","profile",-545963874),new cljs.core.Keyword(null,"is-company","is-company",-388060704)], null));
if(cljs.core.truth_(and__4115__auto__)){
return (!(cljs.core.contains_QMARK_(doc,new cljs.core.Keyword(null,"company-wallet","company-wallet",-1325172306))));
} else {
return and__4115__auto__;
}
})())){
var user = doc;
var company = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(user,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"profile","profile",-545963874),new cljs.core.Keyword(null,"company","company",-340475075)], null));
var data = cljs.core.assoc_in(cljs.core.assoc_in(new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"first-name","first-name",-1559982131),new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(company),new cljs.core.Keyword(null,"ewallet-reference-id","ewallet-reference-id",-242385872),["meteor_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(user))].join(''),new cljs.core.Keyword(null,"type","type",1174270348),"company",new cljs.core.Keyword(null,"contact","contact",609093372),new cljs.core.Keyword(null,"contact","contact",609093372).cljs$core$IFn$_invoke$arity$1(company)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"contact","contact",609093372),new cljs.core.Keyword(null,"contact-type","contact-type",-1047584875)], null),"business"),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"contact","contact",609093372),new cljs.core.Keyword(null,"business-details","business-details",-883462743),new cljs.core.Keyword(null,"industry-category","industry-category",-1199582178)], null),"company");
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_76056){
var state_val_76057 = (state_76056[(1)]);
if((state_val_76057 === (1))){
var inst_76043 = camel_snake_kebab.extras.transform_keys(camel_snake_kebab.core.__GT_snake_case_string,data);
var inst_76044 = vp.rapyd.create_company_wallet(inst_76043);
var state_76056__$1 = state_76056;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_76056__$1,(2),inst_76044);
} else {
if((state_val_76057 === (2))){
var inst_76046 = (state_76056[(2)]);
var inst_76047 = new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(user);
var inst_76048 = [new cljs.core.Keyword(null,"$set","$set",-1609409324)];
var inst_76049 = [new cljs.core.Keyword(null,"company-wallet","company-wallet",-1325172306)];
var inst_76050 = [inst_76046];
var inst_76051 = cljs.core.PersistentHashMap.fromArrays(inst_76049,inst_76050);
var inst_76052 = [inst_76051];
var inst_76053 = cljs.core.PersistentHashMap.fromArrays(inst_76048,inst_76052);
var inst_76054 = vp.meteor.upd(new cljs.core.Keyword(null,"users","users",-713552705),inst_76047,inst_76053);
var state_76056__$1 = state_76056;
return cljs.core.async.impl.ioc_helpers.return_chan(state_76056__$1,inst_76054);
} else {
return null;
}
}
});
return (function() {
var vp$server$init_$_state_machine__41292__auto__ = null;
var vp$server$init_$_state_machine__41292__auto____0 = (function (){
var statearr_76058 = [null,null,null,null,null,null,null];
(statearr_76058[(0)] = vp$server$init_$_state_machine__41292__auto__);

(statearr_76058[(1)] = (1));

return statearr_76058;
});
var vp$server$init_$_state_machine__41292__auto____1 = (function (state_76056){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_76056);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e76059){var ex__41295__auto__ = e76059;
var statearr_76060_76305 = state_76056;
(statearr_76060_76305[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_76056[(4)]))){
var statearr_76061_76306 = state_76056;
(statearr_76061_76306[(1)] = cljs.core.first((state_76056[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__76307 = state_76056;
state_76056 = G__76307;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$server$init_$_state_machine__41292__auto__ = function(state_76056){
switch(arguments.length){
case 0:
return vp$server$init_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$server$init_$_state_machine__41292__auto____1.call(this,state_76056);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$server$init_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$server$init_$_state_machine__41292__auto____0;
vp$server$init_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$server$init_$_state_machine__41292__auto____1;
return vp$server$init_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_76062 = f__41438__auto__();
(statearr_76062[(6)] = c__41437__auto__);

return statearr_76062;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
} else {
return null;
}
}));

vp.meteor.get_coll(new cljs.core.Keyword(null,"contracts","contracts",905357673)).after.insert((function (user_id,doc_aux){
var doc = cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(doc_aux,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",1310784252),true], 0));
var user = vp.meteor.user();
var company_user = vp.meteor.one(new cljs.core.Keyword(null,"users","users",-713552705),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(doc,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"plan","plan",1118952668),new cljs.core.Keyword(null,"user-id","user-id",-206822291)], null)));
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_76092){
var state_val_76093 = (state_76092[(1)]);
if((state_val_76093 === (1))){
var inst_76063 = [new cljs.core.Keyword(null,"complete_payment_url","complete_payment_url",2050925952),new cljs.core.Keyword(null,"amount","amount",364489504),new cljs.core.Keyword(null,"cardholder_preferred_currency","cardholder_preferred_currency",-1543579422),new cljs.core.Keyword(null,"ewallet","ewallet",-809125178),new cljs.core.Keyword(null,"currency","currency",-898327568),new cljs.core.Keyword(null,"language","language",-1591107564),new cljs.core.Keyword(null,"payment_method_types_exclude","payment_method_types_exclude",1923666361),new cljs.core.Keyword(null,"country","country",312965309),new cljs.core.Keyword(null,"metadata","metadata",1799301597),new cljs.core.Keyword(null,"error_payment_url","error_payment_url",1207041821)];
var inst_76064 = new cljs.core.Keyword(null,"investment","investment",-1072757208).cljs$core$IFn$_invoke$arity$1(doc);
var inst_76065 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_76066 = [new cljs.core.Keyword(null,"company-wallet","company-wallet",-1325172306),new cljs.core.Keyword(null,"data","data",-232669377),new cljs.core.Keyword(null,"id","id",-1388402092)];
var inst_76067 = (new cljs.core.PersistentVector(null,3,(5),inst_76065,inst_76066,null));
var inst_76068 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(company_user,inst_76067);
var inst_76069 = cljs.core.PersistentVector.EMPTY;
var inst_76070 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_76071 = [new cljs.core.Keyword(null,"profile","profile",-545963874),new cljs.core.Keyword(null,"country","country",312965309)];
var inst_76072 = (new cljs.core.PersistentVector(null,2,(5),inst_76070,inst_76071,null));
var inst_76073 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(user,inst_76072);
var inst_76074 = [new cljs.core.Keyword(null,"contract_id","contract_id",-1829507193)];
var inst_76075 = new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(doc);
var inst_76076 = [inst_76075];
var inst_76077 = cljs.core.PersistentHashMap.fromArrays(inst_76074,inst_76076);
var inst_76078 = ["http://ikroa.com/#/user/contracts",inst_76064,true,inst_76068,"USD","en",inst_76069,inst_76073,inst_76077,"http://ikroa.com/#/user/contracts"];
var inst_76079 = cljs.core.PersistentHashMap.fromArrays(inst_76063,inst_76078);
var inst_76080 = vp.rapyd.create_checkout_page(inst_76079);
var state_76092__$1 = state_76092;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_76092__$1,(2),inst_76080);
} else {
if((state_val_76093 === (2))){
var inst_76082 = (state_76092[(2)]);
var inst_76083 = new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(doc);
var inst_76084 = [new cljs.core.Keyword(null,"$set","$set",-1609409324)];
var inst_76085 = [new cljs.core.Keyword(null,"checkout-page","checkout-page",682605245)];
var inst_76086 = [inst_76082];
var inst_76087 = cljs.core.PersistentHashMap.fromArrays(inst_76085,inst_76086);
var inst_76088 = [inst_76087];
var inst_76089 = cljs.core.PersistentHashMap.fromArrays(inst_76084,inst_76088);
var inst_76090 = vp.meteor.upd(new cljs.core.Keyword(null,"contracts","contracts",905357673),inst_76083,inst_76089);
var state_76092__$1 = state_76092;
return cljs.core.async.impl.ioc_helpers.return_chan(state_76092__$1,inst_76090);
} else {
return null;
}
}
});
return (function() {
var vp$server$init_$_state_machine__41292__auto__ = null;
var vp$server$init_$_state_machine__41292__auto____0 = (function (){
var statearr_76094 = [null,null,null,null,null,null,null];
(statearr_76094[(0)] = vp$server$init_$_state_machine__41292__auto__);

(statearr_76094[(1)] = (1));

return statearr_76094;
});
var vp$server$init_$_state_machine__41292__auto____1 = (function (state_76092){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_76092);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e76095){var ex__41295__auto__ = e76095;
var statearr_76096_76308 = state_76092;
(statearr_76096_76308[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_76092[(4)]))){
var statearr_76097_76309 = state_76092;
(statearr_76097_76309[(1)] = cljs.core.first((state_76092[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__76310 = state_76092;
state_76092 = G__76310;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$server$init_$_state_machine__41292__auto__ = function(state_76092){
switch(arguments.length){
case 0:
return vp$server$init_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$server$init_$_state_machine__41292__auto____1.call(this,state_76092);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$server$init_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$server$init_$_state_machine__41292__auto____0;
vp$server$init_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$server$init_$_state_machine__41292__auto____1;
return vp$server$init_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_76098 = f__41438__auto__();
(statearr_76098[(6)] = c__41437__auto__);

return statearr_76098;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
}));

return vp.meteor.get_coll(new cljs.core.Keyword(null,"contracts","contracts",905357673)).after.find((function (user_id,selector,options,cursor){
var docs = cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$variadic(cursor.fetch(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",1310784252),true], 0));
var user = vp.meteor.user();
var c__41437__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41438__auto__ = (function (){var switch__41291__auto__ = (function (state_76220){
var state_val_76221 = (state_76220[(1)]);
if((state_val_76221 === (7))){
var inst_76112 = (state_76220[(7)]);
var inst_76119 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_76120 = [new cljs.core.Keyword(null,"checkout-page","checkout-page",682605245),new cljs.core.Keyword(null,"data","data",-232669377),new cljs.core.Keyword(null,"id","id",-1388402092)];
var inst_76121 = (new cljs.core.PersistentVector(null,3,(5),inst_76119,inst_76120,null));
var inst_76122 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(inst_76112,inst_76121);
var inst_76123 = vp.rapyd.get_checkout_page(inst_76122);
var state_76220__$1 = state_76220;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_76220__$1,(10),inst_76123);
} else {
if((state_val_76221 === (20))){
var inst_76166 = (state_76220[(8)]);
var inst_76173 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_76174 = [new cljs.core.Keyword(null,"checkout-page","checkout-page",682605245),new cljs.core.Keyword(null,"data","data",-232669377),new cljs.core.Keyword(null,"id","id",-1388402092)];
var inst_76175 = (new cljs.core.PersistentVector(null,3,(5),inst_76173,inst_76174,null));
var inst_76176 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(inst_76166,inst_76175);
var inst_76177 = vp.rapyd.get_checkout_page(inst_76176);
var state_76220__$1 = state_76220;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_76220__$1,(23),inst_76177);
} else {
if((state_val_76221 === (1))){
var inst_76103 = cljs.core.seq(docs);
var inst_76104 = inst_76103;
var inst_76105 = null;
var inst_76106 = (0);
var inst_76107 = (0);
var state_76220__$1 = (function (){var statearr_76222 = state_76220;
(statearr_76222[(9)] = inst_76104);

(statearr_76222[(10)] = inst_76107);

(statearr_76222[(11)] = inst_76106);

(statearr_76222[(12)] = inst_76105);

return statearr_76222;
})();
var statearr_76223_76311 = state_76220__$1;
(statearr_76223_76311[(2)] = null);

(statearr_76223_76311[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (24))){
var inst_76195 = [new cljs.core.Keyword(null,"status","status",-1997798413)];
var inst_76196 = ["investment-active"];
var inst_76197 = cljs.core.PersistentHashMap.fromArrays(inst_76195,inst_76196);
var state_76220__$1 = state_76220;
var statearr_76224_76312 = state_76220__$1;
(statearr_76224_76312[(2)] = inst_76197);

(statearr_76224_76312[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (4))){
var inst_76107 = (state_76220[(10)]);
var inst_76112 = (state_76220[(7)]);
var inst_76105 = (state_76220[(12)]);
var inst_76112__$1 = cljs.core._nth(inst_76105,inst_76107);
var inst_76113 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_76114 = [new cljs.core.Keyword(null,"payment","payment",-1682035288),new cljs.core.Keyword(null,"status","status",-1997798413)];
var inst_76115 = (new cljs.core.PersistentVector(null,2,(5),inst_76113,inst_76114,null));
var inst_76116 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(inst_76112__$1,inst_76115);
var inst_76117 = cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(inst_76116,"CLO");
var state_76220__$1 = (function (){var statearr_76225 = state_76220;
(statearr_76225[(7)] = inst_76112__$1);

return statearr_76225;
})();
if(inst_76117){
var statearr_76226_76313 = state_76220__$1;
(statearr_76226_76313[(1)] = (7));

} else {
var statearr_76227_76314 = state_76220__$1;
(statearr_76227_76314[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (15))){
var state_76220__$1 = state_76220;
var statearr_76228_76315 = state_76220__$1;
(statearr_76228_76315[(2)] = null);

(statearr_76228_76315[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (21))){
var state_76220__$1 = state_76220;
var statearr_76229_76316 = state_76220__$1;
(statearr_76229_76316[(2)] = null);

(statearr_76229_76316[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (13))){
var inst_76126 = (state_76220[(13)]);
var inst_76134 = (state_76220[(14)]);
var inst_76127 = (state_76220[(15)]);
var inst_76146 = (state_76220[(2)]);
var inst_76147 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([inst_76134,inst_76146], 0));
var inst_76148 = [inst_76147];
var inst_76149 = cljs.core.PersistentHashMap.fromArrays(inst_76127,inst_76148);
var inst_76150 = vp.meteor.upd(new cljs.core.Keyword(null,"contracts","contracts",905357673),inst_76126,inst_76149);
var state_76220__$1 = state_76220;
var statearr_76230_76317 = state_76220__$1;
(statearr_76230_76317[(2)] = inst_76150);

(statearr_76230_76317[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (22))){
var inst_76157 = (state_76220[(16)]);
var inst_76207 = (state_76220[(2)]);
var inst_76208 = cljs.core.next(inst_76157);
var inst_76104 = inst_76208;
var inst_76105 = null;
var inst_76106 = (0);
var inst_76107 = (0);
var state_76220__$1 = (function (){var statearr_76231 = state_76220;
(statearr_76231[(9)] = inst_76104);

(statearr_76231[(10)] = inst_76107);

(statearr_76231[(17)] = inst_76207);

(statearr_76231[(11)] = inst_76106);

(statearr_76231[(12)] = inst_76105);

return statearr_76231;
})();
var statearr_76232_76318 = state_76220__$1;
(statearr_76232_76318[(2)] = null);

(statearr_76232_76318[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (6))){
var inst_76216 = (state_76220[(2)]);
var state_76220__$1 = state_76220;
var statearr_76233_76319 = state_76220__$1;
(statearr_76233_76319[(2)] = inst_76216);

(statearr_76233_76319[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (25))){
var state_76220__$1 = state_76220;
var statearr_76234_76320 = state_76220__$1;
(statearr_76234_76320[(2)] = null);

(statearr_76234_76320[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (17))){
var inst_76157 = (state_76220[(16)]);
var inst_76161 = cljs.core.chunk_first(inst_76157);
var inst_76162 = cljs.core.chunk_rest(inst_76157);
var inst_76163 = cljs.core.count(inst_76161);
var inst_76104 = inst_76162;
var inst_76105 = inst_76161;
var inst_76106 = inst_76163;
var inst_76107 = (0);
var state_76220__$1 = (function (){var statearr_76235 = state_76220;
(statearr_76235[(9)] = inst_76104);

(statearr_76235[(10)] = inst_76107);

(statearr_76235[(11)] = inst_76106);

(statearr_76235[(12)] = inst_76105);

return statearr_76235;
})();
var statearr_76236_76321 = state_76220__$1;
(statearr_76236_76321[(2)] = null);

(statearr_76236_76321[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (3))){
var inst_76218 = (state_76220[(2)]);
var state_76220__$1 = state_76220;
return cljs.core.async.impl.ioc_helpers.return_chan(state_76220__$1,inst_76218);
} else {
if((state_val_76221 === (12))){
var state_76220__$1 = state_76220;
var statearr_76237_76322 = state_76220__$1;
(statearr_76237_76322[(2)] = null);

(statearr_76237_76322[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (2))){
var inst_76107 = (state_76220[(10)]);
var inst_76106 = (state_76220[(11)]);
var inst_76109 = (inst_76107 < inst_76106);
var inst_76110 = inst_76109;
var state_76220__$1 = state_76220;
if(cljs.core.truth_(inst_76110)){
var statearr_76238_76323 = state_76220__$1;
(statearr_76238_76323[(1)] = (4));

} else {
var statearr_76239_76324 = state_76220__$1;
(statearr_76239_76324[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (23))){
var inst_76166 = (state_76220[(8)]);
var inst_76179 = (state_76220[(2)]);
var inst_76180 = new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(inst_76166);
var inst_76181 = [new cljs.core.Keyword(null,"$set","$set",-1609409324)];
var inst_76182 = [new cljs.core.Keyword(null,"payment","payment",-1682035288)];
var inst_76183 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_76184 = [new cljs.core.Keyword(null,"data","data",-232669377),new cljs.core.Keyword(null,"payment","payment",-1682035288)];
var inst_76185 = (new cljs.core.PersistentVector(null,2,(5),inst_76183,inst_76184,null));
var inst_76186 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(inst_76179,inst_76185);
var inst_76187 = [inst_76186];
var inst_76188 = cljs.core.PersistentHashMap.fromArrays(inst_76182,inst_76187);
var inst_76189 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_76190 = [new cljs.core.Keyword(null,"data","data",-232669377),new cljs.core.Keyword(null,"payment","payment",-1682035288),new cljs.core.Keyword(null,"status","status",-1997798413)];
var inst_76191 = (new cljs.core.PersistentVector(null,3,(5),inst_76189,inst_76190,null));
var inst_76192 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(inst_76179,inst_76191);
var inst_76193 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_76192,"CLO");
var state_76220__$1 = (function (){var statearr_76240 = state_76220;
(statearr_76240[(18)] = inst_76180);

(statearr_76240[(19)] = inst_76181);

(statearr_76240[(20)] = inst_76188);

return statearr_76240;
})();
if(inst_76193){
var statearr_76241_76325 = state_76220__$1;
(statearr_76241_76325[(1)] = (24));

} else {
var statearr_76242_76326 = state_76220__$1;
(statearr_76242_76326[(1)] = (25));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (19))){
var inst_76211 = (state_76220[(2)]);
var state_76220__$1 = state_76220;
var statearr_76246_76327 = state_76220__$1;
(statearr_76246_76327[(2)] = inst_76211);

(statearr_76246_76327[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (11))){
var inst_76141 = [new cljs.core.Keyword(null,"status","status",-1997798413)];
var inst_76142 = ["investment-active"];
var inst_76143 = cljs.core.PersistentHashMap.fromArrays(inst_76141,inst_76142);
var state_76220__$1 = state_76220;
var statearr_76247_76328 = state_76220__$1;
(statearr_76247_76328[(2)] = inst_76143);

(statearr_76247_76328[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (9))){
var inst_76104 = (state_76220[(9)]);
var inst_76107 = (state_76220[(10)]);
var inst_76106 = (state_76220[(11)]);
var inst_76105 = (state_76220[(12)]);
var inst_76153 = (state_76220[(2)]);
var inst_76154 = (inst_76107 + (1));
var tmp76243 = inst_76104;
var tmp76244 = inst_76106;
var tmp76245 = inst_76105;
var inst_76104__$1 = tmp76243;
var inst_76105__$1 = tmp76245;
var inst_76106__$1 = tmp76244;
var inst_76107__$1 = inst_76154;
var state_76220__$1 = (function (){var statearr_76248 = state_76220;
(statearr_76248[(9)] = inst_76104__$1);

(statearr_76248[(10)] = inst_76107__$1);

(statearr_76248[(21)] = inst_76153);

(statearr_76248[(11)] = inst_76106__$1);

(statearr_76248[(12)] = inst_76105__$1);

return statearr_76248;
})();
var statearr_76249_76329 = state_76220__$1;
(statearr_76249_76329[(2)] = null);

(statearr_76249_76329[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (5))){
var inst_76157 = (state_76220[(16)]);
var inst_76104 = (state_76220[(9)]);
var inst_76157__$1 = cljs.core.seq(inst_76104);
var state_76220__$1 = (function (){var statearr_76250 = state_76220;
(statearr_76250[(16)] = inst_76157__$1);

return statearr_76250;
})();
if(inst_76157__$1){
var statearr_76251_76330 = state_76220__$1;
(statearr_76251_76330[(1)] = (14));

} else {
var statearr_76252_76331 = state_76220__$1;
(statearr_76252_76331[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (14))){
var inst_76157 = (state_76220[(16)]);
var inst_76159 = cljs.core.chunked_seq_QMARK_(inst_76157);
var state_76220__$1 = state_76220;
if(inst_76159){
var statearr_76253_76332 = state_76220__$1;
(statearr_76253_76332[(1)] = (17));

} else {
var statearr_76254_76333 = state_76220__$1;
(statearr_76254_76333[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (26))){
var inst_76180 = (state_76220[(18)]);
var inst_76181 = (state_76220[(19)]);
var inst_76188 = (state_76220[(20)]);
var inst_76200 = (state_76220[(2)]);
var inst_76201 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([inst_76188,inst_76200], 0));
var inst_76202 = [inst_76201];
var inst_76203 = cljs.core.PersistentHashMap.fromArrays(inst_76181,inst_76202);
var inst_76204 = vp.meteor.upd(new cljs.core.Keyword(null,"contracts","contracts",905357673),inst_76180,inst_76203);
var state_76220__$1 = state_76220;
var statearr_76255_76334 = state_76220__$1;
(statearr_76255_76334[(2)] = inst_76204);

(statearr_76255_76334[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (16))){
var inst_76214 = (state_76220[(2)]);
var state_76220__$1 = state_76220;
var statearr_76256_76335 = state_76220__$1;
(statearr_76256_76335[(2)] = inst_76214);

(statearr_76256_76335[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (10))){
var inst_76112 = (state_76220[(7)]);
var inst_76125 = (state_76220[(2)]);
var inst_76126 = new cljs.core.Keyword(null,"_id","_id",-789960287).cljs$core$IFn$_invoke$arity$1(inst_76112);
var inst_76127 = [new cljs.core.Keyword(null,"$set","$set",-1609409324)];
var inst_76128 = [new cljs.core.Keyword(null,"payment","payment",-1682035288)];
var inst_76129 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_76130 = [new cljs.core.Keyword(null,"data","data",-232669377),new cljs.core.Keyword(null,"payment","payment",-1682035288)];
var inst_76131 = (new cljs.core.PersistentVector(null,2,(5),inst_76129,inst_76130,null));
var inst_76132 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(inst_76125,inst_76131);
var inst_76133 = [inst_76132];
var inst_76134 = cljs.core.PersistentHashMap.fromArrays(inst_76128,inst_76133);
var inst_76135 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_76136 = [new cljs.core.Keyword(null,"data","data",-232669377),new cljs.core.Keyword(null,"payment","payment",-1682035288),new cljs.core.Keyword(null,"status","status",-1997798413)];
var inst_76137 = (new cljs.core.PersistentVector(null,3,(5),inst_76135,inst_76136,null));
var inst_76138 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(inst_76125,inst_76137);
var inst_76139 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_76138,"CLO");
var state_76220__$1 = (function (){var statearr_76257 = state_76220;
(statearr_76257[(13)] = inst_76126);

(statearr_76257[(14)] = inst_76134);

(statearr_76257[(15)] = inst_76127);

return statearr_76257;
})();
if(inst_76139){
var statearr_76258_76336 = state_76220__$1;
(statearr_76258_76336[(1)] = (11));

} else {
var statearr_76259_76337 = state_76220__$1;
(statearr_76259_76337[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (18))){
var inst_76157 = (state_76220[(16)]);
var inst_76166 = (state_76220[(8)]);
var inst_76166__$1 = cljs.core.first(inst_76157);
var inst_76167 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_76168 = [new cljs.core.Keyword(null,"payment","payment",-1682035288),new cljs.core.Keyword(null,"status","status",-1997798413)];
var inst_76169 = (new cljs.core.PersistentVector(null,2,(5),inst_76167,inst_76168,null));
var inst_76170 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(inst_76166__$1,inst_76169);
var inst_76171 = cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(inst_76170,"CLO");
var state_76220__$1 = (function (){var statearr_76260 = state_76220;
(statearr_76260[(8)] = inst_76166__$1);

return statearr_76260;
})();
if(inst_76171){
var statearr_76261_76338 = state_76220__$1;
(statearr_76261_76338[(1)] = (20));

} else {
var statearr_76262_76339 = state_76220__$1;
(statearr_76262_76339[(1)] = (21));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_76221 === (8))){
var state_76220__$1 = state_76220;
var statearr_76263_76340 = state_76220__$1;
(statearr_76263_76340[(2)] = null);

(statearr_76263_76340[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var vp$server$init_$_state_machine__41292__auto__ = null;
var vp$server$init_$_state_machine__41292__auto____0 = (function (){
var statearr_76264 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_76264[(0)] = vp$server$init_$_state_machine__41292__auto__);

(statearr_76264[(1)] = (1));

return statearr_76264;
});
var vp$server$init_$_state_machine__41292__auto____1 = (function (state_76220){
while(true){
var ret_value__41293__auto__ = (function (){try{while(true){
var result__41294__auto__ = switch__41291__auto__(state_76220);
if(cljs.core.keyword_identical_QMARK_(result__41294__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41294__auto__;
}
break;
}
}catch (e76265){var ex__41295__auto__ = e76265;
var statearr_76266_76341 = state_76220;
(statearr_76266_76341[(2)] = ex__41295__auto__);


if(cljs.core.seq((state_76220[(4)]))){
var statearr_76267_76342 = state_76220;
(statearr_76267_76342[(1)] = cljs.core.first((state_76220[(4)])));

} else {
throw ex__41295__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41293__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__76343 = state_76220;
state_76220 = G__76343;
continue;
} else {
return ret_value__41293__auto__;
}
break;
}
});
vp$server$init_$_state_machine__41292__auto__ = function(state_76220){
switch(arguments.length){
case 0:
return vp$server$init_$_state_machine__41292__auto____0.call(this);
case 1:
return vp$server$init_$_state_machine__41292__auto____1.call(this,state_76220);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
vp$server$init_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$0 = vp$server$init_$_state_machine__41292__auto____0;
vp$server$init_$_state_machine__41292__auto__.cljs$core$IFn$_invoke$arity$1 = vp$server$init_$_state_machine__41292__auto____1;
return vp$server$init_$_state_machine__41292__auto__;
})()
})();
var state__41439__auto__ = (function (){var statearr_76268 = f__41438__auto__();
(statearr_76268[(6)] = c__41437__auto__);

return statearr_76268;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41439__auto__);
}));

return c__41437__auto__;
}));
});
goog.exportSymbol('vp.server.init', vp.server.init);
Object.defineProperty(module.exports, "init", { enumerable: true, get: function() { return vp.server.init; } });
//# sourceMappingURL=vp.server.js.map

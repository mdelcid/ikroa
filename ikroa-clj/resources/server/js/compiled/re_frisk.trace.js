var $CLJS = require("./cljs_env");
var $jscomp = $CLJS.$jscomp;
var COMPILED = false;
require("./cljs.core.js");
require("./re_frisk.diff.diff.js");
require("./re_frisk.utils.js");
require("./re_frame.trace.js");
require("./re_frame.interop.js");
require("./re_frame.db.js");
var cognitect=$CLJS.cognitect || ($CLJS.cognitect = {});
var module$shadow_js_shim_module$numeral=$CLJS.module$shadow_js_shim_module$numeral || ($CLJS.module$shadow_js_shim_module$numeral = {});
var module$shadow_js_shim_module$meteor$accounts_base=$CLJS.module$shadow_js_shim_module$meteor$accounts_base || ($CLJS.module$shadow_js_shim_module$meteor$accounts_base = {});
var module$shadow_js_shim_module$sweetalert2$dist$sweetalert2=$CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 || ($CLJS.module$shadow_js_shim_module$sweetalert2$dist$sweetalert2 = {});
var module$shadow_js_shim_module$$uppy$file_input=$CLJS.module$shadow_js_shim_module$$uppy$file_input || ($CLJS.module$shadow_js_shim_module$$uppy$file_input = {});
var re_frame=$CLJS.re_frame || ($CLJS.re_frame = {});
var module$shadow_js_shim_module$mdb_react_ui_kit=$CLJS.module$shadow_js_shim_module$mdb_react_ui_kit || ($CLJS.module$shadow_js_shim_module$mdb_react_ui_kit = {});
var module$shadow_js_shim_module$react=$CLJS.module$shadow_js_shim_module$react || ($CLJS.module$shadow_js_shim_module$react = {});
var clojure=$CLJS.clojure || ($CLJS.clojure = {});
var module$shadow_js_shim_module$react_phone_number_input=$CLJS.module$shadow_js_shim_module$react_phone_number_input || ($CLJS.module$shadow_js_shim_module$react_phone_number_input = {});
var module$shadow_js_shim_module$cropperjs=$CLJS.module$shadow_js_shim_module$cropperjs || ($CLJS.module$shadow_js_shim_module$cropperjs = {});
var module$shadow_js_shim_module$moment=$CLJS.module$shadow_js_shim_module$moment || ($CLJS.module$shadow_js_shim_module$moment = {});
var module$shadow_js_shim_module$shortid=$CLJS.module$shadow_js_shim_module$shortid || ($CLJS.module$shadow_js_shim_module$shortid = {});
var module$shadow_js_shim_module$$uppy$react$lib$StatusBar=$CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar || ($CLJS.module$shadow_js_shim_module$$uppy$react$lib$StatusBar = {});
var module$shadow_js_shim_module$react_datepicker=$CLJS.module$shadow_js_shim_module$react_datepicker || ($CLJS.module$shadow_js_shim_module$react_datepicker = {});
var module$shadow_js_shim_module$$uppy$aws_s3=$CLJS.module$shadow_js_shim_module$$uppy$aws_s3 || ($CLJS.module$shadow_js_shim_module$$uppy$aws_s3 = {});
var devtools=$CLJS.devtools || ($CLJS.devtools = {});
var cljs=$CLJS.cljs || ($CLJS.cljs = {});
var re_frisk=$CLJS.re_frisk || ($CLJS.re_frisk = {});
var forms=$CLJS.forms || ($CLJS.forms = {});
var module$shadow_js_shim_module$$uppy$dashboard=$CLJS.module$shadow_js_shim_module$$uppy$dashboard || ($CLJS.module$shadow_js_shim_module$$uppy$dashboard = {});
var module$shadow_js_shim_module$react_color=$CLJS.module$shadow_js_shim_module$react_color || ($CLJS.module$shadow_js_shim_module$react_color = {});
var shadow=$CLJS.shadow || ($CLJS.shadow = {});
var module$shadow_js_shim_module$react_dom=$CLJS.module$shadow_js_shim_module$react_dom || ($CLJS.module$shadow_js_shim_module$react_dom = {});
var module$shadow_js_shim_module$meteor$tracker=$CLJS.module$shadow_js_shim_module$meteor$tracker || ($CLJS.module$shadow_js_shim_module$meteor$tracker = {});
var re_chain=$CLJS.re_chain || ($CLJS.re_chain = {});
var module$shadow_js_shim_module$meteor$meteor=$CLJS.module$shadow_js_shim_module$meteor$meteor || ($CLJS.module$shadow_js_shim_module$meteor$meteor = {});
var goog=$CLJS.goog || ($CLJS.goog = {});
var secretary=$CLJS.secretary || ($CLJS.secretary = {});
var reagent=$CLJS.reagent || ($CLJS.reagent = {});
var module$shadow_js_shim_module$remove_accents=$CLJS.module$shadow_js_shim_module$remove_accents || ($CLJS.module$shadow_js_shim_module$remove_accents = {});
var module$shadow_js_shim_module$meteor$react_meteor_data=$CLJS.module$shadow_js_shim_module$meteor$react_meteor_data || ($CLJS.module$shadow_js_shim_module$meteor$react_meteor_data = {});
var module$shadow_js_shim_module$meteor$mongo=$CLJS.module$shadow_js_shim_module$meteor$mongo || ($CLJS.module$shadow_js_shim_module$meteor$mongo = {});
var cljs_time=$CLJS.cljs_time || ($CLJS.cljs_time = {});
var module$shadow_js_shim_module$toastr=$CLJS.module$shadow_js_shim_module$toastr || ($CLJS.module$shadow_js_shim_module$toastr = {});
var expound=$CLJS.expound || ($CLJS.expound = {});
var vp=$CLJS.vp || ($CLJS.vp = {});
var module$shadow_js_shim_module$jquery=$CLJS.module$shadow_js_shim_module$jquery || ($CLJS.module$shadow_js_shim_module$jquery = {});
var module$shadow_js_shim_module$$uppy$core=$CLJS.module$shadow_js_shim_module$$uppy$core || ($CLJS.module$shadow_js_shim_module$$uppy$core = {});
var module$shadow_js_shim_module$meteor$fetch=$CLJS.module$shadow_js_shim_module$meteor$fetch || ($CLJS.module$shadow_js_shim_module$meteor$fetch = {});
var module$shadow_js_shim_module$meteor$gadicc_blaze_react_component=$CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component || ($CLJS.module$shadow_js_shim_module$meteor$gadicc_blaze_react_component = {});
var module$shadow_js_shim_module$$uppy$image_editor=$CLJS.module$shadow_js_shim_module$$uppy$image_editor || ($CLJS.module$shadow_js_shim_module$$uppy$image_editor = {});
var com=$CLJS.com || ($CLJS.com = {});
var re_com=$CLJS.re_com || ($CLJS.re_com = {});
var module$shadow_js_shim_module$reactstrap=$CLJS.module$shadow_js_shim_module$reactstrap || ($CLJS.module$shadow_js_shim_module$reactstrap = {});

$CLJS.SHADOW_ENV.setLoaded("re_frisk.trace.js");

goog.provide('re_frisk.trace');
re_frisk.trace.normalize_traces = (function re_frisk$trace$normalize_traces(traces){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (items,p__47456){
var map__47457 = p__47456;
var map__47457__$1 = (((((!((map__47457 == null))))?(((((map__47457.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__47457.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__47457):map__47457);
var trace = map__47457__$1;
var op_type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47457__$1,new cljs.core.Keyword(null,"op-type","op-type",-1636141668));
var tags = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47457__$1,new cljs.core.Keyword(null,"tags","tags",1771418977));
var duration = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47457__$1,new cljs.core.Keyword(null,"duration","duration",1444101068));
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47457__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var op_type__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.namespace(op_type),"sub"))?new cljs.core.Keyword(null,"sub","sub",-2093760025):op_type);
var item = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"indx","indx",1571035590),id,new cljs.core.Keyword(null,"trace?","trace?",1730690679),true], null);
var G__47459 = op_type__$1;
var G__47459__$1 = (((G__47459 instanceof cljs.core.Keyword))?G__47459.fqn:null);
switch (G__47459__$1) {
case "event":
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(items,cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(item,new cljs.core.Keyword(null,"trace?","trace?",1730690679)),cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(cljs.core.select_keys(trace,new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword(null,"operation","operation",-1267664310),new cljs.core.Keyword(null,"duration","duration",1444101068),new cljs.core.Keyword(null,"start","start",-355208981),new cljs.core.Keyword(null,"end","end",-268185958)], null)),new cljs.core.Keyword(null,"event","event",301435442),new cljs.core.Keyword(null,"event","event",301435442).cljs$core$IFn$_invoke$arity$1(tags),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"truncated-name","truncated-name",1771353320),re_frisk.utils.truncate_name(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.first(new cljs.core.Keyword(null,"event","event",301435442).cljs$core$IFn$_invoke$arity$1(tags)))),new cljs.core.Keyword(null,"app-db-diff","app-db-diff",709588713),re_frisk.diff.diff.diff(new cljs.core.Keyword(null,"app-db-before","app-db-before",-1442902645).cljs$core$IFn$_invoke$arity$1(tags),new cljs.core.Keyword(null,"app-db-after","app-db-after",1477492964).cljs$core$IFn$_invoke$arity$1(tags))], 0))], 0)));

break;
case "event/handler":
var prev = cljs.core.peek(items);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"op-type","op-type",-1636141668).cljs$core$IFn$_invoke$arity$2(prev,new cljs.core.Keyword(null,"event","event",301435442)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(cljs.core.pop(items),cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(prev,new cljs.core.Keyword(null,"handler-duration","handler-duration",262132324),duration));
} else {
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(items,cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([item,cljs.core.select_keys(trace,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword(null,"operation","operation",-1267664310),new cljs.core.Keyword(null,"duration","duration",1444101068)], null))], 0)));
}

break;
case "event/do-fx":
var prev = cljs.core.peek(items);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"op-type","op-type",-1636141668).cljs$core$IFn$_invoke$arity$2(prev,new cljs.core.Keyword(null,"event","event",301435442)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(cljs.core.pop(items),cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(prev,new cljs.core.Keyword(null,"fx-duration","fx-duration",1796312432),duration));
} else {
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(items,cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([item,cljs.core.select_keys(trace,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword(null,"duration","duration",1444101068)], null))], 0)));
}

break;
case "sub":
case "render":
var prev = cljs.core.peek(items);
var trace__$1 = cljs.core.select_keys(trace,new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword(null,"operation","operation",-1267664310),new cljs.core.Keyword(null,"duration","duration",1444101068),new cljs.core.Keyword(null,"start","start",-355208981),new cljs.core.Keyword(null,"end","end",-268185958)], null));
var trace__$2 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(trace__$1,new cljs.core.Keyword(null,"duration-ms","duration-ms",1993555055),re_frisk.utils.str_ms(new cljs.core.Keyword(null,"duration","duration",1444101068).cljs$core$IFn$_invoke$arity$1(trace__$1)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"reaction","reaction",490869788),new cljs.core.Keyword(null,"reaction","reaction",490869788).cljs$core$IFn$_invoke$arity$1(tags),new cljs.core.Keyword(null,"cached?","cached?",86081880),new cljs.core.Keyword(null,"cached?","cached?",86081880).cljs$core$IFn$_invoke$arity$1(tags),new cljs.core.Keyword(null,"input-signals","input-signals",563633497),new cljs.core.Keyword(null,"input-signals","input-signals",563633497).cljs$core$IFn$_invoke$arity$1(tags)], 0));
if(cljs.core.truth_(new cljs.core.Keyword(null,"subs?","subs?",-2085240020).cljs$core$IFn$_invoke$arity$1(prev))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(cljs.core.pop(items),cljs.core.update.cljs$core$IFn$_invoke$arity$4(prev,new cljs.core.Keyword(null,"subs","subs",-186681991),cljs.core.conj,trace__$2));
} else {
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(items,cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([item,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword(null,"subs","subs",-186681991),new cljs.core.Keyword(null,"subs?","subs?",-2085240020),true,new cljs.core.Keyword(null,"subs","subs",-186681991),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [trace__$2], null),new cljs.core.Keyword(null,"app-db-reaction","app-db-reaction",-269835135),re_frame.interop.reagent_id(re_frame.db.app_db),new cljs.core.Keyword(null,"start","start",-355208981),new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(trace__$2)], null)], 0)));
}

break;
default:
return items;

}
}),cljs.core.PersistentVector.EMPTY,cljs.core.sort_by.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"id","id",-1388402092),traces));
});
re_frisk.trace.normalize_durations = (function re_frisk$trace$normalize_durations(first_event){
return (function (p__47513){
var map__47515 = p__47513;
var map__47515__$1 = (((((!((map__47515 == null))))?(((((map__47515.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__47515.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__47515):map__47515);
var trace = map__47515__$1;
var subs_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47515__$1,new cljs.core.Keyword(null,"subs?","subs?",-2085240020));
var subs = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47515__$1,new cljs.core.Keyword(null,"subs","subs",-186681991));
var op_type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47515__$1,new cljs.core.Keyword(null,"op-type","op-type",-1636141668));
var handler_duration = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47515__$1,new cljs.core.Keyword(null,"handler-duration","handler-duration",262132324));
var fx_duration = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47515__$1,new cljs.core.Keyword(null,"fx-duration","fx-duration",1796312432));
var map__47519 = (cljs.core.truth_(subs_QMARK_)?cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([trace,cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (acc,p__47521){
var map__47522 = p__47521;
var map__47522__$1 = (((((!((map__47522 == null))))?(((((map__47522.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__47522.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__47522):map__47522);
var duration = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47522__$1,new cljs.core.Keyword(null,"duration","duration",1444101068));
var op_type__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47522__$1,new cljs.core.Keyword(null,"op-type","op-type",-1636141668));
var end = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47522__$1,new cljs.core.Keyword(null,"end","end",-268185958));
var cached_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47522__$1,new cljs.core.Keyword(null,"cached?","cached?",86081880));
var G__47527 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(acc,new cljs.core.Keyword(null,"duration","duration",1444101068),cljs.core._PLUS_,duration);
var G__47527__$1 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__47527,new cljs.core.Keyword(null,"end","end",-268185958),end)
;
var G__47527__$2 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(op_type__$1,new cljs.core.Keyword("sub","run","sub/run",-1821315581)))?cljs.core.update.cljs$core$IFn$_invoke$arity$4(cljs.core.update.cljs$core$IFn$_invoke$arity$3(G__47527__$1,new cljs.core.Keyword(null,"run-count","run-count",-924546145),cljs.core.inc),new cljs.core.Keyword(null,"run-duration","run-duration",1321930251),cljs.core._PLUS_,duration):G__47527__$1);
var G__47527__$3 = ((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(op_type__$1,new cljs.core.Keyword("sub","create","sub/create",-1301317560))) && (cljs.core.not(cached_QMARK_))))?cljs.core.update.cljs$core$IFn$_invoke$arity$4(cljs.core.update.cljs$core$IFn$_invoke$arity$3(G__47527__$2,new cljs.core.Keyword(null,"created-count","created-count",-1708534686),cljs.core.inc),new cljs.core.Keyword(null,"created-duration","created-duration",-1526658187),cljs.core._PLUS_,duration):G__47527__$2);
var G__47527__$4 = (cljs.core.truth_(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(op_type__$1,new cljs.core.Keyword("sub","create","sub/create",-1301317560)))?cached_QMARK_:false))?cljs.core.update.cljs$core$IFn$_invoke$arity$4(cljs.core.update.cljs$core$IFn$_invoke$arity$3(G__47527__$3,new cljs.core.Keyword(null,"created-count-cached","created-count-cached",1601348000),cljs.core.inc),new cljs.core.Keyword(null,"created-duration-cached","created-duration-cached",1269460585),cljs.core._PLUS_,duration):G__47527__$3);
var G__47527__$5 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(op_type__$1,new cljs.core.Keyword("sub","dispose","sub/dispose",365440536)))?cljs.core.update.cljs$core$IFn$_invoke$arity$4(cljs.core.update.cljs$core$IFn$_invoke$arity$3(G__47527__$4,new cljs.core.Keyword(null,"disposed-count","disposed-count",-457935431),cljs.core.inc),new cljs.core.Keyword(null,"disposed-duration","disposed-duration",-1737482274),cljs.core._PLUS_,duration):G__47527__$4);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(op_type__$1,new cljs.core.Keyword(null,"render","render",-1408033454))){
return cljs.core.update.cljs$core$IFn$_invoke$arity$4(cljs.core.update.cljs$core$IFn$_invoke$arity$3(G__47527__$5,new cljs.core.Keyword(null,"render-count","render-count",-875399191),cljs.core.inc),new cljs.core.Keyword(null,"render-duration","render-duration",1704092121),cljs.core._PLUS_,duration);
} else {
return G__47527__$5;
}
}),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"created-count-cached","created-count-cached",1601348000),new cljs.core.Keyword(null,"created-count","created-count",-1708534686),new cljs.core.Keyword(null,"created-duration-cached","created-duration-cached",1269460585),new cljs.core.Keyword(null,"render-count","render-count",-875399191),new cljs.core.Keyword(null,"run-duration","run-duration",1321930251),new cljs.core.Keyword(null,"duration","duration",1444101068),new cljs.core.Keyword(null,"created-duration","created-duration",-1526658187),new cljs.core.Keyword(null,"disposed-count","disposed-count",-457935431),new cljs.core.Keyword(null,"render-duration","render-duration",1704092121),new cljs.core.Keyword(null,"disposed-duration","disposed-duration",-1737482274),new cljs.core.Keyword(null,"run-count","run-count",-924546145)],[(0),(0),(0),(0),(0),(0),(0),(0),(0),(0),(0)]),subs)], 0)):((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(op_type,new cljs.core.Keyword(null,"event","event",301435442)))?(function (){var handler_fx_duration = (handler_duration + fx_duration);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(trace,new cljs.core.Keyword(null,"handler-fx-duration","handler-fx-duration",1974562002),handler_fx_duration,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"handler-fx-duration-ms","handler-fx-duration-ms",-118285662),re_frisk.utils.str_ms(handler_fx_duration)], 0));
})():trace
));
var map__47519__$1 = (((((!((map__47519 == null))))?(((((map__47519.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__47519.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__47519):map__47519);
var trace__$1 = map__47519__$1;
var render_duration = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47519__$1,new cljs.core.Keyword(null,"render-duration","render-duration",1704092121));
var disposed_duration = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47519__$1,new cljs.core.Keyword(null,"disposed-duration","disposed-duration",-1737482274));
var handler_duration__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47519__$1,new cljs.core.Keyword(null,"handler-duration","handler-duration",262132324));
var created_duration_cached = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47519__$1,new cljs.core.Keyword(null,"created-duration-cached","created-duration-cached",1269460585));
var run_duration = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47519__$1,new cljs.core.Keyword(null,"run-duration","run-duration",1321930251));
var start = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47519__$1,new cljs.core.Keyword(null,"start","start",-355208981));
var duration = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47519__$1,new cljs.core.Keyword(null,"duration","duration",1444101068));
var fx_duration__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47519__$1,new cljs.core.Keyword(null,"fx-duration","fx-duration",1796312432));
var created_duration = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47519__$1,new cljs.core.Keyword(null,"created-duration","created-duration",-1526658187));
var G__47536 = trace__$1;
var G__47536__$1 = (cljs.core.truth_(duration)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__47536,new cljs.core.Keyword(null,"duration-ms","duration-ms",1993555055),re_frisk.utils.str_ms(duration)):G__47536);
var G__47536__$2 = (cljs.core.truth_(handler_duration__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__47536__$1,new cljs.core.Keyword(null,"handler-duration-ms","handler-duration-ms",-491924416),re_frisk.utils.str_ms(handler_duration__$1)):G__47536__$1);
var G__47536__$3 = (cljs.core.truth_(fx_duration__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__47536__$2,new cljs.core.Keyword(null,"fx-duration-ms","fx-duration-ms",-1014211328),re_frisk.utils.str_ms(fx_duration__$1)):G__47536__$2);
var G__47536__$4 = (cljs.core.truth_(run_duration)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__47536__$3,new cljs.core.Keyword(null,"run-duration-ms","run-duration-ms",-1510786585),re_frisk.utils.str_ms(run_duration)):G__47536__$3);
var G__47536__$5 = (cljs.core.truth_(created_duration)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__47536__$4,new cljs.core.Keyword(null,"created-duration-ms","created-duration-ms",1987553257),re_frisk.utils.str_ms(created_duration)):G__47536__$4);
var G__47536__$6 = (cljs.core.truth_(created_duration_cached)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__47536__$5,new cljs.core.Keyword(null,"created-duration-cached-ms","created-duration-cached-ms",1699705669),re_frisk.utils.str_ms(created_duration_cached)):G__47536__$5);
var G__47536__$7 = (cljs.core.truth_(disposed_duration)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__47536__$6,new cljs.core.Keyword(null,"disposed-duration-ms","disposed-duration-ms",-1456969189),re_frisk.utils.str_ms(disposed_duration)):G__47536__$6);
var G__47536__$8 = (cljs.core.truth_(render_duration)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__47536__$7,new cljs.core.Keyword(null,"render-duration-ms","render-duration-ms",-757772115),re_frisk.utils.str_ms(render_duration)):G__47536__$7);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__47536__$8,new cljs.core.Keyword(null,"position","position",-2011731912),(start - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(first_event)));

});
});
Object.defineProperty(module.exports, "normalize_traces", { enumerable: false, get: function() { return re_frisk.trace.normalize_traces; } });
Object.defineProperty(module.exports, "normalize_durations", { enumerable: false, get: function() { return re_frisk.trace.normalize_durations; } });
//# sourceMappingURL=re_frisk.trace.js.map

(ns vp.toastr
  (:require
    ["toastr" :as toastr]))

(set! (.-options toastr) (clj->js {:tapToDismiss false
                                   :timeOut 0
                                   :hideDuration 0
                                   :closeButton true
                                   :preventDuplicates true
                                   :toastClass "toastr"

                                   }))

(defn before-showing-toastr []
  (.clear toastr))

(defn success [message & [title opts]]
  (before-showing-toastr)
  (let [merged-opts (merge opts {:timeOut 5000})]
    ((.-success toastr) message title (clj->js merged-opts))))

(defn info [& args]
  (before-showing-toastr)
  (apply (.-info toastr) args))
(defn warning [& args]
  (before-showing-toastr)
  (apply (.-warning toastr) args))
(defn error [& args]
  (before-showing-toastr)
  (apply (.-error toastr) args))


(def keyword->fn
  {:info info
   :warning warning
   :error error
   :success success})



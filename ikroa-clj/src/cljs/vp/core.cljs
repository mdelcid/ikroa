(ns vp.core
  (:require
   [reagent.dom :as rdom]
   [re-frame.core :as rf]
   [vp.events :as events]
   [vp.routes :as routes]
   [vp.views :as views]
   [vp.config :as config]
   [cljs.core.async :refer [go]]
   ["meteor/mongo" :refer [Mongo]]
   ["meteor/tracker" :refer [Tracker]]
   ["meteor/meteor" :refer [Meteor]]))


(defn dev-setup []
  (when config/debug?
    (println "dev mode2")))

(defn ^:dev/after-load mount-root []
  (rf/clear-subscription-cache!)
  (let [root-el (.getElementById js/document "app")]
    (rdom/unmount-component-at-node root-el)
    (rdom/render [views/main-panel] root-el)))

(def inited? (atom false))
(defn ^:export init []
  #_(let [user-id (.userId Meteor)]
    (.autorun
      Tracker
      (fn [computation]
        (when (and 
                (not @inited?)
                (or (nil? user-id)
                    (some? (.user Meteor))))
          (reset! inited? true)
          ))))
  
  (routes/app-routes)
  (rf/dispatch-sync [::events/initialize-db])
  (rf/dispatch [:init])    
  (dev-setup)
  (mount-root))

;; (go
;;   (println "method res" (<! (myfunction 1 4))))

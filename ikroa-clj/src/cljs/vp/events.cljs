(ns vp.events
  (:require
   [re-frame.core :as rf]
   [vp.db :as db]
   [vp.util :as u]
   [cljs.core.async :refer [go]]
   [vp.macros :refer-macros [<?]]
   [vp.toastr :as toastr]
   [vp.swal :as swal]
   [vp.routes :as routes]
   ["meteor/accounts-base" :refer [Accounts]]))

(rf/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

(rf/reg-event-db
 :set-active-panel
 (fn [db [_ active-panel]]
   (assoc db :active-panel active-panel)))

(rf/reg-event-fx
  :toastr
  (fn [{:keys [db]} [_ k & args]]
    (let []
      (apply (toastr/keyword->fn k) args)
      {})))

(rf/reg-event-fx
  :alert
  (fn [{:keys [db]} [_ & args]]
    (let []
      (if (= (count args) 2) 
        (let [[icon text] args]
          (swal/fire {:icon (name icon) :text text}))
        (swal/fire (first args)))
      {})))

(rf/reg-event-fx
  :confirm
  (fn [{:keys [db]} [_ args cb]]
    (let []
      (swal/confirm args cb)
      {})))

(rf/reg-event-fx
  :redirect
  (fn [{:keys [db]} [_ url]]
    (let []
      (routes/goto url)
      {})))

(rf/reg-event-fx
 :set-user
 (fn
   [{:keys [db]} [_ user]]
   (js/Sentry.setUser (clj->js {:email (:email user)}))

   {:db (assoc db :user user)
    :dispatch-n [[:get-init-info]]}))

(rf/reg-event-fx
 :logout
 (fn
   [{:keys [db]} [_]]
   {:db (dissoc db :init-info)}))

(rf/reg-event-fx
 :init
 (fn
   [{:keys [db]} [_]]
   (.onLogin Accounts #(rf/dispatch [:toastr :success "Welcome!"]))
   {}))


(rf/reg-event-db
 :set-db
 (fn [db [_ & path-n-value]]
   (assoc-in db (butlast path-n-value) (last path-n-value))))

(rf/reg-event-db
 :swap-db
 (fn [db [_ path & args]]
   (apply update-in db path args)))

(rf/reg-event-db
 :update-in-db
 (fn [db [_ & args]]
   (apply update-in db args)))


(rf/reg-event-db
 :dissoc-db
 (fn [db [_ & path]]
   (u/dissoc-in db path)))

(rf/reg-event-fx
  :login-redirect
  (fn [{:keys [db]} [_ after-login-url]]
    {:dispatch-n [[:redirect (routes/home-page)]
                  [:toastr :info "Please login first"]
                  (if after-login-url
                    [:set-db :after-login-url after-login-url])]}))

(rf/reg-event-fx
  :login
  (fn [{:keys [db]} [_ values]]
    
    {:db (update db :after-login-url #(or % (routes/home-page)))}))


(rf/reg-event-db
  :show-modal
  (fn [db [_ cmp]]
    (assoc db :modal-cmp cmp)))

(rf/reg-event-db
  :close-modal
  (fn [db [_ cmp]]
    (dissoc db :modal-cmp)))


(ns vp.crud
  (:require
    [forms.re-frame :as f]
    [forms.validator :as v]
    [re-frame.core :as rf]
    [reagent.core :as r]
    [vp.util :as u]
    [reagent.dom :as rdom]
    [vp.meteor :as meteor]
    [cljs.core.async :refer [go]]
    [clojure.string :as str]
    [vp.macros :refer-macros [<?]]
    [vp.aws :refer [upload-url]]
    ["react-phone-number-input" :refer [default isValidPhoneNumber] :rename {default PhoneInput}]
    ["jquery" :as jq]
    ["cropperjs" :as Cropper]
    ["react-color" :refer [SketchPicker]]
    ["@uppy/core" :as Uppy]
    ["@uppy/dashboard" :as UppyDashboard]
    ["@uppy/file-input" :as UppyFileInput]
    ["@uppy/image-editor" :as UppyImageEditor]
    ["@uppy/aws-s3" :as UppyAwsS3]
    ["@uppy/react/lib/StatusBar" :as UppyStatusBar]
    ["react-datepicker" :refer [default] :rename {default date-picker}]))

(js/console.log "PhoneInput" PhoneInput)
(declare render-field)

(def url-pattern #"(?i)^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$")

(def validations
  {:not-empty {:message "Can't be empty"
               :validator (fn [v] (not (empty? v)))}
   :valid-number {:message "Required"
                  :validator (fn [v]
                               (if (str/blank? v)
                                 (not (js/isNaN v))
                                 true))}
   :is-phone {:message "Invalid phone"
                  :validator (fn [v]
                               (if-not (str/blank? v)
                                 (isValidPhoneNumber v)
                                 true))}
   
   :not-null {:message "Required"
              :validator (fn [v] (some? v))}
   :valid-email {:message "Is not a valid email"
                 :validator (fn [v] (not= -1 (.indexOf (or v "") "@")))}})

(defn value-setter [form-path path]
  (fn [v]
    (rf/dispatch [::f/set! form-path path v])))

(defn setter
  "Set the value of the key path in the data atom"
  [form-path path]
  (fn [e]
    (rf/dispatch [::f/set! form-path path (.. e -target -value)])))

(defn to-validator
  [validations config]
  (reduce-kv (fn [m attr v]
               (assoc m attr
                      (map (fn [k] [k (get-in validations [k :validator])]) v))) {} config))


(defn render-errors
  "Renders the errors for the given key-path."
  [form-path path]
  (let [errors-for-path (rf/subscribe [::f/errors-for-path form-path path])
        errors @errors-for-path]
    (when errors
      [:div.text-danger.errors-wrap
       [:ul.list-unstyled
        (map (fn [error]
               [:li {:key error} (get-in validations [error :message])]) (take 1 (:failed errors)))]])))

(defn image-editor [img-element]
  (r/create-class
    {:component-did-mount
     (fn [cmp]
       
       (let [dn (rdom/dom-node cmp)
             _ (js/console.log dn)
             cropper (new Cropper dn (clj->js {:viewMode 1
                                               :background false
                                               :autoCropArea 1
                                               :aspectRatio 1
                                               :responsive true}))
             ]
         ))


     :reagent-render
     (fn []
        img-element)}))


(defn openModal []
    (let [show-bar? (rf/subscribe [:db ::modal?])]
  [:div
   [:input {:type "button" :value "Upload"}]]))


(defn file-uppy [{:keys [label on-uppy on-change get-upload-promise uppy-opts]}]
  (let [uppy (-> (Uppy (clj->js (merge-with merge
                                            {:autoProceed true
                                             :restrictions {:maxNumberOfFiles 1
                                                            :minNumberOfFiles 1}} 
                                            uppy-opts))))]
    (if on-uppy (on-uppy uppy))
    (r/create-class
      {:component-did-mount
       (fn [cmp-uppy]
         (let [dn (rdom/dom-node cmp-uppy)]
           (-> uppy
               (.use UppyAwsS3
                     (clj->js {:getUploadParameters
                               (fn [file]
                                 (meteor/to-promise
                                   (go
                                     (let [res (<? (upload-url file))]
                                       (.setFileMeta uppy (.-id file) (clj->js {:assetId (str (:asset-id res))}))
                                       res))))}))
               (.use UppyDashboard
                     (clj->js {
                               :id "Dashboard"
                               :pretty true
                               :trigger dn
                               :width 950
                               :height 900
                               :showSelectedFiles true
                               :theme "light"
                               :plugins [UppyImageEditor]
                               :metaFields [:id "name", :name "Name", :placeholder "File"]
                               :locale {:strings {:chooseFiles (or label "Upload")}}}))
              
      
              
               (.use UppyImageEditor
                     (clj->js {
                               :id "ImageEditor"
                               :target UppyDashboard
                               :quality 0.8
                               :cropperOptions {:viewMode 1
                                                :background true
                                                :autoCropArea 1
                                                :aspectRatio 1
                                                :responsive true}}))
              
              )))
        
       :component-will-unmount #(.close uppy)

       :reagent-render
       (fn []
         [:div]
         [openModal])})))

(defn render-uppy-file
  ([form-path field]
   (let [form-data-subs (rf/subscribe [::f/data form-path])
         is-valid? (rf/subscribe [::f/is-valid-path? form-path (:path field)])
         input-setter (value-setter form-path (:path field))
         on-change-handler (fn [v]
                             (input-setter v)
                             (when (not @is-valid?)
                               (rf/dispatch [::f/validate! form-path true])))
         uppy-ref (r/atom nil)]
     (fn []
       (let [form-data @form-data-subs
             v (get-in form-data (:path field))]
         [:div.mb-3
          (if (:label field) [:label.form-label (:label field)])
          (if-let [render-value (:render-value field)]
            [render-value v {:input-setter input-setter
                             :on-change-handler on-change-handler
                             :on-uppy #(reset! uppy-ref %)}]
            (if v
              [:div
               [:button.btn.btn-circle.btn-outline-danger {:type "button"
                                                           :on-click #(input-setter nil)}
                [:i.fas.fa-times]]
               " "
               (:name v)]
              [file-uppy (merge (:input-opts field)
                               {:uppy-opts (:uppy-opts field)
                                :on-change on-change-handler
                                :label "Upload"
                                :on-uppy #(reset! uppy-ref %)})]))
          
          (if @uppy-ref
            [:> UppyStatusBar {:uppy @uppy-ref}])

          [render-errors form-path (:path field)]])))))

(defn file-cmp [{:keys [label on-uppy on-change get-upload-promise uppy-opts] :as opts}]
  (let [uppy (-> (Uppy (clj->js (merge-with merge
                                            {:autoProceed true
                                             :restrictions {:maxNumberOfFiles 1
                                                            :minNumberOfFiles 1}} 
                                            uppy-opts))))]
    (if on-uppy (on-uppy uppy))
    (r/create-class
      {:component-did-mount
       (fn [cmp]
         (let [dn (rdom/dom-node cmp)]
           (-> uppy
               (.use UppyAwsS3
                     (clj->js {:getUploadParameters
                               (fn [file]
                                 (meteor/to-promise
                                   (go
                                     (let [res (<? (meteor/call "UploadURL" file))]
                                       (.setFileMeta uppy (.-id file) (clj->js {:assetId (str (:asset-id res))}))
                                       res))))}))
               (.use UppyFileInput
                     (clj->js {:target dn
                               :pretty true
                               :locale {:strings {:chooseFiles (or label "Upload")}}}))
              

               ;; (.use UppyImageEditor
               ;;       (clj->js {:quality 1
               ;;                 :cropperOptions {:viewMode 1
               ;;                                  :background false
               ;;                                  :autoCropArea 1
               ;;                                  :aspectRatio 1
               ;;                                  :responsive true}}))
               ;;
               
               (.on "upload-success"
                    (fn [file data]
                      (on-change {:name (aget file "name")
                                  :type (aget file "type")
                                  :asset-id (aget file "meta" "assetId")}))))))

       :component-will-unmount #(.close uppy)

       :reagent-render
       (fn []
         [:div (select-keys opts [:class])])})))


(defn render-field-file
  ([form-path field]
   (let [form-data-subs (rf/subscribe [::f/data form-path])
         is-valid? (rf/subscribe [::f/is-valid-path? form-path (:path field)])
         input-setter (value-setter form-path (:path field))
         on-change-handler (fn [v]
                             (input-setter v)
                             (when (not @is-valid?)
                               (rf/dispatch [::f/validate! form-path true])))
         uppy-ref (r/atom nil)]
     (fn []
       (let [form-data @form-data-subs
             v (get-in form-data (:path field))]
         [:div.mb-3
          (if (:label field) [:label.form-label (:label field)])
          (if-let [render-value (:render-value field)]
            [render-value v {:input-setter input-setter
                             :on-change-handler on-change-handler
                             :on-uppy #(reset! uppy-ref %)}]
            (if v
              (if (get-in field [:opts :image?])
                [:div.mb-2.d-flex
                 [:img.img-fluid.rounded.w-200px.mr-3
                  {:src (meteor/asset-url (:asset-id v))}]
                 [:div
                  [file-cmp (merge (:input-opts field)
                                   {:uppy-opts (:uppy-opts field)
                                    :on-change on-change-handler
                                    :label "Change"
                                    :on-uppy #(reset! uppy-ref %)})]]]
                [:div
                 [:button.btn.btn-circle.btn-outline-danger {:type "button"
                                                             :on-click #(input-setter nil)}
                  [:i.fas.fa-times]]
                 " "
                 (:name v)])
              [:div.d-flex.mb-2
               [:div.gray-box.mr-3
                {:style {:width (get-in field [:opts :image-width])
                         :height (get-in field [:opts :image-height])}}]
               [:div
                [file-cmp (merge (:input-opts field)
                                 {:uppy-opts (:uppy-opts field)
                                  :on-change on-change-handler
                                  :label "Upload"
                                  :on-uppy #(reset! uppy-ref %)})] 
                (when (:help-text field)
                  [:div.text-muted (:help-text field)])]]))
          (if @uppy-ref
            [:> UppyStatusBar {:uppy @uppy-ref}])

          [render-errors form-path (:path field)]])))))


(defn render-field-text
  ([form-path {:keys [input-group-append input-group-prepend] :as field}]
   (let [form-data-subs (rf/subscribe [::f/data form-path])
         form-data  @form-data-subs
         is-valid? @(rf/subscribe [::f/is-valid-path? form-path (:path field)])
         input-setter (setter form-path (:path field))
         on-change-handler (fn [e]
                             (input-setter e)
                             (when (not is-valid?)
                               (rf/dispatch [::f/validate! form-path true])))
         errors @(rf/subscribe [::f/errors-for-path form-path (:path field)])
         input-cmp [:input.form-control.login-input (merge {:value (get-in form-data (:path field))
                                                            :type "text"
                                                            :class (when (not is-valid?) "is-invalid")
                                                            :placeholder (:placeholder field)
                                                            :on-change on-change-handler
                                                            :on-blur #(rf/dispatch [::f/validate! form-path true])}
                                                           (:input-opts field))]]
     [:div.mb-3 {}
      (if (:label field) [:label.form-label (:label field)])
      (if (or input-group-prepend input-group-append)
        [:div.input-group
         (if input-group-prepend
           [:div.input-group-prepend
            [:span.input-group-text (:text input-group-prepend)]])
         input-cmp
         (if input-group-append
           [:div.input-group-append
            [:span.input-group-text (:text input-group-append)]])]
        input-cmp)
      
      [render-errors form-path (:path field)]])))


(defn render-field-phone
  ([form-path {:keys [input-group-append input-group-prepend] :as field}]
   (let [form-data-subs (rf/subscribe [::f/data form-path])
         form-data  @form-data-subs
         is-valid? @(rf/subscribe [::f/is-valid-path? form-path (:path field)])
         input-setter (value-setter form-path (:path field))
         on-change-handler (fn [e]
                             (input-setter e)
                             (when (not is-valid?)
                               (rf/dispatch [::f/validate! form-path true])))
         errors @(rf/subscribe [::f/errors-for-path form-path (:path field)])]
     [:div.mb-3
      (if (:label field) [:label.form-label (:label field)])
      [:> PhoneInput (merge {:value (get-in form-data (:path field))
                             :class (when (not is-valid?) "is-invalid")
                             :on-change on-change-handler
                             :on-blur #(rf/dispatch [::f/validate! form-path true])}
                            (:input-opts field))]
      [render-errors form-path (:path field)]])))

(defn render-field-float
  ([form-path {:keys [input-group-append input-group-prepend] :as field}]
   (let [form-data-subs (rf/subscribe [::f/data form-path])
         form-data  @form-data-subs
         is-valid? @(rf/subscribe [::f/is-valid-path? form-path (:path field)])
         input-setter (value-setter form-path (:path field))
         on-change-handler (fn [e]
                             (let [v (.. e -target -value)
                                   parsed-number (if (str/blank? v) nil (js/parseFloat v))]
                               (if-not (js/isNaN parsed-number) 
                                 (input-setter parsed-number)))
                             (when (not is-valid?)
                               (rf/dispatch [::f/validate! form-path true])))
         errors @(rf/subscribe [::f/errors-for-path form-path (:path field)])
         input-cmp [:input.form-control
                    (merge-with merge
                                {:default-value (get-in form-data (:path field))
                                 :type "number"
                                 :placeholder (:placeholder field)
                                 :class (when (not is-valid?) "is-invalid")
                                 :on-change on-change-handler
                                 :on-blur #(rf/dispatch [::f/validate! form-path true])}
                                (merge {:step "0.01"}
                                       (:input-opts field)))]]
     [:div.mb-3
      (if (:label field) [:label.form-label (:label field)])
      (if (or input-group-prepend input-group-append)
        [:div.input-group
         (if input-group-prepend
           [:div.input-group-prepend
            [:span.input-group-text (:text input-group-prepend)]])
         input-cmp
         (if input-group-append
           [:div.input-group-append
            [:span.input-group-text (:text input-group-append)]])]
        input-cmp)
      [render-errors form-path (:path field)]])))


(defn render-field-int
  ([form-path field]
   (let [form-data-subs (rf/subscribe [::f/data form-path])
         form-data  @form-data-subs
         is-valid? @(rf/subscribe [::f/is-valid-path? form-path (:path field)])
         input-setter (value-setter form-path (:path field))
         on-change-handler (fn [e]
                             (let [v (.. e -target -value)
                                   parsed-number (if (str/blank? v) nil (js/parseInt (.. e -target -value)))]
                               (input-setter parsed-number))
                             (when (not is-valid?)
                               (rf/dispatch [::f/validate! form-path true])))
         errors @(rf/subscribe [::f/errors-for-path form-path (:path field)])]
     [:div.mb-3
      (if (:label field) [:label.form-label (:label field)])
      [:input.form-control
       (merge-with merge
                   {:value (get-in form-data (:path field))
                    :type "number"
                    :class (when (not is-valid?) "is-invalid")
                    :placeholder (:placeholder field)
                    :on-change on-change-handler
                    :on-blur #(rf/dispatch [::f/validate! form-path true])}
                   (:input-opts field))]
      [render-errors form-path (:path field)]])))


(defn render-field-checkbox
  ([form-path field]
   (let [form-data-subs (rf/subscribe [::f/data form-path])
         form-data  @form-data-subs
         is-valid? @(rf/subscribe [::f/is-valid-path? form-path (:path field)])
         input-setter (value-setter form-path (:path field))
         on-change-handler (fn [e]
                             (input-setter (true? (.. e -target -checked)))
                             (when (not is-valid?)
                               (rf/dispatch [::f/validate! form-path true])))
         errors @(rf/subscribe [::f/errors-for-path form-path (:path field)])]
     [:div.mb-3
      [:div.form-check {:class [(when (get-in field [:opts :switch]) "form-switch")]}
       [:input.form-check-input (merge {:checked (true? (get-in form-data (:path field)))
                                        :class (when (not is-valid?) "is-invalid")
                                        :type "checkbox"
                                        :on-change on-change-handler
                                        :on-blur #(rf/dispatch [::f/validate! form-path true])}
                                       (:input-opts field))]
       [:label.form-check-label (:label field)]]
      [render-errors form-path (:path field)]])))


(defn render-field-radio
  ([form-path field]
   (let [form-data-subs (rf/subscribe [::f/data form-path])
         form-data  @form-data-subs
         is-valid? @(rf/subscribe [::f/is-valid-path? form-path (:path field)])
         input-setter (value-setter form-path (:path field))
         on-change-handler (fn [v e]
                             (input-setter v)
                             (when (not is-valid?)
                               (rf/dispatch [::f/validate! form-path true])))
         errors @(rf/subscribe [::f/errors-for-path form-path (:path field)])]
     [:div.mb-3
      (if (:label field) [:label.form-label (:label field)])
      [:div.flex-row.d-flex
       (doall
         (for [option (:options field)] ^{:key (:key option)}
           [:div.form-check.mr-3
            [:input.form-check-input.cursor-pointer
             (merge {:checked (= (:key option) (get-in form-data (:path field)))
                     :class (when (not is-valid?) "is-invalid")
                     :type "radio"
                     :value (:key option)
                     :on-change (partial on-change-handler (:key option)) 
                     :on-blur #(rf/dispatch [::f/validate! form-path true])}
                    (:input-opts field))]
            [:label.form-label.cursor-pointer {:on-click (partial on-change-handler (:key option))
                                                  :style {:margin-bottom 0}}
             (:label option)]]))]
      [render-errors form-path (:path field)]])))


(defn render-field-textarea
  ([form-path field]
   (let [form-data-subs (rf/subscribe [::f/data form-path])
         form-data  @form-data-subs
         is-valid? @(rf/subscribe [::f/is-valid-path? form-path (:path field)])
         input-setter (setter form-path (:path field))
         on-change-handler (fn [e]
                             (input-setter e)
                             (when (not is-valid?)
                               (rf/dispatch [::f/validate! form-path true])))
         errors @(rf/subscribe [::f/errors-for-path form-path (:path field)])]
     [:div.mb-3
      (if (:label field) [:label.form-label (:label field)])
      [:textarea.form-control
       (merge {:value (get-in form-data (:path field))
               :type "text"
               :class (when (not is-valid?) "is-invalid")
               :on-change on-change-handler
               :placeholder (:placeholder field)
               :on-blur #(rf/dispatch [::f/validate! form-path true])}
              (:input-opts field))]
      [render-errors form-path (:path field)]])))


(defn render-field-select
  [form-path field]
  (fn []
    (let [form-data-atom (rf/subscribe [::f/data form-path])
          form-data @form-data-atom
          is-valid? @(rf/subscribe [::f/is-valid-path? form-path (:path field)])
          select-setter (setter form-path (:path field))
          set-and-validate (fn [e]
                             (select-setter e)
                             (rf/dispatch [::f/validate! form-path true]))]
      [:div.mb-3
       (if (:label field) [:label.form-label (:label field)])
       [:select.form-select {:on-change set-and-validate 
                             :class (when (not is-valid?) "is-invalid")
                             :value (or (get-in form-data (:path field)) "")}
        [:option {:value ""}
         (or (:placeholder field)
             (str "-- Select --")) ]
        (for [option (:options field)] ^{:key (:key option)}
          [:option {:value (:key option)} (:label option)])]
       [render-errors form-path (:path field)]])))


(defn render-field-button-toggles
  [form-path field]
  (fn []
    (let [form-data-atom (rf/subscribe [::f/data form-path])
          form-data @form-data-atom
          current-value (get-in form-data (:path field))
          is-valid? @(rf/subscribe [::f/is-valid-path? form-path (:path field)])
          set-and-validate (fn [v]
                             (rf/dispatch-sync [::f/set! form-path (:path field) v])
                             (rf/dispatch [::f/validate! form-path true]))]
      [:div.mb-3.text-center {:class (when (not is-valid?) "is-invalid")}
       (for [option (:options field)] ^{:key (:key option)}
         [:label.btn.btn-outline-primary.mx-2
          {:on-click #(set-and-validate (:key option))
           :style {:padding "10px 35px"}
           :class (if (= (:key option) current-value) "active")}
          (:label option)])
       [render-errors form-path (:path field)]])))


(defn render-field-date
  ([form-path field]
   (let [form-data-subs (rf/subscribe [::f/data form-path])
         form-data  @form-data-subs
         is-valid? @(rf/subscribe [::f/is-valid-path? form-path (:path field)])
         on-change-handler (fn [date]
                             (rf/dispatch-sync [::f/set! form-path (:path field) date])
                             (when (not is-valid?)
                               (rf/dispatch [::f/validate! form-path true])))
         errors @(rf/subscribe [::f/errors-for-path form-path (:path field)])]
     [:div.mb-3 {}
      (if (:label field) [:label.form-label (:label field)])
      [:div
      [:> date-picker (merge (:input-opts field)
                             {:on-change on-change-handler
                              :selected (get-in form-data (:path field))
                              :placeholderText (:placeholder field)
                              :className (str " form-control "
                                              (when (not is-valid?) "is-invalid"))}
                             (if (= (:type field) :datetime)
                               {:showTimeSelect true
                                :timeIntervals 15
                                ;:timeCaption="time"
                                :dateFormat "MM/dd/yyyy h:mm aa"}))] 
       ]
      
      
      ;; [:input.form-control (merge (:input-opts field)
      ;;                             {:value (get-in form-data (:path field))
      ;;                              :type :date
      ;;                              :on-change on-change-handler
      ;;                              :on-blur #(rf/dispatch [::f/validate! form-path true])})]
      [render-errors form-path (:path field)]])))


(defn render-row [form-path {:keys [fields]}]
  (let [col-class (case (count fields)
                    0 "col-sm-12"
                    1 "col-sm-12"
                    2 "col-sm-6"
                    3 "col-sm-4"
                    4 "col-sm-3")]
    [:div.row
     (for [field fields] ^{:key (:path field)}
       [:div.col

        [render-field form-path field]])]))


(defn render-field [form-path field]
  (case (:type field)
    :date [render-field-date form-path field]
    :datetime [render-field-date form-path field]
    :textarea [render-field-textarea form-path field]
    :float [render-field-float form-path field]
    :int [render-field-int form-path field]
    :select [render-field-select form-path field]
    :button-toggles [render-field-button-toggles form-path field]
    :row [render-row form-path field]
    :checkbox [render-field-checkbox form-path field]
    :radio [render-field-radio form-path field]
    :uppy-dashboard [render-uppy-file form-path field]
    :file [render-field-file form-path field]
    :phone [render-field-phone form-path field]
    :custom [(:render field) form-path field]

    [render-field-text form-path field]))


(defn render-fields [{:keys [form-path fields]}]
  (let []
    [:div
      (for [[idx field] (map-indexed vector (filter some? fields))] ^{:key idx}
        [:span 
         (when field
           [render-field form-path field])])]))

(defn get-alignment [field]
  (or (get-in field [:opts :alignment])
      (when (contains? #{:float :int :percentage :currency} (:type field)) "text-align-right")))

(defn render-table [{:keys [fields items on-delete on-edit label actions]}]
  (cond
    (nil? items)
    [:i.fas.fa-spinner.fa-spin.fa-2x]

    (= (count items) 0)
    [:p.text-center "There are currently no " (or label "items")]

    :else
    [:table.table
     [:thead
      [:tr
       (for [field fields]
         (let [alignment (get-alignment field)]
           [:th {:scope "col"
                 :key (:path field)
                 :class [alignment]}
            (:label field)]))
       [:th.table-actions "Actions"]]]
     [:tbody
      (for [[idx item] (map-indexed vector items)]
        [:tr {:scope "row"
              :key (:_id item)}
         (for [field fields]
           (let [alignment (get-alignment field) ]
             [:td {:class [alignment]
                   :key (:path field)}
              (let [v (get-in item (:path field))]
                (case (:type field)
                  :date (u/date-format v)

                  :datetime (u/datetime-format v)

                  :percentage (str v "%")
                  :currency (u/format-currency v)

                  :custom
                  [(:render field) item]

                  :link [:a {:href ((:href field) v)}
                         (str v)]

                  (str v)))]))
         [:td.table-actions
          (when on-edit
            [:button.btn.btn-primary.btn-rounded.me-1.btn-sm {:type :button :on-click #(on-edit item idx)}
             [:i.fas.fa-pen]])
          (when on-delete
            [:button.btn.btn-danger.btn-rounded.btn-sm {:type :button :on-click #(on-delete item idx)}
             [:i.fas.fa-trash]])
          (when actions
            [actions item])]])]]))


(ns vp.macros)

(defmacro defmet [& body]
  (let [[fn-name fn-args & fn-body] body
        method-name (str *ns* "." fn-name)]
    `(do
       (defn ~(vary-meta fn-name assoc :export true) ~fn-args
         (if (.-isServer js/Meteor)
           (do
             ~@fn-body)
           (cljs.core.async/go
             (let [response# (~'<! (vp.meteor/call ~method-name (vp.meteor/t-write ~fn-args)))]
               (vp.meteor/t-read response#)))))
       (when (.-isServer js/Meteor)
         (js/Meteor.methods (~'clj->js {~method-name
                                        (fn [args#]
                                          (vp.meteor/to-promise
                                            (cljs.core.async/go
                                              (vp.meteor/t-write (~'<! (apply ~fn-name (vp.meteor/t-read args#)))))))}))))))


(defmacro <m! [& body]
  `(let [chan# (cljs.core.async/chan)]
     (vp.meteor/bind-env chan# #(do ~@body))
     (~'<! chan#)))

(defmacro <m!! [& body]
  `(let [chan# (cljs.core.async/chan)]
     (vp.meteor/bind-env chan# #(do ~@body))
     (~'<! (~'<! chan#))))


(defmacro <? [body]
  `(let [r# (~'<! ~body)]
     (cond 
       (and r#
            (= (.-name r#) "Error"))
       (do
         (js/console.error "err" r#)
         (throw r#))

       :else
       r#)))

(defmacro <p? [body]
  `(let [r# (~'<p! ~body)]
     (cond 
       (and r#
            (= (.-name r#) "Error"))
       (do
         (js/console.error r#)
         (throw r#))

       :else
       r#)))




(ns vp.collections
  (:require
   [re-frame.core :as re-frame]
   [vp.db :as db]
   ["meteor/mongo" :refer [Mongo]]
   ["meteor/meteor" :refer [Meteor]]
   [cljs.core.async :refer [go]]
   [vp.macros :refer-macros [defmet]]))

;; (defonce LinksCollection (new (.-Collection Mongo) "links"))
;; (defonce EventStoreCollection (new (.-Collection Mongo) "event-store"))

(defonce collections
  {:users (.-users Meteor)
   :plans (new (.-Collection Mongo) "plans")
   :contracts (new (.-Collection Mongo) "contracts")
   :payouts (new (.-Collection Mongo) "payouts")
   :countries (new (.-Collection Mongo) "countries")
   :currencies (new (.-Collection Mongo) "currencies")})

(if (.-isClient Meteor)
  (js/console.log (clj->js collections)))

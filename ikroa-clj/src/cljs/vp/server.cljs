(ns vp.server
  (:require
   [vp.config :as config]
   ["meteor/mongo" :refer [Mongo]]
   ["meteor/meteor" :refer [Meteor]]
   ["meteor/tracker" :refer [Tracker]]
   [cljs.core.async :refer [go]]
   ["meteor/fetch" :refer [fetch]]
   [cljs.core.async.interop :refer-macros [<p!]]
   [vp.collections :refer [collections]]
   [vp.services :refer [myfunction]]
   [vp.macros :refer-macros [defmet <m!]]
   [cljs.core.async :refer [go]]
   [vp.meteor :as meteor]
   [vp.rapyd :as rapyd]
   [camel-snake-kebab.core :as csk]
   [camel-snake-kebab.extras :as cske]
   ))

;; (.deny
;;   ^js (:plans collections)
;;   (clj->js
;;     {:insert (fn [userId doc]
;;                true)}))

(defn ^:export init []
  (println "init server")
  (when (zero? (meteor/cnt :countries {}))
    (go
      (let [countries (-> (rapyd/get-countries) <! :data)]
        (doseq [country countries]
          (meteor/cre :countries country)))))
  (when (zero? (meteor/cnt :currencies {}))
    (go
      (let [countries (-> (rapyd/get-currencies) <! :data)]
        (doseq [country countries]
          (meteor/cre :currencies country)))))

  (-> ^js (meteor/get-coll :users)
      (.-after)
      (.update (fn [user-id doc-aux]
                 (let [doc (js->clj doc-aux :keywordize-keys true)]
                   (when (and (get-in doc [:profile :is-company])
                              (not (contains? doc :company-wallet)))
                     (let [user doc
                           company (get-in user [:profile :company])
                           data (-> {:first-name (:name company)
                                     :ewallet-reference-id (str "meteor_" (:_id user)) 
                                     :type "company"
                                     :contact (:contact company)}
                                    (assoc-in [:contact :contact-type] "business")
                                    (assoc-in [:contact :business-details :industry-category] "company"))]
                       (go
                         (let [wallet (-> (rapyd/create-company-wallet (cske/transform-keys csk/->snake_case_string data))
                                          (<!))]
                           (meteor/upd :users (:_id user) {:$set {:company-wallet wallet}})))))))))
  (-> ^js (meteor/get-coll :contracts)
      (.-after)
      (.insert (fn [user-id doc-aux]
                 (let [doc (js->clj doc-aux :keywordize-keys true)
                       user (meteor/user)
                       company-user (meteor/one :users (get-in doc [:plan :user-id]))]
                   (go
                     (let [checkout-page (<! (rapyd/create-checkout-page
                                               {:complete_payment_url "http://ikroa.com/#/user/contracts",
                                                :amount (:investment doc) ,
                                                :cardholder_preferred_currency true,
                                                :ewallet (get-in company-user [:company-wallet :data :id]),
                                                :currency "USD",
                                                :language "en",
                                                :payment_method_types_exclude [],
                                                :country (get-in user [:profile :country]),
                                                :metadata {:contract_id (:_id doc)}
                                                :error_payment_url "http://ikroa.com/#/user/contracts"}))]
                       (meteor/upd :contracts (:_id doc) {:$set {:checkout-page checkout-page}})))))))

  (-> ^js (meteor/get-coll :contracts)
      (.-after)
      (.find (fn [user-id selector options cursor]
                 (let [docs (js->clj (.fetch ^js cursor) :keywordize-keys true)
                       user (meteor/user)]
                   (go
                     (doseq [contract docs]
                       (when (not= (get-in contract [:payment :status]) "CLO")
                         (let [res (<! (rapyd/get-checkout-page (get-in contract [:checkout-page :data :id])))]
                           (meteor/upd :contracts (:_id contract) {:$set (merge {:payment (get-in res [:data :payment])}
                                                                                (when (= (get-in res [:data :payment :status]) "CLO")
                                                                                  {:status "investment-active"}))})))))))))

  ;; (println
  ;;   (macroexpand
  ;;     '(defmet myfunction [{:keys [n1 n2]}]
  ;;        (let [ret (+ arg1 arg2)]
  ;;          (println ret)
  ;;          ret))))

  ;; (println
  ;;   (macroexpand
  ;;     '(<m! (-> LinksCollection
  ;;                          (.find (clj->js {:url {:$exists true}})
  ;;                                 #js {:skip 1 :limit 2})
  ;;                          (.fetch)
  ;;                          count
  ;;                          inc))))

  ;; (go
  ;;   (println (<! (myfunction 1 4))))
  ;; (.methods Meteor
  ;;           (clj->js {:sum (partial +)}))
  )


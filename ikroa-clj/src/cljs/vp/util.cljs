(ns vp.util
  (:require [cljs-time.core :as t]
            [cljs-time.format :as f]
            [cljs-time.coerce :as c]
            [clojure.string :as str]
            ["shortid" :as shortid]
            ["numeral" :as numeral]
            ["remove-accents" :refer [remove] :rename {remove remove-accents}]
            ["moment" :as moment]
            [cljs.pprint :as pp]))


(defonce default-formatter (f/formatter "MMM dd, yyyy"))
(defonce default-time-formatter (f/formatter "hh:mm a"))
(defonce default-datetime-formatter (f/formatter "MMM dd, yyyy hh:mm a"))

(def cl-format pp/cl-format)

(defn get-cljs-time-date [v]
  (let [dateobj (cond
                  (int? v) (c/from-long v)
                  (= (type v) js/Date) (c/from-long (.getTime v))
                  :else v)]
    (t/to-default-time-zone dateobj)))

(defn date-format
  ([v] (date-format v default-formatter))
  ([v formatter]
   (if v
     (let [date (get-cljs-time-date v)
           formatter-aux (cond
                           (keyword? formatter) (f/formatters formatter)
                           (string? formatter) (f/formatter formatter)
                           :else formatter)]
       (f/unparse formatter-aux date)))))

(defn time-format [v]
  (if v
    (let [date (get-cljs-time-date v)]
      (f/unparse default-time-formatter date))))

(defn datetime-format [v]
  (if v
    (let [date (get-cljs-time-date v)]
      (f/unparse default-datetime-formatter date))))

(defn datetime-format2 [v]
  (-> (moment v)
      (js-invoke "tz" "America/Texas")
      (js-invoke "format" "lll")))

(defn format-currency [s]
  (-> (numeral s)
      (.format "$0,0.00")))


(defn format-number [s]
  (-> (numeral s)
      (.format "0,0")))

(defn format-decimal [s]
  (-> (numeral s)
      (.format "0,0.00")))


(defn vec-remove [pos coll]
  (vec (concat (subvec coll 0 pos) (subvec coll (inc pos)))))


(defn dissoc-in [m path]
  (if (nil? (butlast path))
    (dissoc m (last path))
    (update-in m (butlast path) dissoc (last path))))

(defn index-by-id [items]
  (->> items
       (map #(vector (str (:_id %)) %))
       (into {})))

(defn indexes-where
  [pred? coll]
  (keep-indexed #(when (pred? %2) %1) coll))

(defn update-where
  [v pred? f & args]
  (if-let [i (first (indexes-where pred? v))]
    (assoc v i (apply f (v i) args))
    v))

(defn gen-shortid []
  (.generate shortid))

(defn distinct-by [f items]
  (into (sorted-set-by #(compare (f %1) (f %2))) items))

(defn prevdef [f]
  (fn [ev]
    (.preventDefault ev)
    (f ev)))

(defn md5 [s]
  (js/md5 s))

(defn search-compare [s1 s2]
  (and s1 s2 (str/includes?
               (-> s1 str/trim str/lower-case remove-accents)
               (-> s2 str/trim str/lower-case remove-accents))))


(defn search-filter [items ks text]
  (filter
    (fn [item]
      (some
        (fn [k]
          (let [v (k item)]
            (search-compare v text)))
        ks))
    items))


(def email-regex
  #"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")

(defn email? [s]
  (boolean
   (and (string? s)
        (re-matches email-regex s))))

(defn text->emails [text]
  (when (string? text)
    (->> (str/split text #"[ ,\n]")
         (map str/trim)
         (filterv email?))))

;(println "text->emails" (count (text->emails  "facundo.navarro@intive.com, mariangeles.noseda@intive.com, kannan.janardhanan@intive.com")) )


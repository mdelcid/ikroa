(ns vp.meteor
  (:require
    [cognitect.transit :as t]
    [re-frame.core :as rf]  
    [cljs.core.async :as a :refer [go]]
    ["meteor/mongo" :refer [Mongo]]
    ["meteor/tracker" :refer [Tracker]]
    ["meteor/meteor" :refer [Meteor]]
    [vp.collections :refer [collections]]
    [re-chain.core :as chain]
    [cognitect.transit :as t]))

(def writer (t/writer :json))
(def reader (t/reader :json))


(defn t-read [v] (t/read reader v))

(defn t-write [v] (t/write writer v))

(defn call [method-name args]
  (let [chan (a/chan)]
    (.call Meteor (name method-name) args
           (fn [err response]
             (go
               (if err
                 (js/console.error err)
                 (a/put! chan response))
               (a/close! chan))))
    chan))


(defn to-promise [c]
  (js/Promise.
   (fn [res rej]
     (go
       (try
         (let [r (clj->js (<! c))]
           (res r))
         (catch :default e
           (rej (str e))))))))

(def bind-env
  (js/Meteor.bindEnvironment
    (fn [chan f]
      (let [res (f)]
        (go
          (a/put! chan res)
          (a/close! chan))))))

(defn asset-url [asset-id]
  )

(defn get-coll [coll]
  (if (keyword? coll)
    (coll collections)
    coll))

(defn convert-query [query]
  (if (map? query)
    (clj->js query)
    #js {:_id query}))

(defn handle-cb [chan err res]
  (go
    (a/put! chan (js->clj [err res] :keywordize-keys true))
    (a/close! chan)))

(defn cre [coll doc]
  (let [chan (a/chan)
        do-f (fn []
               (.insert ^js (get-coll coll) (clj->js doc) (partial handle-cb chan)))]
    (if (.-isServer ^js Meteor)
      (bind-env chan do-f)
      (do-f))
    chan))

;; (defn cre-many [coll docs]
;;   (let [chan (a/chan)]
;;     (.insertMany (-> ^js (get-coll coll) (.rawCollection)) (clj->js doc) (partial handle-cb chan)) 
;;     chan))

(defn upd [coll query diff & {:keys [opts] :or {opts {}}}]
  (let [chan (a/chan)
        do-f (fn []
               (.update ^js (get-coll coll) (convert-query query) (clj->js diff) (clj->js opts) (partial handle-cb chan))
               true)]
    (if (.-isServer ^js Meteor)
      (bind-env chan do-f)
      (do-f))
    chan))

(defn del [coll query]
  (let [chan (a/chan)]
    (.remove ^js (get-coll coll) (clj->js query) (partial handle-cb chan)) 
    chan))

(defn upsert [coll doc ]
  (if-let [id (:_id doc)]
    (upd coll #js {:_id id} (clj->js {:$set doc}))
    (cre coll (clj->js doc))))

(defn lst
  ([coll] (lst coll {}))
  ([coll query]
   (-> ^js (get-coll coll) (.find (clj->js query)) (.fetch) (js->clj :keywordize-keys true))))

(defn cnt
  ([coll] (cnt coll {}))
  ([coll query]
   (-> ^js (get-coll coll) (.find (clj->js query)) (.count))))

(defn one
  [coll query]
   (-> ^js (get-coll coll)
       (.findOne (if (map? query)
                   (clj->js query)
                   #js {:_id query}))
       (js->clj :keywordize-keys true)))

(defn user []
  (js->clj (.user Meteor) :keywordize-keys true))

(defn user-id []
  (.userId Meteor))

(defn full-name [user-aux]
  (let [user (if (string? user-aux)
               (one :users user-aux)
               user-aux)]
    (str
      (get-in user [:profile :first-name])
      " "
      (get-in user [:profile :last-name]))))

(defn email [user]
  (-> user :emails first :address))
 
(defn logout []
  (.logout Meteor))

(rf/reg-fx
  :meteor
  (fn [[op & all-args]]
    (let [{:keys [cb-key args]} (if (vector? (last all-args))
                                  {:cb-key (last all-args)
                                   :args (butlast all-args)}
                                  {:cb-key [:meteor-result]
                                   :args all-args})
          f (case op
              :cre cre
              :upd upd
              :upsert upsert
              :del del)]
      (go (rf/dispatch (into cb-key (<! (apply f args))))))))



(def default-links [{:effect-present? (fn [effects]
                                        (:meteor effects))
                     :get-dispatch    (fn [effects]
                                        (let [cb-key (-> effects :meteor last)]
                                          (when (vector? cb-key)
                                            cb-key)))
                     :set-dispatch    (fn [effects dispatch]
                                        (update effects :meteor #(into % [dispatch])))}])

(chain/configure! default-links)  

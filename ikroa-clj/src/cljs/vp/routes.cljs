(ns vp.routes
  (:require-macros [secretary.core :refer [defroute]])
  (:import [goog History]
           [goog.history EventType])
  (:require
   [secretary.core :as secretary]
   [goog.events :as gevents]
   [re-frame.core :as rf]
   ))

(defn hook-browser-navigation! []
  (doto (History.)
    (gevents/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token ^js event))))
    (.setEnabled true)))

(defn app-routes []
  (secretary/set-config! :prefix "#")
  ;; --------------------
  ;; define routes here
  (defroute home-page "/" []
    (rf/dispatch [:set-active-panel :home-page]))

  (defroute profile-page "/profile" []
    (rf/dispatch [:init-profile-page])
    (rf/dispatch [:set-active-panel :profile-page]))

  (defroute company-contracts-page "/company/contracts" []
    (rf/dispatch [:set-active-panel :company-contracts]))

  (defroute company-plans-page "/company/plans" []
    (rf/dispatch [:set-active-panel :company-plans]))

  (defroute company-payouts-page "/company/payouts" []
    (rf/dispatch [:set-active-panel :company-payouts]))

  (defroute user-contracts-page "/user/contracts" []
    (rf/dispatch [:set-active-panel :user-contracts]))

  (defroute user-payments-page "/user/payments" []
    (rf/dispatch [:set-active-panel :user-payments]))


  ;; --------------------
  (hook-browser-navigation!))

(defn goto [url]
  (aset js/window "location" "href" url))


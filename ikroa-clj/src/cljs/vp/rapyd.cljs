(ns vp.rapyd
  (:require
    [cljs.core.async :as a :refer [go]]
    [vp.macros :refer-macros [defmet <m! <m!!]]
    ["meteor/fetch" :refer [fetch]]
    [cljs.core.async.interop :refer-macros [<p!]]))

(def secret-key "cfa6dc18129d96c404b540dc4a2d077b6c4093234e94a68f3b9b1a5c890d838e3e4501f6d5f8d2db")
(def access-key "D301146F3586152707B1")
(def base-url "https://sandboxapi.rapyd.net")

(defn gensalt [n]
  (let [charseq (map char (concat
                            (range 48 58)     ; 0-9
                            (range 97 123)))] ; 0-z
    (apply str (take n (repeatedly #(rand-nth charseq))))))


(def methods-map {:get "GET" :post "POST"})

(defn req [method uri & [body]]
  (go
    (let [body-json-str (when body (js/JSON.stringify (clj->js body)))
          {:keys [salt signature timestamp] :as sigres}
          (js->clj (js/signature
                     (name method)
                     uri
                     (or body-json-str ""))
                   :keywordize-keys true)
          res (<m!! (go (-> (fetch (str base-url uri)
                                   (clj->js (merge {:method (method methods-map)
                                                    :headers {"access_key" access-key
                                                              "Content-Type" "application/json"
                                                              "salt" salt
                                                              "timestamp" (str timestamp)
                                                              "signature" signature}}
                                                   (when body-json-str
                                                     {:body body-json-str}))))
                              (<p!) (.json) (<p!) (js->clj :keywordize-keys true))))]
      res)))

;{:id 379, :name "Albania", :iso_alpha2 "AL", :iso_alpha3 "ALB", :currency_code "ALL", :currency_name "Albanian lek", :currency_sign "L", :phone_code "355"}

(defn get-countries []
  (req :get "/v1/data/currencies"))

(defn get-currencies []
  (req :get "/v1/data/currencies"))

(defn get-wallet [id]
  (req :get (str "/v1/user/" id)))

(defn create-company-wallet [data]
  (req :post "/v1/user" data))

(defn create-checkout-page [data]
  (req :post "/v1/checkout" data))

(defn get-checkout-page [id]
  (req :get (str "/v1/checkout/" id)))


;(req :get "/v1/data/countries")
;(mac/hash "abc" {:key secret-key :alg :hmac+sha256})


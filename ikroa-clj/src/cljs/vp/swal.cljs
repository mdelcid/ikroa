(ns vp.swal
  (:require
    [re-frame.core :as rf]
    ["sweetalert2/dist/sweetalert2" :as swal]))


(defn fire [args]
  (.fire swal (clj->js args)))

(defn confirm [params cb]
  (-> swal
      (.fire (clj->js (merge 
                        {:icon "warning"
                         :title "Are you sure?"
                         :showCancelButton true
                         :confirmButtonColor "#3085d6"
                         :cancelButtonColor "#d33"
                         :confirmButtonText "Yes, delete it!"}
                        params)))
      (.then (fn [result]
               (when (aget result "value")
                 (if (vector? cb)
                   (rf/dispatch cb)
                   (cb)))))))


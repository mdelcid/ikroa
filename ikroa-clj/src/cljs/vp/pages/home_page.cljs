(ns vp.pages.home-page
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :as rf]  
            [cljs.core.async :refer [go]]
            [forms.re-frame :as f]
            [forms.validator :as v]
            [clojure.spec.alpha :as s]
            [vp.util :as u]
            [vp.crud :as crud]
            [vp.meteor :as meteor]
            [vp.routes :as routes]
            [vp.services :as services]
            [vp.collections :refer [collections]]
            [re-chain.core :as chain :refer [reg-chain reg-chain-named]]
            ["meteor/react-meteor-data" :refer [useTracker]]
            ["reactstrap" :as rs]))


(defn company-modal [company]
  [:> rs/Modal {:isOpen true :toggle #(rf/dispatch [:close-modal])}
   [:> rs/ModalBody
    [:h3 (:name company)]
    [:p (:description company)]

    [:a {:href (:website company) :target "_blank"}
     (:website company)]]
   [:> rs/ModalFooter
    [:button.btn.btn-primary {:type "button"
                              :on-click #(rf/dispatch [:close-modal])}
     "OK"]]])

(rf/reg-event-fx
  ::show-company
  (fn [{:keys [db]} [_ company]]
    {:dispatch [:show-modal [company-modal company]]}))



(def investment-form-path [::investment-form])
(defn investment-form []
  (let [form-path investment-form-path
        form (rf/subscribe (into [:db] form-path))
        data (rf/subscribe [::f/data form-path])
        submitting? (rf/subscribe [:db :submitting? form-path])
        on-submit (fn [event]
                    (.preventDefault event)
                    (rf/dispatch-sync [::f/validate! form-path])
                    (if @(rf/subscribe [::f/is-valid? form-path])
                      (rf/dispatch [::start-application @data])
                      #_(js/alert "Please fix the validation errors")))]
    (fn []
      (let [plan (:plan @data)]
        [:> rs/Modal {:isOpen true :toggle #(rf/dispatch [:close-modal])}
         [:form {:on-submit on-submit}
          [:> rs/ModalHeader
           "Investment Application Form"]
          [:> rs/ModalBody
           [:h3 (get-in @data [:plan :name])]
           [:div (get-in @data [:plan :description])]

           [:hr]

           [:div.d-flex.flex-row.justify-content-center
            [:div.w-50
             [crud/render-fields
              {:form-path form-path
               :fields [{:path [:investment]
                         :type :float
                         :input-opts {:style {:text-align "right"}
                                      :min (:minimum-investment plan)}
                         :input-group-prepend {:text "$"}
                         :label "Investment"}]}]]]
           [:div.px-5.my-2
            (when (> (:investment @data) 0)
              [:div.text-center 
               [:div.text-muted
                "With an investment of " (u/format-currency (:investment @data)) ", you'd be earning:"]
               [:div.my-2
                [:strong.fs-4 (u/format-currency (* (:investment @data) (get-in @data [:plan :interest]) 0.01))]
                [:strong                 "/month!"]
]])]]
          [:> rs/ModalFooter
           (if @submitting?
             [:button.btn.btn-primary {:type "button"}
              [:i.fas.fa-spinner.fa-spin]]
             [:<>
              [:button.btn.btn-link {:type "button"
                                     :on-click #(rf/dispatch [:close-modal])}
               "Cancel"]
              [:button.btn.btn-primary
               {:type "submit"}
               "Start Application"]])]]]))))


(rf/reg-event-fx
  ::show-investment
  (fn [{:keys [db]} [_ plan]]
    (if-not (empty? (:profile (meteor/user)))
      (let [validator (v/validator
                        (crud/to-validator crud/validations
                                           {:investment [:not-null :valid-number]}))
            form (f/constructor validator investment-form-path)]
        (form {:plan plan
               :investment (:minimum-investment plan)})
        {:dispatch [:show-modal [investment-form]]})
     {:dispatch-n [[:toastr :error "Please complet your profile before investing"]
                   [:redirect (routes/profile-page)]]})))

(reg-chain
 ::start-application
 (fn [{:keys [db]} [_ data]]
   {:meteor [:cre :contracts (assoc data
                                    :user-id (meteor/user-id)
                                    :status "pending-approval")]})
 (fn [{:keys [db]} [_ data err res]]
   (if err
     {:dispatch [:toastr :error (.-reason err)]}
     {:dispatch-n [[:toastr :success "Contract application successfully started!"]
                   [:redirect (routes/user-contracts-page)]
                   [:close-modal]]})))

(defn plan-card [plan]
  (let [company (useTracker #(get-in (meteor/one :users {:_id (:user-id plan)}) [:profile :company]))]
    [:div.plan-card-container
     [:div.card.plan-card
      [:div.card-body
       [:h5.card-title
        [:i.fas.fa-file-invoice-dollar]
        " "
        (:name plan)]
       [:div.card-text (:tagline plan)]
       [:div.bottom
        [:div.card-text.text-success.mb-2
         [:span.fs-4
          [:strong (:interest plan) "%"] " Interest" ]
         
         [:div
          (if (or (zero? (:minimum-investment plan)) (nil? (:minimum-investment plan)))
            [:small.text-muted "No minimum required"]  
            [:small.text-muted (u/format-currency (:minimum-investment plan)) " Minimum entry"])]]
        [:button.btn.btn-secondary.subscribe-button.mb-2 {:on-click #(rf/dispatch [::show-investment plan])
                                                          :type "button"}
         "Invest"]
        [:div
         [:small.text-muted "Offered by "
          [:a {:href "#" :on-click (u/prevdef #(rf/dispatch [::show-company company]))}
           (:name company)]]]]]]]))

(defn contracts-grid []
  (let [plans (useTracker #(meteor/lst :plans {}))]
    [:div.row.row-cols-md-2.row-cols-lg-4
     (doall
       (for [plan plans] ^{:key (:_id plan)}
         [:f> plan-card plan]))]))

(defn home-page []
  (let [page-params (rf/subscribe [:db :page-params])
        plans (rf/subscribe [:db :plans])]
    (fn []
      [:div.container.pt-4
       [:h2 "Find your best deal!"]
       [:f> contracts-grid]])))





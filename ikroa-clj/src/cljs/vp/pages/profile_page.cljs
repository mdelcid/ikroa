(ns vp.pages.profile-page
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :as rf]  
            [cljs.core.async :refer [go]]
            [forms.re-frame :as f]
            [forms.validator :as v]
            [clojure.spec.alpha :as s]
            [vp.util :as u]
            [vp.crud :as crud]
            [vp.meteor :as meteor]
            [vp.routes :as routes]
            [vp.services :as services]
            [vp.collections :refer [collections]]
            [re-chain.core :as chain :refer [reg-chain reg-chain-named]]
            ["meteor/tracker" :refer [Tracker]]
            ["meteor/react-meteor-data" :refer [useTracker]]
            ["reactstrap" :as rs]))


(def user-profile-form-path [::user-profile-form])
(declare user-profile-form)

(def company-profile-form-path [::company-profile-form])
(declare company-profile-form)

(.autorun
  Tracker
  (fn []
    (let [profile (:profile (meteor/user))
          user-validator (v/validator
                           (crud/to-validator crud/validations
                                              {:first-name [:not-empty]
                                               :last-name [:not-empty]
                                               :country [:not-empty]
                                               :phone-number [:is-phone]
                                               }))
          company-validator (v/validator
                              (crud/to-validator crud/validations
                                                 {:company.name [:not-empty]
                                                  :company.description [:not-empty]
                                                  :company.website [:not-empty]
                                                  :company.contact.first-name [:not-empty]
                                                  :company.contact.last-name [:not-empty]
                                                  :company.contact.email [:not-empty]
                                                  :company.contact.phone [:is-phone]
                                                  :company.contact.business-details.address.name [:not-empty]
                                                  :company.contact.business-details.address.line-1 [:not-empty]
                                                  :company.contact.business-details.address.country [:not-empty]
                                                  :company.contact.business-details.address.phone-number [:is-phone]
                                                  }))
          user-form (f/constructor user-validator user-profile-form-path)
          company-form (f/constructor company-validator company-profile-form-path)]
      (user-form (or profile {}))
      (company-form (or profile {})))))


(rf/reg-event-fx
  :init-profile-page
  (fn
    [{:keys [db]} [_]]
    (let []
      {})))


(reg-chain
 ::upsert-profile
 (fn [{:keys [db]} [_ data]]
   {:meteor [:upd :users (:_id (meteor/user)) {:$set {:profile data}}]})
 (fn [{:keys [db]} [_ data err res]]
   (if err
     {:dispatch [:toastr :error (:reason err)]}
     {:dispatch-n [[:toastr :success "Profile updated!"]
                   [:close-modal]]})))

(defn user-profile-form []
  (let [form-path user-profile-form-path
        countries (useTracker #(meteor/lst :countries {:iso_alpha2 "US"}))
        form @(rf/subscribe (into [:db] form-path))
        data @(rf/subscribe [::f/data form-path])
        submitting? @(rf/subscribe [:db :submitting? form-path])
        on-submit (fn [event]
                    (.preventDefault event)
                    (rf/dispatch-sync [::f/validate! form-path])
                    (if @(rf/subscribe [::f/is-valid? form-path])
                      (rf/dispatch [::upsert-profile data])
                      #_(js/alert "Please fix the validation errors")))]
    [:form {:on-submit on-submit}
     (when (not (empty? countries))
       [:div.card.mb-3
        [:div.card-body
         [:h4.card-title "User Profile"]
         [crud/render-fields
          {:form-path form-path
           :fields [{:type :row
                     :fields [{:path [:first-name]
                               :label "First Name"}
                              {:path [:last-name]
                               :label "Last Name"}]}
                    {:path [:country]
                     :type :select
                     :label "Country"
                     :input-opts {:placeholder "Country"}
                     :options (->> countries
                                   (map #(assoc {} :key (:iso_alpha2 %) :label (:name %)))
                                   doall
                                   )}
                    {:type :row
                     :fields [{:path [:phone-number]
                               :type :phone
                               :label "Phone Number"}
                              {:path [:birthdate]
                               :type :date
                               :label "Birthdate"}]}]}]
         (if submitting?
           [:button.btn.btn-primary {:type "button"}
            [:i.fas.fa-spinner.fa-spin]]
           [:button.btn.btn-primary
            {:type "submit"}
            "Save"])]])]))

(defn company-profile-form []
  (let [form-path company-profile-form-path
        countries (useTracker #(meteor/lst :countries {:iso_alpha2 "US"}))
        user (useTracker #(meteor/user))
        form @(rf/subscribe (into [:db] form-path))
        data @(rf/subscribe [::f/data form-path])
        submitting? @(rf/subscribe [:db :submitting? form-path])
        on-submit (fn [event]
                    (.preventDefault event)
                    (rf/dispatch-sync [::f/validate! form-path])
                    (if @(rf/subscribe [::f/is-valid? form-path])
                      (rf/dispatch [::upsert-profile data])
                      #_(js/alert "Please fix the validation errors")))]
    [:form {:on-submit on-submit}
     (when-not (empty? countries)
       [:div.card.mb-3
        [:div.card-body
         [:h4.card-title "Company Profile"]
         (cond
           (empty? (:profile user))
           [:p "Interested in creating a company profile and offer your services in ikroa.com? Please complete the basic User Profile first."]

           (not (:is-company data))
           [:div.text-centered
            [:p "Interested in creating a company profile and offer your services in ikroa.com?" ] 
[crud/render-field form-path {:type :checkbox
                                           :path [:is-company]
                                           :label  "Create company profile"
                                           :opts {:switch true}}]]
           
           :else
           [:<>
            
            [:div
             [:fieldset
              [:legend "General Information"]
              [crud/render-fields
               {:form-path form-path
                :fields [{:path [:company :name]
                          :label "Company Name"}
                         {:path [:company :description]
                          :type :textarea
                          :input-opts {:maxLength 100}
                          :label "About your services"}
                         {:path [:company :website]
                          :input-opts {:type "url"}
                          :label "Website URL"}]}]]
             [:fieldset
              [:legend "Primary Contact"]
              [crud/render-fields
               {:form-path form-path
                :fields [{:type :row
                          :fields [{:path [:company :contact :first-name]
                                    :input-opts {}
                                    :label "First Name"}
                                   {:path [:company :contact :last-name]
                                    :input-opts {}
                                    :label "Last Name"}]}
                         {:type :row
                          :fields [{:path [:company :contact :email]
                                    :input-opts {:type "email"}
                                    :label "Email"}
                                   {:path [:company :contact :phone-number]
                                    :type :phone
                                    :input-opts {}
                                    :label "Phone Number"}]}]}]]
             [:fieldset
              [:legend "Business Address"]
              [crud/render-fields
               {:form-path form-path
                :fields [{:path [:company :contact :business-details :address :name]
                          :input-opts {:placeholder "John Doe"}
                          :label "Contact Name"}
                         {:path [:company :contact :business-details :address :line-1]
                          :input-opts {}
                          :label "Line 1"}
                         {:path [:company :contact :business-details :address :line-2]
                          :input-opts {}
                          :label "Line 2"}
                         {:path [:company :contact :business-details :address :line-3]
                          :input-opts {}
                          :label "Line 3"}
                         {:type :row
                          :fields [{:path [:company :contact :business-details :address :state]
                                    :input-opts {}
                                    :label "State"}
                                   {:path [:company :contact :business-details :address :country]
                                    :type :select
                                    :label "Country"
                                    :input-opts {:placeholder "Country"}
                                    :options (->> (meteor/lst :countries)
                                                  (map #(assoc {} :key (:iso_alpha2 %) :label (:name %))))}
                                   {:path [:company :contact :business-details :address :zip]
                                    :label "ZIP"}]}

                         {:path [:company :contact :business-details :address :phone-number]
                          :type :phone
                          :input-opts {}
                          :label "Phone Number"}]}]]]
            (if submitting?
              [:button.btn.btn-primary {:type "button"}
               [:i.fas.fa-spinner.fa-spin]]
              [:button.btn.btn-primary
               {:type "submit"}
               "Save"])])]])]))

(defn profile-page []
  (let [page-params (rf/subscribe [:db :page-params])]
    (fn []
      [:div.container.mt-4
       [:div.row
        [:div.col
         [:f> user-profile-form]]
        [:div.col
         [:f> company-profile-form]]]])))





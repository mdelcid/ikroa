(ns vp.pages.plans
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :as rf]  
            [cljs.core.async :refer [go]]
            [forms.re-frame :as f]
            [forms.validator :as v]
            [clojure.spec.alpha :as s]
            [vp.util :as u]
            [vp.crud :as crud]
            [vp.meteor :as meteor]
            [vp.routes :as routes]
            [vp.services :as services]
            [vp.collections :refer [collections]]
            [re-chain.core :as chain :refer [reg-chain reg-chain-named]]
            ["meteor/react-meteor-data" :refer [useTracker]]
            ["reactstrap" :as rs]))


(def plan-form-path [::plan-form])
(declare plan-form)

(rf/reg-event-fx
  ::show-plan-form
  (fn [{:keys [db]} [_ plan]]
    (let [validator (v/validator
                      (crud/to-validator crud/validations
                                         {:name [:not-empty]
                                          :description [:not-empty]
                                          :minimum-investment [:not-null :valid-number]
                                          :interest [:not-null :valid-number]
                                          :tos-url [:not-empty]}))
          form (f/constructor validator plan-form-path)]
      (form (or plan
                {:type "info"}))
      {:dispatch [:show-modal [plan-form]]})))

(rf/reg-event-fx
  :init-plans-crud-page
  (fn
    [{:keys [db]} [_]]
    (let []
      {})))


(reg-chain
 ::delete-plan
 (fn [{:keys [db]} [_ id]]
   {:meteor [:del :plans id]})
 (fn [{:keys [db]} [_ data err res]]
   (if err
     {:dispatch [:toastr :error (.-reason err)]}
     {:dispatch-n [[:toastr :success "Plan deleted!"]]})))

(reg-chain
 ::upsert-plan
 (fn [{:keys [db]} [_ data]]
   {:meteor [:upsert :plans (assoc data :user-id (meteor/user-id))]})
 (fn [{:keys [db]} [_ data err res]]
   (if err
     {:dispatch [:toastr :error (.-reason err)]}
     {:dispatch-n [[:toastr :success (if (:_id data) "Plan updated!" "Plan created!")]
                   [:close-modal]]})))

(defn plan-form []
  (let [form-path plan-form-path
        form (rf/subscribe (into [:db] form-path))
        data (rf/subscribe [::f/data form-path])
        submitting? (rf/subscribe [:db :submitting? form-path])
        on-submit (fn [event]
                    (.preventDefault event)
                    (rf/dispatch-sync [::f/validate! form-path])
                    (if @(rf/subscribe [::f/is-valid? form-path])
                      (rf/dispatch [::upsert-plan @data])
                      #_(js/alert "Please fix the validation errors")))]
    (fn []
      [:> rs/Modal {:isOpen true :toggle #(rf/dispatch [:close-modal])}
       [:form {:on-submit on-submit}
        [:> rs/ModalHeader 
         (if (:_id @data) "Edit Plan" "New Plan")]
        [:> rs/ModalBody
         [crud/render-fields
          {:form-path form-path
           :fields [{:path [:name]
                     :label "Name"}
                    {:path [:description]
                     :type :textarea
                     :label "Description"}

                    {:path [:minimum-investment]
                     :type :float
                     :input-opts {:style {:text-align "right"}
                                  }
                     :input-group-prepend {:text "$"}
                     :label "Minimum Investment"}

                    {:path [:interest]
                     :type :float
                     :input-opts {:style {:text-align "right"}
                                  :min "0.01"}
                     :input-group-append {:text "%"}
                     :label "Montly Interest Percentage"}
                    {:path [:tos-url]
                     :label "Terms of Service URL"
                     :input-opts {:type "url"
                                  :placeholder "http://example.com/terms-of-service.pdf"}}]}]]
        [:> rs/ModalFooter

         (if @submitting?
           [:button.btn.btn-primary {:type "button"}
            [:i.fas.fa-spinner.fa-spin]]
           [:<>
            [:button.btn.btn-link {:type "button"
                                   :on-click #(rf/dispatch [:close-modal])}
             "Cancel"]
            [:button.btn.btn-primary
             {:type "submit"}
             "Save"]])]]])))

(defn plans-table []
  (let [plans (useTracker #(meteor/lst :plans {:user-id (meteor/user-id)}))]
    [crud/render-table {:items plans
                        :fields [{:path [:name]
                                  :label "Name"}
                                 {:path [:minimum-investment]
                                  :type :currency
                                  :label "Minimum Investment"}
                                 {:path [:interest]
                                  :type :percentage
                                  :label "Interest"}]
                        :on-delete (fn [plan idx]
                                     (rf/dispatch [:confirm 
                                                   {:title (str "Do you really want to delete '" (:name plan) "' plan?")}
                                                   [::delete-plan (:_id plan)]]))
                        :on-edit (fn [plan idx]
                                   (rf/dispatch [::show-plan-form plan]))}]))

(defn page []
  (let [company (useTracker #(get-in (meteor/user) [:profile :company]))]
    [:div
     [:h3.mb-3 "What plans do you offer?"]
     [:div.card
      [:div.card-header
       [:button.btn.btn-success.float-end {:type "button" :on-click #(rf/dispatch [::show-plan-form])}
        [:i.fas.fa-plus] " New"]
       [:h3 (:name company) " Plans"]]
      [:div.card-body
       [:f> plans-table]]]]))

(defn plans-crud-page []
  (let [page-params (rf/subscribe [:db :page-params])]
    (fn []
      [:div.container.pt-4
       [:f> page]])))





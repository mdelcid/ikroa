(ns vp.pages.contracts
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :as rf]  
            [cljs.core.async :refer [go]]
            [forms.re-frame :as f]
            [forms.validator :as v]
            [clojure.spec.alpha :as s]
            [vp.util :as u]
            [vp.crud :as crud]
            [vp.meteor :as meteor]
            [vp.routes :as routes]
            [vp.services :as services]
            [vp.collections :refer [collections]]
            [re-chain.core :as chain :refer [reg-chain reg-chain-named]]
            ["meteor/react-meteor-data" :refer [useTracker]]
            ["reactstrap" :as rs]))


(def contract-form-path [::contract-form])
(declare contract-form)

(rf/reg-event-fx
  ::show-contract-form
  (fn [{:keys [db]} [_ contract]]
    (let [validator (v/validator
                      (crud/to-validator crud/validations
                                         {:name [:not-empty]
                                          :description [:not-empty]
                                          :minimum-investment [:not-null :valid-number]
                                          :interest [:not-null :valid-number]
                                          :tos-url [:not-empty]}))
          form (f/constructor validator contract-form-path)]
      (form (or contract
                {:type "info"}))
      {:dispatch [:show-modal [contract-form]]})))

(rf/reg-event-fx
  :init-contracts-crud-page
  (fn
    [{:keys [db]} [_]]
    (let []
      {})))


(reg-chain
 ::delete-contract
 (fn [{:keys [db]} [_ id]]
   {:meteor [:del :contracts id]})
 (fn [{:keys [db]} [_ data err res]]
   (if err
     {:dispatch [:toastr :error (.-reason err)]}
     {:dispatch-n [[:toastr :success "Contract deleted!"]]})))

(reg-chain
 ::approve-contract
 (fn [{:keys [db]} [_ contract]]
   {:meteor [:upd :contracts (:_id contract) {:$set {:status "pending-payment"}}]})
 (fn [{:keys [db]} [_ data err res]]
   (if err
     {:dispatch [:toastr :error (.-reason err)]}
     {:dispatch-n [[:toastr :success "Contract approved!"]]})))

(reg-chain
 ::upsert-contract
 (fn [{:keys [db]} [_ data]]
   {:meteor [:upsert :contracts (assoc data :user-id (meteor/user-id))]})
 (fn [{:keys [db]} [_ data err res]]
   (if err
     {:dispatch [:toastr :error (.-reason err)]}
     {:dispatch-n [[:toastr :success (if (:_id data) "Contract updated!" "Contract created!")]
                   [:close-modal]]})))

(defn user-data [f]
  (let [n (useTracker f)]
    n))

(defn contract-form []
  (let [form-path contract-form-path
        form (rf/subscribe (into [:db] form-path))
        data (rf/subscribe [::f/data form-path])
        submitting? (rf/subscribe [:db :submitting? form-path])
        on-submit (fn [event]
                    (.preventDefault event)
                    (rf/dispatch-sync [::f/validate! form-path])
                    (if @(rf/subscribe [::f/is-valid? form-path])
                      (rf/dispatch [::upsert-contract @data])
                      #_(js/alert "Please fix the validation errors")))]
    (fn []
      [:> rs/Modal {:isOpen true :toggle #(rf/dispatch [:close-modal])}
       [:form {:on-submit on-submit}
        [:> rs/ModalHeader 
         (if (:_id @data) "Edit Contract" "New Contract")]
        [:> rs/ModalBody
         [crud/render-fields
          {:form-path form-path
           :fields [{:path [:name]
                     :label "Name"}
                    {:path [:description]
                     :type :textarea
                     :label "Description"}

                    {:path [:minimum-investment]
                     :type :float
                     :input-opts {:style {:text-align "right"}
                                  }
                     :input-group-prepend {:text "$"}
                     :label "Minimum Investment"}

                    {:path [:interest]
                     :type :float
                     :input-opts {:style {:text-align "right"}
                                  :min "0.01"}
                     :input-group-append {:text "%"}
                     :label "Montly Interest Percentage"}
                    {:path [:tos-url]
                     :label "Terms of Service URL"
                     :input-opts {:type "url"
                                  :placeholder "http://example.com/terms-of-service.pdf"}}]}]]
        [:> rs/ModalFooter

         (if @submitting?
           [:button.btn.btn-primary {:type "button"}
            [:i.fas.fa-spinner.fa-spin]]
           [:<>
            [:button.btn.btn-link {:type "button"
                                   :on-click #(rf/dispatch [:close-modal])}
             "Cancel"]
            [:button.btn.btn-primary
             {:type "submit"}
             "Save"]])]]])))

(defn company-name [user-id]
  (let [n (useTracker #(-> (meteor/one :users user-id)
                           (get-in [:profile :company :name])))]
    n))

(defn contract-status [{:keys [status]}]
  (case status
    "pending-approval" [:span.badge.rounded-pill.bg-secondary status]
    "pending-payment" [:span [:span.badge.rounded-pill.bg-primary status]
                       [:br]
                       [:small.text-muted "May take up to 2 business days to confirm a payment"]] 
    "investment-active" [:span [:span.badge.rounded-pill.bg-success status]
                       [:br]
                       [:small.text-muted "Please use the contact information to request your monthly payment"]]
    [:span]))


(defn contract-monthly-payment [contract]
  [:text-success
   (u/format-currency
     (* (get-in contract [:plan :interest])
        (:investment contract)))])

(defn contracts-table []
  (let [contracts (useTracker #(meteor/lst :contracts {:user-id (meteor/user-id)}))]
    [crud/render-table {:items contracts
                        :fields [{:path [:plan :name]
                                  :label "Plan Name"}
                                 {:path [:company]
                                  :label "Ivestment Firm"
                                  :type :custom
                                  :render (fn [contract]
                                            [:span
                                             [:f> company-name (get-in contract [:plan :user-id])]])}

                                 {:path [:company-info]
                                  :label "Contact Info"
                                  :type :custom
                                  :render (fn [contract]
                                            [:span
                                             [:f> user-data (fn []
                                                              (let [user (meteor/one :users (get-in contract [:user-id]))]
                                                                [:div
                                                                 (get-in user [:profile :company :contact :first-name])
                                                                 " "
                                                                 (get-in user [:profile :company :contact :last-name])
                                                                 [:br]
                                                                 (get-in user [:profile :company :contact :email])
                                                                 [:br]
                                                                 (get-in user [:profile :company :contact :phone-number]) ]))]])}

                                 {:path [:status]
                                  :type :custom
                                  :label "Status"
                                  :render contract-status}
                                 {:path [:plan :interest]
                                  :type :percentage
                                  :label "Interest"}
                                 {:path [:investment]
                                  :type :currency
                                  :label "My Investment"}
                                 {:path [:revenue]
                                  :type :custom
                                  :label "Monthly Revenue"
                                  :opts {:alignment "text-align-right"}
                                  :render contract-monthly-payment}]
                        :actions (fn [contract]
                                   [:div
                                    (when (contains? #{"pending-payment"} (:status contract))
                                      (let [checkout-url (get-in contract [:checkout-page :data :redirect_url])]
                                        [:a.btn.btn-success.btn-rounded.btn-sm.mx-1
                                         {:href checkout-url
                                          :title "Pay"}
                                         [:i.fas.fa-money-bill]]))
                                    (when (contains? #{"pending-approval" "pending-payment"} (:status contract))
                                      [:button.btn.btn-danger.btn-rounded.btn-sm.mx-1
                                       {:type :button
                                        :title "Withraw application"
                                        :on-click #(rf/dispatch [:confirm 
                                                                 {:title (str "Do you really want to withdraw your application?")
                                                                  :confirmButtonText "Withdraw"}
                                                                 [::delete-contract (:_id contract)]])}
                                       [:i.fas.fa-ban]])])}]))

(defn page []
  (let [company (useTracker #(get-in (meteor/user) [:profile :company]))]
    [:div
     [:div.card
      [:div.card-header
       [:h3 "My Contracts"]]
      [:div.card-body
       [:f> contracts-table]]]]))

(defn contracts-page []
  (let [page-params (rf/subscribe [:db :page-params])]
    (fn []
      [:div.container.pt-4
       [:f> page]])))


;; company

(defn customer-name [user-id]
  (let [n (useTracker #(-> (meteor/one :users user-id)
                           (meteor/full-name)))]
    n))

(defn customer-email [user-id]
  (let [n (useTracker #(-> (meteor/one :users user-id)
                           (meteor/email)))]
    [:a {:href (str "mailto:"n)} n]))


(defn company-contracts-table []
  (let [contracts (useTracker #(meteor/lst :contracts {:plan.user-id (meteor/user-id)}))]
    [crud/render-table {:items contracts
                        :fields [{:path [:plan :name]
                                  :label "Plan Name"}
                                 {:path [:customer]
                                  :label "Customer Name"
                                  :type :custom
                                  :render (fn [contract]
                                            [:span
                                             [:f> customer-name (get-in contract [:user-id])]])}
                                 {:path [:customer-email]
                                  :label "Customer Email"
                                  :type :custom
                                  :render (fn [contract]
                                            [:span
                                             [:f> customer-email (get-in contract [:user-id])]])}
                                 {:path [:customer-country]
                                  :label "Customer Country"
                                  :type :custom
                                  :render (fn [contract]
                                            [:span
                                             [:f> user-data #(-> (meteor/one :users (get-in contract [:user-id]))
                                                                     (get-in [:profile :country]))]])}
                                 {:path [:status]
                                  :type :custom
                                  :label "Status"
                                  :render contract-status}
                                 {:path [:plan :interest]
                                  :type :percentage
                                  :label "Interest"}
                                 {:path [:investment]
                                  :type :currency
                                  :label "Customer Investment"}
                                 {:path [:revenue]
                                  :type :custom
                                  :label "Monthly Payment"
                                  :opts {:alignment "text-align-right"}
                                  :render contract-monthly-payment}]

                        :actions (fn [contract]
                                   [:div
                                    (when (= (:status contract) "pending-approval")
                                      [:button.btn.btn-primary.btn-sm
                                       {:type "button"
                                        :on-click #(rf/dispatch [:confirm 
                                                                 {:title (str "About to approve contract and request a payment to "
                                                                              (meteor/full-name (:user-id contract)))
                                                                  :confirmButtonText "Approve"}
                                                                 [::approve-contract contract]])}
                                       "Approve"])])}]))


(defn company-page []
  (let [company (useTracker #(get-in (meteor/user) [:profile :company]))]
    [:div
     [:div.card
      [:div.card-header
       [:h3 "Company Contracts"]]
      [:div.card-body
       [:f> company-contracts-table]]]]))



(defn company-contracts-page []
  (let [page-params (rf/subscribe [:db :page-params])]
    (fn []
      [:div.container.pt-4
       [:f> company-page]])))





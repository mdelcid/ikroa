(ns vp.views
  (:require
   [re-frame.core :as rf]
   [reagent.core :as r]
   [vp.services.users :as users]
   [vp.subs :as subs]
   [vp.collections :refer [collections]]
   [vp.services :refer [myfunction]]
   [clojure.string :as str]
   [cljs.core.async :refer [go]]
   [vp.meteor :as meteor]
   [vp.routes :as routes]
   ["mdb-react-ui-kit" :refer [MDBDropdown, MDBDropdownMenu, MDBDropdownToggle, MDBDropdownItem, MDBDropdownLink
                               MDBContainer, MDBNavbar, MDBNavbarBrand, MDBNavbarToggler, MDBNavbarNav, MDBNavbarItem,
                               MDBNavbarLink, MDBCollapse MDBIcon]]
   ["meteor/mongo" :refer [Mongo]]
   ["meteor/react-meteor-data" :refer [useTracker]]
   ["meteor/gadicc:blaze-react-component" :refer [default] :rename {default Blaze}]
   [vp.pages.plans :refer [plans-crud-page]]
   [vp.pages.contracts :refer [contracts-page company-contracts-page]]
   [vp.pages.profile-page :refer [profile-page]]
   [vp.pages.home-page :refer [home-page]]))


;; home


(defn about-panel []
  [:div
   [:h1 "This is th Pa2."]

   [:div
    [:a {:href "#/"}
     "go to Home Page"]]])


;; main

(defn modal-mount []
  (let [modal-cmp (rf/subscribe [:db :modal-cmp])]
    (fn [] [:div (if @modal-cmp @modal-cmp) ])))


(defn user-dropdown []
  
  )

(defn navbar []
  (let [user (useTracker #(meteor/user))
        show-nav? (rf/subscribe [:db ::show-nav?])]
    [:> MDBNavbar {:bgColor "white", :light true :expand "lg"}
     [:> MDBContainer {:fluid true}
      [:> MDBNavbarBrand {:href (routes/home-page)}
       [:img {:src "/img/ikroa-logo.png" :height 30}]]
      [:> MDBNavbarToggler {:onClick #(rf/dispatch [:swap-db [::show-nav?] not])}
       [:> MDBIcon {:fas true :icon "bars"}]]
      [:> MDBCollapse {:show @show-nav? :navbar true}
       [:> MDBNavbarNav
        [:<>
           [:> MDBNavbarItem
            [:> MDBNavbarLink
             {:href (routes/home-page) , :aria-current "page", :active "active"}
             "Find deals"]]
           [:> MDBNavbarItem [:> MDBNavbarLink {:href (routes/user-contracts-page)} "My Contracts"]]
           (when (get-in user [:profile :is-company])
             [:> MDBDropdown
              [:> MDBDropdownToggle {:tag "a" :className "nav-link"}
               "Company"]
              [:> MDBDropdownMenu
               [:> MDBDropdownItem]
               [:> MDBDropdownItem
                [:> MDBDropdownLink {:href (routes/company-plans-page)} "Plans"]]
               [:> MDBDropdownItem
                [:> MDBDropdownLink {:href (routes/company-contracts-page)} "Contracts & Applications"]]]])]]
       [:div
        (if user
          [:div
           [:> MDBDropdown
            [:> MDBDropdownToggle {:tag "a" :className "nav-link"}
             (meteor/email user)
             #_[:img.ms-2 {:style {:max-height "40px"}
                    :src (users/user-portrait user)}]

             ]
            [:> MDBDropdownMenu
             [:> MDBDropdownItem]
             [:> MDBDropdownItem
              [:> MDBDropdownLink {:href (routes/profile-page)} "Profile"]]
             [:> MDBDropdownItem
              [:hr.dropdown-divider]]
             [:> MDBDropdownItem
              [:> MDBDropdownLink {:href "#" :on-click #(meteor/logout)} "Logout"]]]]]
          [:div.me-2 {:style {:min-width "65px"}}
           [:> Blaze {:template "loginButtons" :align (if @show-nav? "left" "right")}]])]]]]))

(defn- panels [panel-name]
  [:div
   [:f> navbar]
   (case panel-name
     :home-page [home-page]
     :company-plans [plans-crud-page]
     :profile-page [profile-page]
     :user-contracts [contracts-page]
     :company-contracts [company-contracts-page]
     [:div])
   [modal-mount]])

(defn show-panel [panel-name]
  [panels panel-name])

(defn main-panel []
  (let [active-panel (rf/subscribe [::subs/active-panel])]
    [show-panel @active-panel]))

(ns vp.services.users
  (:require [clojure.string :as str]
            [vp.meteor :as meteor]))

(defn gravatar-url [email]
  (let [md5hash (js/CryptoJS.MD5 email)]
    (str "https://www.gravatar.com/avatar/" md5hash)))

(defn user-portrait [user]
  (if-let [asset-id (get-in user [:portrait :asset-id])] 
    (meteor/asset-url asset-id)
    (or (get-in user [:profile :pictureUrl])
        (cond
          ;; (or (:first-name user) (:last-name user))
          ;; (let [letter-url (str "https://ui-avatars.com/api/" (str (first (get-in [:profile :first-name]))
          ;;                                                          " "
          ;;                                                          (first (:last-name user))))]
          ;;   (if-let [gravatar-url (:gravatar-url user)]
          ;;     (str gravatar-url "?d=" (js/encodeURIComponent letter-url))
          ;;     letter-url))
          ;;
          :else
          (let [md5hash (js/CryptoJS.MD5 (:_id user))]
            (str "https://www.gravatar.com/avatar/" md5hash
                 "?d=retro&r=PG"))))))
